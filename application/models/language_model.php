<?php
class Language_model extends MY_Model {
    public function __construct()
    {
        parent::__construct();
    }
    public function get_language()
    {
        $select = parent::select();
        $select->from('lang')
                ->where('active_status = ?', 'Y')
                ->where('code != ?', DEFAULT_LANG_CODE);
        return parent::fetch_all($select);
    }
}