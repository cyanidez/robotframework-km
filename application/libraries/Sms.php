<?php
class SMS { 
	private $_config = array();

	public function __construct()
	{
        if ( ! defined("CRON"))
        {
            $CI =& get_instance();
            $CI->load->config('sms_config');
            $this->_config['username'] = $CI->config->item('username');
            $this->_config['password'] = $CI->config->item('password');
            $this->_config['api_url'] = $CI->config->item('api_url');
        }
        else
        {
            $path_include = __DIR__.'/../config/sms_config.php';
            include($path_include);
            $this->_config['username'] = $config['username'];
            $this->_config['password'] = $config['password'];
            $this->_config['api_url'] = $config['api_url'];
        }
	}

	public function send($msisdn = null, $message = null, $sender = null)
	{
		if (empty($msisdn))
		{
			return null;
		}
		if (empty($message))
		{
			return null;
		}
		if (empty($sender))
		{
			return null;
		}

		$args['username'] = $this->_config['username'];
		$args['password'] = $this->_config['password'];
		$args['msisdn'] = $msisdn; #"0979744246";
		$args['message'] = $message; //"This is a test messagse ทดสอบการส่งภาษาไทยด้วยนะจ๊ะ";
		$args['sender'] = $sender; //"THAIBULKSMS";
		$args['force'] = "Standard";
		#alert($args, 'red', 'args');

        #print_r($args);

		

		$agent = "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4)
		Gecko/20030624 Netscape/7.1 (ax)";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->_config['api_url']);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($args));
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15);
		$result = curl_exec ($ch);
		curl_close ($ch);

		return $result;

	}
}