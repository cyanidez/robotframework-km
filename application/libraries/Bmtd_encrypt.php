<?php
/*
*		@author : Cyanidez  <cyanidez@gmail.com> 
*		@Since:   24 May , 2012 
*
*/
class bmtd_encrypt {
	private $CI; 
	function __construct() 
	{
		$this->CI =& get_instance(); 		
		$this->CI->load->library('encrypt'); 
	}

	public function encrypt_url($str = null)
	{
		$encode =  $this->CI->encrypt->encode($str, $this->CI->encrypt->get_key()); 	

		$encode = strtr(
                $encode,
                array(
                    '+' => '.',
                    '=' => '-',
                    '/' => '~'
                )
            );
		return $encode; 
	}
	public function decrypt_url($name = null)
	{
		$name = strtr(
            $name,
            array(
                '.' => '+',
                '-' => '=',
                '~' => '/'
            )
        );

		$decode = $this->CI->encrypt->decode($name, $this->CI->encrypt->get_key()); 	


		return $decode; 
	}

	function my_encrypt($str = NULL) 
	{
		return $this->CI->encrypt->encode($str, $this->CI->encrypt->get_key()); 
	}

	function my_decrypt($name = NULL) 
	{
		return $this->CI->encrypt->decode($name, $this->CI->encrypt->get_key()); 	
	}
}