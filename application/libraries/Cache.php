<?php
class Cache { 
	
	public $CI; 

	public static $app_cache = NULL;

	public static $app_key = NULL; 

	function __construct()
	{
		$this->CI =& get_instance(); 
		$this->CI->load->library('zend'); 
		$this->CI->zend->load('Zend/Registry');
		$this->CI->zend->load('Zend/Cache');
	}

	public function initialize($prefix = 'default', $lifetime = 216000)
	{
		// config cache use for access control list
		$cacheFrontends = array(
			'lifetime' =>  $lifetime,
			'automatic_serialization' => TRUE,
			'automatic_cleaning_factor' => 50
		);
		$cacheBackends = array(
			'cache_dir' => config_item('cache_dir') . '/' . $prefix,
			//'cache_db_complete_path' => config_item('cache_dir') . '/acl/cache.sqlite',
			'file_name_prefix' => $prefix,
			'hashed_directory_umask' => '0777',
			'cache_file_umask' => '644',
			'hashed_directory_level' => '0',
			'server' => config_item('cache_server'),
			'compression' => TRUE
		);
		self::$app_cache = Zend_Cache::factory('Core', config_item('cache_method'), $cacheFrontends, $cacheBackends);
	}

	function set($key, $data)
	{
		return self::$app_cache->save($data, $key);
	}

	function get($key)
	{
		return self::$app_cache->load($key); 
	}

	function remove($key)
	{
		return self::$app_cache->remove($key); 
	}

}