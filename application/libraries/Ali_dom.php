<?php
class Ali_dom { 
	public function __construct()
	{

	}

	public function get_link_detail_from_list_page($html = null)
	{
		$dom = new DomDocument;
		/* Load the HTML */
		$dom->loadHTML($html);

			
		/* Create a new XPath object */
		$xpath = new DomXPath($dom);
		/* Query all <td> nodes containing specified class name */
		$nodes = $xpath->query("//ul[@class='items-list util-clearfix']");
		/* Set HTTP response header to plain text for debugging output */
		#header("Content-type: text/plain");
		/* Traverse the DOMNodeList object to output each DomNode's nodeValue */
		$return = array();

		foreach ($nodes as $key => $value)
		{
			$i=0;
            while(is_object($finance = $value->getElementsByTagName("li")->item($i)))
            {
                foreach($finance->childNodes as $node)
                {

	                	
                	if (isset($node->tagName))
                	{
	                		
                		$class = $node->getAttribute('class');
                		if ($class == 'detail')
                		{
	                			
                			$i2 = 0;
                			while(is_object($h3 = $node->getElementsByTagName("h3")->item($i2)))
                			{
                				foreach($h3->childNodes as $h3_node)
                				{

                					if (isset($h3_node->tagName) && $h3_node->tagName == "a")
                					{
                						$product_detail_url = $h3_node->getAttribute('href');
                						if ( ! preg_match("/^http:\/\/(.*)/", $product_detail_url))
                						{
                							$return[] = "http:".$product_detail_url; 
                						}
                						else 
                						{
                							$return[] = $product_detail_url; 
                						}

                						
                						#var_dump($product_detail_url);
                					}
	                					
                				}
	               				$i2++;
	               			}
	               		}
	              	}
	               
	            }
	    		$i++;
	        }
		}
		return $return; 
	}

	public function get_image($html = null)
	{
		#echo "html = ".$html; 
		
		$return = array();
		$dom = new DomDocument;
		/* Load the HTML */
		$dom->loadHTML($html);
		$xpath = new DomXPath($dom);
		$i = 0;
		#echo $html; 
		$data = $xpath->query("//a[@class='ui-image-viewer-thumb-frame']")->item(0);
		#var_dump($data);
		if ( ! empty($data))
		{
			
			$image_url = $data->getAttribute('href');
			if ( ! preg_match("/^http:(.*)/", $image_url))
			{
				$image_url = "http:".$image_url; 
			}
			$CI =& get_instance();
			$CI->load->library('curl');

			#echo $image_url; 

			$html = $CI->curl->get($image_url);
			$dom = new DomDocument;
			/* Load the HTML */
			$dom->loadHTML($html);
			$xpath = new DomXPath($dom);
			$i = 0; 
			while (is_object($data = $xpath->query("//ul[@class='new-img-border']/li")->item($i)))
			{
				$image_path = $data->firstChild->firstChild->getAttribute('src');
				$return[] = $image_path; 
				$i++;
			}
		}
		


		return $return; 
		
	}

	public function get_color($html = null)
	{
		$return = array();
		$dom = new DomDocument;
		/* Load the HTML */
		$dom->loadHTML($html);
		$xpath = new DomXPath($dom);
		$i = 0; 
		
		while (is_object($data = $xpath->query("//ul[@id='j-sku-list-1']/li")->item($i)))
		{
			$return[] = $data->firstChild->firstChild->getAttribute('src');

			$i++;
			
			
		}
		#alert($return, 'blue');
		return $return; 
	}

	public function get_size($html = null)
	{
		$return = array();
		$dom = new DomDocument;
		$dom->loadHTML($html);
		$xpath = new DomXPath($dom);
		$i = 0; 
		
		while (is_object($data = $xpath->query("//ul[@id='j-sku-list-2']/li")->item($i)))
		{
			$return[] = $data->firstChild->firstChild->textContent;

			$i++;
			
		}
		return $return; 
	}

	public function get_description($html = null)
	{
		$return = array();
		$dom = new DomDocument;
		/* Load the HTML */
		$dom->loadHTML($html);
		$xpath = new DomXPath($dom);
		$i = 0; 
		
		while ($data = $xpath->query("//*[@class='ui-box pnl-packaging-main']/div[@class='ui-box-body']/ul[contains(@class, 'product-packaging-list')]/li")->item($i))
		{
			$return[] =  trim(str_replace(array('\n', '\r'), array('', ''), $data->textContent));
			$i++;
		}
		#alert($return, 'blue');
		return $return; 
		
	}

	public function get_feature($html = null)
	{
		$return = array();
		$dom = new DomDocument;
		/* Load the HTML */
		$dom->loadHTML($html);
		$xpath = new DomXPath($dom);
		$i = 0; 
		#$data = $xpath->query("//*[@id='j-product-desc']/div[1]/div[@class='ui-box-body']/ul[contains(@class, 'product-property-list')]");
		#var_dump($data);

		while ($data = $xpath->query("//*[@id='j-product-desc']/div[1]/div[@class='ui-box-body']/ul[contains(@class, 'product-property-list')]/li")->item($i))
		{
			$return[] =  trim(str_replace(array('\n', '\r'), array('', ''), $data->textContent));
			$i++;
		}
		#alert($return, 'blue');
		return $return; 
		
	}

	public function get_text_breadcrumb($html = null)
	{
		$return = array();
		$dom = new DomDocument;
		/* Load the HTML */
		$dom->loadHTML($html);
		$xpath = new DomXPath($dom);
		$i = 0; 
		while (is_object($data = $xpath->query("//div[@class='module m-sop m-sop-crumb']/*[local-name()='a' or local-name()='b']")->item($i)))
		{
			$return[] =  $data->textContent; 
			$i++;
		}
		#alert($return, 'blue');
		return $return; 
		
	}

	public function get_node_from_detail_page($html = null) 
	{
		$return = array();
		$dom = new DomDocument;
		/* Load the HTML */
		$dom->loadHTML($html);
		$xpath = new DomXPath($dom);
		$return['product_name'] = $xpath->query("//h1[@class='product-name']")->item(0)->textContent;

		if (isset($xpath->query("//span[@id='j-sku-discount-price']")->item(0)->textContent))
		{
			$return['price'] = $xpath->query("//span[@id='j-sku-discount-price']")->item(0)->textContent;
		}
		else 
		{
			$return['price'] = null; 
		}


		return $return; 

	}

	public function get_shop_name($html = null)
	{
		
		$dom = new DomDocument;
		$dom->loadHTML($html);
		$xpath = new DomXPath($dom);
		$data = $xpath->query("//span[@class='shop-name']/a")->item(0);
		if (isset($data->textContent))
		{
			return $data->textContent; 
		}
		return null; 

		
	}

	public function split_keyword($str = null)
	{
		$return = array();
		$data = explode(" ", $str);
		if ( ! empty($data))
		{
			foreach ($data as $key => $value)
			{
				$return[] = $value; 
			}
		}
		return $return;
	}

	public function get_pagination_link($html = null)
	{
		$return = array();
		
		$dom = new DomDocument;
		$dom->loadHTML($html);
		$xpath = new DomXPath($dom);
		$i = 0;
		$data = $xpath->query("//div[@id='pagination-bottom']/div[1]/a");
		
		while (is_object($data = $xpath->query("//div[@id='pagination-bottom']/div[1]/a")->item($i)))
		{
			if ( ! in_array($data->getAttribute('href'), $return))
			{
				$return[] = $data->getAttribute('href');
			}
			$i++;
		}
		#alert($return, 'blue', 'return');
		return $return; 
		

		

		
		
	}

	public function get_package_detail($html = null)
	{
		$return = array();
		
		$dom = new DomDocument;
		$dom->loadHTML($html);
		$xpath = new DomXPath($dom);
		$i = 0;
		$data = $xpath->query("//div[@id='pagination-bottom']/div[1]/a");
		
		while (is_object($data = $xpath->query("//div[@id='pagination-bottom']/div[1]/a")->item($i)))
		{
			if ( ! in_array($data->getAttribute('href'), $return))
			{
				$return[] = $data->getAttribute('href');
			}
			$i++;
		}
		#alert($return, 'blue', 'return');
		return $return; 
	}
}