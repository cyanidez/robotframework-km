<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
/* a library to do some initialize */
class Static_file {
    public $file_path = NULL; 

	function __construct($params = NULL)
	{
		if ($params['lang'] == "") 
		{
			$params['lang'] == "en"; 
		}
		$path = 'data/static/'.strtolower($params['lang']).'/'; 
	    $this->file_path = $path;
	}
    function get($file = NULL, $params = array())
	{
        if (is_null($file))
		{
			return NULL; 
	    }
		if (file_exists($this->file_path.$file.'.php')) 
		{
			// Fix a problem, if any a PHP tags  in static file 
			// By Pop   26 Aug 2009 
			ob_start();
			include $this->file_path.$file.'.php';
			$parsed = ob_get_contents();
			ob_end_clean();
			return $parsed; 
		}
		else 
		{
			return NULL; 
		}
    }
}
