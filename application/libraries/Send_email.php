<?php
class Send_Email
{
	public $mail_to, $mail_from, $mail_subject, $mail_message; 
	public $bcc = null; 
	public $cc; 
	public $is_attach_file = null; 

	function __construct() 
	{
		$this->CI =& get_instance(); 
		$this->CI->load->library('phpmailer'); 
		$this->CI->load->config('mail_config'); 
		$this->CI->load->model('configuration/configuration_model', 'configuration');

		$configuration = $this->CI->configuration->get_configuration();
		if ( ! empty($configuration))
		{
			$c = array();
			foreach ($configuration as $key => $value)
			{
				$c[$value['config_key']] = $value['config_value'];
			}
		}
		
		$this->mail_to = $this->CI->config->item('mail_to'); 

		// BCC inform new booking to this mail 
		$this->mail_bcc = $this->CI->config->item('mail_bcc'); 

		// CC inform a new booking to this email 
		$this->mail_cc = $this->CI->config->item('mail_cc'); 
		
		//  Show on email that sent to customer 
		

		$this->from_mail = $this->CI->config->item('from_mail'); 
		$this->from_name = $this->CI->config->item('from_name'); 
		
		$this->port = $c['SMTP_PORT'];
		$this->host = $c['SMTP_HOST'];
		$this->user = $c['SMTP_USERNAME'];
		$this->pass = $c['SMTP_PASSWORD'];
		$this->charset = "UTF-8";

	}

	public function set_smtp_host($host = null)
	{
		if (empty($this->host))
		{
			$this->host = $host;
		}
	}
	public function set_smtp_port($port = null)
	{
		if (empty($this->port))
		{
			$this->port = $port; 
		}
	}
	public function set_smtp_user($user = null)
	{
		if (empty($this->user))
		{
			$this->user = $user; 
		}
	}
	public function set_smtp_pass($pass = null)
	{
		if (empty($this->pass))
		{
			$this->pass = $pass; 
		}
	}
	public function set_smtp_charset($charset = null)
	{
		if (empty($this->charset))
		{
			$this->charset = $charset; 
		}
	}

	
	function set_cc($cc = null) 
	{
		if (is_null($cc) || $cc == "")
		{
			$this->cc = $this->mail_cc; 
		}
		else
		{
			$this->cc = $cc; 
		}
	}
	function set_bcc($bcc = null) 
	{
		if (is_null($bcc) || $bcc == "") 
		{
			$this->bcc = $this->mail_bcc; 
		}
		else
		{
			$this->bcc = $bcc; 
		}
	}
	function set_attach_file($file = null) 
	{
		if (is_null($file) || $file == "") 
		{
			return null; 
		}
		$this->is_attach_file = $file; 
	}

	
	//---  For most clients expecting the Priority header: ---//
	//--- 1 = High, 2 = Medium, 3 = Low ---//
	function set_priority($priority = 1)
	{
		$this->CI->phpmailer->Priority = $priority; 
	}

	//$yourMessage->Priority = 1;


	function send($mail_to = NULL, $mail_subject = NULL, $mail_message = NULL, $from_mail = NULL, $from_name = NULL) 
	{
		
		if (is_null($mail_to) OR empty($mail_to) OR
			is_null($mail_subject) OR empty($mail_subject) OR
			is_null($mail_message) OR empty($mail_message)
			) 
		{
			return FALSE; 
		}

		if ( ! is_null($from_mail) && ! empty($from_mail)) 
		{
			$this->from_mail = $from_mail; 
		}
		if ( ! is_null($from_name) && ! empty($from_name)) 
		{
			$this->from_name = $from_name; 
		}
		
		$this->CI->phpmailer->From     = $this->from_mail; 
		$this->CI->phpmailer->FromName = $this->from_name; 

		$this->CI->phpmailer->AddAddress($mail_to);		

		if (is_array($this->cc) && count($this->cc) > 0 ) 
		{
			for ($i = 0; $i < count($this->cc); $i++) 
			{
				$this->CI->phpmailer->AddCC($this->cc[$i]); 
			}
		}
		else 
		{
			if ($this->cc) 
			{
				$this->CI->phpmailer->AddCC($this->cc); 
			}
		}

		if (is_array($this->bcc) && count($this->bcc) > 0 ) 
		{
			for ($i = 0; $i < count($this->bcc); $i++) 
			{
				$this->CI->phpmailer->AddBCC($this->bcc[$i]); 
			}
		}
		else 
		{
			if ($this->bcc) 
			{
				$this->CI->phpmailer->AddBCC($this->bcc); 
			}
		}
		$this->CI->phpmailer->Subject = $mail_subject;
		$this->CI->phpmailer->Body = $mail_message;
		$this->CI->phpmailer->IsHTML(TRUE);

		if ($this->CI->config->item('mailer') == "mail") 
		{
			$this->CI->phpmailer->IsMail();
		}
		elseif ($this->CI->config->item('mailer') == "smtp")
		{
			$this->CI->phpmailer->IsSMTP(); 
			$this->CI->phpmailer->Host = $this->host; 
			$this->CI->phpmailer->Port = $this->port; 
			$this->CI->phpmailer->Username = $this->user; 
			$this->CI->phpmailer->Password = $this->pass;
            #$this->CI->phpmailer->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
            $this->CI->phpmailer->SMTPAuth = true; // authentication enabled
            #$this->CI->phpmailer->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
		}

		$this->CI->phpmailer->CharSet  = $this->charset; 
		if ($this->is_attach_file) 
		{
			#$this->CI->phpmailer->AddAttachment($_SERVER['DOCUMENT_ROOT']."/attach_files/Voucher.doc", "test.doc", "base64", "application/octet-stream");
		}
		#alert($this->CI->phpmailer);
		#exit;
				
		$resp = $this->CI->phpmailer->Send();
		$this->CI->phpmailer->ClearAddresses();
		if ( ! $resp) 
		{
		    return $this->CI->phpmailer->ErrorInfo;
		}
		else 
		{
			return TRUE; 
		}

	}
}
