<?php
class GoogleClient {
    public $endpoint;
    public $CI;
	public function __construct()
	{
		#$this->initialize($confg);
        $this->CI =& get_instance();

        $this->endpoint = $this->CI->load->config('endpoint');
	}

    public function nearlySearch()
    {

    }

    public function textSearch($args =  NULL, $method = 'get', $debug = FALSE)
    {
        $r = $this->exec_curl('textsearch', $args, $method, $debug);
        #alert($r, 'red', 'r');
        return $r;
    }

    public function radarSearch()
    {
    }

	public function initialize(array $config)
	{
		if ( ! empty($config))
		{
			foreach ($config as $key => $val)
        	{
            	$this->{$key} = $val;
        	}
        }
	}

	public function exec_curl($uri = NULL, $post = NULL, $method_type = 'get', $debug = false, $timeout = 25) 
	{
        #alert($url)
		/**
        if ($method_type == 'get')
		{
			if ( ! empty($post))
			{
				if (substr_count($url, "?") > 0)
                {
                    $url = $url . '&' . http_build_query($post);
                }
                else
                {
                    $url = $url . '?' . http_build_query($post);
                }
			}
		}
		**/
        #$this->config->item();
		$endpointUrl = $this->endpoint['google']['place']['endpointUrl']; #Config::get('endpoints.google.place.endpointUrl');
		
		$url = $endpointUrl . '/' . $uri .'/json';
		if (strtolower($method_type) == 'get')
		{
			#$url .= '?key='.Config::get('endpoints.google.place.api_server_key').'&'. http_build_query($post);
            $url .= '?key='.$this->endpoint['google']['place']['api_server_key'].'&'. http_build_query($post);
		}
		#alert($post, 'blue');
		#alertd($url, 'red', 'url');
		$curl = curl_init($url);
		if (is_resource($curl) === TRUE)
		{
			curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);

			$headers[] = 'charset=UTF-8';

			if (is_array($headers)) 
			{
				curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
			}
			
			if ($method_type == 'post')
			{
				if ( ! empty($post))
				{
					//print_r($post);die;
					curl_setopt($curl, CURLOPT_POST, TRUE);
					curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
				}
			}

			$result = curl_exec($curl);

            #alertd($result, 'red', 'result');

			if ($debug == TRUE) 
			{
				self::debugCurl($curl, $result); 
			}
			curl_close($curl);
			return $result;
		}
		return FALSE;
    }

    public function place_photo($path = NULL, $maxwidth = 800)
    {
        #return "https://maps.googleapis.com/maps/api/place/photo?".photoreference$path;

        return "https://maps.googleapis.com/maps/api/place/photo?maxwidth=".$maxwidth."&key=".$this->endpoint['google']['place']['api_server_key']."&photoreference=".$path;

    }
    public function get_place_detail($placeid = NULL)
    {
        $args['placeid'] = $placeid;
        $args['key'] = $this->endpoint['google']['place']['api_server_key'];
        #$url = "https://maps.googleapis.com/maps/api/place/details/json";
        $response = $this->exec_curl('details', $args, 'get');

        return $response;

    }

	
	public static function debugCurl($curl, $response)
    {
        echo "=============================================<br/>\n";
        echo "<h2>CURL Test</h2>\n";
        echo "=============================================<br/>\n";
        echo "<h3>Response</h3>\n";
        echo "<code>" . nl2br(htmlentities($response)) . "</code><br/>\n\n";

        if (curl_error($curl))
        {
            echo "=============================================<br/>\n";
            echo "<h3>Errors</h3>";
            echo "<strong>Code:</strong> " . curl_errno($curl) . "<br/>\n";
            echo "<strong>Message:</strong> " . curl_error($curl) . "<br/>\n";
        }
        echo "=============================================<br/>\n";
        echo "<h3>Info</h3>";
        echo '<pre style="text-align:left;">';
        print_r(curl_getinfo($curl));
        echo "</pre>";
    }

}