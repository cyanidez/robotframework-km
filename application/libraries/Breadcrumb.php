<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Breadcrumb {
	var $crumbs = array();
	var $separetor = '';
	var $bread_st = '<ul class="breadcrumb">';
	var $bread_end = '<li class="clear"></li></ul>';
	var $crumb_highlight = '<li class="active">';
	var $crumb_st = '<li>';
	var $crumb_end = '</li>';
	
	function __construct($init = NULL)
	{
		if ( ! is_null($init))
		{
			if ($init['separetor']) $this->separetor = $init['separetor'];
			if ($init['bread_st']) $this->bread_st = $init['bread_st'];
			if ($init['bread_end']) $this->bread_end = $init['bread_end'];
			if ($init['crumb_highlight']) $this->crumb_highlight = $init['crumb_highlight'];
			if ($init['crumb_st']) $this->crumb_st = $init['crumb_st'];
			if ($init['crumb_end']) $this->crumb_end = $init['crumb_end'];
		}
	}

	function init($init = NULL)
	{
		if ( ! is_null($init))
		{
			if ( ! empty($init['separetor'])) $this->separetor = $init['separetor'];
			if ( ! empty($init['bread_st'])) $this->bread_st = $init['bread_st'];
			if ($init['bread_end']) $this->bread_end = $init['bread_end'];
			if ($init['crumb_highlight']) $this->crumb_highlight = $init['crumb_highlight'];
			if ($init['crumb_st']) $this->crumb_st = $init['crumb_st'];
			if ($init['crumb_end']) $this->crumb_end = $init['crumb_end'];
		}
	}


	
	// add crumb to the specified slot, or the new slow if no index specified
	function add($crumb, $index = NULL)
	{
		if ( ! is_array($crumb))
		{
			return FALSE;
		}
    	else
    	{
			if (( ! is_null($index)) && (is_numeric($index)))
			{
				$this->crumbs[$index] = $crumb;
			}
			else
			{
				$this->crumbs[] = $crumb;
			}
    	}
	}
	
	// make a crumb in a correct format and return to the origin
	function make($name = NULL, $url = NULL, $highlight = FALSE, $lastcrumb = FALSE)
	{
		$crumb = array('name' => htmlspecialchars($name)
						, 'url' => $url
						, 'highlight' => $highlight
						, 'last_crumb' => $lastcrumb);
		$this->add($crumb);
	}
	
	// create a breadcrumbs trial
	function make_bread($thai_lang = TRUE)
	{
		$bread = $this->bread_st;
		
    	foreach ($this->crumbs as $crumb)
    	{
			if ($crumb['highlight'])
			{
    			$bread.=$this->crumb_highlight;
			}
			else
			{
    			$bread.=$this->crumb_st;
			}
    			
			if ( ! is_null($crumb['url']))
			{
				if ($thai_lang == TRUE) 
				{
					$bread.='<a href="'.$crumb['url'].'" title="'.htmlspecialchars($crumb['name']).'">'.$crumb['name'].'</a> ';				
				}
				else 
				{
    				$bread.='<a href="'.htmlentities($crumb['url']).'" title="'.htmlspecialchars($crumb['name']).'">'.$crumb['name'].'</a> ';
				}
			}
			else
			{
				$bread.=$crumb['name'];
			}
				
			if ( ! $crumb['last_crumb'])
			{
				$bread.=$this->separetor;
			}
			
			$bread.=$this->crumb_end;
		}
		
		$bread.=$this->bread_end;
		
		return $bread;
	}
}
