<?php
$config['node'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'node',
    'width' => 1000, //386,
    'height' => 1000, // 272
    'resize_width' => 329,
    'resize_height' => 119,
    'resize_latest_width' => 50,
    'resize_latest_height' => 50
);

$config['node_image'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'node',
    'width' => 2000,
    'height' => 2000
);

$config['node_paragraph'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'node',
    'width' => 1000, //386,
    'height' => 1000, // 272
);

$config['node_file'] = array(
    'ext' => 'jpg|jpeg|gif|png|pdf|doc|xls|docx|xlsx|ppt|pptx',
    'size' => 20000000,
    'dir' => 'node_file'
);


$config['news'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'news',
    'width' => 1000, //386,
    'height' => 1000, // 272
    'resize_width' => 329,
    'resize_height' => 119,
);

$config['news_image'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'news',
    'width' => 2000,
    'height' => 2000
);

$config['news_paragraph'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'news',
    'width' => 1000, //386,
    'height' => 1000, // 272
);

$config['aira_group'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'aira_group',
    'width' => 1000, //386,
    'height' => 1000, // 272
    'resize_width' => 340,
    'resize_height' => 118,
    'resize_slider_width' => 165,
    'resize_slider_height' => 99
);
$config['aira_group_image'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'aira_group',
    'width' => 2000, //386,
    'height' => 2000
);

$config['partner'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'partner',
    'width' => 1000, //386,
    'height' => 1000, // 272
    'resize_width' => 340,
    'resize_height' => 118,
    'resize_slider_width' => 165,
    'resize_slider_height' => 99
);

$config['web_template'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'web_template',
    'width' => 1000, //386,
    'height' => 1000, // 272
);

$config['download'] = array(
    'ext' => 'jpg|jpeg|gif|png|pdf|doc|xls|docx|xlsx|ppt|pptx',
    'size' => 20000000,
    'dir' => 'download'
);

$config['splash_page'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'splash_page',
    'width' => 800, //386,
    'height' => 420, // 272
);

$config['career_profile'] = array(
    'ext' => 'jpg|jpeg|gif|png',
    'size' => 20000000,
    'dir' => 'career',
    'width' => 1000,
    'height' => 1000
);

$config['career_resume'] = array(
    'ext' => 'pdf|docx|doc|xls|xlsx',
    'size' => 20000000,
    'dir' => 'career'
);




// Other configuration get from banner_area table
$config['banner'] = array(
    'dir' => 'banner'

);


$config['google_map_api_key'] = "AIzaSyCcr6jpJVSBCICSqKBxnRuLOt2B8Qt_4mg";

$config['thai_alphabet'] = array('0-9', 'ก', 'ข', 'ฃ', 'ค', 'ฅ', 'ฆ', 'ง', 'จ', 'ฉ', 'ช', 'ซ', 'ฌ', 'ญ', 'ฎ', 'ฏ', 'ฐ', 'ฑ', 'ฒ', 'ณ', 'ด', 'ต', 'ถ', 'ท', 'ธ', 'น', 'บ', 'ป', 'ผ', 'ฝ', 'พ', 'ฟ', 'ภ', 'ม', 'ย', 'ร', 'ล', 'ว', 'ศ', 'ษ', 'ส', 'ห', 'ฬ', 'อ', 'ฮ');

$config['th_month'] = array('มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม');
$config['th_month_short'] = array('ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.');

$config['en_month'] = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
$config['en_month_short'] = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');

$config['thai_day'] = array('อาทิตย์', 'จันทร์', 'อังคาร', 'พุธ', 'พฤหัส', 'ศุกร์', 'เสาร์');
$config['thai_day_short'] = array('อา.', 'จ.', 'อ.', 'พ.', 'พฤ.', 'ศ.', 'ส.');

$config['excel_headers'] = array(
    array('row1' => 'Seller Sku', 'row2' => 'item_sku'),
    array('row1' => 'Title', 'row2' => 'item_name'),
    array('row1' => 'Manufacturer', 'row2' => 'manufacturer'),
    array('row1' => 'Model Number', 'row2' => 'model'),
    array('row1' => 'Product Type', 'row2' => 'feed_product_type'),
    array('row1' => 'Item Type Keyword', 'row2' => 'item_type'),
    array('row1' => 'Brand Name', 'row2' => 'brand_name'),
    array('row1' => 'Update Delete', 'row2' => 'update_delete'),
    array('row1' => 'Product ID', 'row2' => 'external_product_id'),
    array('row1' => 'Product ID Type', 'row2' => 'external_product_id_type'),
    array('row1' => 'Product Descriptin', 'row2' => 'product_description'),
    array('row1' => 'Standard Price', 'row2' => 'standard_price'),
    array('row1' => 'Quantity', 'row2' => 'quantity'),
    array('row1' => 'Launch Date', 'row2' => 'product_site_launch_date'),
    array('row1' => 'Product Tax Code', 'row2' => 'product_tax_code'),
    array('row1' => "Manufacturer's Suggested Retail Price", 'row2' => 'list_price'),
    array('row1' => "Sale Price", 'row2' => 'sale_price'),
    array('row1' => "Sale Start Date", 'row2' => 'sale_from_date'),
    array('row1' => "Sale End Date", 'row2' => 'sale_end_date'),
    array('row1' => "Release Date", 'row2' => 'merchant_release_date'),
    array('row1' => "Package Quantity", 'row2' => 'item_package_quantity'),
    array('row1' => "Fulfillment Latency", 'row2' => 'fulfillment_latency'),
    array('row1' => "Restock Date", 'row2' => 'restock_date'),
    array('row1' => "Max Aggregate Ship Quantity", 'row2' => 'max_aggregate_ship_quantity'),
    array('row1' => "Offering Can Be Gift Messaged", 'row2' => 'offering_can_be_gift_messaged'),
    array('row1' => "Is Gift Wrap Available", 'row2' => 'offering_can_be_giftwrapped'),
    array('row1' => "Is Discontinued by Manufacturer", 'row2' => 'is_discontinued_by_manufacturer'), 
    array('row1' => "Shipping-Template", 'row2' => 'merchant_shipping_group_name'),
    array('row1' => "Shipping Weight", 'row2' => 'website_shipping_weight'),
    array('row1' => "Website Shipping Weight Unit Of Measure", 'row2' => 'website_shipping_weight_unit_of_measure'),
    array('row1' => "Width" ,'row2' => 'item_display_width'),
    array('row1' => "Item Display Length", 'row2' => 'item_display_length'),
    array('row1' => "Display Dimensions Unit Of Measure", 'row2' => 'display_dimensions_unit_of_measure'),
    array('row1' => "Item Length", 'row2' => 'item_length'),
    array('row1' => "Item Width", 'row2' => 'item_width'),
    array('row1' => "Item Height", 'row2' => 'item_height'),
    array('row1' => "Item Dimensions Unit Of Measure", 'row2' => 'item_dimensions_unit_of_measure'),
    array('row1' => 'Key Product Features1', 'row2' => 'bullet_point1'),
    array('row1' => 'Key Product Features2', 'row2' => 'bullet_point2'),
    array('row1' => 'Key Product Features3', 'row2' => 'bullet_point3'),
    array('row1' => 'Key Product Features4', 'row2' => 'bullet_point4'),
    array('row1' => 'Key Product Features5', 'row2' => 'bullet_point5'),
    array('row1' => 'Target Audience1', 'row2' => 'target_audience_keywords1'),
    array('row1' => 'Target Audience2', 'row2' => 'target_audience_keywords2'),
    array('row1' => 'Target Audience3', 'row2' => 'target_audience_keywords3'),
    array('row1' => 'Catalog Number', 'row2' => 'catalog_number'),
    array('row1' => 'Intended Use1', 'row2' => 'specific_uses_keywords1'),
    array('row1' => 'Intended Use2', 'row2' => 'specific_uses_keywords2'),
    array('row1' => 'Intended Use3', 'row2' => 'specific_uses_keywords3'),
    array('row1' => 'Intended Use4', 'row2' => 'specific_uses_keywords4'),
    array('row1' => 'Intended Use5', 'row2' => 'specific_uses_keywords5'),
    array('row1' => 'Subject Matter1', 'row2' => 'thesaurus_subject_keywords1'),
    array('row1' => 'Subject Matter2', 'row2' => 'thesaurus_subject_keywords2'),
    array('row1' => 'Subject Matter3', 'row2' => 'thesaurus_subject_keywords3'),
    array('row1' => 'Subject Matter4', 'row2' => 'thesaurus_subject_keywords4'),
    array('row1' => 'Subject Matter5', 'row2' => 'thesaurus_subject_keywords5'),
    array('row1' => 'Search Terms', 'row2' => 'generic_keywords'),
    array('row1' => 'Main Image URL', 'row2' => 'main_image_url'),
    array('row1' => 'Swatch Image URL', 'row2' => 'swatch_image_url'),
    array('row1' => 'Other Image URL1', 'row2' => 'other_image_url1'),
    array('row1' => 'Other Image URL2', 'row2' => 'other_image_url2'),
    array('row1' => 'Other Image URL3', 'row2' => 'other_image_url3'),
    array('row1' => 'Fulfillment Center ID', 'row2' => 'fulfillment_center_id'),
    array('row1' => 'Package Width', 'row2' => 'package_width'),
    array('row1' => 'Package Weight Unit Of Measure', 'row2' => 'package_weight_unit_of_measure'),
    array('row1' => 'Package Weight', 'row2' => 'package_weight'),
    array('row1' => 'Package Length', 'row2' => 'package_length'),
    array('row1' => 'Package Height', 'row2' => 'package_height'),
    array('row1' => 'Package Dimensions Unit Of Measure', 'row2' => 'package_dimensions_unit_of_measure'),
    array('row1' => 'Parentage', 'row2' => 'parent_child'),
    array('row1' => 'Parent SKU', 'row2' => 'parent_sku'),
    array('row1' => 'Relationship Type', 'row2' => 'relationship_type'),
    array('row1' => 'Variation Theme', 'row2' => 'variation_theme'),
    array('row1' => 'Country of Publication', 'row2' => 'country_of_origin'),
    array('row1' => 'Consumer Notice', 'row2' => 'prop_65'),
    array('row1' => 'Cpsia Warning1', 'row2' => 'cpsia_cautionary_statement1'),
    array('row1' => 'Cpsia Warning2', 'row2' => 'cpsia_cautionary_statement2'),
    array('row1' => 'Cpsia Warning3', 'row2' => 'cpsia_cautionary_statement3'),
    array('row1' => 'Cpsia Warning4', 'row2' => 'cpsia_cautionary_statement4'),
    array('row1' => 'CPSIA Warning Description', 'row2' => 'cpsia_cautionary_description'),
    array('row1' => 'Gender', 'row2' => 'department_name'),
    array('row1' => 'Other Attributes1', 'row2' => 'thesaurus_attribute_keywords1'),
    array('row1' => 'Other Attributes2', 'row2' => 'thesaurus_attribute_keywords2'),
    array('row1' => 'Other Attributes3', 'row2' => 'thesaurus_attribute_keywords3'),
    array('row1' => 'Other Attributes4', 'row2' => 'thesaurus_attribute_keywords4'),
    array('row1' => 'Other Attributes5', 'row2' => 'thesaurus_attribute_keywords5'),
    array('row1' => 'Total Metal Weight', 'row2' => 'total_metal_weight'),
    array('row1' => 'Total Metal Weight Unit Of Measure', 'row2' => 'total_metal_weight_unit_of_measure'),
    array('row1' => 'Total Diamond Weight', 'row2' => 'total_diamond_weight'),
    array('row1' => 'Total Diamond Weight Unit Of Measure', 'row2' => 'total_diamond_weight_unit_of_measure'),
    array('row1' => 'Total Gem Weight', 'row2' => 'total_gem_weight'),
    array('row1' => 'Total Gem Weight Unit Of Measure', 'row2' => 'total_gem_weight_unit_of_measure'),
    array('row1' => 'Material Type', 'row2' => 'material_type'),
    array('row1' => 'Metal Type', 'row2' => 'metal_type'),
    array('row1' => 'Metal Stamp', 'row2' => 'metal_stamp'),
    array('row1' => 'Setting Type', 'row2' => 'setting_type'),
    array('row1' => 'Number Of Stones', 'row2' => 'number_of_stones'),
    array('row1' => 'Clasp Type', 'row2' => 'clasp_type'),
    array('row1' => 'Chain Type', 'row2' => 'chain_type'),
    array('row1' => 'Ring Size', 'row2' => 'ring_size'),
    array('row1' => 'Ring Sizing Lower Range', 'row2' => 'ring_sizing_lower_range'),
    array('row1' => 'Ring Sizing Upper Range', 'row2' => 'ring_sizing_upper_range'),
    array('row1' => 'Back Finding', 'row2' => 'back_finding'),
    array('row1' => 'Gem Type1', 'row2' => 'gem_type1'),
    array('row1' => 'Gem Type2', 'row2' => 'gem_type2'),
    array('row1' => 'Gem Type3', 'row2' => 'gem_type3'),
    array('row1' => 'Stone Cut', 'row2' => 'stone_cut'),
    array('row1' => 'Stone Color1', 'row2' => 'stone_color1'),
    array('row1' => 'Stone Color2', 'row2' => 'stone_color2'),
    array('row1' => 'Stone Clarity1', 'row2' => 'stone_clarity1'),
    array('row1' => 'Stone Clarity2', 'row2' => 'stone_clarity2'),
    array('row1' => 'Stone Shape1', 'row2' => 'stone_shape1'),
    array('row1' => 'Stone Shape2', 'row2' => 'stone_shape2'),
    array('row1' => 'Stone Creation Method1', 'row2' => 'stone_creation_method1'),
    array('row1' => 'Stone Creation Method2', 'row2' => 'stone_creation_method2'),
    array('row1' => 'Stone Treatment Method1', 'row2' => 'stone_treatment_method1'),
    array('row1' => 'Stone Treatment Method2', 'row2' => 'stone_treatment_method2'),
    array('row1' => 'Stone Weight1', 'row2' => 'stone_weight1'),
    array('row1' => 'Stone Weight2', 'row2' => 'stone_weight2'),
    array('row1' => 'Certificate Type', 'row2' => 'certificate_type'),
    array('row1' => 'Pearl Type', 'row2' => 'pearl_type'),
    array('row1' => 'Pearl Minimum Color', 'row2' => 'pearl_minimum_color'),
    array('row1' => 'Pearl Lustre', 'row2' => 'pearl_lustre'),
    array('row1' => 'Pearl Shape', 'row2' => 'pearl_shape'),
    array('row1' => 'Pearl Uniformity', 'row2' => 'pearl_uniformity'),
    array('row1' => 'Pearl Surface Blemishes', 'row2' => 'pearl_surface_blemishes'),
    array('row1' => 'Pearl Stringing Method', 'row2' => 'pearl_stringing_method'),
    array('row1' => 'Size Per Pearl', 'row2' => 'size_per_pearl'),
    array('row1' => 'Number Of Pearls', 'row2' => 'number_of_pearls'),
    array('row1' => 'Style', 'row2' => 'style_name'),
    array('row1' => 'Color', 'row2' => 'color_name'),
    array('row1' => 'Colour Map1', 'row2' => 'color_map1'),
    array('row1' => 'Colour Map2', 'row2' => 'color_map2')
);
