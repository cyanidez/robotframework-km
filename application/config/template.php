<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Active template
|--------------------------------------------------------------------------
|
| The $template['active_template'] setting lets you choose which template 
| group to make active.  By default there is only one group (the 
| "default" group).
|
*/
$template['active_template'] = 'adm.main';

/*
|--------------------------------------------------------------------------
| Explaination of template group variables
|--------------------------------------------------------------------------
|
| ['template'] The filename of your master template file in the Views folder.
|   Typically this file will contain a full XHTML skeleton that outputs your
|   full template or region per region. Include the file extension if other
|   than ".php"
| ['regions'] Places within the template where your content may land. 
|   You may also include default markup, wrappers and attributes here 
|   (though not recommended). Region keys must be translatable into variables 
|   (no spaces or dashes, etc)
| ['parser'] The parser class/library to use for the parse_view() method
|   NOTE: See http://codeigniter.com/forums/viewthread/60050/P0/ for a good
|   Smarty Parser that works perfectly with Template
| ['parse_template'] FALSE (default) to treat master template as a View. TRUE
|   to user parser (see above) on the master template
|
| Region information can be extended by setting the following variables:
| ['content'] Must be an array! Use to set default region content
| ['name'] A string to identify the region beyond what it is defined by its key.
| ['wrapper'] An HTML element to wrap the region contents in. (We 
|   recommend doing this in your template file.)
| ['attributes'] Multidimensional array defining HTML attributes of the 
|   wrapper. (We recommend doing this in your template file.)
|
| Example:
| $template['default']['regions'] = array(
|    'header' => array(
|       'content' => array('<h1>Welcome</h1>','<p>Hello World</p>'),
|       'name' => 'Page Header',
|       'wrapper' => '<div>',
|       'attributes' => array('id' => 'header', 'class' => 'clearfix')
|    )
| );
|
*/

/*
|--------------------------------------------------------------------------
| Default Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
*/




$template['login']['template'] = 'layouts/admin/login_layout';
$template['login']['regions'] = array(
   'header',
   'content',
   'footer',
   'navigator'
);
$template['login']['parser'] = 'parser';
$template['login']['parser_method'] = 'parse';
$template['login']['parse_template'] = FALSE;


$template['adm.main']['template'] = 'layouts/admin/main_layout';
$template['adm.main']['regions'] = array(
   'header',
   'sidebar',
   'breadcrumb',
   'page_title',
   'content',
   'footer',
);
$template['adm.main']['parser'] = 'parser';
$template['adm.main']['parser_method'] = 'parse';
$template['adm.main']['parse_template'] = FALSE;

$template['adm.overlay']['template'] = 'layouts/admin/overlay_layout';
$template['adm.overlay']['regions'] = array(
   'content'
);
$template['adm.overlay']['parser'] = 'parser';
$template['adm.overlay']['parser_method'] = 'parse';
$template['adm.overlay']['parse_template'] = FALSE;


/** 
*		Home Template for Frontend
**/

$template['home']['template'] = 'layouts/front/home_layout';
$template['home']['regions'] = array(
    'title',
    'description',
    'keyword',
    'header',
    'sidebar',
    'slider',
    'ga',
    'service',
    'investor',
    'aira_group',
    'partner',
    'breadcrumb',
    'content',
    'footer_banner',
    'footer'
);
$template['home']['parser'] = 'parser';
$template['home']['parser_method'] = 'parse';
$template['home']['parse_template'] = FALSE;

/** 
*   Blank Template
**/

$template['blank']['template'] = 'layouts/admin/blank_layout';
$template['blank']['regions'] = array(
    'content'
);
$template['blank']['parser'] = 'parser';
$template['blank']['parser_method'] = 'parse';
$template['blank']['parse_template'] = FALSE;


$template['main']['template'] = 'layouts/front/main_layout';
$template['main']['regions'] = array(
    'title',
    'description',
    'keyword',
    'ga',
    'header',
    'sidebar',
    'subject',
    'breadcrumb',
    'content',
    'aira_group',
    'partner',
    'footer',
);
$template['main']['parser'] = 'parser';
$template['main']['parser_method'] = 'parse';
$template['main']['parse_template'] = FALSE;





$template['iframe']['template'] = 'layouts/front/iframe_layout';
$template['iframe']['regions'] = array(
    'title',
    'description',
    'keyword',
    'toppane',
    'header',
    'style_picker',
    'slider',
    'navigator',
    'breadcrumb',
    'content',
    'footer',
);
$template['iframe']['parser'] = 'parser';
$template['iframe']['parser_method'] = 'parse';
$template['iframe']['parse_template'] = FALSE;


$template['detail']['template'] = 'layouts/front/detail_layout';
$template['detail']['regions'] = array(
    'title',
    'keyword',
    'description',
    'header',
    'content',
    'footer',
);
$template['detail']['parser'] = 'parser';
$template['detail']['parser_method'] = 'parse';
$template['detail']['parse_template'] = FALSE;














/* End of file template.php */
/* Location: ./system/application/config/template.php */