<html lang="en">
<head>
    <meta charset="utf-8">
    <title>AMZTOOL - Amazon Tools | BACK OFFICE SYSTEM</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo css_path('bootstrap.css', 'admin'); ?>">
    <link rel="stylesheet" href="<?php echo css_path('font-awesome/css/font-awesome.css', 'admin'); ?>">

    <script src="<?php echo js_path('jquery.1.9.1.js', 'admin'); ?>" type="text/javascript"></script>

    <style>
        label.error {
            display:block ; color:#FF0000; font-weight:bold;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<?php echo css_path('theme.css', 'admin'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo css_path('premium.css', 'admin'); ?>">
    
    <?php echo $_styles; ?>
    <link rel="stylesheet" media="screen" type="text/css" href="<?php echo css_path('jquery.fancybox.css?v=2.1.5', 'global'); ?>">
</head>
<body class=" theme-blue">
<!-- Demo page code -->

<script type="text/javascript">
    
    $(function() {
        var match = document.cookie.match(new RegExp('color=([^;]+)'));
        if(match) var color = match[1];
        if(color) {
            $('body').removeClass(function (index, css) {
                return (css.match (/\btheme-\S+/g) || []).join(' ')
            })
            $('body').addClass('theme-' + color);
        }

        $('[data-popover="true"]').popover({html: true});

    });

</script>
<style type="text/css">
    #line-chart {
        height:300px;
        width:800px;
        margin: 0px auto;
        margin-top: 1em;
    }
    .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover {
        color: #fff;
    }
</style>

<script type="text/javascript">
    $(function() {
        var uls = $('.sidebar-nav > ul > *').clone();
        uls.addClass('visible-xs');
        $('#main-menu').append(uls.clone());
    });
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">


<!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
<!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
<!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
<!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<!--<![endif]-->

<div class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <a class="" href="index.html"><span class="navbar-brand"><!-- <span class="fa fa-paper-plane"></span>--> AIRA FACTORING | BACK OFFICE SYSTEM</span></a></div>

    <div class="navbar-collapse collapse" style="height: 1px;">

    </div>
</div>
</div>



<?php echo $content; ?>



<script src="<?php echo js_path('bootstrap/js/bootstrap.js', 'admin'); ?>"></script>
<script type="text/javascript">
    
    
    $(function() {
        $("[rel=tooltip]").tooltip();
        $('.demo-cancel-click').click(function(){return false;});
    });
    
</script>
    <?php echo $_scripts; ?>
    <script src="<?php echo js_path('jquery.fancybox.pack.js', 'global'); ?>"></script>  

</body>
</html>
