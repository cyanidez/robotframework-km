

<div class="sidebar-nav">
    <ul>
<?php if ( ! empty($menu)) : ?>
    <?php foreach ($menu as $key => $value) : ?>
        <?php if (count($value['child']) > 0) : ?>
        <li><a href="#" data-target=".dashboard-menu<?php echo $key; ?>" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-arrow-circle-right"></i> <?php echo $value['menu_name']; ?> </a></li>
        <li>
            <ul class="dashboard-menu<?php echo $key; ?> nav nav-list collapse">
               <?php foreach ($value['child'] as $c_key => $c_value) : ?>
               <li><a href="<?php echo site_admin_url($c_value['menu_path']); ?>"><span class="fa fa-caret-right"></span> <?php echo $c_value['menu_name']; ?></a></li>
               <?php endforeach; ?>
            </ul>
        </li>
        <?php else : ?>
        <li><a href="<?php echo site_admin_url($value['menu_path']); ?>" class="nav-header"><i class="fa fa-fw fa-arrow-circle-right"></i> <?php echo $value['menu_name']; ?></a></li>
        <?php endif; ?>

    <?php endforeach; ?>
<?php endif; ?>
<?php #alert($category_menu, 'red'); ?>




      
    </ul>
</div>

<!-- Navigation -->
<?php /****
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo site_url(); ?>">BAAN MONTIDA BACK OFFICE</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><!-- <i class="fa fa-user"></i> --> <?php echo get_admin_profile('username'); ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">

                        <li>
                            <a href="<?php echo site_admin_url('user/forgot_password'); ?>"><!-- <i class="fa fa-fw fa-power-off"></i> --> Reset Password</a>
                        </li>
                        <li>
                            <a href="<?php echo site_admin_url('user/user/update/'.get_admin_profile('user_id')); ?>"><!-- <i class="fa fa-fw fa-power-off"></i> --> User Profile</a>
                        </li>
                        <li>
                            <a href="<?php echo site_admin_url('auth/auth/logout'); ?>"><!-- <i class="fa fa-fw fa-power-off"></i> --> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <?php if ( ! empty($menu)) : ?>
                <ul class="nav navbar-nav side-nav">

				<?php foreach ($menu as $key => $value) : ?>
					<?php if (count($value['child']) > 0) : ?>
					 <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#sub<?php echo $key; ?>" class="collapsed" aria-expanded="false"><i class="fa fa-fw fa-desktop"></i> <?php echo $value['menu_name']; ?> <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="sub<?php echo $key; ?>" class="collapse" aria-expanded="false" style="height: 0px;">
							<?php foreach ($value['child'] as $child_key => $child_value) : ?>
                            <li><a href="<?php echo site_admin_url($child_value['menu_path']); ?>" tabindex="-1"><i class="fa fa-fw fa-file"></i> <?php echo $child_value['menu_name']; ?></a></li>
							<?php endforeach; ?>
                        </ul>
                    </li>
					<?php else : ?>
					<li>
                        <li><a href="<?php echo site_admin_url($value['menu_path']); ?>"><?php echo $value['menu_name']; ?></a></li>
                    </li>
					<?php endif; ?>
				<?php endforeach; ?>

                </ul>
               <?php endif; ?>
            </div>
            <!-- /.navbar-collapse -->
        </nav>
 **/ ?>