<!-- End Admin Content -->

<footer>
    <hr>

    <p class="pull-right">Developed & License by : <img src="http://www.aseanwebdesign.com/images/awd_icon.png" width="11" height="11" style="margin-bottom:.20em; vertical-align:middle;"> <a href="http://www.aseanwebdesign.com" target="_blank">AseanWebDesign™</a></p>
    <p>© 2014-2016 <a href="http://www.airafactoring.co.th" target="_blank">Aira Factoring  Co.,Ltd.</a></p>
</footer>