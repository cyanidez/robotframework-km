<!doctype html>
<html lang="en">
    <head>
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta charset="utf-8">
    <title>AIRA FACTORING | BACK OFFICE</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo css_path('bootstrap.css', 'admin'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo js_path('bootstrap-wysihtml5/lib/css/prettify.css', 'admin'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo js_path('bootstrap-wysihtml5/src/bootstrap-wysihtml5.css', 'admin'); ?>">
    
    <link rel="stylesheet" href="<?php echo css_path('font-awesome/css/font-awesome.css', 'admin'); ?>">
    <script src="<?php #echo js_plugins_path('wysihtml5-0.3.0.js', 'admin'); ?>" type="text/javascript"></script>
    <script src="<?php echo js_path('jquery-1.11.1.min.js', 'admin'); ?>" type="text/javascript"></script>
    <script src="<?php #echo js_path('bootstrap-wysihtml5/lib/js/prettify.js', 'admin'); ?>" type="text/javascript"></script>
    <script src="<?php #echo js_path('bootstrap-wysihtml5/src/bootstrap-wysihtml5.js', 'admin'); ?>" type="text/javascript"></script>
    <script src="<?php echo js_plugins_path('ckeditor/ckeditor.js', 'admin'); ?>"></script>
    <script src="<?php echo js_plugins_path('ckeditor/adapters/jquery.js', 'admin'); ?>"></script>
    <style>
        label.error {
            display:block ; color:#FF0000; font-weight:bold;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="<?php echo css_path('theme.css', 'admin'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo css_path('premium.css', 'admin'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo css_path('addon.css', 'admin'); ?>">

</head>
<body class="theme-blue">

<!-- Demo page code -->

<script type="text/javascript">
    $(function() {
        var match = document.cookie.match(new RegExp('color=([^;]+)'));
        if(match) var color = match[1];
        if(color) {
            $('body').removeClass(function (index, css) {
                return (css.match (/\btheme-\S+/g) || []).join(' ')
            })
            $('body').addClass('theme-' + color);
        }

        $('[data-popover="true"]').popover({html: true});

    });
</script>
<style type="text/css">
    #line-chart {
        height:300px;
        width:800px;
        margin: 0px auto;
        margin-top: 1em;
    }
    .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover {
        color: #fff;
    }
</style>

<script type="text/javascript">
    $(function() {
        var uls = $('.sidebar-nav > ul > *').clone();
        uls.addClass('visible-xs');
        $('#main-menu').append(uls.clone());
    });
</script>

<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="../assets/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">


<!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
<!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
<!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
<!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->

<!--<![endif]-->

<div class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="" href="index.html"><span class="navbar-brand"> AIRA FACTORING | BACK OFFICE</span></a>
    </div>

    <div class="navbar-collapse collapse" style="height: 1px;">
        <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> <?php echo get_admin_profile('username'); ?>
                    <i class="fa fa-caret-down"></i>
                </a>

                <ul class="dropdown-menu">
                    <li><a href="<?php echo site_admin_url('user/user/update/'.get_admin_profile('user_id')); ?>">My Account</a></li>
                    <li><a href="<?php echo site_admin_url('user/forgot_password'); ?>">Reset My Password</a></li>
                    <li class="divider"></li>
                    <li><a tabindex="-1" href="<?php echo site_admin_url('auth/auth/logout'); ?>">Logout</a></li>
                </ul>
            </li>
        </ul>

    </div>
</div>
</div>

<?php echo $navigator; ?>


<div class="content">
    <div class="header">
        <?php echo $breadcrumb;?>
    </div>
    <div class="main-content">
        <!-- Start Admin Content -->
        <?php echo $content; ?>
        <?php echo $footer; ?>
    </div>
</div>


<script src="<?php echo js_path('bootstrap/js/bootstrap.js', 'admin'); ?>"></script>
<script type="text/javascript">
    $("[rel=tooltip]").tooltip();
    $(function() {
        $('.demo-cancel-click').click(function(){return false;});
    });
</script>
    <?php echo $_scripts; ?>

</body></html>
