<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MyClasses {

	function index()
	{
		ini_set('include_path', ini_get('include_path').PATH_SEPARATOR.APPPATH.'classes');
		ini_set('include_path', ini_get('include_path').PATH_SEPARATOR.APPPATH.'libraries');
	}

}
?>