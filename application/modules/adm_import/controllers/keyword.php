<?php
class Keyword extends MY_Controller { 
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('directory_helper');
		$this->load->helper('file');
		$this->load->model('import_keyword_model', 'import_meta_data');
		$this->load->model('adm_keyword/keyword_file_model', 'keyword_file');
		$this->load->model('adm_keyword/keyword_name_model', 'keyword_name');


	}



	public function get_data()
	{

		$data = array(); 
		$shop_status = false; 
		$product_status = array();

		if ($this->input->post('btn-import'))
		{
			$path = $this->input->post('url');
			$files  = get_directory_contents($path);
			#directory_tree($files);
			alertd($files, 'red');
			#exit;
			
			$keyword_file_results = array(); 
			if ( ! empty($files))
			{
				
				foreach ($files as $key => $file)
				{
					#alert($file, 'red', 'file');

					$path = explode("\\", $file);

					$file_name = $path[count($path) - 1];
					
					#alert($path, 'blue', 'path');
					#echo 'last_pos = '.$last_pos.'</p>';
					
					if (preg_match("/^(.*)\.robot$/", $file_name))
					{
						$is_exist = $this->keyword_file->is_exist(url_title($file));
						#echo "is_exist = ".$is_exist; 

						
						

					 	if ($is_exist == false)
					 	{
					 		unset($fields);
					 		$fields = array(
					 			'full_path' => $file,
					 			'full_path_slug' => url_title($file),
					 			'name' => $file_name,
					 			'created_by' => get_admin_profile('user_id'), 
					 			'created_at' => date_now()
					 		);

					 		if ($this->keyword_file->create($fields))
					 		{
					 			$keyword_file_id = $this->keyword_file->get_last_insert_id();
					 			$keyword_file_results[] = array(
					 				'code' => 200,
					 				'message' => '200 OK',
					 				'message' => 'Create keyword file "'.$file.'" successfully'
					 			);
					 		}
					 	}
					 	else 
					 	{
					 		$keyword_file = $this->keyword_file->get_keyword_file_detail(url_title($file), 'full_path_slug');
					 		#alert($keyword_file,'red', 'keyword_file');
					 		$keyword_file_id = $keyword_file['id'];
					 	}

					 	$keywords = read_file($file);
					 	$found_section_keyword = false; 
					 	
					 	$keyword_name_results = array(); 

					 	$keyword_name = array(); 
					 	$found_documentation = false; 
					 	$documents = array();

					 	foreach ($keywords as $keyword_key => $line)
					 	{
					 		if (preg_match("/(\*\*\* keywords \*\*\*|\*\*\* keyword \*\*\*)/", strtolower($line)))
					 		{
					 			$found_section_keyword = true; 
					 			continue; 
					 		}
					 		if ($found_section_keyword == true)
					 		{
					 			if (preg_match("/^\S(.*)$/", $line) && ! preg_match("/^#(.*)$/", $line) && ! empty($line))

					 			{
					 				$found_documentation = false; 
					 				$this->_check_insert_keyword($line, $keyword_file_id);
					 			}
					 		}
					 		// if ($found_section_keyword == true)
					 		// {
					 			
					 		// 	if (preg_match("/^\S(.*)$/", $line) && ! preg_match("/^#(.*)$/", $line) && ! empty($line))
								// {
								// 	#$keyword_name[] = $line; 
								// 	$found_documentation = false; 
								// 	$this->_check_insert_keyword($line, $keyword_file_id);
								// 	echo '<h4>Found keyword '.$line.'</h4>';
								// }								

					 		// 	#echo "keyword = ".strtolower($keyword).'<br>';
					 		// 	if (preg_match("/\[documentation\]/", strtolower($line)))
								// {
								// 	$found_documentation = true; 
								// 	#echo 'documentation tag = '.$keyword.'<br>'; 
								// 	$documents[] = $line; 
								// }

					 		// 	if ($found_documentation == true)
					 		// 	{

								// 	preg_match_all("/^\s*\.\.\.(.*)/", strtolower($line), $doc, PREG_PATTERN_ORDER);
								// 	$doc_line2 = $doc[0][0]
								// 	$doc_line3 = $doc[1][0];
									
								// }
					 			

					 			

					 			

					 		// }

					 	}
					 	//  end foreach 
					}
				}

			}
			exit; 
			$data['results'] = $keyword_file_results; 

   
			
		}

		#http://www.aliexpress.com/item/2016-Summer-New-Style-Vestidos-cute-heart-shaped-love-print-sleeveless-white-dress-female-fashion-loose/32646295720.html?spm=2114.01010108.3.3.rOPe6Y&ws_ab_test=searchweb201556_0,searchweb201602_1_10017_507_401_10040,searchweb201603_3&btsid=7cb78972-10fd-4e74-892a-7c716d72b96f
		$this->template->write_view('content', 'import_meta_data_view', $data);
		$this->template->render();
	}

	private function _check_insert_keyword($keyword = null, $keyword_file_id = null)
	{
		if ( ! $this->keyword_name->is_exist(url_title($keyword), $keyword_file_id))
		{
			unset($fields);
			$fields = array(
			    'name' => $keyword,
				'slug' => url_title($keyword), 
				'keyword_file_id' => $keyword_file_id,
				'created_at' => date_now(), 
				'created_by' => get_admin_profile('user_id')
			);
			if ($this->keyword_name->create($fields))
			{
				$keyword_name_results[] = array(
					'code' => 200,
					'message' => '200 OK',
					'message' => 'Create keyword name "'.$keyword.'" successfully'
				);
			}
		}
		else 
		{
			// unset($fields);
			// $fields = array(
			// 	'name' => $keyword,
			// 	'slug' => url_title($keyword), 
			// 	'keyword_file_id' => $keyword_file_id,
			// 	'created_at' => date_now(), 
			// 	'created_by' => get_admin_profile('user_id')
			// );
			// $where[]
			// if ($this->keyword_name->create($fields))
			// {
			// 	$keyword_name_results[] = array(
			// 		'code' => 200,
			// 		'message' => '200 OK',
			// 		'message' => 'Create keyword name "'.$keyword.'" successfully'
			// 	);
			// }
		}
	}

	private function _check_keyword_name($keyword = null, $keyword_file_id = null)
	{
		
	}

	private function _get_arguments($keyword = null)
	{
		
	}

	private function _get_documentation($keyword = null, $found_documentation = false)
	{
		
	}

	public function index()
	{
		$data = array();
		$args = array();
		$data['page'] = $this->input->get_post('page') ? $this->input->get_post('page', true) : 1;
		$data['limit_page'] = $this->input->get_post('limit_page') ? $this->input->get_post('limit_page', true) : 20;

		


		$this->template->write_view('content', 'shop_list_view', $data);
		$this->template->render();
	}

	public function detail($id = null)
	{
		if (empty($id))
		{
			$this->session->set_flashdata('error', 'Id is required');
			redirect(site_admin_url('import/aliexpress'), 'location');
		}
		$data = array();
		$data['id'] = $id; 
		$data['meta_data'] = $this->import_meta_data->get_meta_data_detail($id);
		if (empty($data['meta_data']['id']))
		{
			$this->session->set_flashdata('error', 'Not found data');
			redirect(site_admin_url('import/aliexpress'), 'location');
		}
		#alert($data['meta_data']);
		#exit;

		$html = $data['meta_data']['meta_data'];

		preg_match_all('/<ui class="items-list util-clearfix">(.*?)<\/ul>/s', $html, $matches);
		alert($matches);


		#echo '<textarea cols="30" rows="30">'.$html.'</textarea>';
		#$html = str_replace("&", "&amp;", $html);
		#$html = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $html);
		#$html = preg_replace('#<style(.*?)>(.*?)</style>#is', '', $html);
		#$html = preg_replace('#<noscript(.*?)>(.*?)</noscript>#is', '', $html);

		#$html = preg_replace('#<textarea(.*?)>(.*?)</textarea>#is', '', $html);
		#$html = preg_replace("/<\s* input [^>]+ >/xi", "", $html);

		#$html = preg_replace('#<link rel(.*?)/>#is', '', $html);
		#$html = preg_replace('#<!--(.*?)-->#is', '', $html);
		#$html = preg_replace()
		#$html = preg_replace('#(.*?)<body#is', '', $html);
		#$html = str_replace(";", '', $html);
		echo '<textarea cols="100" rows="30">'.$html.'</textarea>';
		

		#echo

		$dom = new DOMDocument('1.0', 'UTF-8');
		// set error level
		#$internalErrors = libxml_use_internal_errors(true);
		$dom->loadHTML($html);
		// Restore error level
		#libxml_use_internal_errors($internalErrors);
		#exit;
		
		$xpath = new DOMXPath($dom);
		#$xpath_resultset =  $xpath->query('//div[@class="bc-list-wrap"]/ul[contains(@class, "bc-list-not-standard")]');
		$xpath_resultset = $xpath->query('//ul[contains(@class, "items-list")]/li');
		foreach ($xpath_resultset as $key => $value)
		{
			#alert($value, 'red');
		}
		exit;

		


		
		// dirty fix

		// foreach ($doc->childNodes as $item)
  //   		if ($item->nodeType == XML_PI_NODE)
  //       		$doc->removeChild($item); // remove hack
		// 		$doc->encoding = 'UTF-8'; // insert proper

		$this->template->write_view('content', 'meta_data_detail_view', $data);
		$this->template->render();


	}	

	public function view_content($id = null)
	{
		if (empty($id))
		{
			$this->session->set_flashdata('error', 'Id is required');
			redirect(site_admin_url('import/aliexpress'), 'location');
		}
		$data = array();
		$data['id'] = $id; 
		$data['meta_data'] = $this->import_meta_data->get_meta_data_detail($id);
		if (empty($data['meta_data']['id']))
		{
			$this->session->set_flashdata('error', 'Not found data');
			redirect(site_admin_url('import/aliexpress'), 'location');
		}
		#alert($data['meta_data']);
		#exit;
		$this->template->set_template('blank');
		$this->template->write_view('content', 'meta_data_content_view', $data, true);
		$this->template->render();
	}

	private function _get_content_from_link($links = null, $import_category_id = null, $shop_id = null)
	{
		#echo "<p>=== links = ".$links." ";
		#echo "import_category_id = ".$import_category_id." ";
		#echo "shop_id = ".$shop_id." ====</p>";
		foreach ($links as $value)
		{

			if ( ! $this->import_product->is_imported($value))
			{
				echo '<p>Product data from CURL</p>';
				$detail_html = $this->curl->get($value);
				unset($fields);
				$fields = array(
					'url_link' => $value,
					'shop_id' => $shop_id,
					'meta_data' => $detail_html,
					'import_category_id' => $import_category_id,
					'shop_id' => $shop_id,
					'created_at' => date_now()
				);
				if ($this->import_product->create($fields))
				{

				}

			}
			else 
			{
				echo '<p>Product data from database</p>';
				$import_product = $this->import_product->get_import_product_by_url($value);
				$detail_html = $import_product['meta_data'];
				#echo  $detail_html;
			}


			$detail = $this->ali_dom->get_node_from_detail_page($detail_html);
				
			$product_id = $this->_insert_product($detail, $value, $shop_id);

			#echo "<h1>Product_id = ".$product_id.'</h1>';
			if ( ! empty($product_id))
			{

				 	
			 	$breadcrumbs = $this->ali_dom->get_text_breadcrumb($detail_html);
				 	
			 	$category_status = $this->_insert_category($breadcrumbs, $product_id);
			 	#alert($category_status, 'orange', 'category_status');
			 	$features = $this->ali_dom->get_feature($detail_html);
				 	

			 	$feature_status = $this->_insert_feature($features, $product_id);
			 	#alert($feature_status, 'red', 'feature_status');
				 	
			 	$images = $this->ali_dom->get_image($detail_html);
			 	$image_status = $this->_insert_image($images, $product_id);
			 	#alert($image_status, 'green', 'image_status');
				 	
			 	$color = $this->ali_dom->get_color($detail_html);
			 	$color_status = $this->_insert_sku($color, $product_id, 'color');
			 	#alert($color_status, 'red', 'color_status');
			 	$size = $this->ali_dom->get_size($detail_html);
			 	$size_status = $this->_insert_sku($size, $product_id, 'size');
			 	#alert($size_status, 'blue', 'size_status');
			}
		}
	}

	private function _insert_product($product = null, $url_link = null, $shop_id = null)
	{

		if ( ! $this->product->is_exist(url_title($product['product_name'])))
		{
			$fields = array(
				'shop_id' => $shop_id,
				'name' => $product['product_name'],
				'url_link' => $url_link,
				'slug' => url_title($product['product_name']),
				'price' => $product['price']
			);
			alert($fields, 'red', 'fields_insert_product');
			if ($this->product->create($fields))
			{
				return $this->product->get_last_insert_id();
			}
			return false; 
		}
		else 
		{
			$product = $this->product->get_product_by_slug(url_title($product['product_name']));
			return $product['id'];
		}
		return null; 
		
	}

	private function _insert_category($categories = null, $product_id = null)
	{
		$return = array();
		$data_return = array();
		#alert($images, 'red');

		if ( ! empty($categories))
		{
			foreach ($categories as $key => $value)
			{
				
				if ( ! $this->product_category->is_exist(url_title($value), $product_id))
				{
					unset($fileds);
					$fields = array(
						'product_id' => $product_id,
						'name' => $value,
						'slug' => url_title($value),
						'created_at' => date_now()
					);
					if ($this->product_category->create($fields))
					{
						$data_return = array(
							'code' => 200,
							'status' => '200 OK',
							'message' => 'Create product category successfully',
							'data' => url_title($value)

						);
					}
					else 
					{
						$data_return = array(
							'code' => 400,
							'status' => '400 Error',
							'message' => 'Cannot create product category.',
							'data' => url_title($value)
						);
					}
				}
				else 
				{
					$data_return = array(
						'code' => 400,
						'status' => '400 Error',
						'message' => 'Existing product category.',
						'data' => url_title($value)
					);
				}
				$return[] = $data_return;
				unset($data_return);

			}
			// end foreach
			return $return;
		}	
	}

	private function _insert_image($images = null, $product_id = null)
	{
		$return = array();
		$data_return = array();
		#alert($images, 'red');

		if ( ! empty($images))
		{
			foreach ($images as $key => $value)
			{
				
				if ( ! $this->product_image->is_exist($value, $product_id))
				{
					unset($fileds);
					$fields = array(
						'product_id' => $product_id,
						'img_path' => $value,
						'created_at' => date_now()
					);
					if ($this->product_image->create($fields))
					{
						$data_return = array(
							'code' => 200,
							'status' => '200 OK',
							'message' => 'Create image successfully'
						);
					}
					else 
					{
						$data_return = array(
							'code' => 400,
							'status' => '400 Error',
							'message' => 'Cannot create product image.'
						);
					}
				}
				else 
				{
					$data_return = array(
						'code' => 400,
						'status' => '400 Error',
						'message' => 'Existing data.'
					);
				}
				$return[] = $data_return;
				unset($data_return);

			}
			// end foreach
			return $return;
		}	
	}

	private function _insert_sku($sku = null, $product_id = null, $type = null)
	{
		$return = array();
		$data_return = array();
		if ( ! empty($sku))
		{
			foreach ($sku as $key => $value)
			{
				if ( ! $this->product_sku->is_exist(url_title($value), $product_id, $type))
				{
					unset($fileds);
					$fields = array(
						'product_id' => $product_id,
						'name' => $value,
						'slug' => url_title($value),
						'type' => $type,
						'created_at' => date_now()
					);
					#alert($fields);
					#exit;
					if ($this->product_sku->create($fields))
					{
						$data_return = array(
							'code' => 200,
							'status' => '200 OK',
							'message' => 'Create product sku successfully'
						);
					}
					else 
					{
						$data_return = array(
							'code' => 400,
							'status' => '400 Error',
							'message' => 'Cannot create product sku.'
						);
					}
				}
				else 
				{
					$data_return = array(
						'code' => 400,
						'status' => '400 Error',
						'message' => 'Existing product sku.'
					);
				}
				$return[] = $data_return;
				unset($data_return);

			}
			// end foreach
			return $return;
		}	
	}

	private function _insert_feature($feature = null, $product_id = null)
	{
		$return = array();
		$data_return = array();
		#alert($feature, 'blue', 'feature');
		#exit;
		if ( ! empty($feature))
		{
			foreach ($feature as $key => $value)
			{
				
				if ( ! $this->product_feature->is_exist(url_title($value), $product_id))
				{
					unset($fileds);
					$fields = array(
						'product_id' => $product_id,
						'name' => $value,
						'slug' => url_title($value),
						'created_at' => date_now()
					);
					#alert($fields);
					#exit;
					if ($this->product_feature->create($fields))
					{
						$data_return = array(
							'code' => 200,
							'status' => '200 OK',
							'message' => 'Create product feature successfully'
						);
					}
					else 
					{
						$data_return = array(
							'code' => 400,
							'status' => '400 Error',
							'message' => 'Cannot create product feature.'
						);
					}
				}
				else 
				{
					$data_return = array(
						'code' => 400,
						'status' => '400 Error',
						'message' => 'Existing product feature.'
					);
				}
				$return[] = $data_return;
				unset($data_return);

			}
			// end foreach
			return $return;
		}	
	}

	private function _insert_package($feature = null, $product_id = null)
	{
		$return = array();
		$data_return = array();
		#alert($feature, 'blue', 'feature');
		#exit;
		if ( ! empty($feature))
		{
			foreach ($feature as $key => $value)
			{
				
				if ( ! $this->product_feature->is_exist(url_title($value), $product_id))
				{
					unset($fileds);
					$fields = array(
						'product_id' => $product_id,
						'name' => $value,
						'slug' => url_title($value),
						'created_at' => date_now()
					);
					#alert($fields);
					#exit;
					if ($this->product_feature->create($fields))
					{
						$data_return = array(
							'code' => 200,
							'status' => '200 OK',
							'message' => 'Create product feature successfully'
						);
					}
					else 
					{
						$data_return = array(
							'code' => 400,
							'status' => '400 Error',
							'message' => 'Cannot create product feature.'
						);
					}
				}
				else 
				{
					$data_return = array(
						'code' => 400,
						'status' => '400 Error',
						'message' => 'Existing product feature.'
					);
				}
				$return[] = $data_return;
				unset($data_return);

			}
			// end foreach
			return $return;
		}	
	}
}