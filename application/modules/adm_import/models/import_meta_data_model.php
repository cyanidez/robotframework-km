<?php
class Import_meta_data_model extends MY_Model { 

	private $_table = "import_meta_data"; 

	public function __construct()
	{
		parent::__construct();
	}

	public function get_meta_data_detail($id = null)
	{
		if (empty($id))
		{
			return null; 
		}
		$select = parent::select();
		$select->from(array('IM' => 'import_meta_data'), array('*'))
			->where('IM.id = ?', $id);

		return parent::fetch_row($select);
	}

	public function get_meta_data($args = array(), $page = 1, $limit_page = 20)
	{
		$select = parent::select();
		$select->from(array('I' => 'import_meta_data'), array('*'));
		return parent::fetch_page($select, array(), $page, $limit_page);

	}

	public function create($params = NULL)
    {
        if (empty($params))
        {
            return NULL;
        }

        if (parent::insert($this->_table, $params) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function update($params = NULL, $where = NULL)
    {
        if (empty($params))
        {
            return NULL;
        }

        if (empty($where))
        {
            return NULL;
        }

        if (parent::update($this->_table, $params, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($where = NULL)
    {
        if (empty($where))
        {
            return NULL;
        }
        if (parent::delete($this->_table, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }
}