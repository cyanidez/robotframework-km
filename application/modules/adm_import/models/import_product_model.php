<?php
class Import_product_model extends MY_Model {

    private $_table = "import_product";

    public function get_import_product_by_url($url = null)
    {
        if (empty($url))
        {
            return null;
        }
        $select = parent::select();
        $select->from(array('IP' => $this->_table), array('*'))
            ->where('IP.url_link = ?', $url);
        return parent::fetch_row($select);
    }

    public function is_imported($url_link = null)
    {
        if (empty($url_link))
        {
            return null;
        }
        $select = parent::select();
        $select->from(array('IP' => $this->_table), array('*'))
            ->where('IP.url_link = ?', $url_link);
        $query = parent::fetch_one($select);
        if ($query > 0)
        {
            return true; 
        }
        return false; 

    }




    public function create($params = null)
    {
        if (empty($params))
        {
            return null;
        }

        if (parent::insert($this->_table, $params) !== false)
        {
            return true;
        }
        return false;
    }

    public function update($params = NULL, $where = NULL)
    {
        if (empty($params))
        {
            return NULL;
        }

        if (empty($where))
        {
            return NULL;
        }

        if (parent::update($this->_table, $params, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($where = NULL)
    {
        if (empty($where))
        {
            return NULL;
        }
        if (parent::delete($this->_table, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    
}