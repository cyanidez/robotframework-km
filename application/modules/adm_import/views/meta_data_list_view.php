<div class="row">
	<div class="col-sm-10">
		<a class="btn btn-info" href="<?php echo site_admin_url('import/aliexpress/get_data'); ?>">List content</a>
	</div>
	<div class="col-sm-10 align-center" style="margin:auto;">

		<table class="table table-hover">
			<thead class="bordered-darkorange">
				<tr>
					<th>No.</th>
					<th>URL</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			
			<?php if ($meta_data['row_count'] > 0) : ?>
				<?php foreach ($meta_data['rows'] as $key => $value) : ?>
				<tr>
					<td><?php echo $value['id']; ?></td>
					<td><?php echo $value['url']; ?></td>
					<td>
						<a href="<?php echo site_admin_url('import/aliexpress/detail/'.$value['id']); ?>" class="btn btn-info">Detail</a>
					</td>
				</tr>
				<?php endforeach; ?>
			<?php endif; ?>
			</tbody>
		</table>
		
	</div>
</div>