<div class="row">
	<div class="col-sm-10 align-center" style="margin:auto;">
		<?php if ($this->session->flashdata('success')) : ?>
		<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
		<?php endif; ?>
		<?php if ($this->session->flashdata('error')) : ?>
		<div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
		<?php endif; ?>
		<table class="table table-hover">
			<thead class="bordered-darkorange">
				<tr>
					<th colspan="2">Description</th>
			
				</tr>
			</thead>
			<tbody>
			
				<tr>
					<td>URL : </td>
					<td><?php echo $meta_data['url']; ?></td>
				</tr>
				<tr>
					<td>Content : </td>
					<td>
						<iframe 
							style="width:100%; height:600px;" 
							src="<?php echo site_admin_url('import/aliexpress/view_content/'.$id); ?>">
						</iframe>
						<?php /**<textarea 
							cols="100"
							rows="50" 
							class="form-control"><?php echo htmlspecialchars($meta_data['meta_data']); ?></textarea> **/ ?>
						<a 
							target="_blank" 
							href="<?php echo site_admin_url('import/aliexpress/view_content/'.$id); ?>" 
							class="btn btn-info">View Content</a>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<a href="<?php echo site_admin_url('import/aliexpress'); ?>" class="btn btn-info">&lt;&lt; Back</a>
					</td>
				</tr>
			</tbody>
		</table>
		
	</div>
</div>