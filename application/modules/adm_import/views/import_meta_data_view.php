<div class="row">
	<div class="col-sm-10">
		<a class="btn btn-info" href="<?php echo site_admin_url('import/keyword/get_data'); ?>">List content</a>
	</div>
	<div class="col-sm-10 align-center" style="margin:auto;">
		<?php if ( ! empty($results)) : ?>
			<?php foreach ($results as $key => $value) : ?>
				<?php if ($value['code'] == 200) : ?>
				<div class="alert alert-success">
					<?php echo $value['message']; ?>
				</div>
				<?php else : ?>
				<div class="alert alert-danger">
					<?php echo $value['message']; ?>
				</div>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endif; ?>
		<div class="widget">

			<div class="widget-header bordered-bottom bordered-palegreen">
				URL for get data
			</div>
			<div class="widget-body">
				<?php if ($this->session->flashdata('success')) : ?>
				<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
				<?php endif; ?>
				<?php if ($this->session->flashdata('error')) : ?>
				<div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
				<?php endif; ?>
				<div>
					<form id="import-form" name="import-form"
						method="post"
						class="form-horizontal form-bordered"
						action="<?php echo site_admin_url('import/keyword/get_data'); ?>"
					>
						<div class="form-group">
							<label class="control-label col-sm-2">URL : </label>
							<div class="col-sm-10">
								<input target="_blank" type="text" class="form-control" id="url" name="url" placeholder="Please enter url : http://www.xxx.com">
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-2"></label>
							<div class="col-sm-10">
								<input type="submit" class="btn btn-info" value="Get content" name="btn-import" id="btn-import">
							</div>

						</div>

					</form>
				</div>
			</div>
		</div>
	</div>
</div>