<?php
class user_level extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_level_model', 'user_level');
    }

    public function index()
    {
        $data = array();
        $data['row_per_page'] = $this->input->get_post('row_per_page') ? $this->input->get_post('row_per_page') : 20;
        $data['page'] = $this->input->get_post('page') ? $this->input->get_post('page') : 1;


        $query = array();
        $data['keyword'] = $args['keyword'] = $this->input->get_post('keyword') ? $this->input->get_post('keyword') : "";
        $data['active_status'] = $args['active_status'] = $this->input->get_post('active_status') ? $this->input->get_post('active_status') : "";
        $data['user'] = $this->user_level->get_user_level($args, $data['page'], $data['row_per_page']);
        #alert($data['user']);
        #exit;

        if (isset($_GET))
        {
            foreach ($_GET as $k => $v)
            {
                $query[] = $k.'='.$v;
            }
        }


        if ( ! empty($query))
        {
            $data['query'] = implode('&', $query);
        }
        else
        {
            $data['query'] = "";
        }

        $this->breadcrumb->make('Dashboard', site_admin_url());

        if ( ! empty($query))
        {
            $this->breadcrumb->make('จัดการแผนก', site_admin_url('user/department/index'));
            $this->breadcrumb->make('ค้นหาแผนก', null, true, true);
        }
        else
        {
            $this->breadcrumb->make('จัดการแผนก', null, true, true);
        }


        parent::make_bread();
        #$data['tabs'] = $this->load->view('tab_view', $data, true);
        $this->template->write_view('content', 'department/list_view', $data);
        $this->template->render();
    }

    public function create()
    {
        $data = array();


        if ($this->input->post('btn-save'))
        {
            $rules = array(
                array(
                    'field' => 'name', 'label' => 'Department Name', 'rules' => 'trim|required',
                    'field' => 'status_flg', 'label' => 'Status', 'rules' => 'trim|required'
                )
            );
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() !== FALSE)
            {
                $fields = array(
                    'name' => $this->input->post('name', true),
                    'can_delete' => 'Y',
                    'status_flg' => $this->input->post('status_flg', true)
                );

                if ($this->department->create($fields))
                {
                    
                    $this->session->set_flashdata('success', 'Create user successfully.');
                    redirect(site_admin_url('user/user_level/index'), 'location');
                    

                }

            }
        }


        $this->breadcrumb->make('Dashboard', site_admin_url());
        #$this->breadcrumb->make('User Management', site_admin_url('user/user/index'));
        $this->breadcrumb->make('User Department', null, true, true);
        parent::make_bread();


        $this->template->add_js('user_level.js', 'admin');
        $this->template->write_view('content', 'department/create_view', $data);
        $this->template->render();
    }


    public function update($user_id = null)
    {
        $data = array();
        $id = $user_id;
        if (empty($id))
        {
            $this->session->set_flashdata('error', 'User department id is required.');
            redirect(site_admin_url('user/user_level/index'), 'location');
        }
        //--- Check has id of user ---//

        $data['user_level'] = $this->user_level->get_user_level_detail($id);
        if (empty($data['user']['id']))
        {
            $this->session->set_flashdata('error', 'Not found this user_id in database.');
            redirect(site_admin_url('user/user/index'), 'location');
        }
        //-- Check existing data ---//

        if ($this->input->post('btn-save'))
        {
            $rules = array(
                array(
                    'field' => 'name', 'label' => 'Department Name', 'rules' => 'trim|required',
                    'field' => 'status_flg', 'label' => 'Status', 'rules' => 'trim|required'
                )
            );
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() !== FALSE)
            {
                $fields = array(
                    'name' => $this->input->post('name', true),
                    'status_flg' => $this->input->post('status_flg', TRUE)
                );
                $where[] = 'id = '.$id;
                $is_success = true;
                if ($this->user->update($fields, $where))
                {
                    $this->session->set_flashdata('success', "Update user successfully");
                    redirect(site_admin_url('user/user_level/update/'.$id), 'location');
                }
            }
            //-- End of validation ---//
        }

        //--- End of btn-save ---//

        $data['id'] = $id;

        $this->breadcrumb->make('Dashboard', site_admin_url());
        $this->breadcrumb->make('User Managment', site_admin_url('user/user/index'));
        $this->breadcrumb->make('Update User', null, true, true);
        parent::make_bread();

        $data['user_id'] = $id;
        $this->template->add_js('user.js', 'admin');
        $this->template->write_view('content', 'update_view', $data);
        $this->template->render();
    }

    public function delete()
    {
        $data = array();
        $id = $this->input->get_post('id');
        if (empty($id))
        {
            $this->session->set_flashdata('error', 'Id required');
            redirect(site_admin_url('user/user/index'), 'location');
        }

        $data['user'] = $this->user->get_user_detail($id);
        if (empty($data['user']['id']))
        {
            $this->session->set_flashdata('error', 'Not found this user_id in database.');
            redirect(site_admin_url('user/user/index'), 'location');
        }


        $where[] = 'id = '.$id;




        if ($this->user->delete($where))
        {
            $this->session->set_flashdata('success', 'Delete user successfully.');
            redirect(site_admin_url('user/user/index'), 'location');

        }
        else
        {
            $this->session->set_flashdata('error', 'Cannot delete user');
            redirect(site_admin_url('user/user/index'), 'location');
        }





    }

    private function _gen_password()
    {
        $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $length = rand(6, 10);
        $start = rand(0, 20);
        $str = substr(str_shuffle($str), $start, $length);
        return $str;
    }

    public function reset_password()
    {
        $data = array();
        if ($this->input->post('btn-save'))
        {

        }
        $this->template->write_view('content', 'reset_password_view', $data);
        $this->template->render();
    }

    public function check_duplicate_username()
    {
        $username = $this->user->is_exist_username($this->input->post('username'));
        if ($username > 0)
        {
            echo "false";
        }
        else
        {
            echo "true";
        }
    }

    public function check_duplicate_email()
    {

    }




}