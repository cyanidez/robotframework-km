<?php
class Authentication extends MY_Controller { 

	private $_row_per_page = 20; 

	function __construct() 
	{
		parent::__construct(); 
		$this->load->model('user_model', 'user'); 
		$this->load->model('user_level_model', 'user_level'); 
		$this->load->model('authentication_model', 'authentication'); 
		$this->load->model('adm_cateogry/category_model', 'category');
		$this->lang->load('authentication', LANGCODE); 
		$this->load->library('form_validation');
	}

	function index() 
	{
		$data = array(); 
		$data['page'] = $this->input->get_post('page', TRUE) ? $this->input->get_post('page', TRUE) : 1; 
		$data['user_level'] = $this->user_level->get_user_level(NULL, $data['page'], $this->_row_per_page); 
		$this->breadcrumb->make('Dashboard', site_admin_url());
		$this->breadcrumb->make('User Department Privillege', null, true, true);

		parent::make_bread();		
		$this->template->write_view('content', 'authentication/list', $data); 
		$this->template->render(); 
	}

	public function create() 
	{
		$data = array(); 
		if ($this->input->post('button_save', TRUE) or $this->input->post('btn_apply')) 
		{
			$rules = array(
				array('field' => 'name', 'label' => lang('name'), 'rules' => 'trim|required'),
				array('field' => 'status_flg', 'label' => lang('status_flg'), 'rules' => 'required')
			);
			$this->form_validation->set_rules($rules); 

			if ($this->form_validation->run() !== FALSE) 
			{
				$fields = array(
					'name' => $this->input->post('name', TRUE),
					'status_flg' => $this->input->post('status_flg', TRUE),
					'can_delete' => 'Y'
				);
				if ($this->user_level->create($fields))
				{
					$menuID = $this->input->post('menuID', TRUE); 
					$sub_menuID = $this->input->post('sub_menuID', TRUE); 

					$user_level_id = $this->user_level->get_last_insert_id(); 
					$category_id = $this->input->post('category_id');
					$sub_category_id = $this->input->post('sub_category_id');					

					$is_error = FALSE; 
					foreach ($menuID as $key => $value) 
					{
						unset($fields); 
						$fields = array(
							'menu_id' => $value, 
							'user_level_id' => $user_level_id,
							'menu_type' => 'menu'
						); 

						if ( ! $this->authentication->create($fields)) 
						{						
							$is_error = TRUE; 
						}

						if (isset($sub_menuID[$value]) && count($sub_menuID[$value]) > 0) 
						{
							foreach ($sub_menuID[$value] as $key2 => $value2)
							{
								unset($fields);
								$fields = array(
									'menu_id' => $value2, 
									'user_level_id' => $user_level_id,
									'menu_type' => 'menu'
								); 
								if ( ! $this->authentication->create($fields)) 
								{						
									$is_error = TRUE; 
									break; 
								}
							}
						}						
					}
					if ( ! empty($category_id))
					{
						foreach ($category_id as $key => $value)
						{
							unset($fieds, $where);
							$fields = array(
								'menu_id' => $value, 
								'user_level_id' => $user_level_id,
								'menu_type' => 'category'
							);
							
							if ( ! $this->authentication->create($fields))
							{
								$is_error = true; 
								break; 
							}
							#alert($sub)

							if (isset($sub_category_id[$value]) && count($sub_category_id[$value]) > 0) 
							{
								foreach ($sub_category_id[$value] as $c_key => $c_value)
								{
									unset($fields, $where);
									$fields = array(
										'menu_id' => $c_value, 
										'user_level_id' => $user_level_id,	
										'menu_type' => 'category'
									); 
									alert($fields, 'red');
									if ( ! $this->authentication->create($fields)) 
									{						
										$is_error = TRUE; 
										break; 
									}
								}
							}			
						}
					}
					if ($is_error !== TRUE) 
					{
						$this->session->set_flashdata('success', 'Create user department successfully');
						if ($this->input->post('button_save'))
						{
							redirect(site_admin_url('user/authentication'), 'location'); 	
						}
						else 
						{
							redirect(site_admin_url('user/authentication/create'), 'location'); 	
						}
					}
					else 
					{
						$this->session->set_flashdata('error', 'Cannot create user department successfully');
					}

				}
			}
			
		}

		$data['authentication_category'] = $this->category->get_all_child();

		$this->breadcrumb->make('Dashboard', site_admin_url());
		$this->breadcrumb->make('User Department Privillege', site_admin_url('user/authentication'));
		$this->breadcrumb->make('Create User Department Privillege', null, true, true);
		$data['menu_category'] = $this->category->get_main_category_active();

		parent::make_bread();		
		$this->template->add_js_plugins('jquery.validate.js', 'admin');
		$this->template->add_js('authentication.js', 'admin'); 
		$data['menu'] = $this->authentication->get_all_menu(); 
		$this->template->write_view('content', 'authentication/create', $data); 
		$this->template->render(); 
	}

	public function update() 
	{
		$data = array();
		$user_level_id =  $this->input->get_post('user_level_id', TRUE); 
		if (empty($user_level_id)) 
		{
			redirect(site_admin_url('user/authentication'), 'location'); 
		}

		$user_level = $this->user_level->get_user_level_detail($user_level_id); 

		if (empty($user_level['id'])) 
		{
			redirect(site_admin_url('user/authentication'), 'location'); 
		}

		if ($this->input->post('button_save', TRUE) or $this->input->post('btn_apply')) 
		{
			$rules = array(
				array('field' => 'name', 'label' => lang('name'), 'rules' => 'trim|required'),
				array('field' => 'status_flg', 'label' => lang('status_flg'), 'rules' => 'required')
			);
			$this->form_validation->set_rules($rules); 

			if ($this->form_validation->run() !== FALSE) 
			{
				unset($fields, $where);
				$fields = array(
					'name' => $this->input->post('name', TRUE),
					'status_flg' => $this->input->post('status_flg', TRUE)
				);

				$where[] = 'id = '.$user_level_id; 
				if ($this->user_level->update($fields, $where))
				{
					$menuID = $this->input->post('menuID', TRUE); 
					$sub_menuID = $this->input->post('sub_menuID', TRUE); 
					$category_id = $this->input->post('category_id');
					$sub_category_id = $this->input->post('sub_category_id');					

					$this->authentication->clear($user_level_id); 

	
					$is_error = FALSE; 
					foreach ($menuID as $key => $value) 
					{
						unset($fields, $where); 
						$fields = array(
							'menu_id' => $value, 
							'user_level_id' => $user_level_id,
							'menu_type' => 'menu'
						); 

						if ( ! $this->authentication->create($fields)) 
						{						
							$is_error = TRUE; 
						}

						if (isset($sub_menuID[$value]) && count($sub_menuID[$value]) > 0) 
						{
							foreach ($sub_menuID[$value] as $key2 => $value2)
							{
								unset($fields, $where);
								$fields = array(
									'menu_id' => $value2, 
									'user_level_id' => $user_level_id,	
									'menu_type' => 'menu'
								); 
								if ( ! $this->authentication->create($fields)) 
								{						
									$is_error = TRUE; 
									break; 
								}
							}
						}						

					}
					#alert($_POST, 'red');
					if ( ! empty($category_id))
					{
						foreach ($category_id as $key => $value)
						{
							unset($fieds, $where);
							$fields = array(
								'menu_id' => $value, 
								'user_level_id' => $user_level_id,
								'menu_type' => 'category'
							);
							
							if ( ! $this->authentication->create($fields))
							{
								$is_error = true; 
								break; 
							}
							#alert($sub)

							if (isset($sub_category_id[$value]) && count($sub_category_id[$value]) > 0) 
							{
								foreach ($sub_category_id[$value] as $c_key => $c_value)
								{
									unset($fields, $where);
									$fields = array(
										'menu_id' => $c_value, 
										'user_level_id' => $user_level_id,	
										'menu_type' => 'category'
									); 
									alert($fields, 'red');
									if ( ! $this->authentication->create($fields)) 
									{						
										$is_error = TRUE; 
										break; 
									}
								}
							}			
						}
					}
					#echo "is_error = ".$is_error; 
					if ($is_error !== TRUE) 
					{
						$this->session->set_flashdata('success', 'Update user department successfully');
						if ($this->input->post('button_save'))
						{
							redirect(site_admin_url('user/authentication'), 'location'); 	
						}
						else 
						{
							redirect(site_admin_url('user/authentication/update?user_level_id='.$user_level_id), 'location');
						}
					}
					else 
					{
						$this->session->set_flashdata('error', 'Cannot update user department successfully');
					}
					
					
				}
			}
		}

		$this->breadcrumb->make('Dashboard', site_admin_url());
		$this->breadcrumb->make('User Department Privillege', site_admin_url('user/authentication'));
		$this->breadcrumb->make('Update User Department Privillege', null, true, true);
		parent::make_bread();
		$data['authentication_category'] = $this->authentication->get_category_active_authentication($user_level_id);

		$data['user_level_id'] = $user_level_id; 
		$this->template->add_js('lang/' . LANGCODE . '/authentication_lang.js', 'admin'); 
		$this->template->add_js('authentication.js', 'admin'); 
		$data['menu'] = $this->authentication->get_all_menu(); 
		$data['user_level'] = $this->user_level->get_user_level_detail($user_level_id); 
		$data['authentication'] = $this->authentication->get_authentication($user_level_id); 
		$this->template->write_view('content', 'authentication/update', $data);
		
		$this->template->render(); 

	}

	public function delete() 
	{
		$data = array();
		$user_level_id =  $this->input->get_post('user_level_id', TRUE); 
		if (empty($user_level_id)) 
		{
			$this->session->set_flashdata('error', 'User department id is required');
			redirect(site_admin_url('user/authentication'), 'location'); 
		}

		$user_level = $this->user_level->get_user_level_detail($user_level_id); 

		if (empty($user_level['id'])) 
		{
			$this->session->set_flashdata('error', 'Not found data');
			redirect(site_admin_url('user/authentication'), 'location'); 
		}		

		if ($this->user->can_delete_department($user_level_id) == false) 
		{
			$this->session->set_flashdata('error', 'Has any users is in this department.');
			redirect(site_admin_url('user/authentication'), 'location');
		}
		else
		{
			$where[] = 'id = '.$user_level_id; 
			if ($this->user_level->delete($where)) 
			{
				unset($where);
				$where[] = 'user_level_id = '.$user_level_id;
				if ($this->authentication->delete($where)) 
				{
					$this->session->set_flashdata('success', 'Delete user department successfully.');
					redirect(site_admin_url('user/authentication'), 'location'); 
				}
			}			
		}
	}


}