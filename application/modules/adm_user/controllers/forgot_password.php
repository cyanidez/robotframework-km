<?php
class Forgot_password extends MY_Controller {
    public  function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->library('bmtd_encrypt');
        $this->load->library('send_email');
        $this->template->add_js_plugins('jquery.url.js', 'admin');
    }

    

    public function index()
    {
        $data = array();
        if ($this->input->post('btn-save'))
        {

            $rules = array(
                array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required'),
                array('field' => 'username', 'label' => 'Username', 'rules' => 'trim|required')
            );


            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() !== FALSE)
            {
                
                $user = $this->user->get_user_detail($this->input->post('username'), 'username');

                if ($user['email'] != $this->input->post('email') || $user['username'] != $this->input->post('username'))
                {
                    $this->session->set_flashdata('error', 'You enter a email and username do not match with data from database.');
                    redirect(site_admin_url('user/forgot_password'), 'location');
                }

                $confirm_code = $this->_gen_code();

                $email = base64_encode($user['email']);

                $sum = $user['username']."abcxyz".$email."abcxyz".$confirm_code;
                $sum = $this->bmtd_encrypt->encrypt_url($sum);

                #alert($sum, 'red', 'sum');
                #exit;


                unset($fields, $where);
                $fields = array(
                    'confirm_code' => $confirm_code,
                    'confirm_code_status' => 'Y',
                    'confirmed_date' => date_now()
                );
                $where[] = "username = '".$this->input->post('username', true)."' ";

                if ($this->user->update($fields, $where))
                {                    
                
                    $message = "
                      <p>Dear : ".$user['first_name']." ".$user['last_name'].'</p>'
                    . '<br>Please click link below for change password<br><br>'
                    . '<a href="'.site_admin_url('user/forgot_password/change_password?sum='.$sum).'">Set your password</a>'
                    . '<br><br><p>Best Regards</p>'
                    . '<p>Airafactoring Co., Ltd.</p>';

                                        

                    $subject = "You was sent request for change password.";
                    #$this->send_email->set_smtp_host()
                    #$this->send_email->
                    if ($this->send_email->send($user['email'], $subject, $message, "no-reply@Airafactoring.co.th", "Airafactoring"))
                    {
                        $this->session->set_flashdata('success', 'Please check your email and click link to change password.');
                        redirect(site_admin_url('user/forgot_password'), 'location');    
                    }
                    

                }

            }
        }


        parent::set_layouts('login');
        
        #$this->template->add_js_plugins('jquery.validate.js', 'admin');
        $this->template->add_js('user.js', 'admin');
        $this->template->write_view('content', 'forgot_password_view', $data);
        $this->template->render();
    }

    public function change_password()
    {
        $data = array();

        if ($this->input->get_post('sum') == "")
        {
            $this->session->set_flashdata('error', 'Please correct your verify information');
            redirect(site_admin_url('user/forgot_password'), 'location');
        }
        $data['sum'] = $sum = $this->input->get_post('sum');
        $sum = $this->bmtd_encrypt->decrypt_url($sum);

        
        #alert($sum, 'red' ,'sum2');
        list($username, $email, $confirm_code) = explode("abcxyz", $sum);
        $email = base64_decode($email);

        $user = $this->user->get_authen_user_change_password($username, $email, $confirm_code);
        #alert($user, 'red');
        #exit; 


        if (empty($user['id']))
        {
            $this->session->set_flashdata('error', 'Please correct your verify information');
            redirect(site_admin_url('user/forgot_password'), 'location');
        }
        if ($user['confirm_code_status'] != "Y")
        {
            $this->session->set_flashdata('error', 'Your confirm code is expired.');
            redirect(site_admin_url('user/forgot_password'), 'location');
        }


        #alert($_REQUEST, 'red');
        
        if ($this->input->post('btn-save'))
        {
            
            $rules = array(            
                array('field' => 'new_password', 'label' => 'New Password', 'rules' => 'trim|required|matches[confirm_new_password]'),
                array('field' => 'confirm_new_password', 'label' => 'Confirm new Password', 'rules' => 'trim|required')
    
            );
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() !== FALSE)
            {
               
                $user = $this->user->get_user_detail($username, 'username');
                $password = $this->bmtd_encrypt->my_encrypt($this->input->post('new_password', true));

                $fields = array(
                    'password' => $password,
                    'confirm_code_status' => 'N',
                    'updated_at' => date("Y-m-d H:i:s"),
                    'updated_by' => $user['id']
                );
                $where[] = 'id = '.$user['id'];

                if ($this->user->update($fields, $where))
                {
                    $this->session->set_flashdata('success', 'Password has been changed.');
                    redirect(site_admin_url('user/forgot_password'), 'location');

                }

            }
        }
        parent::set_layouts('login');
        
        
        $this->template->add_js_plugins('jquery.validate.js', 'admin');
        $this->template->add_js('user.js', 'admin');
        $this->template->write_view('content', 'reset_password_view', $data);
        $this->template->render();
    }

    private function _gen_code()
    {
        $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $length = rand(6, 10);
        $start = rand(0, 20);
        $str = substr(str_shuffle($str), $start, $length);
        return $str;
    }


}