<?php
class User_log extends MY_Controller { 
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_log_model', 'user_log');
		$this->load->model('user_model', 'user');
	}

	public function index()
	{
		$data = array();
        $data['row_per_page'] = $this->input->get_post('row_per_page') ? $this->input->get_post('row_per_page') : 20;
        $data['page'] = $this->input->get_post('page') ? $this->input->get_post('page') : 1;

        $query = array();
        $data['user_id'] = $args['user_id'] = $this->input->get_post('user_id') ? $this->input->get_post('user_id') : "";
        $data['action'] = $args['action'] = $this->input->get_post('action') ? $this->input->get_post('action') : array();
        $data['module'] = $args['module'] = $this->input->get_post('module') ? $this->input->get_post('module') : array();
        #alert($data['action'], 'red');
        
        $data['user_log'] = $this->user_log->get_user_log($args, $data['page'], $data['row_per_page']);
        #alert($_GET, 'red');

        if (isset($_GET))
        {
        	$key  = array();
            foreach ($_GET as $k => $v)
            {
            	#echo "key = ".$k; 
            	#alert($v, 'red', 'v');
            	if (is_array($v))
            	{
            		if ($k == "action")
            		{
	                	foreach ($v as $action_key => $action_value)
	                	{
	                		$action[]  = 'action[]='.$action_value; 
	                	}

						$query[] = implode("&", $action);
	                }
	                if ($k == "module")
            		{
	                	foreach ($v as $module_key => $module_value)
	                	{
	                		$module[]  = 'module[]='.$module_value; 
	                	}

						$query[] = implode("&", $module);
	                }
                }
                else 
                {
                	$query[] = $k.'='.$v;
                }
            }
        }
        #$query[] = http_build_query($action);
        #alert($query, 'red', 'query');
        


        if ( ! empty($query))
        {
            $data['query'] = implode('&', $query);
        }
        else
        {
            $data['query'] = "";
        }

        #alert($data['query']);

        $this->breadcrumb->make('Dashboard', site_admin_url());

        if ( ! empty($query))
        {
            $this->breadcrumb->make('User Log', site_admin_url('user/user_log/index'));
            $this->breadcrumb->make('Search User Log', null, true, true);
        }
        else
        {
            $this->breadcrumb->make('User Log', null, true, true);
        }

        $data['user'] = $this->user->get_all_user();
        parent::make_bread();
        #$data['tabs'] = $this->load->view('tab_view', $data, true);
        $this->template->write_view('content', 'user_log/list_view', $data);
        $this->template->render();
        
	}

	public function detail($id = null)
	{
		$data = array();
		if (empty($id))
		{
			$this->session->set_flashdata('error', 'id of log is required.');
			redirect(site_admin_url('user/user_log/index'), 'location');
		}
		$data['user_log'] = $user_log = $this->user_log->get_user_log_detail($id);
		if (empty($user_log['id']))
		{
			$this->session->set_flashdata('error', 'Not found user log');
			redirect(site_admin_url('user/user_log/index'), 'location');
		}
		
		$this->breadcrumb->make('Dashboard', site_admin_url('dashboard'));
		$this->breadcrumb->make('User Log', site_admin_url('user/user_log/index'));
		$this->breadcrumb->make('View User Log Detail', null, true, true);
		parent::make_bread();

		$this->template->write_view('content', 'user_log/detail_view', $data);
		$this->template->render();
	}
}