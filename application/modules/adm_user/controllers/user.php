<?php
class User extends MY_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model', 'user');
        $this->load->model('user_level_model', 'user_level');
        $this->load->library('bmtd_encrypt');
        $this->template->add_js_plugins('jquery.url.js', 'admin');
        $this->load->library('send_email');
    }

    public function index()
    {
        $data = array();
        $data['row_per_page'] = $this->input->get_post('row_per_page') ? $this->input->get_post('row_per_page') : 20;
        $data['page'] = $this->input->get_post('page') ? $this->input->get_post('page') : 1;


        $query = array();
        $data['keyword'] = $args['keyword'] = $this->input->get_post('keyword') ? $this->input->get_post('keyword') : "";
        $data['active_status'] = $args['active_status'] = $this->input->get_post('active_status') ? $this->input->get_post('active_status') : "";
        $data['user'] = $this->user->get_user($args, $data['page'], $data['row_per_page']);
        #alert($data['user']);
        #exit;

        if (isset($_GET))
        {
            foreach ($_GET as $k => $v)
            {
                $query[] = $k.'='.$v;
            }
        }


        if ( ! empty($query))
        {
            $data['query'] = implode('&', $query);
        }
        else
        {
            $data['query'] = "";
        }

        $this->breadcrumb->make('Dashboard', site_admin_url());

        if ( ! empty($query))
        {
            $this->breadcrumb->make('User Management', site_admin_url('user/user/index'));
            $this->breadcrumb->make('Search User', null, true, true);
        }
        else
        {
            $this->breadcrumb->make('User Management', null, true, true);
        }


        parent::make_bread();
        #$data['tabs'] = $this->load->view('tab_view', $data, true);
        $this->template->write_view('content', 'list_view', $data);
        $this->template->render();
    }

    public function request_change()
    {
        $data = array();
        if ($this->input->post('btn-save'))
        {

            $rules = array(
                array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required'),
                array('field' => 'username', 'label' => 'Username', 'rules' => 'trim|required')
            );


            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() !== FALSE)
            {
                
                $user = $this->user->get_user_detail(get_admin_profile('user_id'));

                if ($user['email'] != $this->input->post('email') || $user['username'] != $this->input->post('username'))
                {
                    $this->session->set_flashdata('error', 'You enter a email and username do not match with data from database.');
                    redirect(site_admin_url('user/user/request_change'), 'location');
                }
                $confirm_code = $this->_gen_code();
                $email = base64_encode($user['email']);
                $sum = $user['username']."abcxyz".$email."abcxyz".$confirm_code;
                $sum = $this->bmtd_encrypt->encrypt_url($sum);

                unset($fields, $where);
                $fields = array(
                    'confirm_code' => $confirm_code,
                    'confirm_code_status' => 'Y',
                    'confirmed_date' => date_now()
                );
                $where[] = 'id = '.get_admin_profile('user_id');

                if ($this->user->update($fields, $where))
                {                    
                
                    $message = "
                      <p>Dear : ".$user['first_name']." ".$user['last_name'].'</p>'
                    . 'You is requested to change your password.<br>'
                    . '<br>You can click link below to set your new password<br><br>'
                    . '<a href="'.site_admin_url('user/user/change_password?sum='.$sum).'">Set your password</a>'
                    . '<br><br><p>Best Regards</p>'
                    . '<p>Airafactoring Co., Ltd.</p>';

                                        

                    $subject = "You was sent request for change password.";
                    #$this->send_email->set_smtp_host()
                    #$this->send_email->
                    if ($this->send_email->send($user['email'], $subject, $message, "no-reply@Airafactoring.co.th", "Airafactoring"))
                    {
                        $this->session->set_flashdata('success', 'Please check your email and click link to change password.');
                        redirect(site_admin_url('user/user/request_change'), 'location');    
                    }
                    

                }

            }
        }


        $this->breadcrumb->make('Dashboard', site_admin_url());
        $this->breadcrumb->make('Request change password', null, true, true);
        parent::make_bread();
        $this->template->add_js_plugins('jquery.validate.js', 'admin');
        $this->template->add_js('user.js', 'admin');
        $this->template->write_view('content', 'request_change_view', $data);
        $this->template->render();
    }

    public function change_password()
    {
        $data = array();

        if ($this->input->get_post('sum') == "")
        {
            $this->session->set_flashdata('error', 'Please correct your verify information');
            redirect(site_admin_url('user/user/request_change'), 'location');
        }
        $data['sum'] = $sum = $this->input->get_post('sum');
        $sum = $this->bmtd_encrypt->decrypt_url($sum);

        list($username, $email, $confirm_code) = explode("abcxyz", $sum);
        $email = base64_decode($email);

        $user = $this->user->get_authen_user_change_password($username, $email, $confirm_code);
        if (empty($user['id']))
        {
            $this->session->set_flashdata('error', 'Please correct your verify information');
            redirect(site_admin_url('user/user/request_change'), 'location');
        }
        if ($user['confirm_code_status'] != "Y")
        {
            $this->session->set_flashdata('error', 'Your confirm code is expired.');
            redirect(site_admin_url('user/user/request_change'), 'location');
        }


        #alert($_REQUEST, 'red');
        
        if ($this->input->post('btn-save'))
        {
            
            $rules = array(            
                array('field' => 'old_password', 'label' => 'Old Password', 'rules' => 'trim|required'),
                array('field' => 'new_password', 'label' => 'New Password', 'rules' => 'trim|required|matches[confirm_new_password]'),
                array('field' => 'confirm_new_password', 'label' => 'Confirm new Password', 'rules' => 'trim|required')
    
            );
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() !== FALSE)
            {
                echo "pass validation";
                $user = $this->user->get_user_detail(get_admin_profile('user_id'));
                $current_password = $this->bmtd_encrypt->my_decrypt($user['password']);
                #alert($current_password, 'red');
                #exit;

                if ($this->input->post('old_password') != $current_password)
                {
                    $this->session->set_flashdata('error', 'You enter WRONG current password.');
                    redirect(site_admin_url('user/user/change_password?sum='.$data['sum']), 'location');
                }


                $password = $this->bmtd_encrypt->my_encrypt($this->input->post('new_password', true));

                $fields = array(
                    'password' => $password,
                    'confirm_code_status' => 'N',
                    'updated_at' => date("Y-m-d H:i:s"),
                    'updated_by' => get_admin_profile('user_id')
                );
                $where[] = 'id = '.get_admin_profile('user_id');

                if ($this->user->update($fields, $where))
                {
                    
                    unset($fields, $where);
                    $fields = array(
                        'modulable_id' => get_admin_profile('user_id'),
                        'module' => "user",
                        'full_url' => $this->input->server('REQUEST_URI'),
                        'created_at' => date_now(),
                        'user_id' => get_admin_profile('user_id'),
                        'action' => 'update',
                        'content' => json_encode($this->input->post(), true)
                    );
                    $this->user_log->create($fields);

                    $subject = "Airafactoring was sent your new password.";
                    if ($this->send_email->send($user['email'], $subject, $message))
                    {
                        
                    }
                    $this->session->set_flashdata('success', 'New password has been changed.');
                    redirect(site_admin_url('user/user/request_change'), 'location');

                }

            }
        }

        
        


        $this->breadcrumb->make('Dashboard', site_admin_url());
        $this->breadcrumb->make('Change Password', null, true, true);
        parent::make_bread();
        $this->template->add_js_plugins('jquery.validate.js', 'admin');
        $this->template->add_js('user.js', 'admin');
        $this->template->write_view('content', 'change_password_view', $data);
        $this->template->render();
    }

    public function create()
    {
        $data = array();


        if ($this->input->post('btn-save'))
        {
            $rules = array(
                array('field' => 'username', 'label' => 'Username', 'rules' => 'trim|required'),
                array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required'),
                array('field' => 'first_name', 'label' => 'Fistname', 'rules' => 'trim|required'),
                array('field' => 'last_name', 'label' => 'Lastname', 'rules' => 'trim|required'),
                array('field' => 'user_level_id', 'label' => 'User department', 'rules' => 'trim|required')
            );
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() !== FALSE)
            {
                $gen_password = $this->_gen_password();
                $password = $this->bmtd_encrypt->my_encrypt($gen_password);

                $fields = array(
                    'username' => $this->input->post('username', true),
                    'password' => $password,
                    'email' => $this->input->post('email', true),
                    'first_name' => $this->input->post("first_name", true),
                    'last_name' => $this->input->post("last_name", true),
                    'user_level_id' => $this->input->post("user_level_id", true),
                    'active_status' => $this->input->post('active_status', TRUE),
                    'created_at' => date("Y-m-d H:i:s"),
                    'created_by' => get_admin_profile('user_id')
                );



                if ($this->user->create($fields))
                {
                    $message = "
                      <p>Dear : ".$this->input->post('first_name', true)." ".$this->input->post('last_name', true).'</p>'
                    . '<p>User : '.$this->input->post('username', true).'</p>'
                    . '<p>Password : '.$gen_password.'</p>'
                    . '<br><br><p>Best Regards</p>'
                    . '<p>Airafactoring Co., Ltd.</p>';
                    $user_id = $this->user->get_last_insert_id();

                    unset($fields, $where);
                    $fields = array(
                        'modulable_id' => $user_id,
                        'module' => "user",
                        'full_url' => $this->input->server('REQUEST_URI'),
                        'created_at' => date_now(),
                        'user_id' => get_admin_profile('user_id'),
                        'action' => 'create',
                        'content' => json_encode($this->input->post(), true)
                    );
                    $this->user_log->create($fields);

                    $subject = "Your account has been created.";

                    #echo "email = ".$this->input->post('email')."<br>";
                    #echo "subject = ".$subject."<br>";
                    #echo "message = ".$message."<br>"; 

                    if ($this->send_email->send($this->input->post('email'), $subject, $message))
                    {
                       # echo "send email completed";
                    }
                    $this->session->set_flashdata('success', 'Create user successfully.');
                    redirect(site_admin_url('user/user/index'), 'location');

                }

            }
        }


        $this->breadcrumb->make('Dashboard', site_admin_url());
        $this->breadcrumb->make('User Management', site_admin_url('user/user/index'));
        $this->breadcrumb->make('Create User', null, true, true);
        parent::make_bread();

        $data['user_level'] = $this->user_level->get_active_user_level();
        $this->template->add_js('user.js', 'admin');
        $this->template->write_view('content', 'create_view', $data);
        $this->template->render();
    }


    public function update($user_id = null)
    {
        $data = array();
        $id = $user_id;
        if (empty($id))
        {
            $this->session->set_flashdata('error', 'User id required.');
            redirect(site_admin_url('user/user/index'), 'location');
        }
        //--- Check has id of user ---//

        $data['user'] = $this->user->get_user_detail($id);
        if (empty($data['user']['id']))
        {
            $this->session->set_flashdata('error', 'Not found this user_id in database.');
            redirect(site_admin_url('user/user/index'), 'location');
        }
        //-- Check existing data ---//

        if ($this->input->post('btn-save') or $this->input->post('btn-apply'))
        {
            $rules = array(
                array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required'),
                array('field' => 'first_name', 'label' => 'Fistname', 'rules' => 'trim|required'),
                array('field' => 'last_name', 'label' => 'Lastname', 'rules' => 'trim|required'),
                array('field' => 'user_level_id', 'label' => 'User department Id', 'rules' => 'trim|required')
            );

            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() !== FALSE)
            {
                $fields = array(
                    'email' => $this->input->post('email', true),
                    'first_name' => $this->input->post("first_name", true),
                    'last_name' => $this->input->post("last_name", true),
                    'user_level_id' => $this->input->post("user_level_id", true),
                    'active_status' => $this->input->post('active_status', TRUE),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'updated_by' => get_admin_profile('user_id')
                );
                $where[] = 'id = '.$id;
                $is_success = true;
                if ($this->user->update($fields, $where))
                {
                    unset($fields, $where);
                    $fields = array(
                        'modulable_id' => $id,
                        'module' => "user",
                        'full_url' => $this->input->server('REQUEST_URI'),
                        'created_at' => date_now(),
                        'user_id' => get_admin_profile('user_id'),
                        'action' => 'update',
                        'content' => json_encode($this->input->post(), true)
                    );
                    $this->user_log->create($fields);
                    $this->session->set_flashdata('success', "Update user successfully");
                    if ($this->input->post('btn-save')) 
                    {
                        redirect(site_admin_url('user/user/index'), 'location');
                    }
                    else 
                    {
                        redirect(site_admin_url('user/user/update/'.$id), 'location');   
                    }
                }
            }
            //-- End of validation ---//
        }

        //--- End of btn-save ---//

        $data['id'] = $id;

        $this->breadcrumb->make('Dashboard', site_admin_url());
        $this->breadcrumb->make('User Managment', site_admin_url('user/user/index'));
        $this->breadcrumb->make('Update User', null, true, true);
        parent::make_bread();
        $data['user_level'] = $this->user_level->get_active_user_level();
        $data['user_id'] = $id;
        $this->template->add_js('user.js', 'admin');
        $this->template->write_view('content', 'update_view', $data);
        $this->template->render();
    }

    public function delete()
    {
        $data = array();
        $id = $this->input->get_post('id');
        if (empty($id))
        {
            $this->session->set_flashdata('error', 'Id required');
            redirect(site_admin_url('user/user/index'), 'location');
        }

        $data['user'] = $this->user->get_user_detail($id);
        if (empty($data['user']['id']))
        {
            $this->session->set_flashdata('error', 'Not found this user_id in database.');
            redirect(site_admin_url('user/user/index'), 'location');
        }


        $where[] = 'id = '.$id;




        if ($this->user->delete($where))
        {
            unset($fields, $where);
                    $fields = array(
                        'modulable_id' => $id,
                        'module' => "user",
                        'full_url' => $this->input->server('REQUEST_URI'),
                        'created_at' => date_now(),
                        'user_id' => get_admin_profile('user_id'),
                        'action' => 'delete',
                        'content' => json_encode($this->input->get(), true)
                    );
                    $this->user_log->create($fields);
            $this->session->set_flashdata('success', 'Delete user successfully.');
            redirect(site_admin_url('user/user/index'), 'location');

        }
        else
        {
            $this->session->set_flashdata('error', 'Cannot delete user');
            redirect(site_admin_url('user/user/index'), 'location');
        }





    }

    private function _gen_code()
    {
        $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $length = rand(6, 10);
        $start = rand(0, 20);
        $str = substr(str_shuffle($str), $start, $length);
        return $str;
    }

    private function _gen_password()
    {
        $str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $length = rand(6, 10);
        $start = rand(0, 20);
        $str = substr(str_shuffle($str), $start, $length);
        return $str;
    }

    public function reset_password($user_id = null)
    {
        if (empty($user_id))
        {
            $this->session->set_flashdata('error', 'User id is required.');
            redirect(site_admin_url('user/user/index'), 'location');
        }
        $user = $this->user->get_user_detail($user_id);
        if (empty($user['id']))
        {
            $this->session->set_flashdata('error', 'Usre id not found');
            redirect(site_admin_url('user/user/index'), 'location');
        }

        $password = $this->_gen_password();

        $fields = array(
            'password' => $this->bmtd_encrypt->my_encrypt($password)
        );
        $where[] = 'id = '.$user_id; 
        if ($this->user->update($fields, $where))
        {

            $message = "<p>Dear : ".$user['first_name']." ".$user['last_name']."</p>"
                . '<p>User : '.$user['username'].'</p>'
                . '<p>Password : '.$password.'</p>'
                . '<br><br><p>Best Regards</p>'
                . '<p>Airafactoring Co., Ltd.</p>';
              
            $subject = "Your password has been reset.";

            if ($this->send_email->send($user['email'], $subject, $message, "no-reply@airafactoring.co.th", "Airafactoring"))
            {
                $this->session->set_flashdata('success', 'Reset and sent password to  user successfully');
                redirect(site_admin_url('user/user/index'), 'location');
            }
            else 
            {
                $this->session->set_flashdata('success', 'Reset password to  user successfully but cannot send email');
                redirect(site_admin_url('user/user/index'), 'location');   
            }
        }
        else 
        {
            $this->session->set_flashdata('error', 'Cannot rest password');
            redirect(site_admin_url('user/user/index'), 'location');
        }


        
    }

    public function check_duplicate_username()
    {
        $username = $this->user->is_exist_username($this->input->post('username'));
        if ($username > 0)
        {
            echo "false";
        }
        else
        {
            echo "true";
        }
    }

    public function check_duplicate_email()
    {

    }




}