<?php
class User_model extends MY_Model {

    private $_table = "user";

    public function __construct()
    {
        parent::__construct();
    }

    public function get_authen_user_change_password($username = null, $email = null, $confirm_code = null)
    {
        if (empty($username))
        {
            return null; 
        }
        if (empty($email))
        {
            return null; 
        }
        if (empty($confirm_code))
        {
            return null; 
        }
        $select = parent::select();
        $select->from(array('U' => 'user'), array('U.id', 'U.confirm_code_status', 'U.password'))
            ->where('U.username = ?', $username)
            ->where('U.email = ?', $email)
            ->where('U.confirm_code = ?', $confirm_code)
            ->where('U.active_status = ?', 'Y');
        return parent::fetch_row($select);

    }

    public function get_all_user()
    {
        $select = parent::select();
        $select->from(array('U' => 'user'), array(
            'U.id',
            'U.username', 'U.email', 'U.first_name', 'U.last_name',
            'U.active_status', 'U.created_at', 'U.updated_at'
        ))
            ->join(array('UL' => 'user_level'), 'U.user_level_id = UL.id', array('UL.name AS level_name'))
            ->where('U.deleted_at IS NULL')
            ->where('U.active_status = ?' , 'Y')
            ->order('U.id desc');
        return parent::fetch_all($select);
    }

    public function get_user($args = array(), $page = 1, $row_per_page = 20)
    {
        $select = parent::select();
        $select->from(array('U' => 'user'), array(
            'U.id',
            'U.username', 'U.email', 'U.first_name', 'U.last_name',
            'U.active_status', 'U.created_at', 'U.updated_at'
        ))
            ->join(array('UL' => 'user_level'), 'U.user_level_id = UL.id', array('UL.id as user_level_id', 'UL.name AS level_name'))
            ->where('U.deleted_at IS NULL');

        $select->order('U.id desc');

        if ( ! empty($args['username']))
        {
            $select->where('U.username = ?', $args['username']);
        }
        if ( ! empty($args['email']))
        {
            $select->where('U.email = ?', $args['email']);
        }
        if ( ! empty($args['user_level_id']))
        {
            $select->where('UL.id = ?', $args['user_level_id']);
        }
        if ( ! empty($args['first_name']))
        {
            $select->where('U.first_name LIKE ?', '%'.$args['first_name'].'%');
        }
        if ( ! empty($args['last_name']))
        {
            $select->where('U.last_name LIKE ?', '%'.$args['last_name'].'%');
        }
        $data = parent::fetch_page($select, array(), $page, $row_per_page);
        return $data;
    }

    public function get_user_detail($id = NULL, $condition = 'id')
    {
        if (is_null($id) OR empty($id))
        {
            return NULL;
        }
        $select = parent::select();
        $select->from(array('U' => 'user'), array('U.id', 'U.password', 'U.user_level_id', 'U.active_status', 'U.first_name', 'U.last_name', 'U.email', 'U.username'))
            ->join(array('UL' => 'user_level'), 'U.user_level_id = UL.id', array('UL.name'));

        if ($condition == "id")
        {
            $select->where('U.id = ?', $id);
        }
        elseif ($condition == "username")
        {
            $select->where('U.username = ?', $id);
        }

        return parent::fetch_row($select);
    }

    public function create($params = NULL)
    {
        if (is_null($params) OR count($params) == 0)
        {
            return NULL;
        }

        if (parent::insert($this->_table, $params) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function update($params = NULL, $where = NULL)
    {
        if (is_null($params) OR count($params) == 0)
        {
            return NULL;
        }

        if (is_null($where) OR count($where) == 0)
        {
            return NULL;
        }

        if (parent::update($this->_table, $params, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($where = NULL)
    {
        if (is_null($where)OR count($where) == 0)
        {
            return NULL;
        }
        if (parent::delete($this->_table, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function is_exist_username($username = null)
    {
        if (empty($username))
        {
            return null;
        }
        $select = parent::select();

        $select->from(array('U' => 'user'), array('count(*)'))
            ->where('U.username = ?', $username)
            ->where('U.active_status = ?', 'Y');

        $data = parent::fetch_one($select);
        if ($data > 0)
        {
            return true;
        }
        return false;
    }
    public function can_delete_department($user_level_id = null)
    {
        $select = parent::select();
        $select->from(array('U' => 'user'), array('count(*)'))
            ->where('U.user_level_id = ?', $user_level_id);

        $query = parent::fetch_one($select);
        if ($query > 0)
            return false; 
        else 
            return true;
    }





}