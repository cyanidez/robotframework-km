<?php
class user_level_model extends MY_Model {

    private $_table = "user_level";

    public function __construct()
    {
        parent::__construct();
    }
    public function get_active_user_level()
    {
        $select = parent::select();
        $select->from(array('UD' => 'user_level'), array(
            'UD.id',
            'UD.name', 'UD.can_delete', 'UD.status_flg'
        ))
            ->order('UD.id desc');
        return parent::fetch_all($select);
    }

    public function get_all_user_level()
    {
        $select = parent::select();
        $select->from(array('UD' => 'user_level'), array('*'))
            ->where('UD.status_flg = ?' ,'Y');
        return parent::fetch_all($select);
    }
    public function get_user_level($args = array(), $page = 1, $row_per_page = 20)
    {
        $select = parent::select();
        $select->from(array('UD' => 'user_level'), array(
            'UD.id as user_level_id',
            'UD.name', 'UD.can_delete', 'UD.status_flg'
        ));

        $select->order('UD.id desc');

        if ( ! empty($args['name']))
        {
            $select->where('UD.name = ?', $args['username']);
        }
    
        if ( ! empty($args['status_flg']))
        {
            $select->where('UD.status_flg = ?', $args['status_flg']);
        }
        $data = parent::fetch_page($select, array(), $page, $row_per_page);
        return $data;
    }

    public function get_user_level_detail($id = NULL, $condition = 'id')
    {
        if (is_null($id) OR empty($id))
        {
            return NULL;
        }
        $select = parent::select();
        $select->from(array('UD' => 'user_level'), array('UD.id', 'UD.name', 'UD.status_flg'));

        if ($condition == "id")
        {
            $select->where('UD.id = ?', $id);
        }


        return parent::fetch_row($select);
    }

    public function create($params = NULL)
    {
        if (is_null($params) OR count($params) == 0)
        {
            return NULL;
        }

        if (parent::insert($this->_table, $params) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function update($params = NULL, $where = NULL)
    {
        if (empty($params))
        {
            return null;
        }

        if (empty($where))
        {
            return null;
        }

        if (parent::update($this->_table, $params, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($where = NULL)
    {
        if (empty($where))
        {
            return NULL;
        }
        if (parent::delete($this->_table, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function user_is_in_department($id = null)
    {
        if (empty($id))
        {
            return true;
        }
        $select = parent::select();

        $select->from(array('U' => 'user'), array('count(*)'))
            ->where('U.user_level_id = ?', $id)
            ->where('U.active_status = ?', 'Y');

        $data = parent::fetch_one($select);
        if ($data > 0)
        {
            return true;
        }
        return false;
    }






}