<?php
class Authentication_model extends MY_Model { 
	private $_table = "authentications";
	function __construct() 
	{
		parent::__construct(); 
	}

	public function get_category_active_authentication($user_level_id = null, $parent = -1)
	{
		if (empty($user_level_id))
		{
			return null;
		}
		$select = parent::select();
		$select->from(array('C' => 'categories'), array('C.id', 'C.level', 'C.slug', 'C.parent'))
			->join(array('CL' => 'category_lang'), 'C.id = CL.category_id AND CL.lang_id = '.DEFAULT_LANG_ID, array("CL.name"))
			->joinLeft(array('A' => 'authentications'), 'A.menu_id = C.id AND A.menu_type = "category" AND A.user_level_id = '.$user_level_id, array('A.menu_id'))
			->where('C.active_status = ?', 'Y')
			->where('C.parent = ?', $parent)
			->order('C.id ASC');
		
		$query =  parent::fetch_all($select);	
		if ( ! empty($query))
		{
			foreach ($query as $key => $value)
			{
				
				$current[] = array(
					'id' => $value['id'],
					'name' => $value['name'],
					'slug' => $value['slug'],							
					'level' => $value['level'],
					'parent' => $value['parent'],
					'menu_id' => $value['menu_id'],
					'child' => $this->get_category_active_authentication($user_level_id, $value['id'])
				);
			
			}

			return $current;
		}
	}

	public function is_has_authen($user_level_id = null, $menu_id = null, $menu_type = 'menu')
	{
		if (empty($user_level_id))
		{
			return null;
		}
		if (empty($menu_id))
		{
			return null;
		}
		$select = parent::select();
		$select->from(array('A' => 'authentications'), array('count(*)'))
			->where('A.user_level_id = ?', $user_level_id)
			->where('A.menu_id = ?', $menu_id)
			->where('A.menu_type = ?', $menu_type);

		$query = parent::fetch_one($select);
		if ($query > 0)
		{
			return true; 
		}
		else 
		{
			return false; 
		}
	}

	function get_authentication($user_level_id = NULL) 
	{
		if (is_null($user_level_id) OR empty($user_level_id)) 
		{
			return NULL; 
		}

		$select = parent::select(); 

		$select->from(array('M' => 'menus'), array('M.id', 'M.level', 'M.menu_name'))
					->join(array('A' => 'authentications'), 'M.id = A.menu_id AND A.menu_type = "menu" AND A.user_level_id = '.$user_level_id, array()); 
		
		$query = parent::fetch_all($select); 
		$out = array();
		foreach ($query as $key => $value) 
		{
			$out[] = $value['id']; 
		}

		return $out; 
	}

	public function create($params = NULL) 
	{
		if (empty($params))
		{
			return NULL; 
		}
		
		if (parent::insert($this->_table, $params) !== FALSE) 
		{
			return TRUE; 
		}
		return FALSE; 
	}

	public function update($params = NULL, $where = NULL)  
	{
		if (empty($params))
		{
			return NULL; 
		}
		
		if (parent::update($this->_table, $params, $where) !== FALSE)
		{
			return TRUE; 
		}
		return FALSE; 
	}

	public function delete($where = NULL) 
	{
		if (empty($where))
		{
			return NULL; 
		}
		if (parent::delete($this->_table, $where) !== FALSE)
		{
			return TRUE; 
		}
		return FALSE; 
	}

	function clear($user_level_id = NULL) 
	{
		if (is_null($user_level_id) OR empty($user_level_id)) 
		{
			return NULL; 
		}

		$where[] = 'user_level_id = '.$user_level_id; 
		if (parent::delete($this->_table, $where) !== FALSE) 
		{
			return TRUE; 
		}
		return FALSE; 
	}

	//---
	public function get_all_menu() 
	{
		$select = parent::select(); 
		$select->from(array('M' => 'menus'), array('M.id', 'M.menu_name', 'M.parent', 'M.level', 'M.module', 'M.controller', 'M.method', 'M.status_flg', 'M.menu_path'))
					->where('M.level = ?', 1)
					->where('M.status_flg = ?', 'Y'); 

		$query = parent::fetch_all($select); 

		$out = array(); 
		$i = 0;
		foreach ($query as $key => $value) 
		{
			$child = $this->_get_child($value['id']); 
			$out[$i]['id'] = $value['id']; 
			$out[$i]['parent'] = $value['parent']; 
			$out[$i]['level'] = $value['level']; 
			$out[$i]['module'] = $value['module']; 
			$out[$i]['controller'] = $value['controller']; 
			$out[$i]['method'] = $value['method']; 
			$out[$i]['menu_path'] = $value['menu_path']; 
			$out[$i]['menu_name'] = $value['menu_name']; 

			$j = 0;
			$out2 = array(); 
			foreach ($child as $key2 => $value2) 
			{
				$out2[$j]['id'] = $value2['id']; 
				$out2[$j]['parent'] = $value2['parent']; 
				$out2[$j]['level'] = $value2['level']; 
				$out2[$j]['module'] = $value2['module']; 
				$out2[$j]['controller'] = $value2['controller']; 
				$out2[$j]['method'] = $value2['method']; 
				$out2[$j]['menu_path'] = $value2['menu_path']; 
				$out2[$j]['menu_name'] = $value2['menu_name']; 

				$j++;
			}
			$out[$i]['child'] = $out2; 
			$i++; 
		}

		return $out; 

	}

	private function _get_child($parent = NULL) 
	{
		if (is_null($parent) OR empty($parent)) 
		{
			return NULL; 
		}

		$select = parent::select(); 
		$select->from(array('M' => 'menus'), array('M.id', 'M.menu_name', 'M.status_flg', 'M.parent', 'M.controller', 'M.module', 'M.method', 'M.level', 'M.menu_path'))
					->where('M.parent = ?',  $parent)
					->where('M.status_flg = ?', 'Y')
					->order('M.id ASC'); 

		return parent::fetch_all($select); 
	}
}