<?php
class User_level_model extends MY_Model { 
	function __construct() 
	{
		parent::__construct(); 
	}

	function get_user_level($params = NULL, $start = NULL, $row_per_page = NULL) 
	{
		$select = parent::select();

		$select->from(array('UL' => 'user_level'), array('UL.user_levelID', 'UL.level_name', 'UL.status_flg')); 

		if (isset($params['level_name']) && ! empty($params['level_name']))
		{
			$select->where('UL.level_name = ?', $params['level_name']); 
		}		

		if (isset($params['status_flg']) && ! empty($params['status_flg']))
		{
			$select->where('UL.status_flg = ?', $params['status_flg']); 
		}

		if ( ( ! is_null($start) OR $start != "") && ( ! is_null($row_per_page) OR $row_per_page != "") ) 
		{
			return parent::fetch_page($select, array(), $start, $row_per_page); 	
		}
		else
		{
			return parent::fetch_all($select); 
		}	
	}

	function get_user_level_detail($user_levelID = NULL) 
	{
		if (is_null($user_levelID) OR empty($user_levelID)) 
		{
			return NULL; 
		}

		$select = parent::select(); 
		$select->from(array('UL' => 'user_level'), array('UL.user_levelID', 'UL.level_name', 'UL.status_flg'))
					->where('user_levelID = ?', $user_levelID); 

		return parent::fetch_row($select); 
	}

	function create($params = NULL) 
	{
		if (is_null($params) OR count($params) == 0) 
		{
			return NULL; 
		}
		return parent::insert('user_level', $params); 
	}

	function update($params = NULL, $where = NULL)  
	{
		if (is_null($params) OR count($params) == 0) 
		{
			return NULL; 
		}
		if (is_null($where) OR count($where) == 0) 
		{
			return NULL; 
		}
		
		if (parent::update('user_level', $params, $where) !== FALSE)
		{
			return TRUE; 
		}
		return FALSE; 
	}

	function delete($where = NULL) 
	{
		if (is_null($where) OR count($where) == 0) 
		{
			return NULL; 
		}
		if (parent::delete('user_level', $where) !== FALSE)
		{
			return TRUE; 
		}
		return FALSE; 
	}

}
