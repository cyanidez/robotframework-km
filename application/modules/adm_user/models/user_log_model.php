<?php
class User_log_model extends MY_Model { 
	private $_table = "user_logs";

	function __construct() 
	{
		parent::__construct(); 
	}

	public function get_user_log($args, $page = 1, $per_page = 20)
	{
		#alert($args, 'red');
		#exit;
		$select = parent::select();
		$select->from(array('UL' => 'user_logs'), array('UL.id', 'UL.modulable_id', 'UL.module', 'UL.full_url', 'UL.content', 'UL.user_id', 'UL.action', 'UL.created_at'))
			->join(array('U' => 'user'), 'U.id = UL.user_id', array('U.username', 'U.first_name', 'U.last_name'));

		if ( ! empty($args['module']))
		{
			$select->where('UL.module IN (?)', $args['module']);
		}
		if ( ! empty($args['action']))
		{
			$select->where('UL.action IN (?)', $args['action']);
		}
		if ( ! empty($args['user_id']))
		{
			$select->where('UL.user_id = ?', $args['user_id']);
		}
		if ( ! empty($args['created_at']))
		{
			$select->where('UL.created_at = ?', $args['created_at']);
		}
		#echo $select->__toString();
		#exit;
		$select->order('UL.id desc');
		return parent::fetch_page($select, array(), $page, $per_page);


	}
	public function get_user_log_detail($id = null)
	{
		if (empty($id))
		{
			return null; 
		}
		$select = parent::select();
		$select->from(array('UL' => 'user_logs'), array('*'))
			->join(array('U' => 'user'), 'UL.user_id = U.id', array('U.first_name', 'U.last_name', 'U.username'))
			->where('UL.id = ?', $id);
		return parent::fetch_row($select);
	}

	public function create($params = NULL) 
	{
		if (empty($params))
		{
			return NULL; 
		}
		
		if (parent::insert($this->_table, $params)) 
		{
			return TRUE; 
		}
		return FALSE; 
	}

	public function update($params = NULL, $where = NULL)  
	{
		if (empty($params))
		{
			return null; 
		}
		if (empty($where))
		{
			return null; 
		}
		if (parent::update($this->_table, $params, $where) !== FALSE) 
		{
			return TRUE; 
		}
		return FALSE; 
	}

	public function delete($where = NULL) 
	{
		if (empty($where))
		{
			return NULL; 
		}
		if (parent::delete($this->_table, $where) !== FALSE)
		{
			return TRUE; 
		}
		return FALSE; 
	}

	
}

/* End of file category_model.php */
/* Location: ./application/modules/adm_category/models/category_model.php  */