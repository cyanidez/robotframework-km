<div class="page-header">
    <h3>Create User</h3>
</div>
<form class="form-horizontal" name="create_user_form" id="create_user_form" method="post" action="<?php echo site_admin_url('user/user/create'); ?>" data-action="<?php echo site_admin_url('user/user/index'); ?>" enctype="multipart/form-data">

    <div class="form-group">
        <label for="detail" class="col-sm-2 control-label">User Department</label>
        <div class="col-sm-10">


            <select name="user_level_id" id="user_level_id" class="form-control" title="Pleas select department (Level)">
                <option value="">==== Select Department ====</option>
            <?php if ( ! empty($user_level)) : ?>
                <?php foreach ($user_level as $key => $value) : ?>
                <option value="<?php echo $value['id']; ?>"><?php echo $value['name']; ?></option>
                <?php endforeach; ?>
            <?php endif; ?>

            </select>
            <?php echo form_error('user_level_id'); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="detail" class="col-sm-2 control-label">Username</label>
        <div class="col-sm-10">
            <input type="text" name="username" id="username" value="" class="form-control">
            <?php echo form_error('username'); ?>
            <?php if ( ! empty($username_error)) : ?>
            <label class="error"><?php echo $username_error; ?></label>
            <?php endif; ?>
        </div>
    </div>

    <div class="form-group">
        <label for="detail" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="text" name="email" id="email" value="<?php echo set_value('email'); ?>" title="Please enter email" class="form-control">
            <?php echo form_error('email'); ?>
        </div>
    </div>


    <div class="form-group">
        <label class="col-sm-2 control-label">First name</label>
        <div class="col-sm-10">
            <input type="text" name="first_name" id="first_name" value="<?php echo set_value('first_name'); ?>" class="form-control">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Last Name</label>
        <div class="col-sm-10">
            <input type="text" name="last_name" id="last_name" value="<?php echo set_value('last_name'); ?>" class="form-control">
        </div>
    </div>


    <div class="form-group">
        <label for="active_status" class="col-sm-2 control-label">Active status</label>
        <div class="col-sm-10">
            <input type="radio" name="active_status" id="enable" value="Y" <?php echo (set_value('active_status') == "" OR set_value('active_status') == "Y") ? 'checked="checked"' : ""; ?>> Enable &nbsp;
            <input type="radio" name="active_status" id="disable" value="N" <?php echo (set_value('active_status') == "N") ? 'checked="checked"' : ""; ?>> Disable

        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <input class="btn btn-info" type="submit" name="btn-save" id="btn-save" value="Save">
            <input class="btn btn-info" type="submit" name="btn-apply" id="btn-apply" value="Apply">
            <input data-href="<?php echo site_admin_url('user/user/index'); ?>" class="btn btn-default btn-cancel" type="button" name="btn-cancel" id="btn-cancel" value="Cancel">
        </div>
    </div>
</form>