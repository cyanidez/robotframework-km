<div class="page-header">
    <h3>Create Department</h3>
</div>
<form class="form-horizontal" name="create_user_level_form" id="create_user_level_form" method="post" action="<?php echo site_admin_url('user/user/create'); ?>" enctype="multipart/form-data">

    <div class="form-group">
        <label for="detail" class="col-sm-2 control-label">Level</label>
        <div class="col-sm-10">
            <input type="text" name="name" id="name" class="form-control" value="<?php echo set_value('name'); ?>">

            <?php echo form_error('name'); ?>

        </div>
    </div>
    <div class="form-group">
        <label for="active_status" class="col-sm-2 control-label">Active status</label>
        <div class="col-sm-10">
            <input type="radio" name="active_status" id="enable" value="Y" <?php echo (set_value('active_status') == "" OR set_value('active_status') == "Y") ? 'checked="checked"' : ""; ?>> Enable &nbsp;
            <input type="radio" name="active_status" id="disable" value="N" <?php echo (set_value('active_status') == "N") ? 'checked="checked"' : ""; ?>> Disable

        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <input class="btn btn-info" type="submit" name="btn-save" value="Save">
            <input class="btn btn-default cancel" data-href="<?php echo site_admin_url('user/user/index'); ?>" type="button" value="cancel">
        </div>
    </div>
</form>