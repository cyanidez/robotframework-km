<?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?>
<?php if ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php endif; ?>
<div class="page-header">
    <h3>User Management</h3>
</div>

<table>
    <tbody>
    <tr>
        <td>
            <a class="btn btn-info" href="<?php echo site_admin_url('user/user/create'); ?>">Create user</a>
        </td>
    </tr>
    </tbody>
</table>




<table class="table">
    <thead>
    <tr>
        <th>Username</th>
        <th>Level</th>
        <th>Full name</th>
        <th>Active</th>
        <th>Method</th>
    </tr>
    </thead>
    <tfoot>
        <tr>
            <td colspan="8"><?php echo bootstrap_pagination($user, site_admin_url('user/user/index'), $query); ?></td>

        </tr>
    </tfoot>
    <tbody>
    <?php if ($user['row_count'] > 0) : ?>
        <?php foreach ($user['rows'] as $key => $value) : ?>
            <tr>
                <td>
                    <?php echo ($value['username']); ?><br>
                    (<?php echo $value['email']; ?>)
                </td>
                <td><?php echo $value['level_name']; ?></td>
                <td><?php echo $value['first_name']; ?> <?php echo $value['last_name']; ?></td>
                <td>
                    <?php echo lang($value['active_status']); ?>
                </td>
                <td>
                    <a href="<?php echo site_admin_url('user/user/update/'.$value['id']); ?>" class="btn btn-info">Edit</a>
                    <a href="#" data-href="<?php echo site_admin_url('user/user/delete?id='.$value['id']); ?>" class="btn btn-danger btn-delete">Delete</a>
                    <?php if (in_array(get_admin_profile('user_level_id'), array(1, 2))) : ?>
                    <a class="btn btn-info" href="<?php echo site_admin_url('user/user/reset_password/'.$value['id']); ?>">Reset Password</a>
                    <?php endif; ?>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else : ?>
        <tr>
            <td colspan="5">Not found data yet.</td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>