
<div class="dialog">
    <?php if ($this->session->flashdata('success')) : ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
    <?php endif; ?>

    <?php if ($this->session->flashdata('error')) : ?>
        <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
    <?php endif; ?>
    <div class="panel panel-default">
        <p class="panel-heading no-collapse">FORGOT YOUR PASSWORD</p>
        <div class="panel-body">
            <form  name="forgot_password_form" id="forgot_password_form" method="post" action="<?php echo site_admin_url('user/forgot_password'); ?>">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" id="username" class="form-controlspan12 form-control">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" name="email" id="email" class="form-controlspan12 form-control">
                </div>
                <input type="submit" class="btn btn-info" value="Reset password" id="btn-save" name="btn-save">

                <div class="clearfix"></div>
            </form>
        </div>
    </div> <a href="<?php echo site_admin_url(); ?>">&raquo; Back to login page</a>
    <p style="font-size: .75em; margin-top: .25em;">Developed & License by : <img src="http://www.aseanwebdesign.com/images/awd_icon.png" width="11" height="11" style="margin-bottom:.20em; vertical-align:middle;"> <a href="http://www.aseanwebdesign.com" target="_blank">AseanWebDesign™</a>
</div>


