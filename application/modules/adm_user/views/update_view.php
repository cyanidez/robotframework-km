<div class="page-header">
    <h3>Update User</h3>
</div>

<?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php endif; ?>


<form class="form-horizontal" enctype="multipart/form-data" name="update_user_form" id="update_user_form" method="post" action="<?php echo site_admin_url('user/user/update/'.$id); ?>">
    <div class="form-group">
        <label class="col-sm-2 control-label">User Department</label>
        <div class="col-sm-10">
            <select id="user_level_id" name="user_level_id" class="form-control">
                <option value="">==== Select Department ====</option>
                <?php foreach ($user_level as $key => $value) : ?>
                <option value="<?php echo $value['id']; ?>" <?php echo ($user['user_level_id'] == $value['id']) ? 'selected="selected"' : ""; ?>><?php echo $value['name']; ?></option>
                <?php endforeach; ?>
            </select>
            <?php echo form_error('user_level_id'); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="user" class="col-sm-2 control-label">First name</label>
        <div class="col-sm-10">

            <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo $user['first_name']; ?>">
            <?php echo form_error('first_name'); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Last name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo $user['last_name']; ?>">
            <?php echo form_error('last_name'); ?>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="email" id="email" value="<?php echo $user['email']; ?>">
            <?php echo form_error('email'); ?>
        </div>
    </div>


    <div class="form-group">
        <label for="active_status" class="col-sm-2 control-label">Active status</label>
        <div class="col-sm-10">
            <input type="radio" name="active_status" id="active" value="Y" <?php echo ($user['active_status'] == "Y") ? 'checked="checked"' : ""; ?>> Enable &nbsp;
            <input type="radio" name="active_status" id="inactive" value="N" <?php echo ($user['active_status'] == "N") ? 'checked="checked"' : ""; ?>> Disable
            <?php echo form_error('active_status'); ?>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <input class="btn btn-info" type="submit" name="btn-save" id="btn-save" value="Save">
            <input class="btn btn-info" type="submit" name="btn-apply" id="btn-apply" value="Apply">
            <input data-href="<?php echo site_admin_url('user/user/index'); ?>" class="btn btn-default btn-cancel" type="button" name="btn-cancel" id="btn-cancel" value="Cancel">

        </div>
    </div>
</form>