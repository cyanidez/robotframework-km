<div class="page-header">
    <h3>Change password</h3>
</div>

<?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?>

<?php if ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php endif; ?>


<form 
    class="form-horizontal" 
    name="change_password_form"
    id="change_password_form" 
    method="post"
    action="<?php echo site_admin_url('user/user/change_password?sum='.$sum); ?>">
    <div class="form-group">
        <label class="col-sm-2 control-label">Current Password</label>
        <div class="col-sm-10">
            <input type="password" id="old_password" name="old_password" class="form-control">
            <?php echo form_error('old_password'); ?>
        </div>
    </div>

    <div class="form-group">
        <label for="user" class="col-sm-2 control-label">New Password</label>
        <div class="col-sm-10">

            <input type="password" id="new_password" name="new_password" class="form-control">
            <?php echo form_error('new_password'); ?>
        </div>
    </div>
    <div class="form-group">
        <label for="user" class="col-sm-2 control-label">Confirm New Password</label>
        <div class="col-sm-10">

            <input type="password" id="confirm_new_password" name="confirm_new_password" class="form-control">
            <?php echo form_error('confirm_new_password'); ?>
        </div>
    </div>

    

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <input class="btn btn-info" type="submit" name="btn-save" id="btn-save" value="Change Password">

        </div>
    </div>
</form>