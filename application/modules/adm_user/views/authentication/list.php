
<div class="page-header">
	<h3>User Department Privilege Management</h3>  
</div>
<div class="button">
	<a href="<?php echo site_admin_url('user/authentication/create'); ?>" class="btn btn-info">Create User Department</a>
</div>

<?php if ($this->session->flashdata('success')) : ?>
<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php endif; ?>
<?php if ($this->session->flashdata('error')) : ?>
<div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
<?php endif; ?>

<form class="list" action="" method="post" >
	<table class="table">
		<thead>
			<tr>			
				<th>Department Name</th>
				<th>Status</th>
				<th>Method</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td colspan="3">
					Total Items : <?php echo count($user_level['rows']); ?> <?php echo lang('rows'); ?>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<?php echo bootstrap_pagination($user_level, site_admin_url('user/authentication')) ?>
				</td>
			</tr>
			
			
		</tfoot>
		<tbody>
			<?php if ($user_level['row_count'] > 0) : ?>
			<?php foreach ($user_level['rows'] as $key => $value) : ?>
			<tr>
				<td><?php echo $value['name']; ?></td>
				<td><?php echo lang($value['status_flg']); ?></td>
				<td>
					
					
					<?php if ($value['can_delete'] == "Y") : ?>
					<a class="btn btn-info" href="<?php echo site_admin_url('user/authentication/update?user_level_id='.$value['user_level_id']); ?>" title="<?php echo lang('edit'); ?>"><?php echo lang('edit'); ?></a>
					<a class="btn btn-danger btn-delete" href="#" data-href="<?php echo site_admin_url('user/authentication/delete?user_level_id='.$value['user_level_id']); ?>" title=""><?php echo lang('delete'); ?></a>
					<?php endif; ?>
				</td>
			</tr>
			 <?php endforeach; ?>
			 <?php else : ?>
			 <tr>
				<td colspan="4">No data yet.</td>
			 </tr>
			 <?php endif; ?>
		</tbody>
	</table>
</form>