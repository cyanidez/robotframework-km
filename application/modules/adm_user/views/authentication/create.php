<div class="page-header">
	<h3>Create User Department Privilege</h3>
</div>
<?php if ($this->session->flashdata('success')) : ?>
<div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
<?php endif; ?>
<?php if ($this->session->flashdata('error')) : ?>
<div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
<?php endif; ?>
<form id="create_user_level_form" name="create_user_level_form" method="post" action="<?php echo site_admin_url('user/authentication/create'); ?>" class="form-horizontal">
	<div class="form-group">
		<label class="control-label col-sm-2">ชื่อแผนก</label>
		<div class="col-sm-10">
			<input type="text" name="name" id="name" class="form-control" title="Please enter department name" class="required" />
			<?php echo form_error('level_name'); ?>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label col-sm-2"><?php echo lang('status'); ?></label>
		<div class="col-sm-10">
			<select name="status_flg" id="status_flg" class="form-control">
				<option value="Y" <?php echo (set_value('status_flg') == "Y") ? 'selected="selected"' : ""; ?>><?php echo lang('enable'); ?></option>
				<option value="N" <?php echo (set_value('status_flg') == "N") ? 'selected="selected"' : ""; ?>><?php echo lang('disable'); ?></option>
			</select>
			<?php echo form_error('status_flg'); ?>
		</div>
	</div>

	<div class="form-group">
		<label class="control-label col-sm-2">สิทธิ์เข้าใช้งานเมนู</label>
		<div class="col-sm-10">
			<?php /***
			<ul id="main" class="list-unstyled">
				<?php foreach ($menu as $key => $value) : ?>
				<li class="main">
					<span>-<input type="checkbox" name="menuID[]" id="menuID<?php echo $value['id']; ?>" value="<?php echo $value['id']; ?>" /> <?php echo $value['menu_name']; ?></span>
					<?php if (count($value['child']) > 0) : ?>
					<ul class="sub">
						<?php foreach ($value['child'] as $key2 => $value2) : ?>
						<li class="sub">
							-----<input type="checkbox" name="sub_menuID[<?php echo $value['id']; ?>][<?php echo $key2; ?>]" id="sub_menuID<?php echo $value['id'].'-'.$value2['id']; ?>" value="<?php echo $value2['id']; ?>" />
							<?php echo $value2['menu_name']; ?>
						</li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
				</li>
				<?php endforeach; ?>
			<?php if ( ! empty($menu_category)) : ?>
				<?php foreach ($menu_category as $key => $value) : ?>
				<li>
					<span>- <input type="checkbox" name="category_id[]" id="category_id=<?php echo $key; ?>" value="<?php echo $value['id']; ?>">
					<?php echo $value['name']; ?>
				</li>
				<?php endforeach; ?>
			<?php endif; ?>
			</ul>
			**/ ?>
			<ul id="main" class="list-unstyled">
				<li class="main">
					Main Menu
				</li>
				<?php foreach ($menu as $key => $value) : ?>
				<li class="main">
					<span>
						<input 
							type="checkbox" name="menuID[]" 
							id="menuID<?php echo $value['id']; ?>" 
							value="<?php echo $value['id']; ?>">
						&nbsp; <?php echo $value['menu_name']; ?>
					</span>
					<?php if (count($value['child']) > 0) : ?>
					<ul class="list-unstyled">
						<?php foreach ($value['child'] as $key2 => $value2) : ?>
						<li class="sub <?php echo $value2['id']; ?>">
							<div class="col-sm-offset-1 col-sm-12">
								<input 
									type="checkbox" 
									name="sub_menuID[<?php echo $value['id']; ?>][<?php echo $key2; ?>]" 
									id="sub_menuID<?php echo $value['id'].'-'.$value2['id']; ?>" 
									value="<?php echo $value2['id']; ?>">
								&nbsp; <?php echo $value2['menu_name']; ?>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
				</li>
				<?php endforeach; ?>
				<li class="main">
					<hr>
				</li>
				<li class="main">
					Catgory menu
				</li>
				<?php #alert($authentication_category, 'red'); ?>
				<?php if ( ! empty($authentication_category)) : ?>
				<?php foreach ($authentication_category as $key => $value) : ?>
				<li class="main">
					<span>
						<input <?php echo ( ! empty($value['menu_id'])) ? 'checked="checked"' : ""; ?> 
							type="checkbox" 
							name="category_id[]" 
							id="category_id=<?php echo $key; ?>" 
							value="<?php echo $value['id']; ?>">
						&nbsp; <?php echo $value['name']; ?>
					</span>
					<?php if (count($value['child']) > 0) : ?>
					<ul class="list-unstyled">
						<?php foreach ($value['child'] as $c_key => $c_value) : ?>
						<?php $class['authen_category'] = (in_array($c_value['id'], $authentication_category)) ? 'auth_highlight' : ""; ?>
						<li class="sub <?php echo $class['authen_category']; ?>">
							<div class="col-sm-offset-1 col-sm-12">
								<input 
									type="checkbox" 
									name="sub_category_id[<?php echo $value['id']; ?>][<?php echo $c_key; ?>]" 
									id="sub_category_id<?php echo $value['id'].'-'.$c_value['id']; ?>" 
									value="<?php echo $c_value['id']; ?>" 
									<?php echo ( ! empty($c_value['menu_id'])) ? 'checked="checked"' : ""; ?> 
								>
								&nbsp; <?php echo $c_value['name']; ?>
							</div>
						</li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
				</li>
				<?php endforeach; ?>
			<?php endif; ?>

			</ul>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-2"></div>
		<div class="col-sm-10">
			<input type="submit" class="btn btn-info" name="button_save" id="button_save" value="Save" />
			<input type="submit" class="btn btn-info" name="btn_apply" id="btn_apply" value="Apply" />
			<input type="button" data-href="<?php echo site_admin_url('user/authentication'); ?>" class="btn btn-default btn-cancel" name="btn_cancel" id="btn_cancel" value="Cancel" />

		</div>
	</div>
</form>