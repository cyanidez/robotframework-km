
<div class="dialog">
    <?php if ($this->session->flashdata('success')) : ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
    <?php endif; ?>

    <?php if ($this->session->flashdata('error')) : ?>
        <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
    <?php endif; ?>
    <div class="panel panel-default">
        <p class="panel-heading no-collapse">SET NEW PASSWORD</p>
        <div class="panel-body">
            <form  name="reset_password_form" id="reset_password_form" method="post" action="<?php echo site_admin_url('user/forgot_password/change_password?sum='.$sum); ?>">
                <div class="form-group">
                    <label>New Password</label>
                    <input type="password" name="new_password" id="new_password" class="form-controlspan12 form-control">
                    <?php echo form_error('new_password'); ?>
                </div>

                <div class="form-group">
                    <label>Confirm new Password</label>
                    <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-controlspan12 form-control">
                    <?php echo form_error('confirm_new_password'); ?>
                </div>

                <input type="submit" class="btn btn-info" value="Change password" id="btn-save" name="btn-save">

                <div class="clearfix"></div>
            </form>
        </div>
    </div> <a href="<?php echo site_admin_url(); ?>">&raquo; Back to login page</a>
    <p style="font-size: .75em; margin-top: .25em;">Developed & License by : <img src="http://www.aseanwebdesign.com/images/awd_icon.png" width="11" height="11" style="margin-bottom:.20em; vertical-align:middle;"> <a href="http://www.aseanwebdesign.com" target="_blank">AseanWebDesign™</a>
</div>


