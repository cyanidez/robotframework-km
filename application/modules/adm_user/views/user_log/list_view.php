<?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success" role="alert">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
<?php endif; ?>
<?php if ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger" role="alert">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
<?php endif; ?>
<div class="page-header">
    <h3>User Management</h3>
</div>
<form id="search-form" name="search-form" method="get" action="<?php echo site_admin_url('user/user_log/index'); ?>">
    <table class="table">
        <thead>
            <tr>
                <th colspan="4">Search User Log</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="col-sm-1">
                    User
                </td>
                <td class="col-sm-5">
                    <select name="user_id" id="user_id" class="form-control">
                        <option value="">==== Select User ====</option>
                        <?php if ( ! empty($user)) : ?>
                        <?php foreach ($user as $key => $value) : ?>
                        <option <?php echo $user_id == $value['id'] ? 'selected="selected"' : ""; ?> value="<?php echo $value['id']; ?>"><?php echo $value['first_name'].' '.$value['last_name']; ?></option>
                        <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                </td>
                <td class="col-sm-1">
                    Action
                </td>
                <td class="col-sm-5">
                    <input type="checkbox" name="action[]" id="create" value="create" <?php echo in_array('create', $action) ? 'checked="checked"' : ""; ?>> Create
                    <input type="checkbox" name="action[]" id="update" value="update" <?php echo in_array('update', $action) ? 'checked="checked"' : ""; ?>> Update
                    <input type="checkbox" name="action[]" id="delete" value="delete" <?php echo in_array('delete', $action) ? 'checked="checked"' : ""; ?>> Delete
                </td>

            </tr>
            <tr>
                <td class="col-sm-1">
                    Module
                    

                </td>
                <td class="col-sm-11" colspan="3">
                    <ul class="list-unstyled">
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="category" <?php echo in_array('category', $module) ? 'checked="checked"' : ""; ?>> Category
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="node"  <?php echo in_array('node', $module) ? 'checked="checked"' : ""; ?>> Node
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="news" <?php echo in_array('news', $module) ? 'checked="checked"' : ""; ?>> News
                        </li>
                        <li class="col-sm-2"> 
                            <input type="checkbox" id="" name="module[]" value="aira_group" <?php echo in_array('aira_group', $module) ? 'checked="checked"' : ""; ?>>AIRA GROUP
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="partner" <?php echo in_array('partner', $module) ? 'checked="checked"' : ""; ?>>Partner
                        </li>
                        <li class="col-sm-2"> 
                            <input type="checkbox" id="" name="module[]" value="download" <?php echo in_array('download', $module) ? 'checked="checked"' : ""; ?>> Download
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="user" <?php echo in_array('user', $module) ? 'checked="checked"' : ""; ?>> User
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="career" <?php echo in_array('career', $module) ? 'checked="checked"' : ""; ?>> Career
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="social" <?php echo in_array('social', $module) ? 'checked="checked"' : ""; ?>> Social
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="configuration" <?php echo in_array('configuration', $module) ? 'checked="checked"' : ""; ?>> Web Configuration
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="banner" <?php echo in_array('banner', $module) ? 'checked="checked"' : ""; ?>> Banner
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="splash_page" <?php echo in_array('splash_page', $module) ? 'checked="checked"' : ""; ?>> Splash Page
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="contact_group" <?php echo in_array('contact_group', $module) ? 'checked="checked"' : ""; ?>> Contact GROUP
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="web_template" <?php echo in_array('web_template', $module) ? 'checked="checked"' : ""; ?>> Web template
                        </li>
                        <li class="col-sm-2">
                            <input type="checkbox" id="" name="module[]" value="web_template_group" <?php echo in_array('web_template_group', $module) ? 'checked="checked"' : ""; ?>> Web template group
                        </li>
                    </ul>
                </td>
            </tr>

        </tbody>
        <tfoot>
            <tr>
                <td class="col-sm-1"></td>
                <td colspan="3" class="col-sm-10">
                    <input type="submit" value="Search" id="btn-search" name="btn-search" class="btn btn-info">
                </td>
            </tr>

        </tfoot>
    </table>

</form>


<table class="table">
    <thead>
    <tr>
        <th>Full name / username</th>
        <th>Module</th>
        <th>Action</th>
        <th>Date time</th>
        <th>Method</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <td colspan="8"><?php echo bootstrap_pagination($user_log, site_admin_url('user/user_log/index'), $query); ?></td>

    </tr>
    </tfoot>
    <tbody>
    <?php if ($user_log['row_count'] > 0) : ?>
        <?php foreach ($user_log['rows'] as $key => $value) : ?>
            <tr>
                <td>
                    <?php echo $value['first_name'].' '.$value['last_name']; ?>
                    <br><strong>Username : </strong><?php echo $value['username']; ?>
                </td>
                <td>
                    <?php echo ($value['module']); ?><br>
                </td>

                <td><?php echo $value['action']; ?></td>
                <td><?php echo $value['created_at']; ?></td>
                <td>
                    <a href="<?php echo site_admin_url('user/user_log/detail/'.$value['id']); ?>" class="btn btn-info">Detail</a>
                </td>
            </tr>
        <?php endforeach; ?>
    <?php else : ?>
        <tr>
            <td colspan="5">Not found data yet.</td>
        </tr>
    <?php endif; ?>

    </tbody>
</table>