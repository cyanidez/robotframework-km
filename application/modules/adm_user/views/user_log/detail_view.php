<div class="page-header">
    <h3>User Log Detail</h3>
</div>
<table class="table">
    <tbody>
        <tr>
            <td>Module</td>
            <td><?php echo $user_log['module']; ?></td>
        </tr>
        <tr>
            <td>Username</td>
            <td><?php echo $user_log['username']; ?></td>
        </tr>
        <tr>
            <td>Full Name</td>
            <td><?php echo $user_log['first_name'].' '.$user_log['last_name']; ?></td>
        </tr>
        <tr>
            <td>Action</td>
            <td><?php echo $user_log['action']; ?></td>
        </tr>
        <tr>
            <td>Content</td>
            <td><pre><?php print_r(json_decode($user_log['content'], true)); ?></pre></td>
        </tr>
        
        
        


    </tbody>
    <tfoot>
        <tr>
            <td colspan="3"><input type="button" class="btn btn-info ิbtn-cancel" value="Back" data-href="<?php echo site_admin_url('user/user_log/index'); ?>"></td>
        </tr>
    </tfoot>

</table>