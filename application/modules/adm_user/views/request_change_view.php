<?php if ($this->session->flashdata('success')) : ?>
        <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
    <?php endif; ?>

    <?php if ($this->session->flashdata('error')) : ?>
        <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
    <?php endif; ?>

<div class="dialog">
    
    <div class="panel panel-default">
        <p class="panel-heading no-collapse">REQUEST CHANGE PASSWORD</p>
        <div class="panel-body">
            <form  name="request_change_form" id="request_change_form" method="post" action="<?php echo site_admin_url('user/user/request_change'); ?>">
                <div class="form-group">
                    <label>Username *</label>
                    <input type="text" name="username" id="username" class="form-controlspan12 form-control">
                </div>
                <div class="form-group">
                    <label>Email *</label>
                    <input type="text" name="email" id="email" class="form-controlspan12 form-control">
                </div>
                <div class="form-group">
                    <label>* Mandatory</label>
                </div>
                <input type="submit" class="btn btn-info" value="Request change password" id="btn-save" name="btn-save">

                <div class="clearfix"></div>
            </form>
        </div>
    </div>
</div>


