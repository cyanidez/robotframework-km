<?php
class Keyword_name_model extends MY_Model { 

	private $_table = "keyword_name";

	public function __construct()
	{
		parent::__construct();
	}

    public function is_exist($slug = null, $keyword_file_id =  null)
    {
        if (empty($slug))
        {
            return null; 
        }
        if (empty($keyword_file_id))
        {
            return null; 
        }
        $select = parent::select();
        $select->from(array('KN' => 'keyword_name'), array('*'))
            ->where('KN.slug = ?', $slug)
            ->where('KN.keyword_file_id = ?', $keyword_file_id);

        $query = parent::fetch_one($select);
        if ($query > 0)
        {
            return true; 
        }
        return false;

    }

	public function create($params = NULL)
    {
        if (empty($params))
        {
            return NULL;
        }

        if (parent::insert($this->_table, $params) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function update($params = NULL, $where = NULL)
    {
        if (empty($params))
        {
            return NULL;
        }

        if (empty($where))
        {
            return NULL;
        }

        if (parent::update($this->_table, $params, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($where = NULL)
    {
        if (empty($where))
        {
            return NULL;
        }
        if (parent::delete($this->_table, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }
}