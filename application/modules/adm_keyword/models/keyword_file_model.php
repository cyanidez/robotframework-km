<?php
class Keyword_file_model extends MY_Model { 

	private $_table = "keyword_file";

	public function __construct()
	{
		parent::__construct();
	}

    public function is_exist($path = null)
    {
        if (empty($path))
        {
            return null; 
        }
        $select = parent::select();
        $select->from(array('KF' => 'keyword_file'), array('id'))
            ->where('KF.full_path_slug = ?', $path);
        #echo $select->__toString();
        $query = parent::fetch_one($select);
        #echo "query = ".$query; 
        if ($query > 0)
        {
            return true; 
        }
        return false; 


    }

    public function get_keyword_file_detail($where, $condition = 'id')
    {
        if (empty($where))
        {
            return null; 
        }
        $select = parent::select();
        $select->from(array('KF' => 'keyword_file'), array('*'));

        if ($condition == "id")
        {
            $select->where('KF.id = ?', $where);
        }
        if ($condition == "full_path_slug")
        {
            $select->where('KF.full_path_slug = ?', $where);
        }
        return parent::fetch_row($select);
    }

	public function create($params = NULL)
    {
        if (empty($params))
        {
            return NULL;
        }

        if (parent::insert($this->_table, $params) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function update($params = NULL, $where = NULL)
    {
        if (empty($params))
        {
            return NULL;
        }

        if (empty($where))
        {
            return NULL;
        }

        if (parent::update($this->_table, $params, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($where = NULL)
    {
        if (empty($where))
        {
            return NULL;
        }
        if (parent::delete($this->_table, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }
}