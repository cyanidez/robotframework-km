<?php
class Home extends Web_Controller {
    public function __construct()
    {
        parent::__construct();
        
        #echo "langcode = ".LANGCODE
        #$this->lang->load('home', LANGCODE);
    }

    public function index()
    {
        $data = array();
        #$this->template->write_view('slider', 'templates/front/layouts/slider', $data, true);
        parent::set_layouts('home');

        $data['news'] = $this->news->get_all_news();
        $data['section_set'] = $this->web_template->get_web_template_by_section('SETTRADE');
        $data['section_graph'] = $this->web_template->get_web_template_by_section('GRAPH');

        $data['section_video'] = $this->web_template->get_web_template_by_section('VIDEO');
        $data['section_partner'] = $this->web_template->get_web_template_by_section('PARTNER');
        
        $data['show_splashpage'] = $this->show_splashpage;

        #alert($data['section_set'], 'red', 'data');
        #exit;
        
        parent::set_footer_banner();
        parent::set_partner();
        parent::set_slider();
        parent::set_aira_group();
        parent::set_our_service();
        parent::set_investor();
        $this->template->write_view('content', 'home_view', $data);
        $this->template->render();
    }

    

}