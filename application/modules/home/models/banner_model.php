<?php
class Banner_model extends MY_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function get_banner()
    {
        $select = parent::select();
        $select->from(array('B' => 'banner'), array('B.id', 'B.img_full_path'))
            ->where('B.active_status = ?', 'Y');

        return parent::fetch_all($select);
    }

    public function get_banner_by_area($banner_area_id = null)
    {
        if (empty($banner_area_id))
        {
            return null;
        }
        $select = parent::select();
        $select->from(array('B' => 'banner'), array('B.id', 'B.title', 'B.img_full_path'))
            ->where('B.banner_area_id = ?', $banner_area_id)
            ->where('B.active_status = ?', 'Y');
        return parent::fetch_row($select);
    }

    public function get_banner_top_section()
    {
        $select = parent::select();
        $select->from(array('B' => 'banner'), array('B.id', 'B.title', 'B.img_full_path'))
            ->where('B.banner_area_id = ?', 1)
            ->where('B.active_status = ?', 'Y');
        return parent::fetch_all($select);
    }

}