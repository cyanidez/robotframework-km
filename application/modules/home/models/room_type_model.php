<?php
class Room_type_model extends MY_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function get_room_type()
    {
        $select = parent::select();
        $select->from(array('RT' => 'room_type'), array('RT.id', 'RT.img_full_path'))
            ->join(array('RTL' => 'room_type_lang'), 'RT.id = RTL.room_type_id AND RTL.lang_id = '.DEFAULT_LANG_ID, array('RTL.title', 'RTL.detail', 'RTL.short_detail'))
            ->where('RT.active_status = ?', 'Y')
            ->order('RT.id DESC');

        return parent::fetch_all($select);
    }

    public function get_room_type_detail($id = NULL)
    {
        if (is_null($id) OR empty($id))
        {
            return NULL;
        }

        $return = array();

        $select = parent::select();
        $select->from(array('RT' => 'room_type'), array('RT.id', 'RT.active_status', 'RT.img_full_path', 'RT.img_full_path_detail'))
            ->join(array('RTL' => 'room_type_lang'), 'RT.id = RTL.room_type_id and RTL.lang_id = '.DEFAULT_LANG_ID, array('RTL.title', 'RTL.detail', 'RTL.short_detail'))
            ->where('RT.id = ?', $id)
            ->where('RT.active_status = ?', 'Y');

        $data = parent::fetch_row($select);
        if (empty($data))
        {
            return NULL;
        }
        $return = array(
            'id' => $data['id'],
            'title' => $data['title'],
            'short_detail' => $data['short_detail'],
            'detail' => $data['detail'],
            'active_status' => $data['active_status'],
            'img_full_path' => $data['img_full_path'],
            'img_full_path_detail' => $data['img_full_path_detail']
        );

        $select = parent::select();

        $select->from(array('RT' => 'room_type'), array('RT.id', 'RT.active_status'))
            ->join(array("RTL" => "room_type_lang"), 'RT.id = RTL.room_type_id AND RTL.lang_id != '.DEFAULT_LANG_ID, array('RTL.id as translate_id', 'RTL.title', 'RTL.detail'))
            ->join(array('L' => 'lang'), 'L.id = RTL.lang_id', array('L.code'))
            ->where('RT.id  = ?', $id)
            ->where('RT.active_status = ?', 'Y');

        $data = parent::fetch_all($select);
        if ( ! empty($data))
        {
            $translate = array();
            foreach ($data as $key => $value)
            {
                $translate[$value['code']] = array(
                    'id' => $value['translate_id'],
                    'title' => $value['title'],
                    'detail' => $value['detail']
                );
            }

        }

        if ( ! empty($translate))
        {
            $return['translate'] = $translate;
        }
        else
        {
            $return['translate'] = array();
        }
        return $return;
    }
}