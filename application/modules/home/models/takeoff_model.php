<?php
class Takeoff_model extends MY_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function get_takeoff_by_page($page = null)
    {
        if (empty($page))
        {
            return null;
        }
        $select = parent::select();
        $select->from(array('B' => 'takeoff'), array('B.id', 'B.img_full_path', 'B.page'))
            ->where('B.active_status = ?', 'Y')
            ->where('B.page = ?', $page)
            ->order('B.id desc')
            ->limit(1);

        return parent::fetch_row($select);
    }



}