<?php
class Floor_plan_model extends MY_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function get_floor_plan()
    {
        $select = parent::select();
        $select->from(array('FP' => 'floor_plan'), array('FP.id', 'FP.img_full_path'))
            ->join(array('FPL' => 'floor_plan_lang'), 'FP.id = FPL.floor_plan_id AND FPL.lang_id = '.DEFAULT_LANG_ID, array('FPL.title', 'FPL.detail', 'FPL.short_detail'))
            ->where('FP.active_status = ?', 'Y')
            ->order('FP.id DESC');

        return parent::fetch_all($select);
    }

    public function get_floor_plan_detail($id = NULL)
    {
        if (is_null($id) OR empty($id))
        {
            return NULL;
        }

        $return = array();

        $select = parent::select();
        $select->from(array('FP' => 'floor_plan'), array('FP.id', 'FP.active_status', 'FP.img_full_path', 'FP.img_full_path_detail', 'FP.img_detail'))
            ->join(array('FPL' => 'floor_plan_lang'), 'FP.id = FPL.floor_plan_id and FPL.lang_id = '.DEFAULT_LANG_ID, array('FPL.title', 'FPL.detail', 'FPL.short_detail'))
            ->where('FP.id = ?', $id)
            ->where('FP.active_status = ?', 'Y');

        $data = parent::fetch_row($select);
        if (empty($data))
        {
            return NULL;
        }
        $return = array(
            'id' => $data['id'],
            'title' => $data['title'],
            'short_detail' => $data['short_detail'],
            'detail' => $data['detail'],
            'active_status' => $data['active_status'],
            'img_full_path' => $data['img_full_path'],
            'img_full_path_detail' => $data['img_full_path_detail'],
            'img_detail' => $data['img_detail']
        );

        $select = parent::select();

        $select->from(array('FP' => 'floor_plan'), array('FP.id', 'FP.active_status'))
            ->join(array("FPL" => "floor_plan_lang"), 'FP.id = FPL.floor_plan_id AND FPL.lang_id != '.DEFAULT_LANG_ID, array('FPL.id as translate_id', 'FPL.title', 'FPL.detail'))
            ->join(array('L' => 'lang'), 'L.id = FPL.lang_id', array('L.code'))
            ->where('FP.id  = ?', $id)
            ->where('FP.active_status = ?', 'Y');

        $data = parent::fetch_all($select);
        if ( ! empty($data))
        {
            $translate = array();
            foreach ($data as $key => $value)
            {
                $translate[$value['code']] = array(
                    'id' => $value['translate_id'],
                    'title' => $value['title'],
                    'detail' => $value['detail']
                );
            }

        }

        if ( ! empty($translate))
        {
            $return['translate'] = $translate;
        }
        else
        {
            $return['translate'] = array();
        }
        return $return;
    }
}