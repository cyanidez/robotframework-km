<?php
class Content_model extends MY_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function get_content()
    {
        $select = parent::select();
        $select->from(array('C' => 'content'), array('C.id'))
            ->join(array('CL' => 'content_lang'), 'C.id = CL.content_id AND CL.lang_id = '.DEFAULT_LANG_ID, array('CL.content', 'CL.short_content'))
            ->where('C.active_status = ?', 'Y');

        return parent::fetch_all($select);
    }

    public function get_content_detail($id = NULL)
    {
        if (is_null($id) OR empty($id))
        {
            return NULL;
        }

        $return = array();

        $select = parent::select();
        $select->from(array('RT' => 'content'), array('RT.id', 'RT.active_status', 'RT.img_full_path'))
            ->join(array('RTL' => 'content_lang'), 'RT.id = RTL.content_id and RTL.lang_id = '.DEFAULT_LANG_ID, array('RTL.content', 'RTL.short_content'))
            ->where('RT.id = ?', $id)
            ->where('RT.active_status = ?', 'Y');

        $data = parent::fetch_row($select);
        if (empty($data))
        {
            return NULL;
        }
        $return = array(
            'id' => $data['id'],
            'short_content' => $data['short_content'],
            'content' => $data['content'],
            'active_status' => $data['active_status'],
            'img_full_path' => $data['img_full_path']
        );

        $select = parent::select();

        $select->from(array('RT' => 'content'), array('RT.id', 'RT.active_status', 'RT.img_full_path'))
            ->join(array("RTL" => "content_lang"), 'RT.id = RTL.content_id AND RTL.lang_id != '.DEFAULT_LANG_ID, array('RTL.id as translate_id', 'RTL.content'))
            ->join(array('L' => 'lang'), 'L.id = RTL.lang_id', array('L.code'))
            ->where('RT.id  = ?', $id)
            ->where('RT.active_status = ?', 'Y');

        $data = parent::fetch_all($select);
        if ( ! empty($data))
        {
            $translate = array();
            foreach ($data as $key => $value)
            {
                $translate[$value['code']] = array(
                    'id' => $value['translate_id'],
                    'content' => $value['content']
                );
            }

        }

        if ( ! empty($translate))
        {
            $return['translate'] = $translate;
        }
        else
        {
            $return['translate'] = array();
        }
        return $return;
    }
}