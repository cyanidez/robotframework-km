<div class="body-wrap">

    <div class="content">
        <!--container-->
        <div class="container">

            <!-- row -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- Contents Slider -->
                    <div class="albums-slider">
                        <ul id="albums">

                            <?php if ( ! empty($floor_plan)) : ?>
                                <?php foreach ($floor_plan as $key => $value) : ?>
                                    <li class="albums-item">
                                        <!-- Thumbnail Image -->
                                        <div class="albums-player">
                                            <img class="albums-player img-responsive" src="<?php echo site_floor_plan_url($value['img_full_path']); ?>">
                                        </div>
                                        <!-- Title & Detail -->
                                        <span class="albums-title"> <?php echo $value['title']; ?></span>
                                        <span class="albums-subtitle"><?php echo $value['short_detail']; ?></span>
                                        <a target="_top" href="<?php echo site_url('detail?s=floor_plan&id='.$value['id']); ?>" class="btn btn-dark">Read More &raquo;</a>
                                    </li>
                                <?php endforeach; ?>

                            <?php endif; ?>

                        </ul>
                        <a class="prev" id="albums-prev" href="#">&lsaquo;</a>
                        <a class="next" id="albums-next" href="#">&rsaquo;</a>
                    </div>
                    <script>
                        jQuery(document).ready(function ($) {

                            function albumsInit() {
                                $('#albums').carouFredSel({
                                    swipe : {
                                        onMouse: true,
                                        onTouch: true
                                    },
                                    prev: '#albums-prev',
                                    next: "#albums-next",
                                    auto: false,
                                    scroll: {
                                        pauseOnHover: true,
                                        items: 1,
                                        duration: 500,
                                        easing: 'swing'
                                    }
                                });
                            }

                            albumsInit();

                            $(window).resize(function() {
                                albumsInit();
                            });
                        });
                    </script>
                    <!--/ Contents Slider -->
                </div>
            </div>


            <!--/ row -->

        </div>
        <!--/ container -->
    </div>