<div class="row">

          <div class="col-md-4">
            <!-- Classic Heading -->
            <h4 class="classic-title"><span><b><?php echo lang('ข่าวประชาสัมพันธ์'); ?></b></span></h4>

            <!-- Start Testimonials Section -->
            <!-- Start Recent Posts Carousel -->
            <?php if ( ! empty($news)) : ?>
            <div class="latest-posts">

              <div class="latest-posts-classic custom-carousel touch-carousel" data-appeared-items="1">

                <!-- Posts 1 -->
                <?php foreach ($news as $key => $value) : ?>

                <div class="post-row item">
                 
                  <img src="<?php echo site_news_url($value['thumbnail_resize']); ?>" class="img-thumbnail" style="margin-bottom:10px;">
                  <h3 class="post-title"><a href="<?php echo news_detail_url($value['slug'], $value['id']); ?>"><?php echo $value['name']; ?></a></h3>
                  <div class="post-content">
                    <p><?php echo mb_substr($value['short_description'], 0, 120); ?> <a class="read-more" href="<?php echo news_detail_url($value['slug'], $value['id']); ?>"><?php echo lang('อ่านต่อ'); ?> </a></p>
                  </div>
                </div>
                <?php endforeach; ?>

              </div>
            </div>
          <?php endif; ?>
            <!-- End Recent Posts Carousel -->

          </div>

          <div class="col-md-4" style="margin-bottom:10px;">
            <!-- Classic Heading -->
          <?php if ( ! empty($section_video)) : ?>
            <h4 class="classic-title"><span><b><?php echo lang('วิดีโอ'); ?></b></span></h4>
            <!-- Vimeo Iframe -->
            <?php /**<iframe src="https://www.youtube.com/embed/b6QQEIA_gxo" width="800" height="507"></iframe>**/ ?>
            <?php if ($section_video['type'] == "text") : ?>
              <?php echo $section_video['short_description']; ?>
            <?php elseif ($section_video['type'] == "image") : ?>
              <img src="<?php echo site_web_template_url($section_video['img_full_path']); ?>">
            <?php elseif ($section_video['type'] == "script") : ?>
              <?php echo $section_video['script']; ?>
            <?php endif; ?>
           <?php endif; ?>
          </div>
          <div class="col-md-4">
            <!-- Classic Heading -->
            <h4 class="classic-title"><span><b><?php echo lang('ข้อมูลหลักทรัพย์'); ?></b></span></h4>
            <!-- Vimeo Iframe -->
          <?php if ( ! empty($section_set)) : ?>
            <?php if ($section_set['type'] == "script") : ?>
            <div style="background-color:<?php echo $section_set['bgcolor']; ?>;" align="center">
              <?php /*<iframe src="http://xwww.airafactoring.co.th/2009/test2016/testIR.html" width="200" height="200" frameborder=0 scrolling=no allowtransparency="true"></iframe>*/ ?>
              <?php echo $section_set['script']; ?>
            </div>
            <?php endif; ?>
            <?php if ($section_set['type'] == "image") : ?>
            <img src="<?php echo site_web_template_url($section_set['img_full_path']); ?>">
            <?php endif; ?>
          
            <?php if ( ! empty($section_set['url_link'])) : ?>
            <div style="background-color: #999999;" align="center"> 
              <?php if (empty($section_set['text_link'])) : ?>
              <a class="read-more" href="<?php echo $section_set['url_link']; ?>" style="color:#FFFFFF;"><?php echo lang('ข้อมูลนักลงทุนสัมพันธ์'); ?> </a>
              <?php else : ?>
              <a class="read-more" href="<?php echo $section_set['url_link']; ?>" style="color:#FFFFFF;"><?php echo $section_set['text_link']; ?></a>
              <?php endif; ?>
            </div>
            <?php endif; ?>
          <?php endif; ?>
          </div>

        </div>
