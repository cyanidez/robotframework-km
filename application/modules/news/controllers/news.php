<?php
class News extends Web_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('news_model', 'news');
        $this->load->model('news_tag_model', 'news_tag');
        $this->load->model('news_paragraph_model', 'news_paragraph');
        $this->load->model('aira_group/aira_group_model', 'aira_group');
        $this->lang->load('news', 'news', LANGCODE);
    }

    public function index()
    {
        $data = array();
        
        $data['page'] = $args['page'] = $this->input->get_post('page', true) ? $this->input->get_post('page', true) : 1; 
        $data['per_page'] = $args['per_page'] = $this->input->get_post('per_page', true) ? $this->input->get_post('per_page', true) : 20;


                
        $data['news'] = $this->news->get_news($args, $data['page'], $data['per_page']);
        #$this->breadcrumb->make(lang('หน้าแรก'), site_url());
        $this->breadcrumb->make(lang('ข่าวประชาสัมพันธ์'), null, true, true);
        parent::set_breadcrumb();
        parent::set_news_sidebar();
            
        #$this->template->write('subject', lang('ข่าวประชาสัมพันธ์'));
        $this->template->write_view('content', 'list_view', $data);

        
        $this->template->render();
    }

    public function detail($id = null)
    {
        $data = array();
        
        $news = $data['news'] = $this->news->get_news_detail($id);

        $data['id'] = $id; 
        $data['slug'] = $news['slug'];
        $update_view = $this->news->update_view($id);
        #alert($update_view,'blue', 'update-view');
        #exit; 
        

        
        if (empty($data['news']['id']))
        {
            $this->template->write_view('content', 'error/error_view', $data);
        }
        else 
        {
            if (LANGID == DEFAULT_LANG_ID)
            {
                parent::seo_title($news['seo_title']);
                parent::seo_keyword($news['seo_keyword']);
                parent::seo_description($news['seo_description']);
                $name = $news['name'];
            }
            else 
            {
                parent::seo_title($news['seo_title']);
                parent::seo_keyword($news['seo_keyword']);
                parent::seo_description($news['seo_description']);   
            }



            
            $data['news_tag'] = $this->news_tag->get_news_tag($id);
            $data['news_paragraph'] = $this->news_paragraph->get_news_paragraph($id);
            

            #$this->breadcrumb->make(lang('หน้าแรก'));
            #$this->template->write('subject', lang('ข่าวประชาสัมพันธ์'));
            $this->breadcrumb->make(lang('ข่าวประชาสัมพันธ์'), news_url());
            if ( ! empty($news['name']))
            {
                $this->breadcrumb->make($news['name'], null, true, true);
            }
            parent::set_breadcrumb();
            parent::set_news_sidebar($id);
            #$data['news_other'] = $this->news->get_news_other($id);
            $data['aira_group'] = $this->aira_group->get_all_aira_group();
            $this->template->write_view('content', 'detail_view', $data);

        }
        $this->template->render();
    }
}