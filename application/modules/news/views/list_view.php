<!-- Classic Heading -->
<h4 class="classic-title"><span><?php echo lang("ข่าวประชาสัมพันธ์ทั้งหมด"); ?></span></h4>

<div class="latest-posts-classic">

  <!-- Post 1 -->
                  
  <?php if ($news['row_count'] > 0) : ?>
  <?php foreach ($news['rows'] as $key => $value) : ?>
                  <div class="post-row">
                   <div class="col-md-5">
                    <div class="widget-thumb">
                    <a href="<?php echo news_detail_url($value['slug'], $value['id']); ?>"><img src="<?php echo site_news_url($value['thumbnail']); ?>" class="img-thumbnail" style="margin-bottom:30px;"></a>
                  </div>
                  </div>
                  <div class="col-md-7">
                    <h3 class="post-title"><a href="<?php echo news_detail_url($value['slug'], $value['id']); ?>"><?php echo $value['name']; ?></a></h3>
                    <span><i class="fa fa-calendar"></i> <?php echo show_style_date($value['created_at']); ?> <i class="fa fa-eye"></i> <?php echo $value['views']; ?> Views </span>
                    <div class="post-content">
                    
                      <p><?php echo $value['short_description']; ?>  <a class="read-more" href="<?php echo news_detail_url($value['slug'], $value['id']); ?>"><?php echo lang("อ่านต่อ"); ?><i class="fa fa-angle-right"></i></a></p>
                      </div>
                    </div>
                  </div>
                  <?php endforeach; ?>
                  <?php endif; ?>

			
            
                  
                 
                  
                   

                </div>  <!-- Start Pagination -->

<?php echo aira_pagination($news, news_url()); ?>
            <!-- End Pagination -->
              