<h4 class="classic-title"><span><b><?php echo $news['name']; ?></b></span></h4>
                        
	<!-- Some Text -->
  <?php #alert($news, 'red'); ?>
	<?php if ( ! empty($news['image'])) : ?>
	<p style="text-align:center;">
		<img src="<?php echo site_news_url($news['image']); ?>" alt="" class="img-responsive" />		
	</p>
  <?php endif; ?>
	
	<p><?php echo $news['description']; ?></p>
  
  <?php if ( ! empty($news_paragraph)) : ?>
    <?php foreach ($news_paragraph as $key => $value) : ?>
    <img src="<?php echo site_news_url($value['image']); ?>">
    <?php echo $value['description']; ?>
    <?php endforeach; ?>
  <?php endif; ?>



<?php if ( ! empty($news_tag)) : ?>                        
<div class="post-bottom clearfix">
  <div class="post-tags-list">
    <span><?php echo lang('Tag'); ?> :</span>
    <?php foreach ($news_tag as $key => $value) : ?>
    <a href="#"><?php echo $value['name']; ?></a>
    <?php endforeach; ?>
  </div>
</div>
<?php endif; ?>
 
<!-- Divider -->
<div class="hr5" style="margin-top:30px; margin-bottom:45px;"></div>

<?php /*****
<div class="container">
  <div class="row">
    <div class="col-md-9">
      <!-- Start Home Page Partner -->
      <!-- Start Clients Carousel -->
      <?php if ( ! empty($aira_group)) : ?>
      <div class="our-clients">
        <!-- Classic Heading -->
        <h4 class="classic-title"><span><b><?php echo lang('บริษัทในเครือ'); ?></b></span></h4>
        <div class="clients-carousel custom-carousel touch-carousel" data-appeared-items="5">
          <!-- Client 1 -->
          
          
          <?php foreach ($aira_group as $key => $value) : ?>
          <div class="client-item item">
            <a href="<?php echo aira_group_detail_url($value['slug'], $value['id']); ?>"><img src="<?php echo site_aira_group_url($value['thumbnail_slider']); ?>" alt="" /></a>
          </div>
          <?php endforeach; ?>
          
        </div>
      </div>
      <?php endif; ?>
      <!--End Clients Carousel-->
      <!-- End Home Page Partner -->
    </div>
    <div class="col-md-3">
      <h4 class="classic-title"><span><b><?php echo lang('เครือข่ายพันธมิตร'); ?></b></span></h4>
      <p><?php echo lang('บริษัท ไอร่า แฟคตอริ่ง จำกัด (มหาชน) มีบริษัทในเครือมากมาย ภายใต้การดูแลของ ไอร่า กรุ๊ป'); ?></p>
    </div>
  </div>
</div>
**/ ?>
    <!-- End Content -->