<?php
class news_model extends MY_Model {

    private $_table = "news";
    private $_table_lang = "news_lang";

    public function get_news($args = array(), $page = 1, $limit = 20)
    {
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.thumbnail_resize', 'N.created_at', 'N.views'))
                ->joinLeft(array('NL' => $this->_table_lang), 'NL.news_id = N.id AND NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description', 'NL.description'))
                ->where('N.active_status = ?', 'publish');

        if ( ! empty($args['keyword']))
        {
            $select->join(array('NT' => 'news_tag'), 'NT.news_id', array())
                ->where('NT.name LIKE ?', '%'.$args['keyword'].'%');
        }

        $select->where('N.deleted_at IS NULL')
            ->order('N.id desc');

        $data = parent::fetch_page($select, array(), $page, $limit);
        return $data;
    }

    public function update_view($news_id = null)
    {
        if (empty($news_id))
        {
            return null; 
        }
        $sql = "UPDATE news SET views=views+1 WHERE id = ".$news_id; 
        #echo $sql; 
        #exit; 
        return parent::query($sql);

    }

    public function get_news_by_keyword($q = null)
    {
        if (empty($q))
        {
            return null; 

        }
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.thumbnail_resize', 'N.created_at', 'N.views'))
            ->joinLeft(array('NL' => $this->_table_lang), 'NL.news_id = N.id', array('NL.name', 'NL.short_description', 'NL.description'))
            ->where('N.active_status = ?', 'publish')
            ->where('( NL.name LIKE ?', '%'.$q.'%')
            ->orWhere('NL.short_description LIKE ?', '%'.$q.'%')
            ->orWhere('NL.description LIKE ?)', '%'.$q.'%')
            ->where('N.deleted_at IS NULL')
            ->order('N.id desc');

        $data = parent::fetch_all($select);
        return $data;
    }

    public function get_all_news()
    {
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.thumbnail_resize', 'N.thumbnail_resize', 'N.created_at', 'N.views'))
            ->join(array('NL' => $this->_table_lang), 'NL.news_id = N.id AND NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description', 'NL.description'))
            ->where('N.active_status = ?', 'publish')
            ->where('N.deleted_at IS NULL')
            ->order('N.id desc');

        $data = parent::fetch_all($select);
        return $data;
    }
    public function get_news_other($id = null)
    {
        
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.created_at', 'N.views'))
            ->joinLeft(array('NL' => $this->_table_lang), 'NL.news_id = N.id AND NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description', 'NL.description'))
            ->where('N.active_status = ?', 'publish')
            ->where('N.deleted_at IS NULL')
            ->order('N.id desc')
            ->limit(10); 

        if ( ! empty($id))
        {
            $select->where('N.id != ?', $id);
        }
        
        return parent::fetch_all($select);
    }

    public function get_latest_news($is_news = false)
    {
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.created_at', 'N.views'))
            ->joinLeft(array('NL' => $this->_table_lang), 'NL.news_id = N.id AND NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description', 'NL.description'))
            ->where('N.active_status = ?', 'publish')
            ->where('N.deleted_at IS NULL')
            ->limit(10); 
        if ( ! empty($is_news))
        {
            #$select->join(arra('where('NL.name = ?', 'ข่าวประชาสัมพันธ์')

        }
        return parent::fetch_all($select);
    }
    public function get_news_detail($id = NULL)
    {
        if (empty($id))
        {
            return NULL;
        }

        $return = array();

        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.thumbnail_resize', 'N.image', 'N.created_at', 'N.views'))
            ->joinLeft(array('NL' => $this->_table_lang), 'N.id = NL.news_id AND NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description', 'NL.description', 'NL.seo_title', 'NL.seo_description', 'NL.seo_keyword'))
            ->where('N.id = ?', $id)
            ->where('N.active_status = ?', 'publish')
            ->where('N.deleted_at IS NULL');
        $data = parent::fetch_row($select);
        if (empty($data))
        {
            return NULL;
        }
        return $data; 
        #alert($data, 'red');
        #exit; 

        $return = array(
            'id' => $data['id'],
            'active_status' => $data['active_status'],
            'views' => $data['views'],
            'name' => $data['name'],
            'slug' => $data['slug'],
            'thumbnail' => $data['thumbnail'],
            'thumbnail_resize' => $data['thumbnail_resize'],
            'image' => $data['image'],
            'short_description' => $data['short_description'],
            'description' => $data['description'],
            'seo_title' => $data['seo_title'],
            'seo_description' => $data['seo_description'],
            'seo_keyword' => $data['seo_keyword']
        );

        
    }
    /***
    public function get_news_detail($id = NULL)
    {
        if (empty($id))
        {
            return NULL;
        }

        $return = array();

        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.thumbnail_resize', 'N.image', 'N.created_at', 'N.views'))
            ->join(array('NL' => $this->_table_lang), 'N.id = NL.news_id and NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description', 'NL.description', 'NL.seo_title', 'NL.seo_description', 'NL.seo_keyword'))
            ->where('N.id = ?', $id)
            ->where('N.active_status = ?', 'publish')
            ->where('N.deleted_at IS NULL');
        $data = parent::fetch_row($select);
        if (empty($data))
        {
            return NULL;
        }
        $return = array(
            'id' => $data['id'],
            'active_status' => $data['active_status'],
            'views' => $data['views'],
            'name' => $data['name'],
            'slug' => $data['slug'],
            'thumbnail' => $data['thumbnail'],
            'thumbnail_resize' => $data['thumbnail_resize'],
            'image' => $data['image'],
            'short_description' => $data['short_description'],
            'description' => $data['description'],
            'seo_title' => $data['seo_title'],
            'seo_description' => $data['seo_description'],
            'seo_keyword' => $data['seo_keyword']
        );

        $select = parent::select();

        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.active_status', 'N.slug'))
            ->join(array("NL" => $this->_table_lang), 'N.id = NL.news_id AND NL.lang_id != '.LANGID, array('NL.news_id as translate_id', 'NL.name', 'NL.short_description', 'NL.description', 'NL.seo_title', 'NL.seo_description', 'NL.seo_keyword'))
            ->join(array('L' => 'lang'), 'L.id = NL.lang_id', array('L.code'))
            ->where('N.id  = ?', $id)
            ->where('N.active_status = ?', 'publish');
            #->where('N.deleted_at IS NULL');

        $data = parent::fetch_all($select);
        if ( ! empty($data))
        {
            $translate = array();
            foreach ($data as $key => $value)
            {
                $translate[$value['code']] = array(
                    'id' => $value['translate_id'],
                    'name' => $value['name'],
                    'slug' => $value['slug'],
                    'short_description' => $value['short_description'],
                    'description' => $value['description'],
                    'seo_title' => $value['seo_title'],
                    'seo_description' => $value['seo_description'],
                    'seo_keyword' => $value['seo_keyword']
                );
            }
        }

        if ( ! empty($translate))
        {
            $return['translate'] = $translate;
        }
        else
        {
            $return['translate'] = array();
        }
        return $return;
    }
    ***/

    public function create($params = NULL)
    {
        if (is_null($params) OR count($params) == 0)
        {
            return NULL;
        }

        if (parent::insert($this->_table, $params) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function update($params = NULL, $where = NULL)
    {
        if (is_null($params) OR count($params) == 0)
        {
            return NULL;
        }

        if (is_null($where) OR count($where) == 0)
        {
            return NULL;
        }

        if (parent::update($this->_table, $params, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($where = NULL)
    {
        if (is_null($where)OR count($where) == 0)
        {
            return NULL;
        }
        if (parent::delete($this->_table, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    
}