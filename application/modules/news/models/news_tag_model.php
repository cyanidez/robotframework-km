<?php
class News_tag_model extends MY_Model { 
	public function __construct()
	{
		parent::__construct();
	}

	public function get_news_tag($news_id = null)
	{
		if (empty($news_id))
		{
			return null;
		}
		$select = parent::select();
		$select->from(array('NT' => 'news_tags'), array('NT.id', 'NT.name', 'NT.clicked'))
			->where('NT.news_id = ?', $news_id);

		return parent::fetch_all($select);
	}
}