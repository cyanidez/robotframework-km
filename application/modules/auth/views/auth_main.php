
						<form id="auth_form" name="auth_form"  method="post" class="form-horizontal" action="<?php echo site_url('auth'); ?>">
							<div class="control-group">
								<label class="control-label"><strong class="mandatory">*</strong> <?php echo lang('username'); ?></label>
								<div class="controls">
									<input type="text" name="username" id="username" value="<?php echo set_value('username'); ?>" class="input input-medium" title="<?php echo lang('validate.required_username'); ?>" />
									<?php if (isset($username_error) && ! empty($username_error)) : ?>
									<label class="error"><?php echo $username_error; ?></label>
									<?php endif; ?>

									<?php if (isset($account_error) && ! empty($account_error)) : ?>
									<label class="error"><?php echo $account_error; ?></label>
									<?php endif; ?>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label"><strong class="mandatory">*</strong> <?php echo lang('password'); ?></label>
								<div class="controls">
									<input type="password" name="password" id="password" value="<?php echo set_value('password'); ?>" class="input input-medium" title="<?php echo lang('validate.required_password'); ?>" />
									<?php if (isset($password_error) && ! empty($password_error)) : ?>
									<label class="error"><?php echo $password_error; ?></label>
									<?php endif; ?>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label"><strong class="mandatory">*</strong> <?php echo lang('captcha_code'); ?></label>
								<div class="controls">
									<img id="siimage" style="border: 1px solid #000; margin-right: 15px" src="/securimage/securimage_show.php?sid=<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image" align="left">
									<object type="application/x-shockwave-flash" data="/securimage/securimage_play.swf?audio_file=/securimage/securimage_play.php&amp;bgColor1=#fff&amp;bgColor2=#fff&amp;iconColor=#777&amp;borderWidth=1&amp;borderColor=#000" height="32" width="32">
										<param name="movie" value="/securimage/securimage_play.swf?audio_file=./securimage_play.php&amp;bgColor1=#fff&amp;bgColor2=#fff&amp;iconColor=#777&amp;borderWidth=1&amp;borderColor=#000" />
									</object>
										&nbsp;
									<a tabindex="-1" style="border-style: none;" href="#" title="Refresh Image" onclick="document.getElementById('siimage').src = '/securimage/securimage_show.php?sid=' + Math.random(); this.blur(); return false"><img src="/securimage/images/refresh.png" alt="Reload Image" onclick="this.blur()" align="bottom" border="0"></a>
									<div style="display:block; clear:both;"><?php echo lang('please_enter_character_or_numberic_on_left'); ?>:</div>
									<?php echo @$_SESSION['ctform']['captcha_error'] ?>
									<input type="text" name="captcha_code" id="captcha_code" size="12" maxlength="8" title="<?php echo lang('validate.required_captcha'); ?>" />
									<?php if (isset($captcha_error) && ! empty($captcha_error)) : ?>
									<label class="error"><?php echo $captcha_error; ?></label>
									<?php endif; ?>
								</div>
							</div>	
							<div class="control-group">
								<div class="controls">
									<a href="<?php echo site_url('member/forgot_password'); ?>" title="<?php echo lang('forgot_password'); ?>"><?php echo lang('forgot_password'); ?></a>
								</div>
							</div>
							<div class="control-group">
								<div class="controls">
									<input type="submit" name="login_button" id="login_button" class="btn btn-large btn-primary" value="<?php echo lang('login'); ?>" />
								</div>
							</div>
						</form>
