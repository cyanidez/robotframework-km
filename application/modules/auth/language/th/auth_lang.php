<?php
$lang['title.authentication_management'] = "จัดการสิทธิระดับผู้ใช้"; 
$lang['title.add_user_level'] = "เพิ่มสิทธิ์ระดับผู้ใช้";
$lang['title.edit_user_level'] = "แก้ไขระดับสิทธิ์ผู้ใช้"; 
$lang['title.confirm_delete_user_level'] = "ยืนยันการลบสิทธิ์ผู้ใช้";
$lang['level_name'] = "ระดับกลุ่มผู้ใช้"; 
$lang['select_user_level'] = "เลือกระดับผู้ใช้"; 
$lang['user_group_privillege'] = "จัดการสิทธิ์ระดับผู้ใช้"; 
$lang['menu'] = "เมนู";
$lang['cannot_delete_because_used'] = "ไม่สามารถลบระดับผู้ใช้นี้ได้ เนื่องจากมีผู้ใช้ที่ยังอยู่ในระดับนี้"; 


$lang['do_not_found_username_or_email'] = "ไม่พบชื่อผู้ใช้ หรือ อีเมล์นี้ในระบบครับ"; 
$lang['wrong_password'] = "คุณกรอกรหัสผ่านไม่ถูกต้อง กรุณาลองอีีกครั้งครับ"; 
$lang['account_suspsend'] = "ชื่อบัญชีผู้ใช้ของคุณ ถูกระงับการใช้งานชั่วคราวครับ"; 
$lang['validate.required_level_name'] = "กรุณากรอกชื่อระดับผู้ใช้";


$lang['signin'] = "เข้าระบบ";
$lang['please_login'] = 'กรุณาเข้าระบบ songparty เพื่อใช้งานส่วนต่างๆ ของสมาชิกได้เลยครับ';
$lang['username'] = "ชื่อผู้ใช้ หรือ อีเมล์";
$lang['password'] = "รหัสผ่าน";
$lang['forgot_password'] = "ลืมรหัสผ่านหรือเปล่า ?";
$lang['validate.required_username'] = "กรุณากรอกชื่อบัญชีผู้ใช้ครับ";
$lang['validate.required_password'] = "กรุณากรอกรหัสผ่านครับ";
$lang['validate.required_captcha'] = "กรุณากรอกรหัสรักษาความปลอดภัย";
$lang['login'] = "เข้าระบบ";
$lang['captcha_code'] = "รหัสรักษาความปลอดภัย";
$lang['please_enter_character_or_numberic_on_left'] = "กรุณาพิมพ์รหัสรักษาความปลอดภัยที่เห็นด้านบนครับ";
$lang['validate.captcha_incorrect'] = "คุณกรอกรหัสรักษาความปลอดภัยไม่ถูกต้องครับ";

return $lang; 