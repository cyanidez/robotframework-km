<?php
class Auth_model extends MY_Model {
	function __construct()
	{
		parent::__construct(); 
	}

	function get_authen_data($condition = NULL, $mode = 'email') 
	{
		if (is_null($condition) OR empty($condition)) 
		{
			return NULL; 
		}
		
		$select = parent::select(); 
		$select->from(array('M' => 'member')
							, array('M.id', 'M.email', 'M.password', 'M.mobile', 'M.active_status'));

		if ($mode == 'email') 
		{
			$select->where('M.email = ?', $condition); 
		}
		elseif ($mode == 'id')
		{
			$select->where('M.id = ?', $condition);
		}
		return parent::fetch_row($select); 					

	}


}