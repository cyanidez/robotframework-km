<?php
class Open_ids_model extends MY_Model {
	function __construct() 
	{
		parent::__construct();
	}

	function create($params = NULL) 
	{
		if (is_null($params) OR count($params) == 0)
		{
			return  NULL; 
		}

		if (parent::insert('open_ids', $params) !== FALSE) 
		{
			return TRUE; 
		}
		return FALSE; 
	}

	function update($params = NULL, $where = NULL) 
	{
		if (is_null($params) OR count($params) == 0)
		{
			return NULL; 
		}
		if (is_null($where) OR count($where) == 0) 
		{
			return NULL; 
		}

		if (parent::update('open_ids', $params, $where) !== FALSE) 
		{
			return TRUE; 
		}
		return FALSE; 
	}


	function is_connected($uid = NULL, $service = 'facebook') 
	{
		if (is_null($uid) OR empty($uid))
		{
			$return['status'] = FALSE; 
			return $return; 
		}
		if (is_null($service) OR empty($service))
		{
			$return['status'] = FALSE; 
			return $return; 
		}
		
		$select = parent::select();

		$select->from(array('O' => 'open_ids'), array('O.memberID', 'O.access_token', 'O.dump', 'O.service'))
					//->join(array('M' => 'member'), 'O.memberID = M.memberID', array('M.username'))
					->where('O.uid = ?', $uid)
					->where('O.service = ?', $service);

		$query = parent::fetch_row($select);

		if ( ! empty($query['memberID'])) 
		{
			$return['dump'] = $query['dump']; 
			$return['memberID'] = $query['memberID']; 
			$return['status'] = TRUE; 
			return $return; 
		}
		else
		{
			$return['status'] = FALSE; 
			return $return; 
		}
		

	}

	function get_service_detail($uid = NULL, $service = 'facebook') 
	{
		if (is_null($uid) OR empty($uid))
		{
			return NULL; 
		}
		if (is_null($service) OR empty($service))
		{
			return NULL; 
		}

		$select = parent::select();
		$select->from(array('O' => 'open_ids'), array('O.memberID', 'O.dump', 'O.uid', 'O.access_token', 'O.service'))
						->where('O.uid = ?', $uid)
						->where('O.service = ?', $service);
	
		return parent::fetch_row($select);
	}
}