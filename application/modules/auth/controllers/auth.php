<?php
class Auth extends Web_Controller { 
	function __construct() 
	{
		parent::__construct(); 
		$this->load->model('auth_model', 'auth'); 
		
		$this->load->library('bmtd_encrypt');
		$this->load->library('form_validation');
		$this->lang->load('auth', 'auth'); 
		$this->load->library('securimage');

	}

	public function index() 
	{
		$data = array(); 

		$email = $this->input->post('signin_email', TRUE); 
		$password = $this->input->post('signin_password', TRUE); 

		
		$return = array();
		if ($this->input->server('REQUEST_METHOD') != "POST")
		{
			$return = array(
				'code' => 400,
				'status' => '400 Error',
				'message' => 'Method now allowed'
			);
			echo json_encode($return, true);
			exit;
		}
		if ( ! empty($email) && ! empty($password))
		{
			if ($this->securimage->check($this->input->post('signin_captcha_code', TRUE)) == FALSE) 
			{
				$return['message'] = lang('validate.captcha_incorrect');
				$return['valid'] = "captcha";
			}
			else
			{

				// if ($this->form_validation->valid_email($email))
				// {
				// 	$mode = 'email'; 
				// }
				// else
				// {
				// 	$mode = 'username'; 
				// }

				$result =$this->auth->get_authen_data($email); 
				#alert($result, 'red');
				
				if (isset($result['id']) && ! empty($result['id'])) 
				{
					if ($password == $this->bmtd_encrypt->my_decrypt($result['password']))
					{
						if ($result['active_status'] == "Y") 
						{		
							$session = array(
								'id' => $result['id'],
								'email' => $result['email'],
								'mobile' => $result['mobile']
							);
							set_member_profile($session);			
							 
							$return['code'] = 200;
							$return['status'] = "200 OK"; 
						}
						else
						{
							$return['code'] = 400;
							$return['status'] = "400 Error"; 
							$return['message'] = lang('account_suspsend');
							$return['valid'] = "email";
						}
					}
					else
					{
						$return['code'] = 400;
						$return['status'] = "400 Error"; 
						$return['message'] = lang('wrong_password'); 
						$return['valid'] = "password";
					}
				}
				else
				{
					$return['code'] = 404;
					$return['status'] = "404 Not found"; 
					$return['message'] = lang('do_not_found_username_or_email'); 
					$return['valid'] = "email";
				}		


				//-- Login Success ---//
			}
			//-- Check Captcha ---//
		} 
		//--- Post $username & $password ---// 

		echo json_encode($return, true);
		// parent::seo_title(lang('title.signin'));
		// parent::seo_keyword(lang('keyword.signin'));
		// parent::seo_description(lang('description.signin'));

		// $this->breadcrumb->make(lang('home'), site_url());
		// $this->breadcrumb->make(lang('signin'), NULL, TRUE, TRUE);
		// $this->template->write('breadcrumb', $this->breadcrumb->make_bread());
		// $this->template->add_css('bootstrap-customize.css', 'front');
		// $this->template->add_js('auth.js', 'front');
		// #$this->template->write('icon_header', '<img src="'.image_path('icon/icon-signin.png', 'front').'" alt="" />', $data); 
		// $this->template->write_view('content', 'auth_view', $data); 
		// $this->template->render(); 
		
	}

		

	function signout()
	{
		delete_member_profile('member_profile');
		redirect('/', 'location'); 
		exit; 
	}


	function fb_logout() 
	{
		setcookie('fbs_'.$facebook->getAppId(), '', time()-100, '/', 'domain.com');
		session_destroy();
		redirect(site_url(), 'location');
		exit; 
	}

	function not_authorize() 
	{
		$data = array(); 

		$data['code'] = $this->input->get_post('code', TRUE); 
		
		$this->template->write_view('content', 'not_authorize', $data); 
		$this->template->render(); 
	}

	function access()
	{
		$data = array();

		$this->template->write_view('content', 'access', $data);
		$this->template->render();
	}

}