<?php
class Connect extends Web_Controller { 
	function __construct()
	{
		parent::__construct();
		$this->load->model('open_ids_model', 'open_ids');
		$this->load->model('member/member_model', 'member');
		$this->load->model('auth/auth_model', 'auth');
		#$this->load->model('geo_location/geo_location_model', 'geo_location');		
		$this->load->config('site_config'); 
		$this->load->helper('date');
		$this->load->library('form_validation');
		$this->load->helper('log');
		$this->load->helper('directory');		
	}

	function index()
	{
		//--- Nothing... :) ---//
	}

	function signin()
	{
		$data = array();		
		$fields = array();
		$fb_user = $this->facebook->getUser();
		$access_token = $this->facebook->getAccessToken();
		if ( ! empty($fb_user))
		{
			try 
			{
				$fb = $this->facebook->api('/me');			
			}
			catch (FacebookApiException $e) 
			{
				#echo $e->getMessage(); 
				write_log($e->getMessage(), 'signup_with_facebook');
				$user = null;
			}
		}

		if (isset($fb['id'])) 
		{
			$is_error = FALSE; 

			//--- ������բ������ Open_ids �����ѧ ---// 
			$open_ids = $this->open_ids->is_connected($fb['id'], 'facebook'); 		

			#alert($open_ids, 'blue');

			$fb_profile = $fb;

			//--- �ѧ����ա���� Facebook connect �Ѻ�к� Songparty ---// 
			if ($open_ids['status'] == FALSE) 
			{				
				$authen = $this->auth->get_authen_data($fb_profile['email'], 'email');
				
				//--- If not yet be member before (email from facebook don't match in our member table ---//
				if (empty($authen['memberID']))
				{
					#echo  "<h1>11111111111</h1>"; 
					unset($fields);
					#$geo_location = $this->geo_location->get_country_by_ip(dot_2_long_ip($this->input->ip_address()));
					
					#write_log('not yet member', 'signup_with_facebook');
					$sex = ($fb_profile['gender'] == "male") ? "M" : "F"; 
					$avatar = $this->_save_avatar($fb_profile['id'], $fb_profile['id']);
					#echo '<p>filename = '.$filename.'</p>';
					#exit; 
					$fields = array(
									'member_levelID' => 1, 
									'username' => $fb_profile['id'],  //-- User facebook_id be username 
									'password' => $this->chs_encrypt->my_encrypt('member'),  //--- Set up default password ---//
									'firstname' => $fb_profile['first_name'],
									'lastname' => $fb_profile['last_name'],
									'email' => $fb_profile['email'],
									'nickname' => $fb_profile['name'],
									'birthday' => facebook_to_db($fb_profile['birthday']),
									'sex'=> $sex,
									'signed_up_type' => 'facebook',
									'avatar' => $avatar['file_name'],
									'status_flg' => 'Y',
									'signed_up_date' => date("Y-m-d H:i:s"),
									'last_logged_in_ip' => $this->input->ip_address()
									
								);
					#echo '<h1>member data</h1>';
					#alert($fields, 'red');
					#write_log('field member = '.print_r($fields, TRUE), 'signup_with_facebook');
					if ( ! $this->member->create($fields))
					{
						echo 'create fail';
					}


					$memberID = $this->member->get_last_insert_id(); 

					$session = array(
									'memberID' => $memberID,
									'email' => $fb_profile['email'],
									'username' => $fb_profile['first_name'].' '.$fb_profile['last_name'],
									'member_name' => $fb_profile['first_name'].' '.$fb_profile['last_name'],
									'logged_in_date' => date("Y-m-d H:i:s"),
									'member_levelID' => 1,
									'fb_id' => $fb_profile['id'],
									'is_fb' => 1
								);
				}
				else
				{
					#echo  "<h1>222222222222</h1>"; 
					$session = array(
									'memberID' => $authen['memberID'],
									'email' => $authen['email'],
									'username' => $fb_profile['first_name'].' '.$fb_profile['last_name'],
									'logged_in_date' => date("Y-m-d H:i:s"),
									'member_levelID' => $authen['member_levelID'],
									'fb_id' => $fb_profile['id'],
									'is_fb' => 1
								);
					$memberID = $authen['memberID'];
				}

				#echo '<h1>session</h1>'; 
				#alert($session, 'blue');
				
				//--- Set Session ---//
				set_member_profile($session);

				unset($fields);
				//--- Create open_ids ---// 
				$fields = array(
								'memberID' => $memberID,
								'uid' => $fb_profile['id'],
								'access_token' => $this->facebook->getAccessToken(),
								'service' => 'facebook',
								'dump' => json_encode($fb_profile),
								'status_flg' => 'Y',
								'created_date' => date("Y-m-d H:i:s")
							);
				
				#echo '<h1>Open_ids</h1>'; 
				#alert($fields, 'red'); 
				if ($this->open_ids->create($fields))
				{
					if ($this->input->get_post('redir', TRUE))
					{
						redirect(site_url($this->input->get_post('redir', TRUE)), 'location');
					}
					else
					{
						redirect(home_url(), 'location');
					}
					exit; 
				}				
			}
			//--- ���� Account facebook connect (open_ids) �Ѻ�к� Songparty ���� ���Щй�鹵�ͧ�� Account ������͹  --// 
			else
			{				
				$member = $this->auth->get_authen_data($open_ids['memberID'], 'memberID');				
				$member['fb_id'] = $fb_profile['id']; 
				$member['is_fb'] = TRUE; 

				$session = $member; 
				$session['username'] = $member['member_name'];
				set_member_profile($session);				
			}

			if ($is_error !== TRUE) 
			{
				if ($this->input->get_post('redir', TRUE)) 
				{
					redirect(site_url($this->input->get_post("redir", TRUE)), 'location');
					exit; 
				}
				else
				{
					redirect(home_url(), 'location');
					exit; 
				}
			}
		}
		//--- Check  has cookie from facebook ---//

		//--- If still don't have cookie then redirect again (cookie should be sync to our domain) ---// 
		else
		{
			redirect($this->input->server('HTTP_REFERER'), 'location');
			exit; 
		}
	}

	private function _save_avatar($username = NULL, $fb_id = NULL) 
	{
		//--- Create folder for contained avatar ---//
		$this->load->config('path_config');
		$this->load->config('site_config');
		$site_cdn_path = site_cdn_path();
		#write_log("avatar_url = ".$avatar_url, 'signup_with_facebook');

		$avatar_date_dir = $site_cdn_path.'/avatar/'.date("Y/m/d");
		$dir = make_dir($avatar_date_dir);

		#write_log("dir = ".$img_decode, 'signup_with_facebook');
		
		$img = file_get_contents('https://graph.facebook.com/'.$fb_id.'/picture?type=large');
		$img_decode = imagecreatefromstring($img);
		#write_log("img_decode = ".$img_decode, 'signup_with_facebook');
		
		imagejpeg($img_decode, $dir . '/'.$username.'.jpg');
		
		$return = array();
		$return['full_path'] = $avatar_date_dir . '/' . $username . ".jpg"; 
		$return['file_name'] = $username.".jpg"; 
		
		return $return; 
		#return $username.'.jpg'; 
		
	}

	private function _upload_avatar($field, $filename = NULL) 
	{
		$this->load->helper('upload'); 
		$this->load->helper('image'); 
		$this->load->config('site_config'); 
		$this->load->config('path_config'); 
		$this->load->helper('directory'); 
		
		$site_cdn_path = site_cdn_path();
		
		$dir = make_dir($site_cdn_path . '/avatar/' . date("Y/m/d"));

		if ( ! is_null($filename) && ! empty($filename)) 
		{
			$file_name = $filename; 
		}
		else
		{
			$file_name = time(); 
		}

		$config['file_name'] = $file_name; 
		$config['upload_path'] = $dir; 
		$config['allowed_types'] = $this->config->item('avatar.filetype'); 
		$config['max_size']	= $this->config->item('avatar.size'); 
		$config['max_width']  = $this->config->item('avatar.width'); 
		$config['max_height']  = $this->config->item('avatar.height'); 

		$resp = do_upload($field, $config); 

		if ( ! isset($resp['error'])) 
		{
			$out['status'] = TRUE; 
			$out['response'] = $resp['response']; 
		}
		else 
		{
			$out['status'] = FALSE;
			$out['response']['error'] = $resp['error']; 
		}

		return $out; 	
	}
}