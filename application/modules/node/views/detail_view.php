<h4 class="classic-title"><span><b><?php echo $node['name']; ?></b></span></h4>
                        
	<!-- Some Text -->
	
  <?php if ( ! empty($node['image'])) : ?>
	<p style="text-align:center;">
		<img src="<?php echo site_node_url($node['image']); ?>" alt="" class="img-responsive" />		
	</p>
  <?php endif; ?>
  
  <h3><?php echo $node['name']; ?></h3>
  <?php if ( ! empty($node['name_2'])) : ?>
  <p></p><p></p>
  <h3><?php echo $node['name_2']; ?></h5>
  <?php endif; ?>
    
	
	<p><?php echo $node['description']; ?></p>

  <?php if ( ! empty($node_paragraph)) : ?>
    <?php foreach ($node_paragraph as $key => $value) : ?>
    <img src="<?php echo site_node_url($value['image']); ?>">

    <?php echo $value['description']; ?>
    <?php endforeach; ?>
  <?php endif; ?>

<?php if ( ! empty($node_file)) : ?>
<div class="latest-posts-classic">

  <!-- Post 1 -->
  <?php foreach ($node_file as $key => $value) : ?>
  <div class="post-row">
    <div class="col-md-5">
      <div class="widget-thumb">
        <?php if ($value['file_ext'] == ".png" or $value['file_ext'] == ".jpg" or $value['file_ext'] == ".gif") : ?>
        <a href="<?php echo site_node_file_url($value['file_full_path']); ?>"><img src="<?php echo site_node_file_url($value['file_full_path']); ?>" class="img-responsive" style="margin-bottom:30px;"></a>
        <?php elseif ($value['file_ext'] == ".docx" or $value['file_ext'] == ".doc") : ?>
        <img class="img-responsive" style="height:80px;"  src="<?php echo image_path('ico_doc.png', 'front'); ?>">
        <?php elseif ($value['file_ext'] == ".xlsx" or $value['file_ext'] == ".xls") : ?>
        <img class="img-responsive" style="height:80px;" src="<?php echo image_path('ico_xls.gif', 'front'); ?>">
        <?php elseif ($value['file_ext'] == ".pdf") : ?>
        <img class="img-responsive" style="height:80px;" src="<?php echo image_path('ico_pdf.jpg', 'front'); ?>">
        <?php endif; ?>
      </div>
    </div>
    <div class="col-md-7">
      <h3 class="post-title"><a target="_blank" href="<?php echo site_node_file_url($value['file_full_path']); ?>"><?php echo $value['name_'.LANGCODE]; ?></a></h3>
      <h5 class="post-title"><a target="_blank" href="<?php echo site_node_file_url($value['file_full_path']); ?>"><?php echo floor($value['file_size']); ?> KB</a></h5>
      <div class="post-content">
        <p> <a class="read-more" target="_blank" href="<?php echo site_node_file_url($value['file_full_path']); ?>"><?php echo lang('ดาวน์โหลด'); ?> <i class="fa fa-angle-right"></i></a></p>
      </div>
    </div>
  </div>           
  <?php endforeach; ?>       
</div>       
<?php endif; ?>          

<?php if ( ! empty($node_tag)) : ?>                        
<div class="post-bottom clearfix">
  <div class="post-tags-list">
    <span><?php echo lang('Tag'); ?> :</span>
    <?php foreach ($node_tag as $key => $value) : ?>
    <a href="#"><?php echo $value['name']; ?></a>
    <?php endforeach; ?>
  </div>
</div>
<?php endif; ?>
 
<!-- Divider -->
<div class="hr3" style="margin-top:30px; margin-bottom:45px;"></div>




    <!-- End Content -->