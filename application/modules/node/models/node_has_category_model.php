<?php
class Node_has_category_model extends MY_Model { 
	public function __construct()
	{
		parent::__construct();
	}

	public function get_node_category($node_id = null)
	{
		if (empty($node_id))
		{
			return null;
		}
		$select = parent::select();
		$select->from(array('C' => 'categories'), array('C.id'))
			->join(array('CL' => 'category_lang'), 'C.id = CL.category_id AND CL.lang_id = '.LANGID, array('CL.name'))
			->join(array('NC' => 'node_has_category'), 'NC.category_id = C.id', array('NC.category_id', 'NC.level'))
			
			->where('NC.node_id = ?',$node_id);

		#echo $select->__toString();

		$data =  parent::fetch_all($select);
		return $data; 
	}
}