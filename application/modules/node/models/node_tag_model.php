<?php
class Node_tag_model extends MY_Model { 
	public function __construct()
	{
		parent::__construct();
	}

	public function get_node_tag($node_id = null)
	{
		if (empty($node_id))
		{
			return null;
		}
		$select = parent::select();
		$select->from(array('NT' => 'node_tags'), array('NT.id', 'NT.name', 'NT.clicked'))
			->where('NT.node_id = ?', $node_id);

		return parent::fetch_all($select);
	}
}