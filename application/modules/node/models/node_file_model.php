<?php
class Node_file_model extends MY_Model { 
	public function __construct()
	{
		parent::__construct();
	}

	public function get_node_file($node_id = null)
	{
		if (empty($node_id))
		{
			return null;
		}
		$select = parent::select();
		$select->from(array('NF' => 'node_files'), array('*'))
			->where('NF.node_id = ?', $node_id)
			->order('NF.sort_order asc');

		return parent::fetch_all($select);
	}
}