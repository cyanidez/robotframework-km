<?php
class Node_model extends MY_Model {

    private $_table = "nodes";
    private $_table_lang = "node_lang";

    public function get_node($args = array(), $page = 1, $limit = 20)
    {
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.thumbnail_resize', 'N.created_at'))
                ->joinLeft(array('NL' => $this->_table_lang), 'NL.node_id = N.id AND NL.lang_id = '.LANGID, array('NL.name', 'NL.name_2', 'NL.short_description', 'NL.description'))
        

                ->where('N.active_status = ?', 'publish');

        if ( ! empty($args['keyword']))
        {
            $select->where('NL.name LIKE ?', '%'.$args['keyword'].'%');
        }
        if ( ! empty($args['category_id']))
        {
            $select->join(array('NC' => 'node_has_category'), 'NC.node_id = N.id', array())
                ->where('NC.category_id = ?', $args['category_id']);
        }
        $select->where('N.deleted_at IS NULL')
            ->order('N.sort_order asc');

        #echo $select->__toString();
        #exit;
        $data = parent::fetch_page($select, array(), $page, $limit);
        return $data;
    }

    public function get_node_by_keyword($q = null)
    {
        if (empty($q))
        {
            return null; 
        }
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.thumbnail_resize', 'N.created_at'))
            ->joinLeft(array('NL' => $this->_table_lang), 'NL.node_id = N.id', array('NL.name', 'NL.name_2', 'NL.short_description', 'NL.description'))
            ->where('N.active_status = ?', 'publish')
            ->where('N.deleted_at IS NULL')
            ->where('(NL.name LIKE ?', '%'.$q.'%')
            ->orWhere('NL.short_description LIKE ?', '%'.$q.'%')
            ->orWhere('NL.description LIKE ? )', '%'.$q.'%')
            
            ->order('N.sort_order asc');

        #echo $select->__toString();
        #exit;
        $data = parent::fetch_all($select);
        return $data;
    }

    public function get_latest_node($is_news = false)
    {
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.created_at'))
                ->joinLeft(array('NL' => $this->_table_lang), 'NL.node_id = N.id AND NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description', 'NL.description'))
                ->where('N.active_status = ?', 'publish')
                ->where('N.deleted_at IS NULL')
                ->limit(10); 
        if ( ! empty($is_news))
        {
            #$select->join(arra('where('NL.name = ?', 'ข่าวประชาสัมพันธ์')

        }
        return parent::fetch_all($select);
    }
    public function get_node_detail($id = NULL)
    {
        if (empty($id))
        {
            return NULL;
        }

        $return = array();

        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.thumbnail_resize', 'N.image', 'N.created_at'))
            ->joinLeft(array('NL' => $this->_table_lang), 'N.id = NL.node_id and NL.lang_id = '.LANGID, array('NL.name', 'NL.name_2', 'NL.short_description', 'NL.description', 'NL.seo_title', 'NL.seo_description', 'NL.seo_keyword'))
            ->join(array('NC' => 'node_has_category'), 'N.id = NC.node_id', array('NC.category_id AS has_category_id'))
            ->where('N.id = ?', $id)
            ->where('N.active_status = ?', 'publish')
            ->where('N.deleted_at IS NULL');
            #echo $select->__toString();
        $data = parent::fetch_row($select);
        if (empty($data))
        {
            return NULL;
        }
        $return = array(
            'id' => $data['id'],
            'active_status' => $data['active_status'],
            'name' => $data['name'],
            'name_2' => $data['name_2'],
            'slug' => $data['slug'],
            'thumbnail' => $data['thumbnail'],
            'thumbnail_resize' => $data['thumbnail_resize'],
            'image' => $data['image'],
            'short_description' => $data['short_description'],
            'description' => $data['description'],
            'seo_title' => $data['seo_title'],
            'seo_description' => $data['seo_description'],
            'seo_keyword' => $data['seo_keyword'],
            'has_category_id' => $data['has_category_id']
        );

        $select = parent::select();

        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.active_status', 'N.slug'))
            ->join(array("NL" => $this->_table_lang), 'N.id = NL.node_id AND NL.lang_id != '.LANGID, array('NL.node_id as translate_id', 'NL.name', 'NL.name_2', 'NL.short_description', 'NL.description', 'NL.seo_title', 'NL.seo_description', 'NL.seo_keyword'))
            ->join(array('L' => 'lang'), 'L.id = NL.lang_id', array('L.code'))
            ->where('N.id  = ?', $id)
            ->where('N.active_status = ?', 'publish');

        $data = parent::fetch_all($select);
        if ( ! empty($data))
        {
            $translate = array();
            foreach ($data as $key => $value)
            {
                $translate[$value['code']] = array(
                    'id' => $value['translate_id'],
                    'name' => $value['name'],
                    'name_2' => $value['name_2'],
                    'slug' => $value['slug'],
                    'short_description' => $value['short_description'],
                    'description' => $value['description'],
                    'seo_title' => $value['seo_title'],
                    'seo_description' => $value['seo_description'],
                    'seo_keyword' => $value['seo_keyword']
                );
            }
        }

        if ( ! empty($translate))
        {
            $return['translate'] = $translate;
        }
        else
        {
            $return['translate'] = array();
        }
        return $return;
    }

    public function create($params = NULL)
    {
        if (empty($params))
        {
            return NULL;
        }

        if (parent::insert($this->_table, $params) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function update($params = NULL, $where = NULL)
    {
        if (empty($params))
        {
            return NULL;
        }

        if (empty($where))
        {
            return NULL;
        }

        if (parent::update($this->_table, $params, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($where = NULL)
    {
        if (empty($where))
        {
            return NULL;
        }
        if (parent::delete($this->_table, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    
}