<?php
class Node_paragraph_model extends MY_Model {

    private $_table = "node_paragraph";
    private $_table_lang = "node_paragraph_lang";

    public function get_node_paragraph($node_id = null)
    {
        if (empty($node_id))
        {
            return null;
        }
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.image'))
                ->join(array('NL' => $this->_table_lang), 'NL.node_paragraph_id = N.id AND NL.lang_id = '.LANGID, array('NL.description'))
                ->where('N.node_id = ?', $node_id)
                ->order('N.paragraph_no asc');

        #echo $select->__toString();
        #exit;
        $data = parent::fetch_all($select);
        return $data;
    }
    

    public function create($params = NULL)
    {
        if (is_null($params) OR count($params) == 0)
        {
            return NULL;
        }

        if (parent::insert($this->_table, $params) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function update($params = NULL, $where = NULL)
    {
        if (is_null($params) OR count($params) == 0)
        {
            return NULL;
        }

        if (is_null($where) OR count($where) == 0)
        {
            return NULL;
        }

        if (parent::update($this->_table, $params, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($where = NULL)
    {
        if (is_null($where)OR count($where) == 0)
        {
            return NULL;
        }
        if (parent::delete($this->_table, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    
}