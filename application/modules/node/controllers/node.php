<?php
class Node extends Web_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('node_model', 'node');
        $this->load->model('node_has_category_model', 'node_category');
        $this->load->model('node_tag_model', 'node_tag');
        $this->load->model('node_paragraph_model', 'node_paragraph');
        $this->load->model('node_file_model', 'node_file');
        $this->load->model('banner/banner_model', 'banner');

        $this->load->model('aira_group/aira_group_model', 'aira_group');
    }

    public function index($id = null)
    {
        $data = array();
        
        $node = $data['node'] = $this->node->get_node_detail($id);

        $data['node_category'] = $node_category = $this->node_category->get_node_category($id);
        #alert($node_category, 'blue');
        #exit;

        $data['id'] = $id; 
        $data['slug'] = $node['slug'];

        $arr_category_id = array();
        if ( ! empty($node_category))
        {
            foreach ($node_category as $key => $value) 
            {
                $arr_category_id[] = $value['category_id'];
            }
        }

        
        if (empty($data['node']['id']))
        {
            $this->template->write_view('content', 'error/error_view', $data);
        }
        else 
        {
            
            parent::seo_title($node['seo_title']);
            parent::seo_keyword($node['seo_keyword']);
            parent::seo_description($node['seo_description']);
            
            $data['node_tag'] = $this->node_tag->get_node_tag($id);
            $data['node_file'] = $this->node_file->get_node_file($id);
            $data['node_paragraph'] = $this->node_paragraph->get_node_paragraph($id);
            
            $category_parent = $this->category->get_category_detail($node_category[0]['id']);
            #$this->breadcrumb->make(lang('หน้าแรก'));
            #$this->breadcrumb->make();
            if ( ! empty($category_parent['name']))
            {
                $this->breadcrumb->make($category_parent['name'], null, true, true);   
            }
            if ( ! empty($node['name']))
            {
                $this->breadcrumb->make($node['name'], null, true, true);
            }
            parent::set_breadcrumb();

            #alert($node_category);
            #exit;
            parent::set_sidebar($node_category, $node);
           
            $data['aira_group_list'] = $this->aira_group->get_all_aira_group();

            parent::set_partner();
            
            
            
            #$this->template->write('subject', $node['name'], true);
            $this->template->write_view('content', 'detail_view', $data);

        }
        $this->template->render();
    }
}