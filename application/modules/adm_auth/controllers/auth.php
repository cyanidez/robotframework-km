<?php
class Auth extends  MY_Controller { 
	function __construct()
	{
		parent::__construct();
		$this->load->model('auth_model', 'auth');
		$this->lang->load('auth', 'adm_auth');
		$this->load->library('bmtd_encrypt');
	}

	function index()
	{	
		$data = array();

        #alert($_POST, 'red');
        #exit;

		if ($this->input->post('login_button', TRUE))
		{
			$username = $this->input->post('username', TRUE); 
			$password = $this->input->post('password', TRUE); 
			
			$authen = $this->auth->get_authen_data($username); 
	
			#echo $this->pho_encrypt->my_decrypt($authen['password']);
			#echo "usernaem = ".$username;
			#echo "password = ".$password;
			#exit;
			if ( ! empty($authen['id']))
			{
				if ($this->bmtd_encrypt->my_decrypt($authen['password']) == $password)
				{

					if ($authen['active_status'] == "Y")
					{
						$session = array(
							'username' => $authen['username'],
							'user_id' => $authen['id'],
							'user_level_id' => $authen['user_level_id'],
							'email' => $authen['email'],
							'last_logged_in' => date("Y-m-d H:i:s")
						);
						
						set_admin_profile($session);
						redirect(site_admin_url('dashboard'), 'location');
					}
					else
					{
					}
				}
				else
				{
					$data['status'] = FALSE; 
					$data['message'] = ""; 
				}
			}
			else
			{
				$data['status'] = FALSE; 
				$data['message'] = lang('');
			}
		}

        parent::set_layouts('login');
		$this->template->write_view('content', 'auth_main', $data);
		$this->template->render();	
	}

	function logout()
	{
		delete_admin_profile();
		redirect(site_admin_url(), 'location');
	}
}