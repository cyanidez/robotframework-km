<div class="dialog">
    <div class="panel panel-default">
        <p class="panel-heading no-collapse">MEMBER SIGN IN</p>
        <div class="panel-body">
            <form id="auth_form" name="auth_form"  method="post" action="<?php echo site_admin_url('auth'); ?>">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" name="username" id="username" class="form-control span12">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" id="password" class="form-controlspan12 form-control">
                </div>
                <input type="submit" name="login_button" id="login_button" class="btn btn-primary btn-large" value="<?php echo lang('Sign In'); ?>"  />
                <div class="clearfix"></div>
            </form>
        </div>
    </div>
    <p><a href="<?php echo site_admin_url('user/forgot_password'); ?>">&raquo; Forgot password? Click here to reset password.</a></p><p style="font-size: .75em; margin-top: .25em;">Developed & License by : <img src="http://www.aseanwebdesign.com/images/awd_icon.png" width="11" height="11" style="margin-bottom:.20em; vertical-align:middle;"> <a href="http://www.aseanwebdesign.com" target="_blank">AseanWebDesign™</a></p>
</div>

<?php if ($this->input->get_post('debug')) : ?>
<?php echo $this->bmtd_encrypt->my_encrypt('root'); ?>
<?php endif; ?>
