<?php
class Auth_model extends MY_Model { 
	function __construct()
	{
		parent::__construct();
	}

	function get_authen_data($username = NULL)
	{
		if (is_null($username) OR empty($username))
		{
			return NULL;
		}

		$select = parent::select(); 
		
		$select->from(array('U' => 'user'), array('U.id', 'U.username', 'U.password', 'U.active_status', 'U.user_level_id'))
			->join(array('UL' => 'user_level'), 'U.user_level_id = UL.id', array('UL.name'))
			->where('U.username = ?', $username); 
		#echo $select->__toString();
		return parent::fetch_row($select); 
	}

	
}