<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Menu_model extends MY_Model {
	function __construct() 
	{
		parent::__construct(); 
	}

	
	function get_menu() 
	{
		if (get_admin_profile('user_level_id') == 1 or get_admin_profile('user_level_id') == 2) 
		{
			$menu = $this->_get_admin_menu(); 
		}
		else 
		{
			$menu = $this->_get_authen_menu() ;
		}

		return $menu; 

	}

	private function _get_admin_menu() 
	{
		$select = parent::select(); 
		$select->from(array('M' => 'menus'), array('M.id', 'M.parent', 'M.level', 'M.module', 'M.controller', 'M.method', 'M.status_flg', 'M.menu_path', 'M.menu_name'))
					->where('M.is_main_action = ?', 'Y')
					->where('M.level = ?', 1)
					->where('M.status_flg = ?', 'Y'); 

		$query = parent::fetch_all($select); 

		$out = array(); 
		$i = 0;
		foreach ($query as $key => $value) 
		{
			$child = $this->_get_child($value['id']); 
			$out[$i]['id'] = $value['id']; 
			$out[$i]['parent'] = $value['parent']; 
			$out[$i]['level'] = $value['level']; 
			$out[$i]['module'] = $value['module']; 
			$out[$i]['controller'] = $value['controller']; 
			$out[$i]['method'] = $value['method']; 
			$out[$i]['menu_path'] = $value['menu_path']; 
			$out[$i]['menu_name'] = $value['menu_name']; 

			$j = 0;
			$out2 = array(); 
			foreach ($child as $key2 => $value2) 
			{
				$out2[$j]['id'] = $value2['id']; 
				$out2[$j]['parent'] = $value2['parent']; 
				$out2[$j]['level'] = $value2['level']; 
				$out2[$j]['module'] = $value2['module']; 
				$out2[$j]['controller'] = $value2['controller']; 
				$out2[$j]['method'] = $value2['method']; 
				$out2[$j]['menu_path'] = $value2['menu_path']; 
				$out2[$j]['menu_name'] = $value2['menu_name']; 

				$j++;
			}
			$out[$i]['child'] = $out2; 
			$i++; 
		}

		return $out; 
	}

	private function _get_child($parent = NULL) 
	{
		if (is_null($parent) OR empty($parent)) 
		{
			return NULL; 
		}

		$select = parent::select(); 
		$select->from(array('M' => 'menus'), array('M.id', 'M.status_flg', 'M.parent', 'M.controller', 'M.module', 'M.method', 'M.level', 'M.menu_name', 'M.menu_path'))
					->where('M.parent = ?',  $parent)
					->where('M.is_main_action = ?', 'Y')
					->where('M.status_flg = ?', 'Y')
					->order('M.sort ASC'); 

		return parent::fetch_all($select); 
	}

	private function _get_child_authen($parent = NULL) 
	{
		if (is_null($parent) OR empty($parent)) 
		{
			return NULL; 
		}

		$select = parent::select(); 
		$select->from(array('M' => 'menus'), array('M.id', 'M.status_flg', 'M.parent', 'M.controller', 'M.module', 'M.method', 'M.level', 'M.menu_name', 'M.menu_path'))
			->join(array('A' => 'authentications'), 'A.menu_id = M.id AND A.user_level_id = '.get_admin_profile('user_level_id'), array())
			->where('M.parent = ?',  $parent)
			->where('A.menu_type = ?', 'menu')
			->where('M.is_main_action = ?', 'Y')
			->where('M.status_flg = ?', 'Y')
			->order('M.sort ASC'); 

		return parent::fetch_all($select); 
	}

	private function _get_authen_menu() 
	{
		

		$select = parent::select(); 
		$select->from(array('M' => 'menus'), array('M.id', 'M.parent', 'M.level', 'M.module', 'M.controller', 'M.method', 'M.status_flg', 'M.menu_name', 'M.menu_path'))
			->join(array('A' => 'authentications'), 'M.id = A.menu_id AND A.user_level_id = '.get_admin_profile('user_level_id'), array())
			->where('M.is_main_action = ?', 'Y')
			->where('A.menu_type = ?', 'menu')
			->where('M.level = ?', 1)
			->where('M.status_flg = ?', 'Y')
			->order('M.sort ASC');
		
		$query = parent::fetch_all($select); 

		$out = array(); 
		$i = 0;
		foreach ($query as $key => $value) 
		{
			$child = $this->_get_child_authen($value['id']); 
			$out[$i]['id'] = $value['id']; 
			$out[$i]['parent'] = $value['parent']; 
			$out[$i]['level'] = $value['level']; 
			$out[$i]['module'] = $value['module']; 
			$out[$i]['controller'] = $value['controller']; 
			$out[$i]['method'] = $value['method']; 
			$out[$i]['menu_path'] = $value['menu_path']; 
			$out[$i]['menu_name'] = $value['menu_name']; 

			$j = 0;
			$out2 = array(); 
			if ( ! empty($child))
			{
				foreach ($child as $key2 => $value2) 
				{
					$out2[$j]['id'] = $value2['id']; 
					$out2[$j]['parent'] = $value2['parent']; 
					$out2[$j]['level'] = $value2['level']; 
					$out2[$j]['module'] = $value2['module']; 
					$out2[$j]['controller'] = $value2['controller']; 
					$out2[$j]['method'] = $value2['method']; 
					$out2[$j]['menu_path'] = $value2['menu_path']; 
					$out2[$j]['menu_name'] = $value2['menu_name']; 

					$j++;
				}
			}
			$out[$i]['child'] = $out2; 
			$i++; 
		}

		return $out; 

	}
	
	
}

/* End of file menu_model.php */
/* Location: ./application/modules/adm_auth/models/menu_model.php */