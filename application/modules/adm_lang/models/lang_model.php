<?php
class Lang_model extends MY_Model { 
	public function __construct()
	{
		parent::__construct();
	}

	public function get_lang()
	{
		$select = parent::select();
		$select->from(array('L' => 'lang'))
			->where('active_status = ?', 'Y')
			->where('code != ?', DEFAULT_LANG_CODE);
		return parent::fetch_all($select);
	}
}