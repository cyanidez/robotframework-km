<?php
class Search extends Web_Controller { 
	public function __construct()
	{
		parent::__construct();
		$this->load->model('node/node_model', 'node');
		$this->load->model('news/news_model', 'news');
		$this->load->model('aira_group/aira_group_model', 'aira_group');
		$this->load->model('partner/partner_model', 'partner');
		$this->load->model('download/download_model', 'download');

		$this->lang->load('search', 'search', LANGCODE);
	}

	public function index()
	{
		$data['q'] = $q = $this->input->get_post('q');
		$data['page'] = $page = $this->input->get_post('page') ? $this->input->get_post('page', true) : 1; 
		$data['per_page'] = $per_page = $this->input->get_post('per_page') ? $this->input->get_post('per_page', true) : 20; 


		$args['node'] = $this->node->get_node_by_keyword($q);
		$args['news'] = $this->news->get_news_by_keyword($q);
		$args['aira_group'] = $this->aira_group->get_aira_group_by_keyword($q);
		$args['partner'] = $this->partner->get_partner_by_keyword($q);
		$args['download'] = $this->download->get_download_by_keyword($q);
		$data['total_rows'] = $return_data = $this->rearrange_data($args);
		$search = $this->set_pagination($return_data, $page, $per_page);
		#alert($search, 'red');
		#exit;
		$data['search'] = $search['data'];
		$data['total_page'] = $search['total_page'];
		parent::set_blank_sidebar();
		$this->breadcrumb->make(lang('ผลการค้นหาทั้งหมด'), null, true, true);
		parent::set_breadcrumb();
		$this->template->write_view('content', 'list_view', $data);
		$this->template->render();
	}
	private function rearrange_data($data = null)
	{
		$return = array();
		if ( ! empty($data['node']))
		{
			foreach ($data['node'] as $key => $value)
			{
				$return[] = array(
					'id' => $value['id'],
					'slug' => $value['slug'],
					'name' => $value['name'],
					'short_description' => $value['short_description'],
					'created_at' => $value['created_at'],
					'type' => 'node'

				);
			}
		}
		if ( ! empty($data['news']))
		{
			foreach ($data['news'] as $key => $value)
			{
				$return[] = array(
					'id' => $value['id'],
					'slug' => $value['slug'],
					'name' => $value['name'],
					'short_description' => $value['short_description'],
					'created_at' => $value['created_at'],
					'type' => 'news'
				);
			}
		}
		if ( ! empty($data['aira_group']))
		{
			foreach ($data['aira_group'] as $key => $value)
			{
				$return[] = array(
					'id' => $value['id'],
					'slug' => $value['slug'],
					'name' => $value['name'],
					'short_description' => $value['short_description'],
					'created_at' => $value['created_at'],
					'type' => 'aira_group'
				);
			}
		}
		if ( ! empty($data['partner']))
		{
			foreach ($data['partner'] as $key => $value)
			{
				$return[] = array(
					'id' => $value['id'],
					'slug' => $value['slug'],
					'name' => $value['name'],
					'short_description' => $value['short_description'],
					'created_at' => $value['created_at'],
					'type' => 'partner'
				);
			}
		}
		if ( ! empty($data['download']))
		{
			foreach ($data['download'] as $key => $value)
			{
				$return[] = array(
					'id' => $value['id'],
					'name' => $value['name'],
					'created_at' => $value['created_at'],
					'type' => 'download'
				);
			}
		}

		return $return; 
	}

	private function set_pagination($data = null, $page = 1, $per_page = 20)
	{
		$row_count = count($data);
		$total_page = ceil($row_count / $per_page);
		$start = ($page - 1) * $per_page;

		#echo "<p>start = ".$start.'</p>';
		#exit;

		$return = array();
		$return_data = array();
		if ( ! empty($data))
		{
			$count = 1;
			foreach ($data as $key => $value)
			{
				if (($key + 1) >= $start && $count <= $per_page)
				{
					$return_data[] = $value; 
					$count++;
				}
			}
		}
		#alert($return_data, 'red');
		#exit;
		#alert(count($data));
		#alert($per_page, 'red');
		#exit;
		$return['data'] = $return_data; 
		$return['total_page'] = $total_page;
		return $return; 
	}
}