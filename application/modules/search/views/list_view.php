<!-- Classic Heading -->
<h4 class="classic-title"><span><?php echo lang("ผลการค้นหาทั้งหมด"); ?></span></h4>

<div class="latest-posts-classic">

<!-- Post # -->

<?php if ( ! empty($search)) : ?>
  <?php foreach ($search as $key => $value) : ?>
  <?php #alert($value, 'red'); ?>
  <div class="post-row">
    
    <div class="col-md-12">
      <h3 class="post-title">
        <?php if ($value['type'] == "node") : ?>
        <a href="<?php echo node_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
        <?php elseif ($value['type'] == "news") : ?>
        <a href="<?php echo news_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
        <?php elseif ($value['type'] == "aira_group") : ?>
        <a href="<?php echo aira_group_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
        <?php elseif ($value['type'] == "partner") : ?>
        <a href="<?php echo partner_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
        <?php elseif ($value['type'] == "download") : ?>
        <a href="#">
        <?php endif; ?>


          <?php echo $value['name']; ?>
        </a>
      </h3>
        <span>
          <i class="fa fa-calendar"></i> 
          <?php echo show_style_date($value['created_at']); ?>           
        </span>
        <div class="post-content">
          <p>
            <?php if ($value['type'] != "download") : ?>
            <?php echo $value['short_description']; ?> 
            <?php if ($value['type'] == "node") : ?>
        <a class="read-more" href="<?php echo node_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
        <?php elseif ($value['type'] == "news") : ?>
        <a class="read-more" href="<?php echo news_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
        <?php elseif ($value['type'] == "aira_group") : ?>
        <a class="read-more" href="<?php echo aira_group_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
        <?php elseif ($value['type'] == "partner") : ?>
        <a class="read-more" href="<?php echo partner_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
        <?php elseif ($value['type'] == "download") : ?>
        <a href="<?php echo site_download_url($value['file_full_path']); ?>">
        <?php endif; ?>

              <?php echo lang("อ่านต่อ"); ?>
              <i class="fa fa-angle-right"></i>
            </a>

            <?php endif; ?> 

          </p>
        </div>
    </div>
  </div>
  <?php endforeach; ?>
<?php endif; ?>

<?php if ($total_rows > $per_page) : ?>
<div id="pagination">
  

  <?php for ($i = 1; $i <= count($search); $i++) : ?>
    <?php if ($page == $i) : ?>
    <span class="current page-num"><?php echo $i; ?></span>
    <?php else : ?>
    <a href="<?php echo site_url('search?q='.$q.'&page='.$i); ?>"><?php echo $i; ?></a>
    <?php endif; ?>
  <?php endfor; ?>
</div>
<?php endif; ?>
                 
<?php if ( ! empty($node)) : ?>
  <?php foreach ($node as $key => $value) : ?>
  <?php #alert($value, 'red'); ?>
  <div class="post-row">
    
    <div class="col-md-12">
      <h3 class="post-title">
        <a href="<?php echo node_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
          <?php echo $value['name']; ?>
        </a>
      </h3>
        <span>
          <i class="fa fa-calendar"></i> 
          <?php echo show_style_date($value['created_at']); ?>           
        </span>
        <div class="post-content">
          <p>
            <?php echo $value['short_description']; ?> 
            <a class="read-more" href="<?php echo node_detail_url($value['slug'], $value['id']); ?>">
              <?php echo lang("อ่านต่อ"); ?>
              <i class="fa fa-angle-right"></i>
            </a>
          </p>
        </div>
    </div>
  </div>
  <?php endforeach; ?>
<?php endif; ?>

<?php if ( ! empty($news)) : ?>
  <?php foreach ($news as $key => $value) : ?>
  <?php #alert($value, 'red'); ?>
  <div class="post-row">
    
    <div class="col-md-12">
      <h3 class="post-title">
        <a href="<?php echo news_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
          <?php echo $value['name']; ?>
        </a>
      </h3>
        <span>
          <i class="fa fa-calendar"></i> 
          <?php echo show_style_date($value['created_at']); ?>           
        </span>
        <div class="post-content">
          <p>
            <?php echo $value['short_description']; ?> 
            <a class="read-more" href="<?php echo news_detail_url($value['slug'], $value['id']); ?>">
              <?php echo lang("อ่านต่อ"); ?>
              <i class="fa fa-angle-right"></i>
            </a>
          </p>
        </div>
    </div>
  </div>
  <?php endforeach; ?>
<?php endif; ?>


<?php if ( ! empty($aira_group)) : ?>
  <?php foreach ($aira_group as $key => $value) : ?>
  <?php #alert($value, 'red'); ?>
  <div class="post-row">
    
    <div class="col-md-12">
      <h3 class="post-title">
        <a href="<?php echo aira_group_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
          <?php echo $value['name']; ?>
        </a>
      </h3>
        <span>
          <i class="fa fa-calendar"></i> 
          <?php echo show_style_date($value['created_at']); ?>           
        </span>
        <div class="post-content">
          <p>
            <?php echo $value['short_description']; ?> 
            <a class="read-more" href="<?php echo aira_group_detail_url($value['slug'], $value['id']); ?>">
              <?php echo lang("อ่านต่อ"); ?>
              <i class="fa fa-angle-right"></i>
            </a>
          </p>
        </div>
    </div>
  </div>
  <?php endforeach; ?>
<?php endif; ?>

<?php if ( ! empty($partner)) : ?>
  <?php foreach ($partner as $key => $value) : ?>
  <?php #alert($value, 'red'); ?>
  <div class="post-row">
    
    <div class="col-md-12">
      <h3 class="post-title">
        <a href="<?php echo partner_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
          <?php echo $value['name']; ?>
        </a>
      </h3>
        <span>
          <i class="fa fa-calendar"></i> 
          <?php echo show_style_date($value['created_at']); ?>           
        </span>
        <div class="post-content">
          <p>
            <?php echo $value['short_description']; ?> 
            <a class="read-more" href="<?php echo partner_detail_url($value['slug'], $value['id']); ?>">
              <?php echo lang("อ่านต่อ"); ?>
              <i class="fa fa-angle-right"></i>
            </a>
          </p>
        </div>
    </div>
  </div>
  <?php endforeach; ?>
<?php endif; ?>

<?php if ( ! empty($download)) : ?>
  <?php foreach ($download as $key => $value) : ?>
  <?php #alert($value, 'red'); ?>
  <div class="post-row">
    
    <div class="col-md-12">
      <h3 class="post-title">
        <a href="<?php echo site_download_url($value['file_full_path']); ?>" title="<?php echo $value['name']; ?>">
          <?php echo $value['name']; ?>
        </a>
      </h3>
        <span>
          <i class="fa fa-calendar"></i> 
          <?php echo show_style_date($value['created_at']); ?>           
        </span>
        <div class="post-content">
          <p>
            <?php echo $value['short_detail']; ?> 
            <a class="read-more" href="<?php echo site_download_url($value['file_full_path']); ?>">
              <?php echo lang("ดาวน์โหลด"); ?>
              <i class="fa fa-angle-right"></i>
            </a>
          </p>
        </div>
    </div>
  </div>
  <?php endforeach; ?>
<?php endif; ?>


</div>

              