<?php
class Category_model extends MY_Model { 

	private $_table = "categories";
	private $_table_lang = "category_lang";

	public function __construct()
	{
		parent::__construct();
	}


	
	/**
	*	@params
	*		- $position_id 1 = Top
	*		- $position_id 2 = Left
	*		- $position_id 3 = Footer
	**/
	public function get_all_category_by_position($parent = -1, $position_id = null)
	{
		if (empty($position_id))
		{
			return null;
		}

		$select =parent::select();
		$select->from(array('C' => $this->_table), array('C.id', 'C.slug', 'C.parent', 'C.level', 'C.type', 'C.url_link', 'C.show_on_web', 'C.show_on_mobile'
			, new Zend_DB_Expr("
				(select count(*) 
					from node_has_category 
					inner join nodes ON nodes.id = node_has_category.node_id 
					where category_id = C.id
					and nodes.active_status = 'publish'
					and nodes.deleted_at is null
				) as total_nodes
				")
			));

		if ($position_id == 1)
		{	
			$select->joinLeft(array('CL' => $this->_table_lang), 'C.id = CL.category_id AND CL.lang_id = '.LANGID, array('CL.name', 'CL.short_detail', 'CL.detail', 'CL.seo_title', 'CL.seo_keyword', 'CL.seo_description'))
				->joinLeft(array('CP' => 'category_position'), 'CP.category_id = C.id AND CP.position_id = '.$position_id, array());
		}
		else 
		{
			$select->join(array('CL' => $this->_table_lang), 'C.id = CL.category_id AND CL.lang_id = '.LANGID, array('CL.name', 'CL.short_detail', 'CL.detail', 'CL.seo_title', 'CL.seo_keyword', 'CL.seo_description'))
				->join(array('CP' => 'category_position'), 'CP.category_id = C.id AND CP.position_id = '.$position_id, array());
		}

			
		$select->where('C.parent = ?', $parent)
			->where('C.active_status = ?', 'Y')
			->order('C.sort_order asc');
		#echo $select->__toString();
		#exit;
		$query = parent::fetch_all($select);
		
		if (empty($query))
		{
			return null;
		}
		
		$current = array();
		#alert($query, 'red');

		foreach ($query as $key => $value)
		{
			if ($value['total_nodes'] > 0 or $value['type'] == "link")
			{
				$current[] = array(
					'id' => $value['id'],
					'name' => $value['name'],
					'slug' => $value['slug'],							
					'level' => $value['level'],
					'parent' => $value['parent'],
					'type' => $value['type'],
					'show_on_web' => $value['show_on_web'],
					'show_on_mobile' => $value['show_on_mobile'],
					'url_link' => $value['url_link'],
					'child' => $this->get_all_category_by_position($value['id'], $position_id)
				);
			}
		}
		

		return $current;

	}

	public function get_node_under_category($category_id = null)
	{
		if ( empty($category_id))
		{
			return null;
		}
		$select =parent::select();
		$select->from(array('N' => 'nodes'), array('N.id', 'N.slug'))
			->join(array('NL' => 'node_lang'), 'N.id = NL.node_id AND NL.lang_id = '.LANGID, array('NL.name'))
			->join(array('NC' => 'node_has_category'), 'NC.node_id = N.id', array())
			->where('N.active_status = ?', 'publish')
			->where('NC.category_id = ?', $category_id)
			->order('N.sort_order asc');
		#echo $select->__toString();
		#exit;
		return parent::fetch_all($select);

	}

	public function get_news_under_category()
	{
		
		$select =parent::select();
		$select->from(array('N' => 'nodes'), array('N.id', 'N.slug', 'N.thumbnail', 'N.thumbnail_resize'))
			->join(array('NL' => 'node_lang'), 'N.id = NL.node_id AND NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description'))
			->join(array('NC' => 'node_has_category'), 'NC.node_id = N.id', array())
			->join(array('CL' => 'category_lang'), 'CL.category_id = NC.category_id AND CL.lang_id = '.LANGID, array())
			->where('N.active_status = ?', 'publish')
			->where('CL.name = ?', 'ข่าวประชาสัมพันธ์');
		#echo $select->__toString();
		#exit;
		return parent::fetch_all($select);

	}

	public function get_category_detail($id = NULL) 
	{
		if (empty($id)) 
		{
			return NULL; 
		}
		
		$select = parent::select();
		$select->from(array('C' => 'categories'), array('C.id', 'C.active_status', 'C.parent', 'C.level', 'C.slug'))
				->joinLeft(array('CL' => 'category_lang'), 'C.id = CL.category_id and CL.lang_id = '.LANGID, array('CL.name', 'CL.short_detail', 'CL.detail', 'CL.seo_title', 'CL.seo_description', 'CL.seo_keyword'))
				->where('C.id = ?', $id);
		$data = parent::fetch_row($select);
        if (empty($data))
        {
            return NULL;
        }
        return $data; 

        $return = array(
            'id' => $data['id'],
            'active_status' => $data['active_status'],
            'parent' => $data['parent'],
            'level' => $data['level'],
            'name' => $data['name'],
            'slug' => $data['slug'],
            'short_detail' => $data['short_detail'],
            'detail' => $data['detail'],
            'seo_title' => $data['seo_title'],
            'seo_description' => $data['seo_description'],
            'seo_keyword' => $data['seo_keyword']
        );

        $select = parent::select();

        $select->from(array('C' => 'categories'), array('C.id', 'C.active_status', 'C.parent', 'C.level'))
                ->join(array("CL" => "category_lang"), 'C.id = CL.category_id AND CL.lang_id != '.DEFAULT_LANG_ID, array('CL.id as translate_id', 'CL.name', 'CL.short_detail', 'CL.detail', 'CL.seo_title', 'CL.seo_description', 'CL.seo_keyword'))
                ->join(array('L' => 'lang'), 'L.id = CL.lang_Id', array('L.code'))
                ->where('CL.category_id  = ?', $id);

        $data = parent::fetch_all($select);
        if ( ! empty($data))
        {
            $translate = array();
            foreach ($data as $key => $value)
            {
                $translate[$value['code']] = array(
                    'id' => $value['translate_id'],
                    'name' => $value['name'],
                    'short_detail' => $value['short_detail'],
                    'detail' => $value['detail'],
                    'seo_title' => $value['seo_title'],
                    'seo_description' => $value['seo_description'],
                    'seo_keyword' => $value['seo_keyword']
                );
            }

        }

        if ( ! empty($translate))
        {
            $return['translate'] = $translate;
        }
        else
        {
            $return['translate'] = array();
        }


        return $return;
	}
	public function get_child($parent = -1) 
	{
		$select = parent::select(); 
		$select->from(array('C' => 'categories'), array('C.id', 'C.level', 'C.parent', 'C.slug'))
					->join(array('CL' => 'category_lang'), 'C.id = CL.category_id AND CL.lang_id = '.LANGID, array('CL.name'))
					->join(array('NC' => 'node_has_category'), 'NC.category_id = C.id', array())
					->where('C.parent = ?', $parent)
					->where('C.active_status = ?', 'Y'); 

		return parent::fetch_all($select); 

	}

	public function get_child_by_sidebar($parent = -1) 
	{

		if (empty($parent))
		{
			return null; 
		}

		$select = parent::select(); 
		$select->from(array('C' => 'categories'), array('C.id', 'C.level', 'C.parent', 'C.slug'))
					->join(array('CL' => 'category_lang'), 'C.id = CL.category_id AND CL.lang_id = '.LANGID, array('CL.name'))
					->join(array('CP' => 'category_position'), 'CP.category_id = C.id', array())
					->join(array('NC' => 'node_has_category'), 'NC.category_id = C.id', array())
					->where('CP.position_id = ?', 2)
					->where('C.parent = ?', $parent)
					->where('C.active_status = ?', 'Y')
					->order('C.sort_order asc')
					->group('C.id');
		#echo $select->__toString();
		#exit;
		return parent::fetch_all($select); 

	}
}