<!-- Classic Heading -->
<h4 class="classic-title"><span><?php echo $category['name']; ?> <?php echo lang("ทั้งหมด"); ?></span></h4>

<div class="latest-posts-classic">

<!-- Post 1 -->
                  
<?php if ($node['row_count'] > 0) : ?>
  <?php foreach ($node['rows'] as $key => $value) : ?>
  <div class="post-row">
    <div class="col-md-5">
      <div class="widget-thumb">
        <a href="<?php echo node_detail_url($value['slug'], $value['id']); ?>">
          <img src="<?php echo site_node_url($value['thumbnail']); ?>" class="img-thumbnail" style="margin-bottom:30px;">
        </a>
      </div>
    </div>
    <div class="col-md-7">
      <h3 class="post-title">
        <a href="<?php echo node_detail_url($value['slug'], $value['id']); ?>" title="<?php echo $value['name']; ?>">
          <?php echo $value['name']; ?>
        </a>
      </h3>
      <?php if ( ! empty($value['name_2'])) : ?>
      <h5 class="post-title">
        <a href="<?php echo node_detail_url($value['slug'], $value['id']); ?>"><?php echo $value['name_2']; ?></a>
      </h5>
      <?php endif; ?>
      <span><i class="fa fa-calendar"></i> <?php echo show_style_date($value['created_at']); ?> <i class="fa fa-eye"></i> </span>
      <div class="post-content">
        <p>
          <?php echo mb_substr($value['short_description'], 0, 100); ?> <br>
          <a class="read-more" href="<?php echo node_detail_url($value['slug'], $value['id']); ?>">
            <?php echo lang("อ่านต่อ"); ?> 
          </a>
        </p>
      </div>
    </div>
  </div>
  <?php endforeach; ?>
<?php endif; ?>
     
</div>  <!-- Start Pagination -->
    
<?php echo aira_pagination($node, category_url($slug, $id)); ?>
            <!-- End Pagination -->
              