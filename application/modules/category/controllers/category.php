<?php
class Category extends Web_Controller { 
	public function __construct()
	{
		parent::__construct();
		$this->load->model('node/node_model', 'node');
		$this->load->model('category_model', 'category');
	}

	public function index($id = null)	
	{

		$data = array();
		$category = $data['category'] = $this->category->get_category_detail($id);

		$data['id'] = $id;
		$data['slug'] = $category['slug'];

		$args['category_id'] = ! empty($id) ? $id : null; 
		$args['page'] = $data['page'] = $this->input->get_post('page') ? $this->input->get_post('page') : 1;
		$args['per_page'] = $data['per_page'] = $this->input->get_post('per_page') ? $this->input->get_post('per_page') : 20;

		
		$data['node'] = $this->node->get_node($args, $data['page'], $data['per_page']);
		
		#alert($data['node'], 'red');
		#exit;


		// ถ้ามีมากกว่า 1 content แสดงหน้า list เช่น ข่าวประชาสัมพันธ์
		if ($data['node']['row_count'] > 1)
		{
			parent::seo_title($category['seo_title']);
            parent::seo_keyword($category['seo_keyword']);
            parent::seo_description($category['seo_description']);
            #alert($category, 'red');
            #exit;

            $category_parent  = $this->category->get_category_detail($category['parent']);
            
			if ( ! empty($category_parent['name']))
			{
				$this->breadcrumb->make($category_parent['name']);	
			}
            if ( ! empty($category['name']))
            {
            	$this->breadcrumb->make($category['name'], null, true, true);
            }
            parent::set_breadcrumb();


           
            parent::set_category_list_sidebar($category['parent']);
            
            #$this->template->write('subject', $category['name, true);
			$this->template->write_view('content', 'list_view', $data);
		}
		else 
		{


			$node = $data['node']['rows'][0];
			redirect(node_url($node['slug'], $node['id']), 'location');

			#$this->template->write_view('content', 'node/detail_view', $data);
		}
		$this->template->render();
		
	}
}