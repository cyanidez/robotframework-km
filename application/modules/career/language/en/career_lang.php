<?php
$lang = array(
	'ตำแหน่งงาน ทั้งหมด' => 'ALL Career',
	'ร่วมงานกับเรา' => 'Join Us',
	'กว้าง x ยาว ไม่เกิน' => 'width x height not over than',
	'รองรับนามสกุล' => 'Support extension ',
	'ขนาดไฟล์ไม่เกิน' => 'File size not over than',
	'ตำแหน่งงานที่สมัคร' => 'Position',
	'ชื่อ' => 'First Name',
	'นามสกุล' => 'Last Name',
	'อีเมล์' => 'Email',
	'เบอร์มือถือ' => "Mobile No.",
	'ที่อยู่' => 'Address',
	'รูปถ่าย' => 'Your Image',
	'รหัสป้องกันสแปม' => 'Security Code',
	'ส่งข้อความ' => "Send Message"
);

return $lang; 