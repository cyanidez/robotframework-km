<?php
class Career extends Web_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('career_model', 'career');
        $this->load->model('recruit_model', 'recruit');
        $this->load->library('session');
        $this->load->model('configuration/configuration_model', 'configuration');
        $this->lang->load('career', 'career', LANGCODE);
        $this->load->library('securimage');
        $this->load->config('site_config');
        
    }

    public function index()
    {
        $data = array();
        
        $data['page'] = $args['page'] = $this->input->get_post('page', true) ? $this->input->get_post('page', true) : 1; 
        $data['per_page'] = $args['per_page'] = $this->input->get_post('per_page', true) ? $this->input->get_post('per_page', true) : 20;


                
        $data['career'] = $this->career->get_career($args, $data['page'], $data['per_page']);
        #$this->breadcrumb->make(lang('หน้าแรก'), site_url());
        $this->breadcrumb->make(lang('ร่วมงานกับเรา'), null, true, true);
        parent::set_breadcrumb();
        parent::set_blank_sidebar();
            
        $this->template->write_view('content', 'list_view', $data);

        
        $this->template->render();
    }

    public function recruit($id = null)
    {
        $data = array();

        $career = $data['career'] = $this->career->get_career_detail($id);

        if ($this->input->post('btn-save'))
        {
            $rules = array(
                array('field' => 'career_id', 'label' => lang('ตำแหน่งงานที่สมัคร'), 'rules' => 'trim|required'),
                array('field' => 'title', 'label' => lang('คำนำหน้า'), 'rules' => 'trim'),
                array('field' => 'firstname', 'label' => lang('ชื่อ'), 'rules' => 'trim|required'),
                array('field' => 'lastname', 'label' => lang('นามสกุล'), 'rules' => 'trim|required'),
                array('field' => 'firstname_en', 'label' => lang('ชื่อ (อังกฤษ)'), 'rules' => 'trim'),
                array('field' => 'lastname_en', 'label' => lang('นามสกุล (อังกฤษ)'), 'rules' => 'trim'),
                array('field' => 'expected_salary', 'label' => lang('เงินเดือนที่ต้องการ'), 'rules' => 'trim'),
                array('field' => 'dob_year', 'label' => lang('ปีเกิด'), 'rules' => 'trim'),
                array('field' => 'dob_month', 'label' => lang('เดือนเกิด'), 'rules' => 'trim'),
                array('field' => 'dob_day', 'label' => lang('วนเกิด'), 'rules' => 'trim'),
                array('field' => 'email', 'label' => lang('อีเมล์'), 'rules' => 'trim|required'),
                array('field' => 'mobile_no', 'label' => lang('เบอร์โทรศัพท์'), 'rules' => 'trim'),
                array('field' => 'address', 'label' => lang('ที่อยู่'), 'rules' => 'trim|required'),
                array('field' => 'captcha_code', 'label' => lang('รหัสป้องกันสแปม'), 'rules' => 'trim|required')
                
            );
            
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() !== FALSE)
            {
                if ($this->securimage->check($this->input->post('captcha_code', TRUE)) == TRUE)
                {

                    #echo "pass captcha";
                    #exit; 
                    $fields = array(
                        'title' => $this->input->post('title'),
                        'firstname' => $this->input->post('firstname', true),
                        'lastname' => $this->input->post('lastname', true),
                        'firstname_en' => $this->input->post('firstname_en', true),
                        'lastname_en' => $this->input->post('lastname_en', true),
                        'birthdate' => $this->input->post('dob_year').'-'.$this->input->post('dob_month').'-'.$this->input->post('dob_day'),
                        'expected_salary' => $this->input->post('expected_salary'),
                        'email' => $this->input->post('email', true),
                        'mobile_no' => $this->input->post('mobile_no', true),
                        'address' => $this->input->post('address', true),
                        'career_id' => $this->input->post('career_id', true),
                        'created_at' => date_now(),
                        'ip_address' => $this->input->ip_address()
                    );
                    $data_sendmail = $fields; 

                    $is_error = false; 

                    if ($this->recruit->create($fields))
                    {
                        $recruit_id = $this->recruit->get_last_insert_id();

                        
                        if ($_FILES['img_profile']['size'] > 0)
                        {
                            $uploaded_img_profile = $this->_upload_img_profile($recruit_id, 'img_profile', 'profile_'.$recruit_id);
                            if (empty($uploaded_img_profile['error']))
                            {
                                $fields = array(
                                    'img_profile' => $recruit_id.'/'.$uploaded_img_profile['upload_data']['file_name']
                                );
                                $where[] = 'id = '.$recruit_id;
                                if ( ! $this->recruit->update($fields, $where))
                                {
                                    $is_error = true; 
                                }
                            }
                            else 
                            {
                                $data['img_profile_error'] = $uploaded_img_profile['error'];
                                $is_error = true; 
                            }
                        }
                        if ($_FILES['img_resume']['size'] > 0)
                        {
                            $uploaded_img_resume = $this->_upload_img_resume($recruit_id, 'img_resume', 'resume_'.$recruit_id);
                            
                            if (empty($uploaded_img_resume['error']))
                            {
                                $fields = array(
                                    'img_resume' => $recruit_id.'/'.$uploaded_img_resume['upload_data']['file_name']
                                );
                                $where[] = 'id = '.$recruit_id;
                                if ( ! $this->recruit->update($fields, $where))
                                {
                                    $is_error = true; 
                                }
                            }
                            else 
                            {
                                $data['img_resume_error'] = $uploaded_img_resume['error'];
                                $is_error = true; 
                            }
                        }
                        if ( ! empty($data['img_profile_error']) OR ! empty($data['img_resume_error']))
                        {
                            unset($where, $fields);
                            $where[] = 'id = '.$recruit_id;
                            if ($this->recruit->delete($where))
                            {
                                
                            }
                        }

                        

                        if ($is_error == true)
                        {
                            unset($fields, $where);
                            $where[] = 'id = '.$recruit_id;
                            if ($this->recruit->delete($where))
                            {

                            }
                        }
                        

                        if ($is_error == false)
                        {
                            $send_email = $this->_send_email($data_sendmail);
                            if ($send_email == true)
                            {
                                $this->session->set_flashdata('success', lang('ท่านได้ยื่นแบบฟอร์มสมัครงานเรียบร้อยแล้วค่ะ'));
                                redirect(career_recruit_url($this->input->post('career_id')), 'location');
                            }
                        }
                    }
                }
                else
                {
                    $data['captcha_code_error'] = lang('กรอกรหัสป้องกันสแปมผิดค่ะ');
                }
                //--- Check Captcha
            }
        }
        $data['config_month'] = $this->config->item(LANGCODE.'_month');
        #alert($config['config_month']);
        #exit; 
        $data['id'] = $id; 
        $data['config_profile'] = $this->config->item('career_profile');
        $data['config_resume'] = $this->config->item('career_resume');
        if (empty($data['career']['id']))
        {
            $this->template->write_view('content', 'error/error_view', $data);
        }
        else 
        {
            $this->template->add_js_plugins('jquery.validate.js', 'admin');
            $this->template->add_js('recruit.js', 'front');
            $this->breadcrumb->make(lang('สมัครงาน'), career_url());
            $this->breadcrumb->make($career['name'], null, true, true);
            parent::set_breadcrumb();
            parent::set_blank_sidebar();
            $this->template->write_view('content', 'recruit_form_view', $data);
        }
        
        
        $this->template->render();
    }

    public function detail($id = null)
    {
        $data = array();
        
        $career = $data['career'] = $this->career->get_career_detail($id);

        $data['id'] = $id; 
        if (empty($data['career']['id']))
        {
            $this->template->write_view('content', 'error/error_view', $data);
        }
        else 
        {
            #$this->breadcrumb->make(lang('หน้าแรก'));
            $this->template->write('subject', lang('ไอร่ากรุ๊ป'));
            $this->breadcrumb->make(lang('ไอร่ากรุ๊ป'), career_url());
            $this->breadcrumb->make($career['name'], null, true, true);

            parent::set_breadcrumb();
            parent::set_blank_sidebar();
            #$data['career_other'] = $this->career->get_career_other($id);
            
            $this->template->write_view('content', 'detail_view', $data);

        }
        $this->template->render();
    }

    private function _send_email($data = null)
    {

        $configuration = $this->configuration->get_configuration();
        $web_config = array();
        if ( ! empty($configuration))
        {
            foreach ($configuration as $key => $value)
            {
                $web_config[$value['config_key']] = $value['config_value'];
            }
        }

        $career = $this->career->get_career_detail($data['career_id']);
        #alert($career, 'blue', 'career');


        $this->load->library('send_email');
        $subject = "Application Form (สมัครงานตำแหน่ง ".$career['name'].")";


        $message = "<p>Dear All  Officers, <br /><br>
            The customer has just submitted  some information of contact us from the website.<br /><br>
            <strong>First Name : </strong>".$data['firstname']."<strong></strong><br />
            <strong>Last Name : </strong>".$data['lastname']."<strong></strong><br />";
        if ( ! empty($data['mobile_no'])) 
        {
            $message .= "<strong>Mobile : </strong>".$data['mobile_no'].'<br>';
        }
        $message .= "<strong>Email : </strong>".$data['email']."<br>"
            . "<strong>Address : </strong>".$data['address']."<br>";

        $from_email = $data['email'];
        $is_error = false;
        $send_to = preg_split("/,/", $web_config['RECRUITMENT_EMAIL']);
        $this->send_email->set_smtp_user($web_config['SMTP_USERNAME']);
        $this->send_email->set_smtp_pass($web_config['SMTP_PASSWORD']);
        $this->send_email->set_smtp_port($web_config['SMTP_PORT']);
        $this->send_email->set_smtp_host($web_config['SMTP_HOST']);
        $this->send_email->set_smtp_charset('utf-8');
        $success_message = ''; 
        #alert($send_to, 'red', 'send_to');
        foreach ($send_to as $key => $value)
        {
            #alert($value, 'blue', 'value');

            if ( ! $this->send_email->send($value, $subject, $message, $from_email, $data['firstname'].' '.$data['lastname'])) 
            {
                $is_error = true; 
            }
        }

        if ($is_error == true)
        {
            return false; 
        }
        return true; 

        

    }

    private  function _upload_img_profile($recruit_id, $field = null, $file_name = null)
    {
        $this->load->config('site_config');
        $career_config = $this->config->item('career_profile');
        $config['upload_path'] = site_career_profile_url($recruit_id, true);

        $config['allowed_types'] = $career_config['ext'];
        $config['max_size'] = $career_config['size'];
        $config['max_width'] = $career_config['width'];
        $config['max_height'] = $career_config['height'];

        if ( ! empty($file_name))
        {
            $config['file_name'] = $file_name;
            $config['overwrite'] = true;
        }
        $this->load->library('upload');
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($field))
        {
            $error = array('error' => $this->upload->display_errors());
            return $error;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }
    }

    private  function _upload_img_resume($recruit_id, $field = null, $file_name = null)
    {
        $this->load->config('site_config');
        $career_config = $this->config->item('career_resume');
        $config['upload_path'] = site_career_resume_url($recruit_id, true);

        $config['allowed_types'] = $career_config['ext'];
        $config['max_size'] = $career_config['size'];

        if ( ! empty($file_name))
        {
            $config['file_name'] = $file_name;
            $config['overwrite'] = true;
        }
        $this->load->library('upload');
        $this->upload->initialize($config);
        if ( ! $this->upload->do_upload($field))
        {
            $error = array('error' => $this->upload->display_errors());
            return $error;
        }
        else
        {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }
    }
}