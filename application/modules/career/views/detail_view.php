<h4 class="classic-title"><span><b><?php echo $aira_group['name']; ?></b></span></h4>
                        
	<!-- Some Text -->
	
  <?php if ( ! empty($aira_group['image'])) : ?>
	<p style="text-align:center;">
		<img src="<?php echo site_aira_group_url($aira_group['image']); ?>" alt="" class="img-responsive" />		
	</p>
  <?php endif; ?>
	
	<p><?php echo show_content($aira_group['description']); ?></p>

  <!-- Divider -->
<div class="hr5" style="margin-top:30px; margin-bottom:45px;"></div>


    <!-- End Content -->