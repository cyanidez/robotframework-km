

<div class="col-md-9 page-content">
    <h4 class="classic-title"><span><b><?php echo lang('สมัครงาน'); ?></b></span></h4>    
    <?php if ($this->session->flashdata('success')) : ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <?php if ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?>
    

    <form 
        action="<?php echo career_recruit_url($id); ?>"
        id="recruit_form"
        name="recruit_form"
        method="post"
        class="form-horizontal"
        enctype="multipart/form-data"
    >
        <div class="form-group">
            <label class="control-label col-sm-3">
                <span class="mandatory">*</span>
                <?php echo lang('ตำแหน่งงานที่สมัคร'); ?>
            </label>
            <div class="col-sm-9">
                <input 
                    type="text" 
                    class="form-control"
                    disabled="disabled"
                    value="<?php echo $career['name']; ?>">
                <input 
                    type="hidden" 
                    value="<?php echo $career['id']; ?>" 
                    id="career_id"
                    name="career_id">
                <?php echo form_error('contact_group_id'); ?>
            </div>
        </div>
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <span class="mandatory">*</span>
                    <?php echo lang('คำนำหน้า'); ?>
                </label>
                <div class="col-sm-9">
                    <select name="title" id="title" class="form-control">
                        <option value="mr" <?php echo set_value('title') == "mr" ? 'selected="selected"' : ""; ?>>Mr.</option>
                        <option value="ms" <?php echo set_value('title') == "ms" ? 'selected="selected"' : ""; ?>>Ms.</option>
                        <option value="mrs" <?php echo set_value('title') == "mrs" ? 'selected="selected"' : ""; ?>>Mrs.</option>
                        <option value="dr" <?php echo set_value('title') == "dr" ? 'selected="selected"' : ""; ?>>Dr.</option>
                        <option value="other" <?php echo set_value('title') == "other" ? 'selected="selected"' : ""; ?>>Other</option>
                    </select>
                    <?php echo form_error('title'); ?>

                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <span class="mandatory">*</span>
                    <?php echo lang('ชื่อ'); ?>
                </label>
                <div class="col-sm-9">
                    <input 
                        value="<?php echo set_value('firstname'); ?>" 
                        class="form-control" 
                        type="text" 
                        id="firstname" 
                        name="firstname" 
                        title="<?php echo lang('กรุณากรอกชื่อด้วยค่ะ'); ?>">
                    <?php echo form_error('firstname'); ?>

                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <span class="mandatory">*</span>
                    <?php echo lang('นามสกุล'); ?>
                </label>
                <div class="col-sm-9">
                    <input 
                        value="<?php echo set_value('lastname'); ?>" 
                        class="form-control" 
                        type="text" 
                        id="lastname" 
                        name="lastname" 
                        title="<?php echo lang('กรุณากรอกสามสกุลด้วยค่ะ'); ?>">
                    <?php echo form_error('lastname'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <?php echo lang('ชื่อ (อังกฤษ)'); ?>
                </label>
                <div class="col-sm-9">
                    <input 
                        value="<?php echo set_value('firstname_en'); ?>" 
                        class="form-control" 
                        type="text" 
                        id="firstname_en" 
                        name="firstname_en" 
                        title="<?php echo lang('กรุณากรอกชื่อ (อังกฤษ) ด้วยค่ะ'); ?>">
                    <?php echo form_error('firstname_en'); ?>

                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <?php echo lang('นามสกุล (อังกฤษ)'); ?>
                </label>
                <div class="col-sm-9">
                    <input 
                        value="<?php echo set_value('lastname_en'); ?>" 
                        class="form-control" 
                        type="text" 
                        id="lastname_en" 
                        name="lastname_en" 
                        title="<?php echo lang('กรุณากรอกนามสกุล (อังกฤษ) ด้วยค่ะ'); ?>">
                    <?php echo form_error('lastname_en'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">
                    <?php echo lang('วันเกิด'); ?>
                </label>
                <div class="col-sm-3">
                    <select name="dob_year" id="dob_year" class="form-control">
                        <?php for ($i = date("Y") - 20; $i >= (date("Y") - 80); $i--) : ?>
                        <option value="<?php echo $i; ?>" <?php echo (set_value('dob_year') == $i) ? 'selected="selected"' : ""; ?>><?php echo $i; ?></option>
                        <?php endfor; ?>
                    </select>
                    
                </div>
                <div class="col-sm-3">
                    <select name="dob_month" id="dob_month" class="form-control">
                        <?php for ($i = 0; $i < 12; $i++) : ?>
                        <option value="<?php echo sprintf("%02d", $i + 1); ?>" <?php echo (set_value('dob_month') == sprintf("%02d", $i + 1)) ? 'selected="selected"' : ""; ?>><?php echo $config_month[$i]; ?></option>
                        <?php endfor; ?>
                    </select>
                </div>
                <div class="col-sm-3">
                    <select name="dob_day" id="dob_day" class="form-control">
                        <?php for ($i = 0; $i < 31; $i++) : ?>
                        <option value="<?php echo sprintf("%02d", $i + 1); ?>" <?php echo (set_value('dob_day') == sprintf("%02d", $i + 1)) ? 'selected="selected"' : ""; ?>><?php echo $i + 1; ?></option>
                        <?php endfor; ?>
                    </select>
                    <?php echo form_error('lastname_en'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <span class="mandatory">*</span>
                    <?php echo lang('อีเมล์'); ?>
                </label>
                <div class="col-sm-9">
                    <input 
                        value="<?php echo set_value('email'); ?>" 
                        class="form-control" 
                        type="text" 
                        id="email" 
                        name="email" 
                        data-invalid-format="<?php echo lang('กรุณากรอกอีเมล์ให้ถูกต้องด้วยค่ะ'); ?>"
                        title="<?php echo lang('กรุณากรอกอีเมล์ด้วยค่ะ'); ?>">
                    <?php echo form_error('email'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <?php echo lang('เบอร์มือถือ'); ?>
                </label>
                <div class="col-sm-9">
                    <input 
                        value="<?php echo set_value('mobile_no'); ?>" 
                        class="form-control" 
                        type="text" 
                        id="mobile_no" 
                        name="mobile_no" 
                        placeholder="<?php echo lang('กรอกเบอร์มือถือ'); ?>"
                        title="">
                    <?php echo form_error('mobile_no'); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label col-sm-3">
                    <?php echo lang('เงินเดือนที่ต้องการ'); ?>
                </label>
                <div class="col-sm-9">
                    <input 
                        value="<?php echo set_value('expected_salary'); ?>" 
                        class="form-control" 
                        type="text" 
                        id="expected_salary" 
                        name="expected_salary" 
                        placeholder="<?php echo lang('กรอกเงินเดือนที่ต้องการ'); ?>"
                        title="<?php echo lang('กรุณากรอกเงินเดือนที่ต้องการค่ะ'); ?>">
                    <?php echo form_error('expected_salary'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <span class="mandatory">*</span>
                    <?php echo lang('ที่อยู่'); ?>
                </label>
                <div class="col-sm-9">
                    <textarea 
                        
                        class="form-control" 
                        type="text" 
                        cols="30"
                        rows="10"
                        id="address" 
                        name="address" 
                        placeholder="<?php echo lang('กรอกที่อยู่'); ?>"
                        title="<?php echo lang('กรุณากรอกที่อยู่ด้วยค่ะ'); ?>"><?php echo set_value('address'); ?></textarea>
                    <?php echo form_error('address'); ?>
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <?php echo lang('รูปถ่าย'); ?>
                </label>
                <div class="col-sm-9">
                    <input 
                        type="file"
                        class="form-control" 
                        id="img_profile" 
                        name="img_profile" 
                        placeholder="<?php echo lang('เลือกภาพถ่าย'); ?>"
                        title="<?php echo lang('กรุณาเลือกรูปถ่ายด้วยค่ะ'); ?>">
                    <?php if ( ! empty($img_profile_error)) : ?>
                    <label class="error"><?php echo $img_profile_error; ?></label>
                    <?php endif; ?>
                    <ul>
                        <li><?php echo lang('กว้าง x ยาว ไม่เกิน').' '.$config_profile['width'].'x'.$config_profile['height']; ?> px</li>
                        <li><?php echo lang('รองรับนามสกุล').' '.$config_profile['ext']; ?></li>
                        <li><?php echo lang('ขนาดไฟล์ไม่เกิน').' '.($config_profile['size'] / 1000); ?> KB</li>
                    </ul>
                    <?php echo form_error('img_profile'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <?php echo lang('Resume'); ?>
                </label>
                <div class="col-sm-9">
                    <input 
                        type="file"
                        class="form-control" 
                        id="img_resume" 
                        name="img_resume" 
                        placeholder="<?php echo lang('เลือก Resume'); ?>"
                        title="<?php echo lang('กรุณาเลือก Resume ด้วยค่ะ'); ?>">
                    <?php if ( ! empty($img_resume_error)) : ?>
                    <label class="error"><?php echo $img_resume_error; ?></label>
                    <?php endif; ?>
                    <ul>
                        <li><?php echo lang('รองรับนามสกุล').' '.$config_resume['ext']; ?></li>
                        <li><?php echo lang('ขนาดไฟล์ไม่เกิน').' '.($config_resume['size'] / 1000); ?> KB</li>
                    </ul>
                    <?php echo form_error('img_resume'); ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">
                    <span class="mandatory">*</span>
                    <?php echo lang('รหัสป้องกันสแปม'); ?>
                </label>
                <div class="col-sm-9">
                    <img id="siimage" style="border: 1px solid #000; margin-right: 15px" src="/securimage/securimage_show.php?sid=<?php echo md5(uniqid()) ?>" alt="CAPTCHA Image" align="left">
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3"></label>
                <div class="col-sm-9">
                    
                    <input type="text" 
                        class="form-control"
                        id="captcha_code" name="captcha_code" value="" title="<?php echo lang('กรุณากรอกรหัสป้องกันสแปมด้วยค่ะ'); ?>">
                    <?php echo @$_SESSION['ctform']['captcha_error'] ?>
                    <?php if ( ! empty($captcha_code_error)) : ?>
                    <label class="error"><?php echo $captcha_code_error; ?></label>
                    <?php endif; ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-2"></label>
            
            
                <div class="col-sm-10 text-left">
                    <input type="submit" class="btn btn-dark" id="btn-save" name="btn-save" value="<?php echo lang('ส่งข้อความ'); ?>">
                </div>
            </div>


        </form>
                  
        
</div>
<?php // page content ?>
