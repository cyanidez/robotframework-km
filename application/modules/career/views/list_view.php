<!-- Classic Heading -->
<h4 class="classic-title"><span><?php echo lang("ตำแหน่งงาน ทั้งหมด"); ?></span></h4>

<div class="latest-posts-classic">

  <!-- Post 1 -->
<?php if ($career['row_count'] > 0) : ?>
  <?php foreach ($career['rows'] as $key => $value) : ?>
  <div class="post-row">
    <div class="col-md-12">
      <h3 class="post-title">
        <a href="<?php echo career_recruit_url($value['id']); ?>"><?php echo $value['name']; ?></a>
      </h3>
      <span>
        <i class="fa fa-calendar"></i> 
        <?php echo show_style_date($value['created_at']); ?> 
        <i class="fa fa-eye"></i> 
        20 Views 
      </span>
      <div class="post-content">
                    
        <p>
          <?php echo show_content($value['short_description']); ?> 
          
        </p>
        <?php if ( ! empty($value['description'])) : ?>
        <p>
          <?php echo $value['description']; ?>
        </p>
        <?php endif; ?>
        <p>
          <a class="btn btt-danger" href="<?php echo career_recruit_url($value['id']); ?>">
            <?php echo lang("สมัครงานตำแหน่งนี้"); ?>
            <i class="fa fa-angle-right"></i>
          </a>
        </p>
      </div>
    </div>
  </div>
  <?php endforeach; ?>
<?php endif; ?>

			
            
                  
                 
                  
                   

  </div>  <!-- Start Pagination -->

<?php echo aira_pagination($career, career_url()); ?>
            <!-- End Pagination -->
              