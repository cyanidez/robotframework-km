<?php
class Career_model extends MY_Model {

    private $_table = "careers";
    private $_table_lang = "career_lang";

    public function get_career($args = array(), $page = 1, $limit = 20)
    {
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.created_at'))
                ->joinLeft(array('NL' => $this->_table_lang), 'NL.career_id = N.id AND NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description', 'NL.description'))
        

                ->where('N.active_status = ?', 'publish');

        if ( ! empty($args['keyword']))
        {
            $select->join(array('NT' => 'career_tag'), 'NT.career_id', array())
                ->where('NT.name LIKE ?', '%'.$args['keyword'].'%');
        }

        $select->where('N.deleted_at IS NULL')
            ->order('N.id desc');

        $data = parent::fetch_page($select, array(), $page, $limit);
        return $data;
    }

    public function get_all_career()
    {
        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.thumbnail', 'N.thumbnail_slider', 'N.created_at'))
            ->joinLeft(array('NL' => $this->_table_lang), 'NL.career_id = N.id AND NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description', 'NL.description'))
            ->where('N.active_status = ?', 'publish')
            ->where('N.deleted_at IS NULL')
            ->order('N.id desc');

        $data = parent::fetch_all($select);
        return $data;
    }
  
    public function get_career_detail($id = NULL)
    {
        if (empty($id))
        {
            return NULL;
        }

        $return = array();

        $select = parent::select();
        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.slug', 'N.created_at'))
            ->joinLeft(array('NL' => $this->_table_lang), 'N.id = NL.career_id and NL.lang_id = '.LANGID, array('NL.name', 'NL.short_description', 'NL.description'))
            ->where('N.id = ?', $id)
            ->where('N.active_status = ?', 'publish')
            ->where('N.deleted_at IS NULL');
        $data = parent::fetch_row($select);
        if (empty($data))
        {
            return NULL;
        }
        $return = array(
            'id' => $data['id'],
            'active_status' => $data['active_status'],
            'name' => $data['name'],
            'slug' => $data['slug'],
            'short_description' => $data['short_description'],
            'description' => $data['description']
        );

        $select = parent::select();

        $select->from(array('N' => $this->_table), array('N.id', 'N.active_status', 'N.active_status', 'N.slug'))
            ->join(array("NL" => $this->_table_lang), 'N.id = NL.career_id AND NL.lang_id != '.LANGID, array('NL.career_id as translate_id', 'NL.name', 'NL.short_description', 'NL.description'))
            ->join(array('L' => 'lang'), 'L.id = NL.lang_id', array('L.code'))
            ->where('N.id  = ?', $id)
            ->where('N.active_status = ?', 'publish')
            ->where('N.deleted_at IS NULL');

        $data = parent::fetch_all($select);
        if ( ! empty($data))
        {
            $translate = array();
            foreach ($data as $key => $value)
            {
                $translate[$value['code']] = array(
                    'id' => $value['translate_id'],
                    'name' => $value['name'],
                    'short_description' => $value['short_description'],
                    'description' => $value['description']
                );
            }
        }

        if ( ! empty($translate))
        {
            $return['translate'] = $translate;
        }
        else
        {
            $return['translate'] = array();
        }
        return $return;
    }

    public function create($params = NULL)
    {
        if (is_null($params) OR count($params) == 0)
        {
            return NULL;
        }

        if (parent::insert($this->_table, $params) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function update($params = NULL, $where = NULL)
    {
        if (is_null($params) OR count($params) == 0)
        {
            return NULL;
        }

        if (is_null($where) OR count($where) == 0)
        {
            return NULL;
        }

        if (parent::update($this->_table, $params, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($where = NULL)
    {
        if (is_null($where)OR count($where) == 0)
        {
            return NULL;
        }
        if (parent::delete($this->_table, $where) !== FALSE)
        {
            return TRUE;
        }
        return FALSE;
    }

    
}