<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists("get_profile")) 
{
	function get_profile($key = NULL, $type = 'session') 
	{
		if (is_null($key) OR empty($key)) 
		{
			return NULL; 
		}
		switch ($type) 
		{
			case 'session' : 
				$CI =& get_instance(); 
				$CI->load->library('session'); 
				return $CI->session->userdata($key); 
				break; 

			case 'cookie' : 
				$CI =& get_instance(); 
				$CI->load->helper('cookie'); 
				return get_cookie($key); 
				break; 

			default : 
				break; 
		}
	}
}

/*** 
*		@return : Void
*/
if ( ! function_exists("set_profile")) 
{
	function set_profile($params = NULL, $value = NULL, $type = 'session') 
	{
		$CI =& get_instance(); 
		#$CI->load->library('native_session', NULL, 'session'); 
		$CI->load->library('session'); 
		$CI->load->helper('cookie'); 

		if (is_array($params)) 
		{
			foreach ($params as $k => $v) 
			{
				switch ($type) 
				{
					case 'session' : 
						#echo "k = ".$k.", v = ".$v; 
						$CI->session->set_userdata($k, $v); 
						break; 
				
					case 'cookie' : 
						set_cookie($k, $v); 
						break; 

					default: 
						break; 
				}
			}
		}
		else 
		{
			switch ($type) 
			{
				case 'session' : 
					$CI->session->set_userdata($params, $value); 
					break; 
				
				case 'cookie' : 					
					set_cookie($params, $value); 
					break; 

				default : 
					break; 
			}
		}
	}
}

if ( ! function_exists("delete_profiles")) 
{
	function delete_profile($params = NULL, $type = 'session')
	{
		$CI =& get_instance();
		#$CI->load->library('native_session',  NULL, 'session'); 
		$CI->load->library('session'); 
		if (is_array($params)) 
		{			
			foreach ($params as $k) 
			{
				$CI->session->unset_userdata($k); 
			}
		}
		else 
		{
			switch ($type) 
			{
				case 'session' : 
					$CI->session->unset_userdata($params); 
					break; 
				
				case 'cookie' : 
					break; 
			
				default : 
					break; 
			}
		}
	}
}

/* End of file my_profile_helper.php */
/* Location: ./webapps/backend/helpers/my_profile_helper.php */
