<?php
if ( ! function_exists('get_title')) 
{
	function get_title($key = 'title.default', $params = NULL) 
	{
		$CI =& get_instance(); 
		$CI->lang->load('meta_title'); 
		return (lang('title.'.$key, $params)) ? lang('title.'.$key, $params) : lang('title.default'); 
	}
}

if ( ! function_exists('get_description')) 
{
	function get_description($key = 'description.default', $params = NULL) 
	{
		$CI =& get_instance(); 
		$CI->lang->load('meta_description'); 
		return (lang('description.'.$key, $params)) ? lang('description.'.$key, $params) : lang('description.default'); 
	}
}

if ( ! function_exists('get_keyword')) 
{
	function get_keyword($key = 'keyword.default', $params = NULL) 
	{
		$CI =& get_instance(); 
		$CI->lang->load('meta_keyword'); 
		return (lang('keyword.'.$key, $params)) ? lang('keyword.'.$key, $params) : lang('keyword.default'); 
	}
}
