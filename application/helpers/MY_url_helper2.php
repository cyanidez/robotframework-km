<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Site product thumbnail
 *
 *
 * @access	 public
 * @param string key from config
 * @return	string
 */
function site_product_thumbnail($item)
{
	$ci =& get_instance();
	return $ci->config->item('static_url').$item;
}

/**
 * slash item
 *
 *
 * @access	 public
 * @param string key from config
 * @return	string
 */
function slash_item($item)
{
	$CI =& get_instance();
	return $CI->config->slash_item($item);
}

/**
 * URI Segment
 *
 *
 * @access	 public
 * @param integer segment of uri
 * @return	string
 */
function uri_segment($n)
{
	$CI =& get_instance();
	return $CI->uri->segment($n);
}

/**
 * Site assets url [Front]
 *
 *
 * @access	 public
 * @param string sub-path
 * @return	string
 */
function site_assets_front($uri='')
{
	$CI =& get_instance();
	if (is_array($uri))
	{
		$uri = implode('/', $uri);
	}

	return ($uri == '') ? slash_item('assets_url') : slash_item('assets_url').trim($uri, '/');
}

/**
 * Site assets url [Backoffice]
 *
 *
 * @access	 public
 * @param string sub-path
 * @return	string
 */
function site_assets_backoffice($uri='')
{
	$CI =& get_instance();
	if (is_array($uri))
	{
		$uri = implode('/', $uri);
	}

	return ($uri == '') ? slash_item('assets_url_backoffice') : slash_item('assets_url_backoffice').trim($uri, '/');
}

/**
 * Site thumbnail upload url
 *
 *
 * @access	 public
 * @param string sub-path
 * @param string true is absolute path
 * @return	string
 */
function site_thumbnail_upload_url($uri='', $server_path=false)
{
	$CI =& get_instance();
	return $CI->site_path->site_thumbnail_upload_url($uri, $server_path);
}

/**
 * Site upload url
 *
 *
 * @access	 public
 * @param string sub-path
 * @param string true is absolute path
 * @return	string
 */
function site_upload_url($uri='', $server_path=false)
{
	$CI =& get_instance();
	if (is_array($uri))
	{
		$uri = implode('/', $uri);
	}
	//echo $uri;
	if ($server_path === true)
	{
		$the_uri = ($uri == '') ? slash_item('upload_dir') : slash_item('upload_dir').trim($uri, '/');
		//echo $the_uri;
		$directory = dirname($the_uri);
		//echo $directory;
		if (!file_exists($directory) && !is_dir($directory))
		{
			mkdir($directory, 0777, true);
		}
		return $the_uri;
	}
	else
	{
		return ($uri == '') ? slash_item('upload_url') : slash_item('upload_url').trim($uri, '/');
	}
}

function site_post_listing($category=NULL, $type='category')
{
	$url = "";
	if ($type === 'category')
	{
		foreach ($category as $key=>$cat)
		{
			if ($key == 0)
			{
				$url.= $cat['type'];
			}
			elseif ($key == 1)
			{
				if ($url != '') $url.= "-";
				$url.= $cat['type'];
			}
			elseif ($key == 2)
			{
				if ($url != '') $url.= "/";
				$url.= $cat['type'];
			}
			elseif ($key == 3)
			{
				if ($url != '') $url.= "/";
				$url.= $cat['type'];
			}
		}
		
		if (!empty($url)) $url.= '.html';
	}
	else 
	{
		$url = 'search/?type='.$category['type'].'&condition='.$category['condition'].'&province='.$category['province'].'&amphur='.$category['amphur'].'&';
	}
	
	return site_url().$url;
}

if ( ! function_exists('site_image_url')) 
{
	function site_image_url($file = NULL) 
	{
		if (is_null($file) OR $file == "") 
		{
			return NULL; 
		}
		$image_path = config_item('image_relative_path'); 
		return site_url($image_path . $file); 	
	}
}


