<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 *
 * @description  Convert fields from array to string 
 * @access	public
 * @param	NULL
 * @return  field list <String>
 */	
if ( ! function_exists('parse_fields'))
{
	function parse_fields($fields = '*')
	{
		if (is_array($fields)) 
		{
			$arr_fields = array(); 
			foreach ($fields as $key => $value) 
			{
				$arr_fields[] = $value; 
			}
			$str_fields = implode(", ", $arr_fields); 
			$out = $str_fields; 
		}
		else
		{
			$out = $fields; 
		}
		return $out; 

	}
}

if ( ! function_exists('parse_list_order')) 
{
	function parse_list_order($order_by, $order_type = 'ASC', $lang_code = 'en') 
	{
		if ($lang_code == 'en') 
		{
			$sql_order = " ORDER BY ".$order_by." ".$order_type." "; 
		}
		else if ($lang_code == 'th') 
		{
			$sql_order = " ORDER BY CONVERT(".$order_by." USING TIS620) ".$order_type." "; 
		}
		else 
		{
			return NULL; 
		}
		return $sql_order; 
	
	}
}

/* End of file my_date_helper.php */
/* Location: ./system/application/helpers/my_date_helper.php */
