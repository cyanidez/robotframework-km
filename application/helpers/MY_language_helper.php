<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Lang
 *
 * Fetches a language variable and optionally outputs a form label
 *
 * @access	public
 * @param	string	the language line
 * @param	string	the id of the form element
 * @return	string
 */
 
function lang($line, $params = NULL)
{
	$CI =& get_instance();

	$line = $CI->lang->line($line, $params);
	
	$numargs = func_num_args();
	
	if ($numargs > 1)
	{
		$args = func_get_args();
		$args[0] = $line;
		$line = call_user_func_array('sprintf', $args);
	}



	if ($CI->config->item('log_translate') == 1)
	{
		$log_path = $CI->config->item('log_path') . 'translate.php';

		ob_start();
		include($log_path);
		$contents = ob_get_clean();

		//preg_match_all('/\[(.*)\]/', $contents, $matches);
		preg_match_all('/\$lang\[\'(.*)\'\]/', $contents, $matches);

		$words = $matches[1];

		if (!in_array($line, $words))
		{
			//if (!preg_match('/[ก-ฮ]+/u', $line)) 
			//{
				// logs
				//$script = $_SERVER['PHP_SELF'];
				//$data = "[" . $line . "]" . '---' . $script . "\n";
				//file_put_contents($log_path, $data, FILE_APPEND);
				
				// array
				$script = $_SERVER['PHP_SELF'];
				$data = '/* '.$script ."*/\n";
				$data .= '$lang[\''. $line .'\']' .' = '. '"";' ."\n";
				file_put_contents($log_path, $data, FILE_APPEND);
			//}
		}
	}
	return $line;
}

/**
 * Purge cache when reload language set
 *
 *
 * @access	 public
 * @return	bool
 */
function reload_languages()
{
	$CI =& get_instance();
	$CI->load->library('language');
	$CI->lang->purge_cache();
	return true;
}

?>