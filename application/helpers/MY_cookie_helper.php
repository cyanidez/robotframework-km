<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_my_cookie'))
{
	function get_my_cookie($index = '', $xss_clean = FALSE)
	{
		$CI =& get_instance(); 
		
		$CI->load->library('encrypt'); 		
		$encrypt_key = $CI->config->item('encrypt_key'); 

		if (get_cookie('logged_in_data') == "") 
		{
			return NULL; 
		}		
		$logged_in_data = get_cookie('logged_in_data'); 		
		$json_string = $CI->encrypt->decode($logged_in_data, $encrypt_key); 	
		$json_obj = json_decode($json_string); 
		if (isset($json_obj->$index))
		{
			return $json_obj->$index; 
		}
		else
		{
			return NULL; 
		}
	}	
}

if ( ! function_exists('set_my_cookie'))
{
	function set_my_cookie($name, $value, $expire, $domain, $path, $prefix, $secure)
	{
		set_cookie($name, $value, $expire, $domain, $path, $prefix, $secure);
	}
}


/**
*		@Desc : Get all cookie in logged_in_data key and set new key to this cookie key 
*/
if ( ! function_exists('set_new_cookie'))
{
	function set_new_cookie($name, $value = NULL, $expire = NULL, $domain = NULL, $path = NULL, $prefix = NULL, $secure = NULL)
	{
		$CI =& get_instance(); 
		
		$CI->load->library('encrypt'); 		
		$encrypt_key = $CI->config->item('encrypt_key'); 

		if (get_cookie('logged_in_data') == "") 
		{
			return NULL; 
		}		
		$logged_in_data = get_cookie('logged_in_data'); 		
		$json_string = $CI->encrypt->decode($logged_in_data, $encrypt_key); 	
		$json_obj = json_decode($json_string); 

		$new_cookie = array();
		foreach ($json_obj as $json_key => $json_value)
		{
			$new_cookie[$json_key] = $json_value; 
		}

		if (is_assoc($name))
		{
			foreach ($name as $json_key => $json_value)
			{
				$new_cookie[$json_key] = $json_value; 
			}
		}
		
		$json_string = json_encode($new_cookie); 
		$data_encrypt = $CI->encrypt->encode($json_string, $encrypt_key); 

		$cookie['logged_in_data'] = $data_encrypt; 
								
								
		$cookie = array(
								'name'  => 'logged_in_data',
								'value'  => $data_encrypt,
								'expire' => 9999999,
								'path' => '/'
						  );
		
		set_cookie($cookie);
		
	}
}

/* End of file MY_cookie_helper.php */
/* Location: ./application/helpers/MY_cookie_helper.php */