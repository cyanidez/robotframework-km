<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_member_profile'))
{
	function get_member_profile($index = '', $xss_clean = FALSE)
	{
		$CI =& get_instance(); 
		
		$CI->load->library('bmtd_encrypt');
		$CI->load->helper('cookie');

		if (get_cookie('member_profile') == "")
		{
			return NULL;
		}

		$member_profile = get_cookie('member_profile');

		$json_string = $CI->bmtd_encrypt->my_decrypt($member_profile);
		$json_obj = json_decode($json_string, true); 
		#alert($json_obj, 'blue', 'json_o')
		if (isset($json_obj[$index]))
		{
			return $json_obj[$index]; 
		}
		else
		{
			return NULL; 
		}
	}	
}

if ( ! function_exists('get_all_member_profile'))
{
	function get_all_member_profile()
	{
		$CI =& get_instance();
		$CI->load->library('bmtd_encrypt');
		$CI->load->helper('cookie');
		if (get_cookie('member_profile') == "")
		{
			return null;
		}
		$member_profile = get_cookie('member_profile');
		$json_string = $CI->bmtd_encrypt->my_decrypt($member_profile);
		$json_array = json_decode($json_string, true); 

		alert($json_array, 'red');
		exit;

		$return = array();

		alert($json_array, 'blue', 'json_array');

		if ( ! empty($json_array))
		{
			foreach ($json_array as $key => $value)
			{
				$return[$key] = $value;
			}
		}
		return $return;
	}
}

// if ( ! function_exists('set_member_profile'))
// {
// 	function set_member_profile($name, $value = NULL, $expire = 999999, $domain = NULL, $path = NULL, $prefix = NULL, $secure = NULL)
// 	{
// 		$CI =& get_instance();
// 		$CI->load->library('bmtd_encrypt');
// 		$CI->load->library('session');
// 		if (is_assoc($name))
// 		{
// 			$data_cookie = array();
// 			foreach ($name as $json_key => $json_value) 
// 			{
// 				$data_cookie[$json_key] = $json_value; 
// 			}


// 			$json_string = json_encode($data_cookie); 
// 			$data_encrypt = $CI->bmtd_encrypt->my_encrypt($json_string);

// 			#$CI->session->set_userdata('member_profile', $data_encrypt);
// 			$cookie = array(
// 								'name'  => 'member_profile',
// 								'value'  => $data_encrypt,
// 								'expire' => 9999999,
// 								'path' => '/'
// 						  );
		
// 			set_cookie($cookie);
// 		}
// 		else
// 		{
// 			$cookie[$name] = $value; 
// 			$json_string = json_encode($cookie); 
// 			$data_encrypt = $CI->chs_encrypt->my_encrypt($json_string); 
// 			$cookie = array(
// 								'name'  => 'member_profile',
// 								'value'  => $data_encrypt,
// 								'expire' => 9999999,
// 								'path' => '/'
// 						  );		
// 			set_cookie($cookie);
// 		}
// 	}
// }

if ( ! function_exists('remove_member_profile'))
{
	function remove_member_profile($keyname = null)
	{
		if (empty($keyname))
		{
			return null;
		}

		$CI =& get_instance();
		$CI->load->library('bmtd_encrypt');
		$CI->load->helper('cookie');
		if (get_cookie('member_cookie') == "")
		{
			return null;
		}
		$member_profile = get_cookie('member_profile');
		$json_string = $CI->bmtd_encrypt->my_decrypt($member_profile);
		$json_array = json_decode($json_string, true); 

		$session = array();

		if ( ! empty($json_array))
		{
			foreach ($json_array as $key => $value)
			{
				if ($key != $keyname)
				{
					$session[$key] = $value;
				}
			}

			$json_string = json_encode($session, true); 
			$data_encrypt = $CI->bmtd_encrypt->my_encrypt($json_string);
			$cookie = array(
				'name'  => 'member_profile',
				'value'  => $data_encrypt,
				'expire' => 9999999,
				'path' => '/'
			);
			set_cookie($cookie);

		}

		
		

	}
}
if ( ! function_exists('delete_member_profile'))
{
	function delete_member_profile($key = NULL)
	{
		$CI =& get_instance();
		$CI->load->helper('cookie');
		delete_cookie('member_profile');
	}
}

/**
*		@Desc : Get all cookie in 'member_profile_cookie' key and set new key to this cookie key 
*/
if ( ! function_exists('set_member_profile'))
{
	function set_member_profile($name, $value = NULL, $expire = NULL, $domain = NULL, $path = NULL, $prefix = NULL, $secure = NULL)
	{
		$CI =& get_instance(); 
		
		$CI->load->library('bmtd_encrypt');

		//--- If has existing member profile ---//
		$new_cookie = array();
		if (get_cookie('member_profile') != "")
		{
			$member_profile_data = get_cookie('member_profile'); 		
			$json_string = $CI->bmtd_encrypt->my_decrypt($member_profile_data);
			$json_obj = json_decode($json_string, true); 

			#alert($json_obj, 'red', 'json_obj');
			
			foreach ($json_obj as $json_key => $json_value)
			{
				$new_cookie[$json_key] = $json_value; 
			}
		}
		if (is_assoc($name))
		{
			foreach ($name as $json_key => $json_value)
			{
				$new_cookie[$json_key] = $json_value; 
			}
		}
		else 
		{
			$new_cookie[$name] = $value;
		}

		#alert($new_cookie, 'blue', 'new cookie');
		#exit; 
		$json_string = json_encode($new_cookie, true); 
		$data_encrypt = $CI->bmtd_encrypt->my_encrypt($json_string);
		$cookie = array(
			'name'  => 'member_profile',
			'value'  => $data_encrypt,
			'expire' => 0,
			'path' => '/'
		);
		set_cookie($cookie);
	}
}

if ( ! function_exists('create_customer_ref_id'))
{
	function create_customer_ref_id()
 	{
		$str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxzy0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvxzy0123456789';
		$shuffle = str_shuffle($str);
		$customer_ref_id = substr($shuffle, 4, 38);
		
		$session = array(
			'customer_ref_id' => $customer_ref_id,
			'customer_type' => 'non-user'
		);
			
		set_member_profile($session);
		return $session;
	}
}


/* End of file MY_member_profile_helper.php */
/* Location: ./application/helpers/MY_member_profile_helper.php */