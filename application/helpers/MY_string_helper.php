<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Encoding source code when show email on webpage
 * @access	public
 * @param	NULL
 * @return  String (encoding)
 */	
 if ( ! function_exists("hide_email")) 
 {
	function hide_email($email) {
	  $character_set  = '+-.0123456789@ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';
	  $key = str_shuffle($character_set);
	  $cipher_text = '';
	  $id = 'e'.rand(1,999999999);

	  for ($i=0;$i<strlen($email);$i+=1) $cipher_text.= $key[strpos($character_set,$email[$i])];
	  $script = 'var a="'.$key.'";
	  var b=a.split("").sort().join("");
	  var c="'.$cipher_text.'";var d="";';
	  $script.= 'for(var e=0;e<c.length;e++)d+=b.charAt(a.indexOf(c.charAt(e)));';
	  $script.= 'document.getElementById("'.$id.'").innerHTML="<a href=\\"mailto:"+d+"\\">"+d+"</a>"';
	  $script = "eval(\"".str_replace(array("\\",'"'),array("\\\\",'\"'), $script)."\")";
	  $script = '<script type="text/javascript">/*<![CDATA[*/'.$script.'/*]]>*/</script>';
	  return '<span id="'.$id.'">[javascript protected email address]</span>'.$script;
	}
 }

 if ( ! function_exists("my_substr")) 
 {
	function my_substr($str = NULL, $length = 9) 
	{
		if (is_null($str) OR empty($str)) 
		{
			return NULL; 
		}
		return (mb_strlen($str, 'utf-8') > $length) ? mb_substr($str, 0, $length, 'utf-8').'..' : $str; 
	}
 }

if ( ! function_exists('valid_datetime'))
{
	function valid_datetime($datetime =  null)
	{
		if (empty($datetime))
		{
			return false;
		}

		if (preg_match("/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}\s[0-9]{2}\:[0-9]{2}\:[0-9]{2}$/", $datetime))
		{
			return true;
		}
		return false;
	}
}
 
if ( ! function_exists('datetime_show_on_web'))
{
	function datetime_show_on_web($datetime = null)
	{
		if (empty($datetime))
		{
			return null;
		}
		if ( ! valid_datetime($datetime))
		{
			return null;
		}
		
	}
}

if ( ! function_exists('bb_code'))
{
    function bb_code($str = null)
    {
        if  (empty($str))
        {
            return null;
        }

        $replace = array('<br>', '&nbsp;', '<strong>', '</strong>', '<i>', '</i>', '<u>', '</u>');
        $search = array("\n", ' ', '[b]', '[/b]', '[i]', '[/i]', '[u]', '[/u]');

        $str = nl2br(str_replace($search, $replace, $str));
        return $str;
    }
}

if ( ! function_exists('convert_error_msg'))
{
    function convert_error_msg($str = null)
    {
        if (empty($str))
        {
            return null;
        }

        $str = trim(strip_tags($str));

        if ($str == "upload_invalid_dimensions")
        {
            $str =  "The image you are attempting to upload exceedes the maximum height or width.";

        }
        elseif ($str == "upload_invalid_filetype")
        {
            $str = "The filetype you are attempting to upload is not allowed.";
        }
        elseif ($str == "upload_invalid_filesize")
        {
            $str = "The file you are attempting to upload is larger than the permitted size.";
        }

        return $str;
    }
}

if ( ! function_exists('show_content'))
{
	function show_content($str = null)
	{
		if (empty($str))
		{
			return null; 
		}
		$replace = array(
			'\n', '\r', '\t', ' '
		);
		$search = array(
			'<br>', '<br>', '&nbsp;&nbsp;&nbsp;&nbsp;', '&nbsp;'
		);
		return str_replace($replace, $search, $str);
	}
}

/* End of file MY_string_helper.php */
/* Location: ./application/helpers/MY_string_helper.php */
