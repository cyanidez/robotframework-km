<?php
if ( ! function_exists('get_tree_collection'))
{
	function get_tree_collection($array, $html = '')
	{
		#alert($array, 'red');
	    foreach($array as $key => $item)
	    {
	    	#alert($item, 'red');
	    	#echo $html;

	        if (is_array($item) && isset($item['name']))
	        {
	            $html .= "<ul>";

	            if ( ! empty($item['children']))
	            {
	                $html .= "<li style='background:green;'>".$item['name'];
	                get_tree_collection($item, $html);
	                $html .= "</li>\n";
	            }
	            else 
	            {
	                $html .= "<li style='background:red;'>".$item['name']."</li>\n";
	            }

	            $html .= "</ul>\n\n";
	        }
	    }

	    return $html;	
	}
}
function olLiTree( $tree ) {
    echo '<ul style="list-style:none; margin:10px 0px;">';
    foreach ( $tree as $item ) {
        echo '<li id="'.$item['id'].'" parent_id="$item[parent]" data-level="'.$item['level'].'">';
        	if ( ! empty($item['product_id']))
        	{
        		$checked = 'checked="checked"';
        	}
        	else 
        	{
        		$checked = "";
        	}
        	if ($item['level'] == 1)
        	{
        		$class = " collection-main";
        	}
        	else
        	{
        		$class = "";
        	}
        	echo '<input type="checkbox" class="collections'.$class.'" name="collection_id[]" '.$checked.' value="'.$item['id'].'|'.$item['level'].'"> &nbsp; ';
        	echo $item['name'];
        echo '</li>';
        if ( isset( $item['children'] ) ) {
            olLiTree( $item['children'] );
        }
    }
    echo '</ul>';
}

// function display_with_children($parentRow, $level) { 
//     echo '<li>'.$parentRow['name']; 
//     // if your id column is integer, you don't need the quotation mark
//     $result = mysql_query('SELECT * FROM category WHERE parant_id='.$parentRow['id'].';');
//     if (mysql_num_rows($result) != 0) {
//         echo '<ul>';
//         // use the fetch_assoc to get an associative array
//         while ($row = mysql_fetch_assoc($result)) { 
//             display_with_children($row, $level+1); 
//         } 
//         echo '</ul>';
//     }
//     echo '</li>';
// } 