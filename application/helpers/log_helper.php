<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('write_log')) 
{
	function write_log($message = NULL, $module = NULL) 
	{
		if (is_null($message) OR empty($message)) 
		{
			return NULL; 
		}

		$FILE = fopen(trim(config_item('log_dir'), '/') . '/' .'log-'.$module.'-'.date("Y-m-d").'.txt', "a+"); 
		flock($FILE, LOCK_EX); 
		fputs($FILE,  '['.date("Y-m-d H:i:s").'] #' . $message."\n"); 
		fputs($FILE, '-------------------------------------------------------------------------------');
		flock($FILE, LOCK_UN); 
		fclose($FILE); 
	}
}

if ( ! function_exists('save_log')) 
{
	function save_log($message = NULL) 
	{
		if (is_null($message) OR $message == "") 
		{
			return NULL; 
		}

		$FILE = fopen($_SERVER['DOCUMENT_ROOT']."/log.txt", "a+"); 
		flock($FILE, LOCK_EX);
		fputs($FILE, '['.date("Y-m-d H:i:s").'] # '.$message."  \n"); 
		flock($FILE, LOCK_UN); 
		fclose($FILE); 
	}
}

if ( ! function_exists('read_log')) 
{
	function read_log($file = NULL) 
	{
		if (is_null($file) OR $file == "") 
		{
			return NULL; 
		}
		$FILE = fopen($file, "r+"); 
		
	}
}

if ( ! function_exists('check_exist_log')) 
{
	function check_exist_log($log_file = NULL) 
	{
		if (is_null($log_file) OR $log_file == "") 
		{
			return FALSE; 
		}

		$CI =& get_instance(); 

		if (file_exists($CI->input->server('DOCUMENT_ROOT').'log/'.$log_file)) 
		{
			return TRUE; 
		}
		return FALSE; 
	}
}

/* End of file log_helper.php */
/* Location: ./application/helpers/log_helper.php */