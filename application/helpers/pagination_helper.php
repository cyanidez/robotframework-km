<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('replace_query_string')) 
{
	function replace_query_string() 
	{
	}
}

if ( ! function_exists('sort_by')) 
{
	function sort_by($field = NULL) 
	{
		$CI =& get_instance(); 

		$request_uri = htmlspecialchars($CI->input->server('REQUEST_URI')); 

		if (is_assoc($field)) 
		{
			$request_uri = preg_replace("/&amp;sort_by=(.*)/", "", $request_uri); 
			if (preg_match("/desc_asc=desc/", $request_uri))
			{
				$request_uri = preg_replace("/desc_asc=desc/", "desc_asc=asc", $request_uri); 
			}
			else
			{
				$request_uri = preg_replace("/desc_asc=asc/", "desc_asc=desc", $request_uri); 
			}
			$query_string = $CI->input->server('QUERY_STRING');
			if (empty($query_string)) 
			{
				$request_uri = $request_uri . '?'.http_build_query($field); 
			}
			else
			{
				$request_uri = $request_uri . '&amp;'.http_build_query($field); 
			}

			if ($CI->input->get_post('desc_asc', TRUE) == "")
			{
				$request_uri = $request_uri . '&amp;desc_asc=desc'; 
			}
			$uri = $request_uri; 
		}
		else
		{
			foreach ($field as $key => $value) 
			{
				$request_uri = preg_replace("/".$key."=".$value.'/', '', $request_uri); 
			}
			if (preg_match("/desc_asc=desc/", $request_uri))
			{
				$request_uri = preg_replace("/desc_asc=desc/", "desc_asc=asc", $request_uri); 
			}
			else
			{
				$request_uri = preg_replace("/desc_asc=asc/", "desc_asc=desc", $request_uri); 
			}
			$uri = $request_uri . $field; 
		}
		return site_url($uri); 
	}
}

