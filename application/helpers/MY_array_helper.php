<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('is_assoc'))
{
	function is_assoc($arr) 
	{
        return (is_array($arr) && ( ! count($arr) OR count(array_filter(array_keys($arr), 'is_string')) == count($arr)));
    }
}

if ( ! function_exists('unique_multi_array'))
{
	function unique_multi_array($array, $sub_key) 
	{
		$target = array();
		$existing_sub_key_values = array();
		foreach ($array as $key => $sub_array) 
		{
			if ( ! in_array($sub_array[$sub_key], $existing_sub_key_values)) 
			{
				$existing_sub_key_values[] = $sub_array[$sub_key];
				$target[$key] = $sub_array;
			}
		}
		return $target;
	} 
}

/* End of file MY_array_helper.php */
/* Location: ./application/helpers/MY_array_helper.php */
