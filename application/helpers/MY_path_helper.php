<?php
if ( ! function_exists("css_path")) 
{
	function css_path($file = NULL, $module = 'mtc') 
	{
		if (is_null($file) OR $file == "") 
		{
			return NULL; 
		}
		$CI =& get_instance(); 
		$CI->load->config('path_config'); 

		$css_path = $CI->config->item('css_path'); 
		return site_url(str_replace('{0}', $module, $css_path).$file); 
	}
}

if ( ! function_exists('js_path')) 
{
	function js_path($file = NULL, $module = 'mtc') 
	{
		if (is_null($file) OR $file == "") 
		{
			return NULL; 
		}
		$CI =& get_instance(); 
		$CI->load->config('path_config'); 
		$js_path = $CI->config->item('js_path'); 
		return site_url(str_replace('{0}', $module, $js_path).$file); 
	}
}

if ( ! function_exists('js_plugins_path')) 
{
	function js_plugins_path($file = NULL, $module = 'mtc') 
	{
		if (is_null($file) OR $file == "") 
		{
			return NULL; 
		}
		$CI =& get_instance(); 
		$CI->load->config('path_config'); 
		$js_plugins_path = $CI->config->item('js_plugins_path'); 

		return site_url(str_replace('{0}', $module, $js_plugins_path).$file); 
	}
}

if ( ! function_exists('image_path')) 
{
	function image_path($file = NULL, $module = 'mtc') 
	{
		if (is_null($file) OR $file == "") 
		{
			return NULL; 
		}
		$CI =& get_instance(); 
		$CI->load->config('path_config'); 
		$image_path = $CI->config->item('images_path'); 
		return site_url(str_replace('{0}', $module, $image_path).$file); 
	}

}

if ( ! function_exists('assets_path')) 
{
	function assets_path($file = NULL) 
	{
		if (is_null($file) OR $file == "") 
		{
			return NULL; 
		}
		$CI =& get_instance(); 
		$CI->load->config('path_config'); 
		$assets_path = $CI->config->item('assets_path'); 
		return site_url($assets_path.$file); 
	}

}

if ( ! function_exists('cdn_image_path')) 
{
	function cdn_image_path($file = NULL) 
	{
		if (is_null($file) OR $file == "") 
		{
			return NULL; 
		}
		$CI =& get_instance(); 
		$CI->load->config('path_config'); 
		$cdn_image_path = $CI->config->item('cdn_image_path'); 
		return site_url($cdn_image_path .  $file); 
	}
}



if ( ! function_exists('assets_url'))
{
	function assets_url($file = NULL, $server_path = false)
	{
		$CI =& get_instance(); 
		$CI->load->config('path_config'); 
		if ($server_path === TRUE) 
		{
            if (is_array($file))
            {
                $file = implode('/', $file);
            }
            mkdir($file, TRUE);
		}
		else
		{

		}
		
		return site_url($cover_video_path . $file); 
	}
}







