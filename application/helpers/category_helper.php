<?php
function show_parent($parent = NULL)
{
	if (count($parent) > 0)
	{
		$i = 0;
		foreach ($parent as $key => $value)
		{
			
			$return[$i] = array(
								'key' => $key,
								'categoryID' => $value['categoryID'],
								'category_name' => $value['category_name'],
								'category_slug' => $value['category_slug'],
								'parent' => $value['parent'],
								'level' => $value['level']
							);
			if (count($value['child']) > 0)
			{
				$i++;
				$return[$i] = show_parent($value['child']);
			}
			
			$i++;

		}

		alert($return, 'red');
	}

	return $return;
}

function array_value_recursive($key, array $arr){
    $val = array();
    array_walk_recursive($arr, function($v, $k) use($key, &$val){
        if($k == $key) array_push($val, $v);
    });
    return count($val) > 1 ? $val : array_pop($val);
}

function test($item = NULL, $key = NULL)
{
	echo "<p>key = ".$item." key = ".$key.'</p>';
}

function array2str($array, $pre = '', $pad = '', $sep = ', ')
{
    $str = '';
    if (is_array($array)) 
	{
        if (count($array)) 
		{
            foreach ($array as $v) 
			{
                $str .= $pre.$v.$pad.$sep;
            }
            $str = substr($str, 0, -strlen($sep));
        }
    } 
	else 
	{
        $str .= $pre.$array.$pad;
    }

    return $str;
}

if ( ! function_exists('category_recursive'))
{
	function category_recursive($array = NULL, $list_open = FALSE, $first = FALSE, $current_categoryID = NULL)
	{
		$CI =& get_instance();
		$html = ""; 
		if ( ! empty($array))
		{
			if ($list_open == TRUE)
			{
				if ($first == TRUE)
				{
					$html .=  '<ul class="list-unstyled">'; 			
				}
				else
				{
					$html .=  '<ul class="list-unstyled" style="margin-left:20px;">'; 
				}
			}
			foreach ($array as $key => $value) 
			{
				
				$is_bold = ($CI->input->get_post('id') == $value['id']) ? 'style="font-weight:bold;"' : ""; 
				#$checked = ( ! empty($value['songID'])) ? 'checked="checked"' : ""; 

				$html .= '<li style="text-align:left;">'
						. ' - <a '.$is_bold.' href="'.site_admin_url('category/category/update?id='.$value['id']).'" id="category-anchor-'.$value['id'].'" title="'.$value['name'].'">'.$value['name'].'</a>';
				
				if ( ! empty($value['child']))
				{
					$html .= category_recursive($value['child'], TRUE, FALSE, $current_categoryID);
				}

				$html .= '</li>';
			}
			if ($list_open == TRUE)
			{
				$html .= '</ul>'; 
			}
		}

	   	return $html;

    }

 

}

if ( ! function_exists('manage_category_recursive'))
{
	function manage_category_recursive($array = NULL, $list_open = FALSE, $first = FALSE)
	{
		$html = ""; 
		if ( ! empty($array))
		{
			if ($list_open == TRUE)
			{
				if ($first == TRUE)
				{
					$html .=  '<ul>'; 			
				}
				else
				{
					$html .=  '<ul style="margin-left:20px;">'; 
				}
			}
			foreach ($array as $key => $value) 
			{
				$checked = ( ! empty($value['songID'])) ? 'checked="checked"' : ""; 

				$html .= '<li style="text-align:left;">'
							. ' <a href="'.site_admin_url('category/category/update?categoryID='.$value['categoryID']).'" id="category-anchor-'.$value['categoryID'].'" title="'.$value['category_name'].'">'.$value['category_name'].'</a>';
				
				if (count($value['child']) > 0) 
				{
					$html .= manage_category_recursive($value['child'], TRUE);
				}

				$html .= '</li>';
			}
			if ($list_open == TRUE)
			{
				$html .= '</ul>'; 
			}
		}
	   return $html;
    }
}

function recursive($array, $category_id = NULL)
{
	$html = '';

    foreach ($array as $item) 
	{
		if ( ! empty($category_id) && $category_id == $item['id'])
		{
			echo '<option value="'.$item['id'].'" selected="selected">|'.str_repeat('-', $item['level'] * 3).$item['name'].'</option>';
		}
		else
		{
			echo '<option value="'.$item['id'].'">|'.str_repeat('-', $item['level'] * 3).$item['name'].'</option>';
		}
        if (count($item['child']) > 0) 
		{
            $html .= recursive($item['child'], $category_id);
        } 		
    }

    return $html;
}

function olLiTree($tree = null, $current = null) {
    echo '<ul style="list-style:none; margin:10px 0px;">';
    foreach ( $tree as $item ) {
        echo '<li data-name="'.$item['name'].'" id="'.$item['id'].'" parent_id="$item[parent]" data-level="'.$item['level'].'">';
        	if ( ! empty($item['category_id']))
        	{
        		$checked = 'checked="checked"';
        	}
        	else 
        	{
        		$checked = "";
        	}
        	if ($item['level'] == 1)
        	{
        		$class = " category-main";
        	}
        	else
        	{
        		$class = "";
        	}
        	echo '<input type="checkbox" class="category'.$class.'" name="category_id[]" '.$checked.' value="'.$item['id'].'|'.$item['level'].'"> &nbsp; ';
        	echo $item['name'];
        
        if ( isset( $item['children'] ) ) 
        {
        	
            olLiTree( $item['children'] );
            
        }
        echo '</li>';
    }
    echo '</ul>';
}
