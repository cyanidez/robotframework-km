<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('home_url')) 
{
	function home_url() 
	{
		return site_url(); 
	}
}

if ( ! function_exists('page_url'))
{
	function page_url($page_slug = NULL, $pageID = NULL)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 

		$config_url = $CI->config->item('page_url'); 
		
		$replace = array($page_slug, $pageID); 
		$search = array('{PAGE_SLUG}', '{PAGE_ID}');
		return site_url(str_replace($search, $replace, $config_url)); 
	}
}

if ( ! function_exists('static_page_url'))
{
	function static_page_url($uri = NULL) 
	{
		
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('static_page_url'); 
		
		if (is_array($uri))
		{
			$uri = implode('/', $uri);
		}
		$replace = array($uri); 
		$search = array('{STATIC_PAGE_SLUG}'); 
		return ($uri == '') ? $CI->config->slash_item('static_page_url') : base_url() . str_replace($search, $replace, $CI->config->slash_item('static_page_url')); 

	}
}

if ( ! function_exists('auth_url'))
{
	function auth_url($redir = NULL) 
	{
		$CI =& get_instance();
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('auth_url');

		if ( ! is_null($redir) && ! empty($redir))
		{
			return site_url($config_url. '?redir='.$redir); 
		}
		else
		{
			return site_url($config_url);
		}
	}
}

if ( ! function_exists('node_url'))
{
	function node_url($slug = NULL, $id = NULL) 
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('node_url'); 
		$replace = array($slug, $id); 
		$search = array('{SLUG}', '{ID}'); 
		return lang_url(str_replace($search, $replace, $config_url));
	}
}

if ( ! function_exists('node_detail_url'))
{
	function node_detail_url($slug = NULL, $id = NULL) 
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('node_detail_url'); 
		$replace = array($slug, $id); 
		$search = array('{SLUG}', '{ID}'); 
		return lang_url(str_replace($search, $replace, $config_url));
	}
}

if ( ! function_exists('category_url'))
{
	function category_url($slug = NULL, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('category_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));
	}
}

if ( ! function_exists('category_detail_url'))
{
	function category_detail_url($slug = NULL, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('category_detail_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));
	}
}


if ( ! function_exists('news_url'))
{
	function news_url($slug = NULL, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('news_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));
	}
}

if ( ! function_exists('news_detail_url'))
{
	function news_detail_url($slug = NULL, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('news_detail_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));
	}
}

if ( ! function_exists('aira_group_url'))
{
	function aira_group_url($slug = NULL, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('aira_group_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));
	}
}

if ( ! function_exists('aira_group_detail_url'))
{
	function aira_group_detail_url($slug = NULL, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('aira_group_detail_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));
	}
}

if ( ! function_exists('category_detail_url'))
{
	function category_detail_url($slug = NULL, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('category_detail_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));
	}
}

if ( ! function_exists('download_url'))
{
	function download_url($slug = NULL, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('download_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));
	}
}

if ( ! function_exists('partner_url'))
{
	function partner_url($slug = NULL, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('partner_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));
	}
}

if ( ! function_exists('partner_detail_url'))
{
	function partner_detail_url($slug = NULL, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('partner_detail_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));
	}
}


if ( ! function_exists("contactus_url"))
{
	function contactus_url()
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('contactus_url'); 
		return lang_url($config_url);	
	}
}

if ( ! function_exists("career_detail_url"))
{
	function career_detail_url($slug = null, $id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('career_detail_url'); 
		$replace = array($slug, $id);
		$search = array('{SLUG}', '{ID}');
		return lang_url(str_replace($search, $replace, $config_url));	
	}
}


if ( ! function_exists('career_url'))
{
	function career_url()
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('career_url'); 
		return lang_url($config_url);	
	}
}

if ( ! function_exists('career_recruit_url'))
{
	function career_recruit_url($id = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('career_recruit_url'); 
		return lang_url($config_url.'/'.$id);	
	}
}

if ( ! function_exists('splash_page_url'))
{
	function splash_page_url($uri = null)
	{
		$CI =& get_instance(); 
		$CI->load->config('url_config'); 
		$config_url = $CI->config->item('splash_page_url'); 
		return site_url($config_url.'/'.$uri);	
	}
}

if ( ! function_exists("lang_url"))
{
	function lang_url($uri = null)
	{
		if (LANGCODE == DEFAULT_LANG_CODE) 
		{
			return site_url($uri);
		}
		else 
		{
			return site_url(LANGCODE . '/' . trim($uri, '/'));
		}
	}
}

if ( ! function_exists('th_url'))
{
	function th_url($uri = null) 
	{
		if (empty($uri))
		{
			return null; 
		}
		if (preg_match("/^\/en/", $uri))
		{
			$uri = preg_replace("/^\/[a-z]{2}/", "", $uri);
		}

		return site_url($uri);
	}
}

if ( ! function_exists('en_url'))
{
	function en_url($uri = null) 
	{
		if (empty($uri))
		{
			return null; 
		}
		if ( ! preg_match("/en/", $uri))
		{
			$uri = "en/".trim($uri, "/"); 
		}

		return site_url($uri);
	}
}




/* End of file url_manager_helper.php */
/* Location: ./application/helpers/url_manager_helper.php */