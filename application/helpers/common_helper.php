<?php
if ( ! function_exists('alert'))
{
	function alert($str = NULL, $color = '#000000', $message = NULL, $debug = FALSE)
	{
		echo '<pre><font color="'.$color.'">';
		if ($debug == TRUE)
		{
			echo '<h3 style="margin:20px 0px;">#################################### START DEBUG ################################################</h3>';
		}
		if ( ! empty($message))
		{
			echo '<h1 style="color:'.$color.'; margin:10px 0px;">'.$message.'</h1>';
		}
		if ($debug == TRUE)
		{
		echo '<p style="margin:20px 0px;">=================== Array Value =======================</p>'; 
		}
		print_r($str);
		if ($debug == TRUE)
		{
		echo '<p style="margin:20px 0px;">===================Debug Backtrace=======================</p>'; 
		print_r(debug_backtrace());
		
		
		echo '<h3 style="margin:20px 0px;">#################################### END DEBUG ################################################</h3>';
		}
		echo '</font></pre>';
	}
}

if ( ! function_exists('alertd'))
{
    function alertd($str = NULL, $color = '#000000', $message = NULL, $debug = FALSE)
    {
        echo '<pre><font color="'.$color.'">';
        if ($debug == TRUE)
        {
            echo '<h3 style="margin:20px 0px;">#################################### START DEBUG ################################################</h3>';
        }
        if ( ! empty($message))
        {
            echo '<h1 style="color:'.$color.'; margin:10px 0px;">'.$message.'</h1>';
        }
        if ($debug == TRUE)
        {
            echo '<p style="margin:20px 0px;">=================== Array Value =======================</p>';
        }
        print_r($str);
        if ($debug == TRUE)
        {
            echo '<p style="margin:20px 0px;">===================Debug Backtrace=======================</p>';
            print_r(debug_backtrace());


            echo '<h3 style="margin:20px 0px;">#################################### END DEBUG ################################################</h3>';
        }
        echo '</font></pre>';
        exit;
    }
}

if ( ! function_exists('show_debug'))
{
	function show_debug()
	{
	}
}

/**
 * @todo convert len for utf8
 * @return length
 */
if ( ! function_exists('utf8_strlen'))
{
	function utf8_strlen($s) 
	{
		$c = strlen($s); $l = 0;
	    for ($i = 0; $i < $c; ++$i) if ((ord($s[$i]) & 0xC0) != 0x80) ++$l;
	    return $l;
	}
}

/**
 * @todo replace str for condition
 * @return string
 */
if ( ! function_exists('cleanurl'))
{
	function cleanurl($word) 
	{
		$newphrase = str_replace('/', '-', $word);
		$newphrase = str_replace('?', '-', $newphrase);
		$newphrase = str_replace(' ', '-', $newphrase);
		$newphrase = str_replace('%', '', $newphrase);
		return $newphrase;
	}
}

/**
 * @todo strip word
 * @return string
 */
if ( ! function_exists('strip_word'))
{
	function strip_words($word, $lchar=140, $replace="...") 
	{	
		$text = mb_substr(strip_tags(htmlspecialchars_decode($word)),0,$lchar,'UTF-8');
		if ((utf8_strlen($word)) > $lchar) 
		{
			$text = $text.$replace;
		}
		return $text;
	}
}

/**
 * @todo check ood
 * @return boolean
 */
if ( ! function_exists('isOdd'))
{
	function isOdd($number) {
		if ($number % 2 == 0) {
			return FALSE;
		} else {
			return TRUE;
		}
	}
}

/**
 * @todo gen tag pagination
 * @param $data=array pagination
 * @param $url
 * @param $css_style
 * @return html tag
 */
if ( ! function_exists('gen_html_pagination'))
{
	function gen_html_pagination($data = array(), $url = '', $query_string = NULL) 
	{
		$html = '';
		if ($data['row_count'] > 0 && $data['total_page'] > 1) 
		{
			$html.= '<ul class="pagination">';
			if ($data['current_page'] > 1) 
			{
				$back = $data['current_page'] - 1;
				
				if (preg_match('/(.*)\?(.*)=(.*)/', $url)) 
				{
					if ( ! is_null($query_string) && $query_string != "") 
					{
						$html .= "<li class=\"page_back\"><a href=\"".$url."&amp;start=".$back."&amp;".$query_string."\" rel=\"nofollow\">".lang('prev')."</a></li>";
					}
					else
					{
						$html .= "<li class=\"page_back\"><a href=\"".$url."&amp;start=".$back."\" rel=\"nofollow\">".lang('prev')."</a>";
					}
				}
				else
				{
					if ( ! is_null($query_string) && $query_string != "") 
					{
						$html .= "<li class=\"page_back\"><a href=\"".$url."?start=".$back."&amp;".$query_string."\" rel=\"nofollow\">".lang('prev')."</a></li>";
					}
					else
					{
						$html .= "<li class=\"page_back\"><a href=\"".$url."?start=".$back."\" rel=\"nofollow\">".lang('prev')."</a></li>";
					}
				}
			}
			
			if ($data['start'] > 1) 
			{
				#$html.= "<span>&nbsp;...&nbsp;</span>";
			}
			for ($i = $data['start']; $i <= $data['end']; $i++) 
			{
				if ($i == $data['current_page']) 
				{
					$html .= "<li class=\"current\">".$i."</li>";
				}
				else 
				{
					if (preg_match('/(.*)\?(.*)=(.*)/', $url)) 
					{
						if ( ! is_null($query_string) && $query_string != "") 
						{
							$html .= "<li><a href=\"".$url."&amp;start=".$i."&amp;".$query_string."\" rel=\"nofollow\">".$i."</a></li>";
						}
						else 
						{
							$html .= "<li><a href=\"".$url."&amp;start=".$i."\" rel=\"nofollow\">".$i."</a></li>";
						}
					}
					else
					{
						if ( ! is_null($query_string) && $query_string != "") 
						{
							$html .= "<li><a href=\"".$url."?start=".$i."&amp;".$query_string."\" rel=\"nofollow\">".$i."</a></li>";
						}
						else 
						{
							$html .= "<li><a href=\"".$url."?start=".$i."\" rel=\"nofollow\">".$i."</a></li>";
						}
					}
				}
			}
			if ($data['current_page'] < $data['total_page']) 
			{
				$next = $data['current_page']+1;
				if (preg_match('/(.*)\?(.*)=(.*)/', $url)) 
				{
					if ( ! is_null($query_string) && $query_string != "") 
					{
						$html .= "<li><a href=\"".$url."&amp;start=".$next."&amp;".$query_string."\" rel=\"nofollow\">".lang('next')."</a></li>";
					}
					else 
					{
						$html .= "<li><a href=\"".$url."&amp;start=".$next."\" rel=\"nofollow\">".lang('next')."</a></li>";
					}
				}
				else
				{
					if ( ! is_null($query_string) && $query_string != "") 
					{
						$html .= "<li><a href=\"".$url."?start=".$next."&amp;".$query_string."\" rel=\"nofollow\">".lang('next')."</a></li>";
					}
					else 
					{
						$html .= "<li><a href=\"".$url."?start=".$next."\" rel=\"nofollow\">".lang('next')."</a></li>";
					}
				}
			}
			if ($data['end'] < $data['total_page']) 
			{
				#$html.= "<span>&nbsp;...&nbsp;</span>";
			}	
			$html.= '</ul>';
		}
		
		return $html;
	}
}

if ( ! function_exists('bootstrap_pagination'))
{
    function bootstrap_pagination($data = array(), $url = '', $query_string = NULL)
    {
        $html = '';
        $query_string = preg_replace('/page=[0-9]*/', '', $query_string);
        if ($data['row_count'] > 0 && $data['total_page'] > 1)
        {
            $html.= '<ul class="pagination">';
            if ($data['current_page'] > 1)
            {
                $back = $data['current_page'] - 1;

                if (preg_match('/(.*)\?(.*)=(.*)/', $url))
                {
                    if ( ! is_null($query_string) && $query_string != "")
                    {
                        $html .= "<li class=\"page_back\"><a href=\"".$url."&amp;page=".$back."&amp;".$query_string."\" rel=\"nofollow\">".lang('prev')."</a></li>";
                    }
                    else
                    {
                        $html .= "<li class=\"page_back\"><a href=\"".$url."&amp;page=".$back."\" rel=\"nofollow\">".lang('prev')."</a>";
                    }
                }
                else
                {
                    if ( ! is_null($query_string) && $query_string != "")
                    {
                        $html .= "<li class=\"page_back\"><a href=\"".$url."?page=".$back."&amp;".$query_string."\" rel=\"nofollow\">".lang('prev')."</a></li>";
                    }
                    else
                    {
                        $html .= "<li class=\"page_back\"><a href=\"".$url."?page=".$back."\" rel=\"nofollow\">".lang('prev')."</a></li>";
                    }
                }
            }

            if ($data['start'] > 1)
            {
                #$html.= "<span>&nbsp;...&nbsp;</span>";
            }
            for ($i = $data['start']; $i <= $data['end']; $i++)
            {
                if ($i == $data['current_page'])
                {
                    $html .= "<li class=\"active\"><a href=\"#\">".$i."</a></li>";
                }
                else
                {
                    if (preg_match('/(.*)\?(.*1)=(.*)/', $url))
                    {
                        if ( ! is_null($query_string) && $query_string != "")
                        {
                            $html .= "<li><a href=\"".$url."&amp;page=".$i."&amp;".$query_string."\" rel=\"nofollow\">".$i."</a></li>";
                        }
                        else
                        {
                            $html .= "<li><a href=\"".$url."&amp;page=".$i."\" rel=\"nofollow\">".$i."</a></li>";
                        }
                    }
                    else
                    {
                        if ( ! is_null($query_string) && $query_string != "")
                        {
                            $html .= "<li><a href=\"".$url."?page=".$i."&amp;".$query_string."\" rel=\"nofollow\">".$i."</a></li>";
                        }
                        else
                        {
                            $html .= "<li><a href=\"".$url."?page=".$i."\" rel=\"nofollow\">".$i."</a></li>";
                        }
                    }
                }
            }
            if ($data['current_page'] < $data['total_page'])
            {
                $next = $data['current_page']+1;
                if (preg_match('/(.*)\?(.*)=(.*)/', $url))
                {
                    if ( ! is_null($query_string) && $query_string != "")
                    {
                        $html .= "<li><a href=\"".$url."&amp;page=".$next."&amp;".$query_string."\" rel=\"nofollow\">".lang('next')."</a></li>";
                    }
                    else
                    {
                        $html .= "<li><a href=\"".$url."&amp;page=".$next."\" rel=\"nofollow\">".lang('next')."</a></li>";
                    }
                }
                else
                {
                    if ( ! is_null($query_string) && $query_string != "")
                    {
                        $html .= "<li><a href=\"".$url."?page=".$next."&amp;".$query_string."\" rel=\"nofollow\">".lang('next')."</a></li>";
                    }
                    else
                    {
                        $html .= "<li><a href=\"".$url."?page=".$next."\" rel=\"nofollow\">".lang('next')."</a></li>";
                    }
                }
            }
            if ($data['end'] < $data['total_page'])
            {
                #$html.= "<span>&nbsp;...&nbsp;</span>";
            }
            $html.= '</ul>';
        }

        return $html;
    }
}

/**
* @todo Number record
*/
if ( ! function_exists('getNumIndex')) 
{
	function getNumIndex($curr = 1, $pSize = 20)
	{
		global $_index_;
		
		$curr = ($curr != '') ? $curr : 1;
		
		$t = (($curr-1) * $pSize ) + (++$_index_);
		print "<span>".$t.".</span>";
	}
}

if ( ! function_exists('sys_Substr')) 
{
	function sys_Substr($str, $end = 10, $start = 0, $len = FALSE, $charset = 'UTF-8')
	{
		$str = str_replace("&amp;nbsp;", "", $str);
		$ret = strip_tags($str);
		$myLen = mb_strlen($ret);
		if ($myLen > $end)
		{
			$ret = mb_substr($ret, $start, $end, $charset) . "...";
		}
		return $ret;
	}
}

if ( ! function_exists('get_aff_id'))
{
	function get_aff_id($refer = NULL)
	{
		if (is_null($refer) OR empty($refer))
		{
			$out['status'] = FALSE; 
			return $out; 
		}
		$CI =& get_instance(); 
		$decode = base64_decode($refer); 
		if (preg_match("/^affID=([0-9]*)$/", $decode))
		{
			list($var, $affID) = explode("=", $decode); 
			$CI->load->model('aff/aff_model', 'aff');
			$data['aff'] = $CI->aff->get_aff_detail($affID); 
			if (isset($data['aff']['affID']) && ! empty($data['aff']['affID']))
			{
				if ($data['aff']['open_status'] == 'Y')
				{
					$out['status'] = TRUE; 
					$out['affID'] = $data['aff']['affID']; 
				}
				else
				{
					$out['status'] = FALSE; 
				}
			}
			else
			{
				$out['status'] = FALSE; 
			}
		}
		else
		{
			$out['status'] = FALSE; 
		}
		return $out; 
	}
}

if ( ! function_exists('get_real_ip')) 
{
	function get_real_ip()
	{
		if ( ! empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
		  $ip = $_SERVER['HTTP_CLIENT_IP'];
		}
		else if ( ! empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
}

if ( ! function_exists('get_my_ip_address'))
{
	function get_my_ip_address()
	{
		if (getenv('HTTP_X_FORWARDED_FOR')) 
		{ 
			$ipaddress = getenv('HTTP_X_FORWARDED_FOR'); 
		}
		else 
		{ 
			$ipaddress = getenv('REMOTE_ADDR'); 
		}

		return $ipaddress; 
	}
}

if ( ! function_exists('dot_2_long_ip'))
{
	function dot_2_long_ip ($IPaddr)
	{
		if ($IPaddr == "") 
		{
			return 0;
		} 
		else 
		{
			#$ips = split ("\.", "$IPaddr");
			$ips = explode (".", "$IPaddr");
			return ($ips[3] + $ips[2] * 256 + $ips[1] * 65536 + $ips[0] * 16777216); 
		}
	}
}

if ( ! function_exists('get_domain')) 
{
	function get_domain($url = NULL)
	{
		if (is_null($url) OR empty($url)) 
		{
			return NULL; 
		}
		$pieces = parse_url($url);
		$domain = isset($pieces['host']) ? $pieces['host'] : '';
		if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) 
		{
			return $regs['domain'];
		}
		return FALSE;
	}
}

if ( ! function_exists('bootstrap_pagination'))
{
    function bootstrap_pagination($data = array(), $url = '', $query_string = NULL)
    {
        $html = '';
        if ($data['row_count'] > 0 && $data['total_page'] > 1)
        {
            $html .= '<div class="pagination pagination-centered">';
            $html .= '<ul>';
            if ($data['current_page'] > 1)
            {
                $back = $data['current_page'] - 1;

                if (preg_match('/(.*)\?(.*)=(.*)/', $url))
                {
                    if ( ! is_null($query_string) && $query_string != "")
                    {
                        $html .= "<li><a href=\"".$url."&amp;start=".$back."&amp;".$query_string."\" >".lang('prev')."</a></li>";
                    }
                    else
                    {
                        $html .= "<li><a href=\"".$url."&amp;start=".$back."\" >".lang('prev')."</a></li>";
                    }
                }
                else
                {
                    if ( ! is_null($query_string) && $query_string != "")
                    {
                        $html .= "<li><a href=\"".$url."?start=".$back."&amp;".$query_string."\" >".lang('prev')."</a></li>";
                    }
                    else
                    {
                        $html .= "<li><a href=\"".$url."?start=".$back."\" >".lang('prev')."</a></li>";
                    }
                }
            }

            if ($data['start'] > 1)
            {
                #$html.= "<span>&nbsp;...&nbsp;</span>";
            }
            for ($i = $data['start']; $i <= $data['end']; $i++)
            {
                if ($i == $data['current_page'])
                {
                    $html .= "<li class=\"active\"><span>".$i."</span></li>";
                }
                else
                {
                    if (preg_match('/(.*)\?(.*)=(.*)/', $url))
                    {
                        if ( ! is_null($query_string) && $query_string != "")
                        {
                            $html .= "<li><a href=\"".$url."&amp;start=".$i."&amp;".$query_string."\" >".$i."</a></li>";
                        }
                        else
                        {
                            $html .= "<li><a href=\"".$url."&amp;start=".$i."\" >".$i."</a></li>";
                        }
                    }
                    else
                    {
                        if ( ! is_null($query_string) && $query_string != "")
                        {
                            $html .= "<li><a href=\"".$url."?start=".$i."&amp;".$query_string."\" >".$i."</a></li>";
                        }
                        else
                        {
                            $html .= "<li><a href=\"".$url."?start=".$i."\" >".$i."</a></li>";
                        }
                    }
                }
            }
            if ($data['current_page'] < $data['total_page'])
            {
                $next = $data['current_page']+1;
                if (preg_match('/(.*)\?(.*)=(.*)/', $url))
                {
                    if ( ! is_null($query_string) && $query_string != "")
                    {
                        $html .= "<li><a href=\"".$url."&amp;start=".$next."&amp;".$query_string."\" >".lang('next')."</a></li>";
                    }
                    else
                    {
                        $html .= "<li><a href=\"".$url."&amp;start=".$next."\" >".lang('next')."</a></li>";
                    }
                }
                else
                {
                    if ( ! is_null($query_string) && $query_string != "")
                    {
                        $html .= "<li><a href=\"".$url."?start=".$next."&amp;".$query_string."\" >".lang('next')."</a></li>";
                    }
                    else
                    {
                        $html .= "<li><a href=\"".$url."?start=".$next."\" >".lang('next')."</a></li>";
                    }
                }
            }
            if ($data['end'] < $data['total_page'])
            {
                #$html.= "<span>&nbsp;...&nbsp;</span>";
            }
            $html.= '</ul>';
            $html .= '</div>';
        }

        return $html;
    }
}



if ( ! function_exists('is_ajax_request'))
{
	function is_ajax_request()
	{
		if ( ! empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) 
		{
 		   return true; 
		}
		return false; 
	}
}

if ( ! function_exists('numberToColumnName'))
{
	function numberToColumnName($number){
	    $abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	    $abc_len = strlen($abc);

	    $result_len = 1; // how much characters the column's name will have
	    $pow = 0;
	    while( ( $pow += pow($abc_len, $result_len) ) < $number ){
	        $result_len++;
	    }

	    $result = "";
	    $next = false;
	    // add each character to the result...
	    for($i = 1; $i<=$result_len; $i++){
	        $index = ($number % $abc_len) - 1; // calculate the module

	        // sometimes the index should be decreased by 1
	        if( $next || $next = false ){
	            $index--;
	        }

	        // this is the point that will be calculated in the next iteration
	        $number = floor($number / strlen($abc));

	        // if the index is negative, convert it to positive
	        if( $next = ($index < 0) ) {
	            $index = $abc_len + $index;
	        }

	        $result = $abc[$index].$result; // concatenate the letter
	    }
	    return $result;
	}
}
/*** Document for convert IP **/
/*
*

How do I convert a IP Address to a IP Number?

IP address (IPV4) is divided into 4 sub-blocks. Each sub-block has a different weight number each powered by 256. IP number is being used in the database because it is efficient to search between a range of number in database.

Start IP number and End IP Number are calculated based on following formula:

IP Number = 16777216*w + 65536*x + 256*y + z     (1)

where
IP Address = w.x.y.z

For example, if IP address is "169.6.7.20", then its IP Number "2835744532" is based on the formula (1).

IP Address = 169.6.7.20

So, w = 169, x = 6, y = 7 and z = 20

IP Number = 16777216*169 + 65536*6 + 256*7 + 20
          = 2835349504 + 393216 + 1792 + 20
          = 2835744532

PHP Function To Convert IP Address to IP Number
-----------------------------------------------
function Dot2LongIP ($IPaddr)
{
if ($IPaddr == "") {
return 0;
} else {
$ips = split ("\.", "$IPaddr");
return ($ips[3] + $ips[2] * 256 + $ips[1] * 65536 + $ips[0]
*16777216); }
}

**/