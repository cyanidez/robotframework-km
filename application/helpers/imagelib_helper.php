<?php
if ( ! function_exists('resize_image'))
{
	function resize_image($images, $new_images, $width, $height)
	{
		$img = getimagesize($images);
		
		$width = $img[0] * 0.3;
		$height = $img[1] * 0.3;

		if ($img['mime'] == 'image/png')
		{
			$images_orig = imagecreatefrompng($images);
		}
		else if ($img['mime'] == 'image/gif')
		{
			$images_orig = imagecreatefromgif($images);
		}
		else if ($img['mime'] == 'image/jpeg')
		{
			$images_orig = imagecreatefromjpeg($images);
		}
		else if ($img['mime']=='image/pjpeg')
		{
			$images_orig = imagecreatefromjpeg($images);
		}
		$photoX = imagesx($images_orig);
		$photoY = imagesy($images_orig);
		$images_fin = imagecreatetruecolor($width, $height);
		imagecopyresampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);
		if ($img['mime'] == 'image/png')
		{
			imagepng($images_fin, $new_images, 100);
		}
		else if ($img['mime'] == 'image/gif')
		{
			imagegif($images_fin, $new_images, 100);
		}
		else if ($img['mime'] == 'image/jpeg')
		{
			imagejpeg($images_fin, $new_images, 100);
		}
		else if ($img['mime'] == 'image/pjpeg')
		{
			imagejpeg($images_fin, $new_images, 100);
		}
		imagedestroy($images_orig);
		imagedestroy($images_fin);
		return $new_images;
	}
}

if ( ! function_exists('getBytesFromHexString'))
{
	function getBytesFromHexString($hexdata)
	{
	  for($count = 0; $count < strlen($hexdata); $count+=2)
		$bytes[] = chr(hexdec(substr($hexdata, $count, 2)));

	  return implode($bytes);
	}
}


if ( ! function_exists('getImageMimeType'))
{
	function getImageMimeType($imagedata)
	{
		$imagemimetypes = array( 
			"jpeg" => "FFD8", 
			"png" => "89504E470D0A1A0A", 
			"gif" => "474946",
			"bmp" => "424D", 
			"tiff" => "4949",
			"tiff" => "4D4D"
		);

		foreach ($imagemimetypes as $mime => $hexbytes)
		{
			$bytes = getBytesFromHexString($hexbytes);
			if (substr($imagedata, 0, strlen($bytes)) == $bytes)
			{
				return $mime;
			}
		}

		return NULL;
	}
}

if ( ! function_exists('save_image_from_base64'))
{
	function save_image_from_base64($string, $mobile = NULL, $output = NULL, $mime_type = 'jpeg') 
	{
		$img = imagecreatefromstring($string); 
		if ($img != FALSE) 		
		{ 
			
			echo '<p>output = '.$output.'</p>'; 
			switch ($mime_type)
			{
				case 'jpeg' : 
					$image_path = trim($output, '/') . '/' . $mobile . '_' . time() .'.jpg'; 
					imagejpeg($img, $image_path); 					
					break; 

				case 'jpg' : 
					$image_path = trim($output, '/') . '/' . $mobile . '_' . time(). '.jpg'; 			
					imagejpeg($img, $image_path); 
					break; 

				case 'gif' : 
					$image_path = trim($output, '/') . '/' . $mobile . '_' . time() . '.gif'; 
					imagegif($img, $image_path); 					
					break; 

				case 'png' : 					
					$image_path = trim($output, '/') . '/' . $mobile . '_' . time() . '.png'; 
					imagepng($img, $image_path); 
					break; 

				default : 
					break; 

			}
			
			return $image_path; 
			
		} 
		else
		{
			return FALSE; 
		}
	}
}