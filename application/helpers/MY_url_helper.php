<?php
if ( ! function_exists('site_admin_url')) 
{
	function site_admin_url($path = NULL) 
	{
		$CI =& get_instance(); 
		$base_url = $CI->config->item('base_url'); 
		if ( ! is_null($path) && $path != "") 
		{
			return $base_url . 'admin/' . $path; 		
		}
		else 
		{
			return $base_url . 'admin/'; 
		}
	}
}

/**
 * Site upload url
 *
 *
 * @access	 public
 * @param string sub-path
 * @param string true is absolute path
 * @return	string
 */

if ( ! function_exists('site_upload_url')) 
{
	function site_upload_url($uri = '', $server_path = FALSE)
	{
		$CI =& get_instance();
		if (is_array($uri))
		{
			$uri = implode('/', $uri);
		}
		//echo $uri;
		if ($server_path === TRUE)
		{
			$the_uri = ($uri == '') ? $CI->config->slash_item('upload_dir') : $CI->config->slash_item('upload_dir') . trim($uri, '/');
			#echo '<p>the_uri = '.$the_uri.'</p>';
			$directory = dirname($the_uri);
			#echo '<p>directory = '.$directory.'</p>';
			if ( ! file_exists($directory) && ! is_dir($directory))
			{
				mkdir($directory, 0777, TRUE);
			}
			return $the_uri;
		}
		else
		{
			return ($uri == '') ? $CI->config->slash_item('upload_url') : $CI->config->slash_item('upload_url') . trim($uri, '/');
		}
	}
}

if ( ! function_exists('site_image_url')) 
{
	function site_image_url($file = NULL) 
	{
		if (is_null($file) OR $file == "") 
		{
			return NULL; 
		}
		$image_path = config_item('image_relative_path'); 
		return site_url($image_path . $file); 	
	}
}

if ( ! function_exists('site_url')) 
{
	function site_url($uri = '', $server_path = FALSE) 
	{
		$CI =& get_instance();
		if ($server_path === TRUE) 
		{
			return $CI->input->server('DOCUMENT_ROOT') . '/' . $uri;  
		}
		else
		{
			return $CI->config->site_url($uri);
		}

	}
}

if ( ! function_exists('site_temp_url'))
{
	function site_temp_url($file = NULL, $server_path = FALSE) 
	{
		$CI =& get_instance(); 
		$CI->load->config('path_config'); 
		$temp_path = $CI->config->item('temp_path');

		$upload_dir = config_item('upload_dir') . '/' .$temp_path;		

		if ( ! is_dir($upload_dir))
		{
			mkdir($upload_dir, 0777, TRUE);
		}

		if ($server_path === TRUE) 
		{

			if (is_array($file))
			{

				$uri = implode("/", $file);

				if ( ! is_dir($upload_dir . '/' . $uri))
				{
					mkdir($upload_dir . '/' . $uri);
				}
				return trim($upload_dir, '/') . '/' . $uri;
			}
			else
			{	
				$path = explode("/", $file);
				$last_path = $path[count($path) - 1];
				$ext = explode(".", $last_path);
				$ext_img = array('jpg', 'gif', 'png', 'jpeg');

				if ( ! @in_array($ext[1], $ext_img))
				{
					if ( ! is_dir($upload_dir . '/' . $file))
					{
						mkdir($upload_dir . '/' . $file, 0777, TRUE);
					}
				}
				return $upload_dir . '/' . $file; 
			}			
		}
		else
		{
			if (is_array($file))
			{
				$uri = implode("/", $file);
				if ( ! is_dir($upload_dir . '/' . $uri))
				{
					mkdir($upload_dir . '/' . $uri);
				}
				return site_url(trim($upload_dir, '/') . '/' . $uri); 
			}
			else
			{
				return site_url($upload_dir . '/' . $file);
			}
		}
	}
}

if ( ! function_exists('site_photo_url'))
{
	function site_photo_url($file = NULL, $server_path = FALSE) 
	{
		$CI =& get_instance(); 
		$CI->load->config('path_config'); 
		$photo_path = $CI->config->item('photo_path');

		$upload_dir = config_item('upload_dir') . '/' .$photo_path;		

		if ( ! is_dir($upload_dir))
		{
			mkdir($upload_dir, 0777, TRUE);
		}

		if ($server_path === TRUE) 
		{

			if (is_array($file))
			{

				$uri = implode("/", $file);

				if ( ! is_dir($upload_dir . '/' . $uri))
				{
					mkdir($upload_dir . '/' . $uri);
				}
				return trim($upload_dir, '/') . '/' . $uri;
			}
			else
			{	
				$path = explode("/", $file);
				$last_path = $path[count($path) - 1];
				$ext = explode(".", $last_path);
				$ext_img = array('jpg', 'gif', 'png', 'jpeg');

				if ( ! @in_array($ext[1], $ext_img))
				{
					if ( ! is_dir($upload_dir . '/' . $file))
					{
						mkdir($upload_dir . '/' . $file, 0777, TRUE);
					}
				}
				return $upload_dir . '/' . $file; 
			}			
		}
		else
		{
			if (is_array($file))
			{
				$uri = implode("/", $file);
				if ( ! is_dir($upload_dir . '/' . $uri))
				{
					mkdir($upload_dir . '/' . $uri);
				}
				return site_url(trim($upload_dir, '/') . '/' . $uri); 
			}
			else
			{
				return site_url($upload_dir . '/' . $file);
			}
		}
	}
}

/*
if ( ! function_exists('album_cover_url'))
{
	function album_cover_url($file = NULL, $server_path = FALSE) 
	{
		$CI =& get_instance(); 
		$CI->load->config('path_config'); 

		$config_url = $CI->config->item('album.cover.path');
		#$cdn_path = $CI->config->item('');
				
		if ($server_path == TRUE) 
		{
			return $CI->input->server('DOCUMENT_ROOT') . '/' . trim($config_url) . $file; 
		}
		else
		{
			return site_url($config_url . $file);
		}
	}
}
*/





if ( ! function_exists('rsegment_to_string'))
{
	function rsegment_to_string() 
	{
		$CI =& get_instance(); 
		$uri = $CI->uri->segment_array(); 

		foreach ($uri as $key => $value) : 
			$out[] = urldecode($value); 
		endforeach; 

		return implode("/", $out); 		
	}
}

if ( ! function_exists('url_title'))
{
	function url_title($str, $separator = 'dash')
	{
		if ($separator == 'dash')
		{
			$search		= '_';
			$replace	= '-';
		}
		else
		{
			$search		= '-';
			$replace	= '_';
		}

		$str = strtolower($str);
		
		$trans = array(
						$search								=> $replace,
						"\""							=> '',
						" "								=> $replace,
						"\/"								=> "",
						"\n"								=> "",
						$replace."+"						=> $replace,
						$replace."$"						=> '',
						"^".$replace						=> ''
					   );

		foreach ($trans as $key => $val)
		{
			$str = preg_replace("/".$key."/", $val, $str);
		}
	
		return trim(stripslashes($str));
	}
}

if ( ! function_exists('url_title_thai'))
{
	function url_title_thai($str, $separator = 'dash')
	{
		if ($separator == 'dash')
		{
			$search		= '_';
			$replace	= '-';
		}
		else
		{
			$search		= '-';
			$replace	= '_';
		}

		$str = strtolower($str);
		
		$trans = array(
						$search								=> $replace,
						"\""							=> '',
						" "								=> $replace,
						"\/"								=> ""
					   );

		foreach ($trans as $key => $val)
		{
			$str = preg_replace("/".$key."/", $val, $str);
		}
	
		return trim(stripslashes($str));
	}
}

/**  
*	Site CDN Url 
*
*	@author:  Preme W.
*	@since     08, Mar, 2013
*	@access    public 
*	@params   string     sub-path
*	@params    string    true is absolute path  
*	@reeturn    string 
*
*/
if ( ! function_exists('site_cdn_path'))
{
	function site_cdn_path($uri = '', $server_path = FALSE) 
	{
		$upload_dir = config_item('upload_dir');

		#echo "<p>upload_dir = ".$upload_dir.'</p>'; 

		$return_path = (empty($uri)) ? $upload_dir : $upload_dir . '/' . $uri; 
		return $return_path;
	}
}

if ( ! function_exists('site_cdn_url'))
{
	function site_cdn_url($url = NULL) 
	{
		$CI =& get_instance(); 
		$static_url = config_item('static_url');

		return (empty($url)) ? rtrim($static_url, '/') : rtrim($static_url, '/') . '/' . $url; 


		
	}
}

if ( ! function_exists('upload_url'))
{
    function upload_url($uri = null, $server_path = false)
    {
        $CI =& get_instance();

        if (is_array($uri))
        {
            $uri = implode('/', $uri);
        }

        $cdn_dir = config_item('cdn_dir') . '/'.$uri;
        if ($server_path == true)
        {

            if ( ! file_exists($cdn_dir) && ! is_dir($cdn_dir))
            {
                mkdir($cdn_dir, 0777, true);
                chmod($cdn_dir, 0777);
            }

            return $cdn_dir;
        }

        else
        {
            return site_url($cdn_dir);
        }
    }
}

if ( ! function_exists('site_room_type_url'))
{
    function site_room_type_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $room_type_config = $CI->config->item('room_type');


        if ( ! empty($uri))
        {
            $room_type_path = upload_url($room_type_config['dir'] . '/' . $uri, $server_path);
        }
        else
        {
            $room_type_path = upload_url($room_type_config['dir'], $server_path);
        }

        return $room_type_path;


    }
}

if ( ! function_exists('site_node_url'))
{
    function site_node_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $node_config = $CI->config->item('node');


        #$style_option_path = upload_url($style_option_config['dir'], $server_path);

        if ( ! empty($uri))
        {
            $node_path = upload_url($node_config['dir'] . '/' . $uri, $server_path);
        }
        else 
        {
            $node_path = upload_url($node_config['dir'], $server_path);
        }

        return $node_path;
    }
}

if ( ! function_exists('site_node_paragraph_url'))
{
    function site_node_paragraph_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $node_paragraph_config = $CI->config->item('node_paragraph');

        if ( ! empty($uri))
        {
            $node_path = upload_url($node_paragraph_config['dir'] . '/' . $uri, $server_path);
        }
        else 
        {
            $node_path = upload_url($node_paragraph_config['dir'], $server_path);
        }

        return $node_path;
    }
}

if ( ! function_exists('site_news_url'))
{
    function site_news_url($uri = null, $server_path = false)
    {
        $CI =& get_instance();
        $CI->load->config('site_config');
        $news_config = $CI->config->item('news');
	    if ( ! empty($uri))
        {
            $news_path = upload_url($news_config['dir'] . '/' . $uri, $server_path);
        }
        else 
        {
            $news_path = upload_url($news_config['dir'], $server_path);
        }

        return $news_path;
    }
}

if ( ! function_exists('site_news_paragraph_url'))
{
    function site_news_paragraph_url($uri = null, $server_path = false)
    {
        $CI =& get_instance();
        $CI->load->config('site_config');
        $news_paragraph_config = $CI->config->item('news_paragraph');
        if ( ! empty($uri))
        {
            $news_path = upload_url($news_paragraph_config['dir'] . '/' . $uri, $server_path);
        }
        else 
        {
            $news_path = upload_url($news_paragraph_config['dir'], $server_path);
        }
        return $news_path;
    }
}

if ( ! function_exists('site_node_file_url'))
{
    function site_node_file_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $node_config = $CI->config->item('node_file');
        if ( ! empty($uri))
        {
            $node_path = upload_url($node_config['dir'] . '/' . $uri, $server_path);
        }
        else 
        {
            $node_path = upload_url($node_config['dir'], $server_path);
        }
        return $node_path;
    }
}



if ( ! function_exists('site_banner_url'))
{
    function site_banner_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $banner_config = $CI->config->item('banner');
        if ( ! empty($uri))
        {
            $banner_path = upload_url($banner_config['dir'] . '/' . $uri, $server_path);
        }
        else
        {
            $banner_path = upload_url($banner_config['dir'], $server_path);
        }

        return $banner_path;


    }
}

if ( ! function_exists('site_aira_group_url'))
{
    function site_aira_group_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $aira_group_config = $CI->config->item('aira_group');
        if ( ! empty($uri))
        {
            $aira_group_path = upload_url($aira_group_config['dir'] . '/' . $uri, $server_path);
        }
        else
        {
            $aira_group_path = upload_url($aira_group_config['dir'], $server_path);
        }

        return $aira_group_path;


    }
}

if ( ! function_exists('site_partner_url'))
{
    function site_partner_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $partner_config = $CI->config->item('partner');
        if ( ! empty($uri))
        {
            $partner_path = upload_url($partner_config['dir'] . '/' . $uri, $server_path);
        }
        else
        {
            $partner_path = upload_url($partner_config['dir'], $server_path);
        }

        return $partner_path;


    }
}

if ( ! function_exists('site_web_template_url'))
{
    function site_web_template_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $web_template_config = $CI->config->item('web_template');
        if ( ! empty($uri))
        {
            $web_template_path = upload_url($web_template_config['dir'] . '/' . $uri, $server_path);
        }
        else
        {
            $web_template_path = upload_url($web_template_config['dir'], $server_path);
        }

        return $web_template_path;


    }
}

if ( ! function_exists('site_download_url'))
{
    function site_download_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $download_config = $CI->config->item('download');


        if ( ! empty($uri))
        {
            $download_path = upload_url($download_config['dir'] . '/' . $uri, $server_path);
        }
        else
        {
            $download_path = upload_url($download_config['dir'], $server_path);
        }

        return $download_path;


    }
}



if ( ! function_exists('site_contactus_url'))
{
    function site_contactus_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $contactus_config = $CI->config->item('contactus');


        if ( ! empty($uri))
        {
            $contactus_path = upload_url($contactus_config['dir'] . '/' . $uri, $server_path);
        }
        else
        {
            $contactus_path = upload_url($contactus_config['dir'], $server_path);
        }

        return $contactus_path;


    }
}

if ( ! function_exists('site_career_profile_url'))
{
    function site_career_profile_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $career_config = $CI->config->item('career_profile');


        if ( ! empty($uri))
        {
            $contactus_path = upload_url($career_config['dir'] . '/' . $uri, $server_path);
        }
        else
        {
            $contactus_path = upload_url($career_config['dir'], $server_path);
        }

        return $contactus_path;


    }
}

if ( ! function_exists('site_career_resume_url'))
{
    function site_career_resume_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $career_config = $CI->config->item('career_resume');


        if ( ! empty($uri))
        {
            $contactus_path = upload_url($career_config['dir'] . '/' . $uri, $server_path);
        }
        else
        {
            $contactus_path = upload_url($career_config['dir'], $server_path);
        }

        return $contactus_path;


    }
}

if ( ! function_exists('site_splash_page_url'))
{
    function site_splash_page_url($uri = null, $server_path = false)
    {

        $CI =& get_instance();
        $CI->load->config('site_config');
        $splash_page_config = $CI->config->item('splash_page');


        if ( ! empty($uri))
        {
            $splash_page_path = upload_url($splash_page_config['dir'] . '/' . $uri, $server_path);
        }
        else
        {
            $splash_page_path = upload_url($splash_page_config['dir'], $server_path);
        }

        return $splash_page_path;


    }
}

