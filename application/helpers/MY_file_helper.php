<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * @description  Convert dd/mm/YYYY to UNIX_TIMESTAMP before insert to database 
 * @access	public
 * @param	NULL
 * @return  Date <String>
 */	
if ( ! function_exists('get_file_ext'))
{
	function get_file_ext($file = NULL)
	{
		if (is_null($file) OR $file  == "") 
		{
			return NULL; 
		}

		 return substr($file, strrpos($file, '.'));
	}
}

if ( ! function_exists("rename_file"))
{
    function rename_file($source = null, $target = null)
    {
        if (empty($target))
        {
            return null;
        }

        if (file_exists($source))
        {
            return rename($source, $target);
        }
        return null;
    }
}

if ( ! function_exists('read_file'))
{
    function read_file($file_path)
    {
        $handle = @fopen($file_path, "r");
        $returns = array();
        if ($handle) 
        {
            while (($buffer = fgets($handle, 4096)) !== false) 
            {
                #echo '<p>'.$buffer.'</p>';
                $returns[] = $buffer; 

            }
            if ( ! feof($handle)) 
            {
                echo "Error: unexpected fgets() fail\n";
            }
            fclose($handle);

        }
        return $returns; 
    }
}

/* End of file my_date_helper.php */
/* Location: ./system/application/helpers/my_date_helper.php */
