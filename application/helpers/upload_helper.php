<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('do_upload')) 
{
	function do_upload($field = NULL, $config = NULL) 
	{
		$CI =& get_instance(); 
		$CI->load->library('upload');

		$CI->upload->initialize($config);

		$resp = $CI->upload->do_upload($field); 
		$out = array();
		if ( ! $resp)
		{
			$out['error'] = $CI->upload->display_errors();
		}	
		else
		{
			$out['response'] = $CI->upload->data();			
		}
		return $out; 
	}
}


/* End of file upload_helper.php */
/* Location: ./application/helpers/upload_helper.php */