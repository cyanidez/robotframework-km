<?php
if ( ! function_exists('field_error')) 
{
	function field_error($message = NULL) 
	{
		return '<em class="error"><span>'.strip_tags($message).'</span></em>'; 
	}
}

if ( ! function_exists('tipsy_tooltip'))
{
	function tipsy_tooltip($message = '', $label = '[?]') 
	{
		return '<a href="#" class="tipsy" title="'.$message.'">'.$label.'</a>'; 
	}
}

if ( ! function_exists('html'))
{
	function html($str)
	{
		$find = array(
		  "\r",
		  "\n",	  
		  "&"
		);

		$replace = array(
		  '<br />',
		  '<br />',
		  '&amp;'
		);

		return str_replace($find, $replace, $str);
	}
}

if ( ! function_exists('show_price'))
{
	function show_price($price = null)
	{
		return number_format($price, 2) . ' ' . lang('baht');
	}
}