<?php 

if ( ! function_exists('get_facebook_cookie'))
{
	function get_facebook_cookie($app_id = NULL, $app_secret = NULL) 
	{
		if (is_null($app_id) OR empty($app_id)) 
		{
			return NULL; 
		}
		if (is_null($app_secret) OR empty($app_secret)) 
		{
			return NULL; 
		}
		$args = array();
		if (isset($_COOKIE['fbs_'.$app_id])) 
		{
			parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
			#alert($args, 'red'); 
			ksort($args);
			$payload = '';
			foreach ($args as $key => $value) 
			{
				if ($key != 'sig') 
				{
					$payload .= $key . '=' . $value;
				}
			}
			if (md5($payload . $app_secret) != $args['sig'])
			{
				return NULL;
			}
			#alert($args, 'red');
			return $args;
		}
		else
		{
			return FALSE; 
		}
	}
}


if ( ! function_exists('save_avatar'))
{
	function save_avatar($fb_id = NULL) 
	{
		$CI =& get_instance(); 
		$CI->load->library('facebook'); 

		$content = file_get_contents('https://graph.facebook.com/'.$fb_id.'/picture?type=large');

		$img = imagecreatefromstring($content);
		
		$avatar_url = file_put_contents(avatar_url($img, TRUE)); 

	}
}

if ( ! function_exists('fb_password')) 
{
	function fb_password($length = 8) 
	{
		if (is_null($length) OR empty($length)) 
		{
			return NULL; 
		}

		$str = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'; 

		do {			
			$password = substr(str_shuffle($str), 0, $length); 
		} while ( ! preg_match('/^(?=.*\p{Lu})(?=.*\d.*\d).{8}$/xu', $password));  		

		return $password; 
	}
}



#$cookie = get_facebook_cookie(FACEBOOK_APP_ID, FACEBOOK_SECRET);
