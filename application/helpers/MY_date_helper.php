<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('date_to_timestamp')) 
{
	function date_to_timestamp($date = NULL, $time = NULL) 
	{
		if (is_null($date) OR $date == "")
		{
			return NULL; 
		}
		list($day, $month, $year) = explode("/", $date); 

		if ( ! is_null($time) && ! empty($time))
		{
			list($hour, $minute, $second) = explode(":", $time); 
			$timestamp = mktime((int)$hour, (int)$minute, (int)$second, (int)$month, (int)$day, (int)$year); 
		}
		else
		{
			$timestamp = mktime(0, 0, 0, (int)$month, (int)$day, (int)$year); 
		}

		return $timestamp; 
	}
}

if ( ! function_exists('date_now'))
{
    function date_now()
    {
        return date("Y-m-d H:i:s");
    }
}

if ( ! function_exists("current_time"))
{
    function current_time()
    {
        return time();
    }
}


if ( ! function_exists('get_today'))
{
	function get_today($format = "Ymd")
	{
		return date($format); 
	}
}

if ( ! function_exists('get_last_day'))
{
	function get_last_day($day = 1)
	{
		$out = array(); 
		
		for ($i = 0; $i < $day; $i++)
		{
			$current = mktime(0, 0, 0, date("m"), date("d") - $i, date("Y")); 
			#$out[] = date("D d, M", $current); 
			$out[$i]['date_show'] = date("D d", $current); 
			$out[$i]['date_db'] = date("Ymd", $current); 
			$out[$i]['timestamp'] = $current; 
		}
		return $out; 
	}
}

if ( ! function_exists('get_last_year'))
{
	function get_last_year($year = 1)
	{
		$out = array(); 
		
		for ($i = 0; $i < $year; $i++)
		{
			$current = mktime(0, 0, 0, date("m"), date("d"), date("Y") - $i); 
			#$out[] = date("D d, M", $current); 
			$out[$i]['date_show'] = date("Y", $current); 
			$out[$i]['date_db'] = date("Y", $current); 
			$out[$i]['timestamp'] = $current; 
		}
		return $out; 
	}
}

if ( ! function_exists('time_to_date_to_db'))
{
	function time_to_date_to_db($timestamp = NULL, $format = "Y-m-d H:i:s")
	{
		if (is_null($timestamp) OR empty($timestamp))
		{
			$timestamp = time(); 
		}
		return date($format, $timestamp);
	}
}

if ( ! function_exists('web_to_db'))
{
	function web_to_db($date = NULL)
	{
		if (is_null($date) OR empty($date))
		{
			return  NULL; 
		}
		list($day, $month, $year) = explode("/", $date);

		return $year . '-' . $month . '-' .$day; 
	}
}

if ( ! function_exists('db_to_web'))
{
	function db_to_web($date = NULL)
	{
		if (is_null($date) OR empty($date))
		{
			return  NULL; 
		}
		list($year, $month, $day) = explode("-", $date);

		return $day . '/' . $month . '/' .$year; 
	}
}

if ( ! function_exists('facebook_to_db'))
{
	function facebook_to_db($date = NULL) 
	{
		if (is_null($date) OR empty($date))
		{
			return NULL; 
		}

		list($month, $day, $year) = explode("/", $date);

		return $year . '-' . $month . '-' .$day; 
	}
}

if ( ! function_exists('yyyymmdd_to_time'))
{
	function yyyymmdd_to_time($datetime =  NULL)
	{
		if (is_null($datetime) OR empty($datetime))
		{
			returN NULL; 
		}
		
		list($date, $time) = explode(" ", $datetime);
		list($year, $month, $day) = explode("-", $date);
		list($hour, $min, $sec) = explode(":", $time);

		$last_logged_in_timestamp = mktime($hour, $min, $sec, $month, $day, $year);
		$now_timestamp = mktime(date("H"), date("i"), date("s"), date("m"), date("j"), date("Y"));
		
		$diff_timestamp = ($now_timestamp - $last_logged_in_timestamp) / 60; 
		
		return $diff_timestamp; 


	}
}

if ( ! function_exists('sp_style_datetime'))
{
	function sp_style_datetime($datetime = NULL)
	{
		if (is_null($datetime) OR empty($datetime))
		{
			return NULL; 
		}

		$CI =& get_instance();
		$CI->load->config('site_config');
		list($date, $time) = explode(" ", $datetime); 
		list($year, $month, $day) = explode("-", $date); 
		list($hour, $min, $sec) = explode(":", $time); 
		
		return lang('sp_style_datetime', array((int) $day . " " . show_thai_month($month) . " " . thai_year($year),  $hour.":".$min));
	}
}

if ( ! function_exists('show_style_date'))
{
	function show_style_date($datetime = NULL)
	{
		if (empty($datetime))
		{
			return NULL; 
		}
		$CI =& get_instance();
		$CI->load->config('site_config');
		list($date, $time) = explode(" ", $datetime); 
		list($year, $month, $day) = explode("-", $date); 
		list($hour, $min, $sec) = explode(":", $time); 
		
		return show_text_month($month) . " " .$day. " ".$year;
	}
}



if ( ! function_exists('thai_year'))
{
	function thai_year($year = NULL) 
	{
		return $year + 543; 
	}
}

if ( ! function_exists('show_thai_month'))
{
	function show_thai_month($month = NULL, $mode = 'short') 
	{
		$CI =& get_instance();
		$CI->load->config('site_config');

		if ($mode == 'short') 
		{
			$thai_month = $CI->config->item('thai_month_short');
		}
		else
		{
			$thai_month = $CI->config->item('thai_month');
		}

		return $thai_month[(int)$month - 1]; 
	}	
}

if ( ! function_exists('show_text_month'))
{
	function show_text_month($month = NULL, $mode = 'short') 
	{
		$CI =& get_instance();
		$CI->load->config('site_config');

		if ($mode == 'short') 
		{
			$text_month = $CI->config->item(LANGCODE.'_month_short');
		}
		else
		{
			$text_month = $CI->config->item(LANGCODE.'_month');
		}

		return $text_month[ (int) $month - 1]; 
	}	
}

if ( ! function_exists('show_thai_date'))
{
	function show_thai_date($date = NULL, $mode = 'short') 
	{
		$CI =& get_instance();
		$CI->load->config('site_config');

		if ($mode == 'short') 
		{
			$thai_date = $CI->config->item('thai_date_short');
		}
		else
		{
			$thai_date = $CI->config->item('thai_date');
		}

		return $thai_date[(int)$date - 1]; 
	}	
}

if ( ! function_exists('time_ago'))
{
	function time_ago($datetime_ago = NULL) 
	{
		if (is_null($datetime_ago) OR empty($datetime_ago))
		{
			return NULL; 
		}
		#echo $datetime_ago; 


		list($date, $time) = explode(" ", $datetime_ago); 
		list($year, $month, $day) = explode("-", $date); 
		list($hour, $min, $sec) = explode(":", $time); 

		$timestamp_ago = mktime((int) $hour, (int) $min, (int) $sec, (int) $month, (int) $day, (int) $year); 
		$current_timestamp = mktime(date("H"), date("i"), date("s"), date("n"), date("j"), date("Y")); 

		$diff_timestamp = $current_timestamp - $timestamp_ago; 

		//-- More than 1 second and less than 1 minute ---// 
		if ($diff_timestamp > 0 && $diff_timestamp < 60)
		{
			return lang('sec_time_ago', $diff_timestamp); 
		}
		//-- More than 1 minutes and less than 1 hours ---//
		else if ($diff_timestamp >= 60 && $diff_timestamp < 3600)
		{
			return lang('min_time_ago', round($diff_timestamp / 60, 2)); 
		}
		//--- More than 1 hour and less than 24 hour ---//
		else if ($diff_timestamp >= 3600 && $diff_timestamp < 86400)
		{
			return lang('hour_time_ago', round($diff_timestamp / (60*60), 2));
		}
		//--- More than 24 hours ---// 
		else if ($diff_timestamp >= 86400)
		{
			return lang('day_time_ago', ($diff_timestamp / (60*60*24))); 
		}
 	}
}

if ( ! function_exists('is_greater_than'))
{
	function is_greater_than($transaction_datetime = null, $datetime = 60, $unit = 'min')
	{
		if ($unit == 'min')
		{
			list($date, $time) = explode(" ", $transaction_datetime);
			list($year, $month, $day) = explode("-", $date);
			list($hour, $min, $sec) = explode(":", $time);

			// echo "year = ".$year."<br>";
			// echo "month = ".$month."<br>";
			// echo "day = ".$day."<br>";
			// echo "hour = ".$hour."<br>";
			// echo "min = ".$min."<br>";
			// echo "sec = ".$sec."<br>";


			$transaction_timestamp = mktime((int) $hour, (int) $min, (int) $sec, (int) $month, (int) $day, (int) $year);			
			#$greater_timestamp = mktime((int) $hour, (int) ($min + $datetime), (int) $sec, (int) $month, (int) $day, (int) $year);			
			$past_timestamp = mktime((int) date("H"), (int) (date("i") - $datetime), (int) date("s"), (int) date("m"), (int) date("d"), (int) date("Y"));
			if ($past_timestamp > $transaction_timestamp)
			{
				return true;
			}
			return false;
		}
		elseif ($unit == 'hour')
		{

		}
		elseif ($unit == 'day')
		{

		}
	}
}

if ( ! function_exists('get_condition_datetime'))
{
	function get_condition_datetime($datetime = null, $condition_type = 'next', $condition = 60, $unit = 'min')
	{	
		if (empty($datetime))
		{
			$datetime = date_now();	
		}

		$CI =& get_instance();
		if ( ! function_exists('valid_datetime'))
		{
			$CI->load->helper('string');
		}
		if ( ! valid_datetime($datetime))
		{
			return false;
		}

		list($date, $time) = explode(" ", $datetime);
		list($year, $month, $day) = explode("-", $date);
		list($hour, $min, $sec) = explode(":", $time);

		if ($unit == 'min')
		{
			
			if ($condition_type == "next")
			{
				$condition_timestamp = mktime((int) $hour, (int) ($min + $condition), (int) $sec, (int) $month, (int) $day, (int) $year);
			}
			elseif ($condition_type == "prev")
			{
				$condition_timestamp = mktime((int) $hour, (int) ($min - $condition), (int) $sec, (int) $month, (int) $day, (int) $year);	
			}
			
		}
		elseif ($unit == 'hour')
		{
			if ($condition_type == "next")
			{
				$condition_timestamp = mktime((int) ($hour + $condition), (int) $min, (int) $sec, (int) $month, (int) $day, (int) $year);
			}
			elseif ($condition_type == "prev")
			{
				$condition_timestamp = mktime((int) ($hour - $condition), (int) $min, (int) $sec, (int) $month, (int) $day, (int) $year);	
			}
		}
		elseif ($unit == 'day')
		{
			if ($condition_type == "next")
			{
				$condition_timestamp = mktime((int) $hour, (int) $min, (int) $sec, (int) $month, (int) ($day + $condition), (int) $year);
			}
			elseif ($condition_type == "prev")
			{
				$condition_timestamp = mktime((int) $hour, (int) $min, (int) $sec, (int) $month, (int) ($day - $condition), (int) $year);	
			}	
		}
		$condition_datetime = date("Y-m-d H:i:s", $condition_timestamp);
		return $condition_datetime;


		
	}
}

if ( ! function_exists('thai_datetime'))
{
	function thai_datetime($datetime = null)
	{
		if (empty($datetime))
		{
			return null;
		}
		if ( ! valid_datetime($datetime))
		{
			return null;
		}
		#alert($datetime, 'blue');
		#exit;
		list($date, $time) = explode(" ", $datetime);
		list($year, $month, $day) = explode("-", $date);
		list($hour, $min, $sec) = explode(":", $time);
		#alert($time, 'red');
		#exit;

		$CI =& get_instance();

		$thai_month = $CI->config->item('thai_month_short');

		$thai_datetime = $day." ".$thai_month[(int) ($month  - 1)]." ".thai_year($year)." เวลา ".$hour.":".$min.":".$sec." น.";
		return $thai_datetime;

		#$timestamp = mktime((int) $hour, (int) $min, (int) $sec, (int) $month, (int) $day, (int) $year);


	}
}

/* End of file MY_date_helper.php */
/* Location: ./application/helpers/MY_date_helper.php */