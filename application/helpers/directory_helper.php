<?php 

if ( ! function_exists('make_dir'))
{
	function make_dir($dir = NULL)
	{
		if (is_null($dir) OR empty($dir))
		{
			return FALSE; 
		}
		if ( ! is_dir($dir))
		{
			if (mkdir($dir, 0777, TRUE))
			{
				chmod($dir, 0777);
				return $dir; 
			}
			else
			{
				return FALSE; 
			}
		}

		return $dir; 
	}
}

define("DS", DIRECTORY_SEPARATOR);

if ( ! function_exists('destroy_dir'))
{
	function destroy_dir($dir) 
	{ 
		if ( ! is_dir($dir) OR is_link($dir)) 
		{
			return unlink($dir); 
		}
	
		foreach (scandir($dir) as $file) 
		{ 
			if ($file == '.' || $file == '..') 
			{
				continue; 
			}
			if ( ! destroy_dir($dir.DS.$file)) 
			{ 
				chmod($dir.DS.$file, 0777); 
				if ( ! destroy_dir($dir.DS.$file)) 
				{
					return FALSE; 
				}
			}; 
        } 
        return rmdir($dir); 
    } 
}


if ( ! function_exists('recursive_remove_directory'))
{
	// ------------ lixlpixel recursive PHP functions -------------
	// recursive_remove_directory( directory to delete, empty )
	// expects path to directory and optional TRUE / FALSE to empty
	// of course PHP has to have the rights to delete the directory
	// you specify and all files and folders inside the directory
	// ------------------------------------------------------------

	// to use this function to totally remove a directory, write:
	// recursive_remove_directory('path/to/directory/to/delete');
 
	// to use this function to empty a directory, write:
	// recursive_remove_directory('path/to/full_directory',TRUE);
 
	function recursive_remove_directory($directory, $empty=FALSE)
	{
		// if the path has a slash at the end we remove it here
		if (substr($directory, -1) == '/')
		{
			 $directory = substr($directory, 0, -1);
		}
  
		// if the path is not valid or is not a directory ...
		if ( ! file_exists($directory) OR ! is_dir($directory))
		{
			// ... we return false and exit the function
			return FALSE;
			// ... if the path is not readable
		}
		else if ( ! is_readable($directory))
		{
			// ... we return false and exit the function
			return FALSE;
			 // ... else if the path is readable
		}
		else
		{
			// we open the directory
			$handle = opendir($directory);
	        // and scan through the items inside
			while (FALSE !== ($item = readdir($handle)))
			{
				// if the filepointer is not the current directory
				// or the parent directory
				if ($item != '.' && $item != '..')
				{
					// we build the new path to delete
					$path = $directory . '/' . $item;
  
					// if the new path is a directory
					if (is_dir($path)) 
					{
						// we call this function with the new path
						recursive_remove_directory($path);
						// if the new path is a file
					}
					else
					{
						// we remove the file
						unlink($path);
					}
				}
			}
			// close the directory
			closedir($handle);
  
			// if the option to empty is not set to true
			if ($empty == FALSE)
			{
				// try to delete the now empty directory
				if ( ! rmdir($directory))
				{
					// return false if not possible
					return FALSE;
				}
			}
			// return success
			return TRUE;
		}
	}
}


if ( ! function_exists('get_directory_contents'))
{
	function get_directory_contents($dir, &$results = array())
	{
	    $files = scandir($dir);

	    foreach($files as $key => $value)
	    {
	        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
	        if ( ! is_dir($path)) 
	        {
	            $results[] = $path;
	        }
	        else if($value != "." && $value != "..") 
	        {
	            get_directory_contents($path, $results);
	            #if ( ! is_dir($path))
	            #{
	            	$results[] = $path;	
	           	#}
	        }
	    }

	    return $results;
	}
}

if ( ! function_exists('directory_tree'))
{
	function directory_tree($path = array())
	{
		foreach ($path as $key => $value)
		{
			echo 'value = '.$value.'<br>';
		}
	}
}