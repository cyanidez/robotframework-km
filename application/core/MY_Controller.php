<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller { 

	protected $modules = 'admin'; 
	protected $my_data; 
	protected $_controller = "";
	protected $_method = "";
	public static $app_acl = NULL;
	public static $app_acl_cache = NULL;

	function __construct() 
	{
		parent::__construct(); 
		
		$this->load->library('zend'); 
		$this->zend->load('Zend/Locale');
		$this->zend->load('Zend/Registry');
        $this->load->library('breadcrumb');
		$this->load->helper('admin_profile');
		$this->load->helper('url');
        $this->load->helper('date');
        $this->load->model('adm_user/user_log_model', 'user_log');
        


		if ( ! defined('LANGCODE'))
		{
			define("LANGCODE", 'th');
		}
		if ( ! defined('LANGID'))
		{
			define("LANGID", 1);
		}
		set_cookie('LANGCODE', 'th');
		set_cookie('LANGID', 1);

		$locale = new Zend_Locale(LANGCODE);

		#echo "locale = ".$locale; 

		Zend_Registry::set('Zend_Locale', $locale);

		#$this->lang->load('default', LANGCODE); 
		#$this->lang->load('template', LANGCODE);
		
		$this->lang->load('validation', LANGCODE); 
		$this->load->helper('html'); 
		$this->load->helper('pagination');
		$this->load->helper('array');
		$this->load->config('site_config'); 
		$this->load->library('session');

		if ($this->uri->rsegment(1) != "auth" && $this->uri->rsegment(1) != "forgot_password")
		{
			if (get_admin_profile('user_id') == "" OR get_admin_profile('user_id') == NULL) 
			{
				redirect('admin', 'location'); 
				exit; 
			}			
			else
			{
				$this->load->model('adm_auth/menu_model', 'menu'); 
				$this->load->model('adm_user/authentication_model', 'authentication');
				$this->my_data['menu'] = $this->menu->get_menu(); 			
				
			}
		}
		$this->load->library('form_validation'); 
		$this->form_validation->set_error_delimiters('<label class="error">', '</label>');

		if ($this->input->get_post('is_ajax', TRUE) == "")
		{
			$this->set_layouts(); 
		}		
		
	}


	function index() 
	{
	
	}
	protected function set_modules($modules) 
	{
		$this->modules = $modules; 
	}
	
	protected function set_layouts($group = 'adm.main')
    {
        $this->template->set_template($group);
		$data = array();
		$this->template->write_view('header', 'layouts/' . $this->modules . '/templates/header', $data, TRUE);
		self::set_css();
		self::set_javascript();
		$this->template->write_view('footer', 'layouts/' . $this->modules . '/templates/footer', $data, TRUE);

		if ($this->uri->rsegment(1) != "auth")
		{
			$this->create_menu();
		}
	}

    protected function make_bread()
    {
        $this->template->write('breadcrumb', $this->breadcrumb->make_bread(), true);
    }

	protected function set_css() 
	{
		$this->template->write_view('_styles', 'layouts/' . $this->modules . '/templates/css', array(), TRUE); 
	}

	protected function set_javascript() 
	{
		$this->template->write_view('_scripts', 'layouts/' . $this->modules . '/templates/javascript', array(), TRUE); 
	}

	protected function create_menu() 
	{
		$this->template->write_view('sidebar', 'layouts/' . $this->modules . '/templates/sidebar', $this->my_data); 
	}
}

