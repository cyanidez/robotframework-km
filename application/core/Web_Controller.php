<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
*	 Front Abstract Controller 
*/

require_once('Zend/Registry.php');
require_once('Zend/Locale.php');
require_once('Zend/Date.php');

class Web_Controller extends MX_Controller {

	public $category_topbar; 
	protected $modules = 'front';
	protected $seo_title = '';
	protected $seo_description = ''; 
	protected $seo_keyword = ''; 
	public $show_splashpage; 

	public $web_configuration = array();

	public function __construct() 
	{
		parent::__construct(); 

		$this->load->config('site_config');	
        $this->load->helper('string');
		$this->load->library('zend'); 
		
		$this->load->library('form_validation');
		$this->load->helper('metatag'); 
		$this->load->helper('url_manager'); 
		$this->load->helper('html'); 
		$this->load->helper('array'); 
		$this->load->helper('date');
		

		$this->load->helper('MY_profile'); 
		$this->load->helper('MY_member_profile'); 
		$breadcrumb_conf = array(
			'separetor' => '&nbsp;',
			'bread_st' => '<ul class="breadcrumbs">',
			'bread_end' => '<li></li></ul>',
			'crumb_highlight' => '<li class="active">',
			'crumb_st' => '<li>',
			'crumb_end' => '</li>'
		);

		
		
		
		$this->load->library('breadcrumb');
		#$this->breadcrumb->init($breadcrumb_conf);
		#alert($_SERVER);

		if (preg_match("/^\/en/", $this->input->server('REQUEST_URI')))
		{
			define("LANGCODE", 'en'); 
			define("LANGID" , 2); 		
		}
		else 
		{
			define("LANGCODE", 'th'); 
			define("LANGID" , 1); 		
		}
		
		// set default language for application
		$locale = new Zend_Locale(LANGCODE);
		Zend_Registry::set('Zend_Locale', $locale);

		// load zend cache for acl
		$this->zend->load('Zend/Cache');

		$this->lang->load('default', LANGCODE); 
		$this->lang->load('template', LANGCODE); 
		$this->lang->load('button', LANGCODE); 
		#$this->template->set_template('home'); 
		if ($this->input->get_post('is_ajax', TRUE) == "")
		{
			$this->set_layouts(); 
		}
		$this->breadcrumb->make(lang('หน้าแรก'), lang_url());		
	}
	
	protected function set_modules($modules = NULL) 
	{
		$this->modules = $modules; 
	}

	
	protected function set_layouts($group = 'main')
	{
		$this->template->set_template($group);
		$data = array(); 

		self::set_header();
		
		self::set_css(); 
		self::set_javascript(); 

		if (empty($this->seo_title))
		{
			self::seo_title();
		}
		else
		{
			self::seo_title($this->seo_title);
		}

		if (empty($this->seo_description))
		{
			self::seo_description();
		}
		else
		{
			self::seo_description($this->seo_description);
		}

		if (empty($this->seo_keyword))
		{
			self::seo_keyword();		
		}
		else
		{
			self::seo_keyword($this->seo_keyword);
		}
		
		self::set_footer();
		#self::set_ga();
	}

	
	protected function set_header()
	{
		$data = array();
		$this->template->write_view('header', 'layouts/' . $this->modules . '/templates/header', $data, TRUE); 
	}
	public function set_footer()
	{
		$data = array();
		$this->template->write_view('footer', 'layouts/' . $this->modules . '/templates/footer', $data, true); 	
	}

	protected function set_sidebar($parent = null, $node = null)
	{
		


		$this->template->write_view('sidebar', 'layouts/' . $this->modules . '/templates/sidebar', $data, true);
	}


	

	

	

	
	protected function set_breadcrumb()
	{
		$breadcrumb_conf = array(
			
			'bread_st' => '<ul class="breadcrumbs">',
			'bread_end' => '</ul>',
			'crumb_highlight' => '<li class="active">',
			'crumb_st' => '<li>',
			'crumb_end' => '</li>'
		);

		$this->breadcrumb->init($breadcrumb_conf);
		$this->template->write('breadcrumb', $this->breadcrumb->make_bread(), true);


	}

	public function set_meta($message = NULL, $type = 'title')
	{
		if (is_null($message) OR empty($message))
		{
			return NULL; 
		}
		switch ($type) 
		{
			case 'title' : 
				$this->seo_title = $message; 
				break; 

			case 'description' :
				$this->seo_description = $message; 
				break; 

			case 'keyword' : 
				$this->seo_keyword = $message; 
				break; 

			default :
				break; 
		}
	}

	protected function seo_title($title = NULL) 
	{
		if (is_null($title) OR empty($title))
		{
			$this->template->write('title', get_title('default'), TRUE); 
		}
		else
		{
			$this->template->write('title', $title, TRUE);
		}		
	}

	

	protected function seo_description($description = NULL) 
	{
		if (is_null($description) OR empty($description))
		{
			$this->template->write('description', get_description('default'), TRUE); 
		}
		else
		{
			$this->template->write('description', $description, TRUE);
		}
		
	}


	protected function seo_keyword($keyword = NULL) 
	{
		if (is_null($keyword) OR empty($keyword)) 
		{
			$this->template->write('keyword', get_keyword('default'), TRUE); 
		}
		else
		{
			$this->template->write('keyword', $keyword, TRUE); 
		}
	}

	protected function set_css() 
	{
		$this->template->write_view('_styles', 'layouts/' . $this->modules . '/templates/css'); 
	}

	protected function set_javascript() 
	{
		$this->template->write_view('_scripts', 'layouts/' . $this->modules . '/templates/javascript');
	}

	

	
}
