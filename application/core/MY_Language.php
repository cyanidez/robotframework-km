<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once('Zend/Translate.php');

class MY_Language extends MX_Language {

	const FILE_EXT = ".php";
	private $_adapter = 'gettext';
	
	//const FILE_EXT = ".php";
	//private $_adapter = 'array';
	
	public $is_loaded = array();

	public static $translate = null;

	private $_cache = null;

	private $_log_translate = false;

	function __construct()
	{
		log_message('debug', "Language Class Initialized");
	}

	/**
	 * Load language file
	 * To load global language file
	 * $_module = false
	 *
	 *
	 * @access	 public
	 * @param string
	 * @param string | bool
	 * @return	bool
	 */
	function load($langfile='', $_module = NULL)
	{
		$langfile = str_replace(EXT, '', str_replace('_lang.', '', $langfile)) . '_lang' . self::FILE_EXT;

		$locale = Zend_Registry::get('Zend_Locale');

		//$idiom = $locale->getLanguage();
		if (get_cookie('language'))
		{
			$idiom = get_cookie('language');
		}
		else
		{
			$idiom = 'en';
		}

		if ($_module === false)
		{
			$path = false;
			$_langfile = $langfile;
		}
		else
		{
			$_module || $_module = CI::$APP->router->fetch_module();
			list ($path, $_langfile) = Modules::find($langfile, $_module, 'language/', $idiom);
		}

		if (in_array($path . $_langfile, $this->is_loaded, true))
		{
			return;
		}

		if ($path === false)
		{
			$path_language = APPPATH.'language/'.$idiom.'/'.$_langfile;
		}
		else
		{
			$path_language = $path . $_langfile;
		}
		if (file_exists($path_language))
		{
			if (isset(self::$translate) && self::$translate instanceof Zend_Translate)
			{
				self::$translate->addTranslation($path_language, $idiom);
			}
			else
			{
				#if (DEVELOPMENTMODE == 0)
				#{
					$CI =& get_instance();
					$CI->zend->load('Zend/Cache');

					$cacheFrontends = array(
						'lifetime' =>  3600,
						'automatic_serialization' => true,
						'automatic_cleaning_factor' => 0
					);
					$cacheBackends = array(
						'cache_dir' => config_item('cache_dir') . '/translate',
						'file_name_prefix' => 'lang',
						'hashed_directory_umask' => '0777',
						'cache_file_umask' => '644',
						'hashed_directory_level' => '0',
						'server' => config_item('cache_server'),
						'compression' => true
					);
					$this->_cache = Zend_Cache::factory('Core', config_item('cache_method'), $cacheFrontends, $cacheBackends);
					Zend_Translate::setCache($this->_cache);
				#}

				//self::$translate = new Zend_Translate('gettext', $path_language, $idiom);
				self::$translate = new Zend_Translate($this->_adapter, $path_language, $idiom);
			}
		}
		else
		{
			log_message('debug', 'Unable to load the requested language file: language/'.$langfile);
		}

		$this->is_loaded[] = $path . $_langfile;

		log_message('debug', 'Language file loaded: language/'.$idiom.'/'.$langfile);
		return TRUE;
	}

	function line($line="", $params = NULL)
	{
		echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
		$line = ($line == '') ? FALSE : self::$translate->_($line);
		
		if ( ! empty($params)) 
		{
			if (is_array($params) && count($params) > 0) 
			{
				foreach ($params as $key => $value) 
				{
					$line = str_replace("{".$key."}", $value, $line); 
				}
			}
			else 
			{
				$line = str_replace("{0}", $params, $line); 
			}
		}
		else 
		{
			if (preg_match('/{(.*)}/', $line)) 
			{
				$line = preg_replace('/{(.*)}/', '', $line); 
			}
		}

		return $line;
	}

	function purge_cache()
	{
		if (Zend_Translate::hasCache($this->_cache))
		{
			Zend_Translate::clearCache($this->_cache);
		}
		return true;
	}

}
