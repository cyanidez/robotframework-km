<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once('Zend/Db.php');

require_once('Zend/Db/Expr.php');

class MY_Model extends CI_Model {

	public $debug = false;

	public $parent_name = null;

	public $tbname = null;

	public static $dbProfile = null;

	public static $conn = null;

	private $_hasLoadConstructor = false;

	private $_connected = array();

	public $row_per_page; 
	public $page; 

	protected $last_insert_id = ''; 


	public function __construct()
	{
		/*parent::_assign_libraries( (method_exists($this, '__get') OR method_exists($this, '__set')) ? FALSE : TRUE );
		$this->parent_name = strtolower(get_class($this));
		log_message('debug', "Model Class Initialized");*/
		parent::__construct();
	}

	public function set_row_per_page($row_per_page) 
	{
		$this->row_per_page  = $row_per_page; 
	}

	public function set_page($start) 
	{
		$thits->page = $start; 
	}


	public function connect($profile)
	{
		if (!isset($this->_connected[$profile]))
		{
			//echo "<div style='color:red;'>Connected to " . $profile . "</div>";
			$this->setDbProfile($profile);
		}
		self::$conn =& self::$dbProfile[$profile];

		if ($this->debug === TRUE)
		{
			self::$conn->getProfiler()->setEnabled(TRUE);
		}
	}

	public function setDbProfile($profile)
	{
		require(APPPATH.'config/database.php');

		if (is_null($profile))
		{
			$profile = $active_group;
		}

		if (!isset(self::$dbProfile[$profile]))
		{
			$dbConf = $db[$profile];

			$params = array(
				'host' => $dbConf['hostname'],
				'username' => $dbConf['username'],
				'password' => $dbConf['password'],
				'dbname' => $dbConf['database'],
				'charset' => $dbConf['char_set'],
				'collation' => $dbConf['dbcollat']
			);

			// Zend DB Driver name format
			$driver = $dbConf['dbdriver'];

			self::$dbProfile[$profile] = Zend_Db::factory($driver, $params);

			/*if ($dbConf['char_set'] && $dbConf['dbcollat'])
			{
				self::$dbProfile[$profile]->Query('SET character_set_results='.$dbConf['char_set']);
				self::$dbProfile[$profile]->Query('SET collation_connection='.$dbConf['dbcollat']);
				self::$dbProfile[$profile]->Query('SET NAMES '.$dbConf['char_set']);
			}*/

			$this->_connected[$profile] = TRUE;
		}
	}

	public function expr($string)
	{
		return new Zend_Db_Expr($string);
	}

	public function query($sql, $attrs=array())
	{
		$this->connect('master');
		return self::$conn->query($sql, $attrs);
	}

	public function insert($table, $data)
	{
		$this->connect('master');

		$resp = self::$conn->insert($table, $data); 

		$this->last_insert_id = self::$conn->lastInsertId(); 

		return $resp; 

		/*
		if (self::$conn->insert($table, $data))
		{
			return self::$conn->lastInsertId();
		}
		return false;
		*/
	}
	public function get_last_insert_id() 
	{
		return $this->last_insert_id; 
	}

	public function update($table, $data, $where)
	{
		$this->connect('master');
		return self::$conn->update($table, $data, $where);
	}

	public function delete($table, $where = NULL)
	{
		$this->connect('master');
		return self::$conn->delete($table, $where);
	}

	public function select()
	{
		$this->connect('slave');
		#$this->connect('slave');
		return self::$conn->select();
	}

	public function quote($value, $type = NULL)
	{
		$this->connect('slave');
		return self::$conn->quote($value, $type);
	}


	public function quoteInto($text, $value, $type = NULL, $count = NULL)
	{
		$this->connect('slave');
		return self::$conn->quoteInto($text, $value, $type, $count);
	}

	/*
	*	Alias of quoteInfo method 
	*/
	public function quote_info($text, $value, $type = NULL, $count = NULL) 
	{
		return $this->quoteInfo($text, $value, $type, $count); 
	}


	public function fetchObject($sql, $attrs = array())
	{
		return self::$conn->fetchObject($sql, $attrs);
	}
	public function fetch_object($sql, $attrs = array()) 
	{
		return fetchObject($sql, $attrs); 
	}

	public function fetchAll($sql, $attrs = array())
	{
		return self::$conn->fetchAll($sql, $attrs);
	}
	/**
	*	Alias of fetchAll method 
	*/
	public function fetch_all($sql, $attrs = array())
	{
		return $this->fetchAll($sql, $attrs); 
	}

	public function fetchRow($sql, $attrs = array())
	{
		return self::$conn->fetchRow($sql, $attrs);
	}
	public function fetch_row($sql, $attrs = array()) 
	{
		return $this->fetchRow($sql, $attrs); 
	}

	public function fetchOne($sql, $attrs = array())
	{
		return self::$conn->fetchOne($sql, $attrs);
	}
	public function fetch_one($sql, $attrs = array()) 
	{
		return $this->fetchOne($sql, $attrs); 
	}

	public function fetchCol($sql, $attrs = array())
	{
		return self::$conn->fetchCol($sql, $attrs);
	}
	public function fetch_col($sql, $attrs = array()) 
	{
		return $this->fetchCol($sql, $attrs); 
	}

	public function fetchPairs($sql, $attrs = array())
	{
		return self::$conn->fetchPairs($sql, $attrs);
	}
	public function fetch_pairs($sql, $attrs = array()) 
	{
		return $this->fetchPairs($sql, $attrs); 
	}

	public function begin_transaction() 
	{
		return self::$conn->beginTransaction(); 
	}
	public function commit() 
	{
		return self::$conn->commit(); 
	}

	public function rollback() 
	{
		return self::$conn->rollBack(); 
	}

	/**
	 * fetchIndex
	 * This function only work with Zend_db_Select statement
	 *
	 *
	 * @access	 public
	 * @param object (Zend_Db_Select)
	 * @param array
	 * @param string
	 * @return	array
	 */
	public function fetchIndex($sql, $attrs=array(), $index='id', $multi = FALSE)
	{
		$rows = self::$conn->fetchAll($sql, $attrs);

		$data = array();
		foreach ($rows as $val)
		{
			$id = $val[$index];
			//unset($val[$index]);
			if ($multi === TRUE)
				$data[$id][] = $val;
			else
				$data[$id] = $val;
		}
		return $data;
	}
	public function fetch_index($sql, $attrs = array(), $index = 'id', $multi = FALSE) 
	{
		$this->fetchIndex($sql, $attrs, $index, $multi); 
	}

	/**
	 * fetchGroup
	 * This function only work with Zend_db_Select statement
	 * The result are the same as fetchIndex, but return in multi array
	 *
	 *
	 * @access	 public
	 * @param object (Zend_Db_Select)
	 * @param array
	 * @param string
	 * @return	array
	 */
	public function fetchGroup($sql, $attrs=array(), $index='id')
	{
		return $this->fetchIndex($sql, $attrs, $index, true);
	}
	public function fetch_group($sql, $attrs = array(), $index = 'id') 
	{
		$this->fetchGroup($sql, $attrs, $index); 
	}

	/**
	 * fetchPage
	 * This function only work with Zend_db_Select statement
	 *
	 *
	 * @access	 public
	 * @param object (Zend_Db_Select)
	 * @param array
	 * @param integer
	 * @param integer
	 * @return	array
	 */
	public function fetch_page($sql, $attrs = array(), $page=1, $limit_per_page=20, $range=5)
	{
		$page = ((int) $page < 1) ? 1 : $page;

		$sql->limitPage($page, $limit_per_page);
		$rows = self::$conn->fetchAll($sql, $attrs);

		$sql->reset(Zend_Db_Select::COLUMNS);
		$sql->reset(Zend_Db_Select::ORDER);
		$sql->reset(Zend_Db_Select::LIMIT_COUNT);
		$sql->reset(Zend_Db_Select::LIMIT_OFFSET);

		$sql->columns('COUNT(*)');

		if ($sql->getPart(Zend_Db_Select::GROUP))
		{
			$records = self::$conn->fetchAll($sql, $attrs);
			$row_count = count($records);
		}
		else
		{
			$row_count = self::$conn->fetchOne($sql, $attrs);
		}
		
		//calculate links
		$total_page = ceil($row_count / $limit_per_page);
		$start = $page - $range;
		$end = $page + $range;
		if ($start <= 1) $start = 1;
		if ($end >= $total_page) $end = $total_page;
		
		$data = array(
			'rows' => $rows,
			'row_count' => $row_count,
			'limit_per_page' => $limit_per_page,
			'current_page' => $page,
			'start' => $start,
			'end' => $end,
			'total_page' => $total_page
		);
		return $data;
	}

	public function __destruct()
	{
		if ($this->debug === true)
		{
			$profiler = self::$conn->getProfiler();
			$queries = $profiler->getQueryProfiles();
			echo "<pre style='background-color:#ffffff'>" . print_r($queries, true) . "</pre>";
		}
	}

}
?>