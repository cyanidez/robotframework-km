$(function(){
	$('#checkall').click(function(){
		if ($(this).is(':checked') == true)
		{
			$('table > tbody > tr > td').find('input[type=checkbox]').prop('checked', 'checked');
		}
		else 
		{
			$('table > tbody > tr > td').find('input[type=checkbox]').prop('checked', false);
		}
	});
	$('.select-product').click(function(){
		$total_checkbox = $('.select-product').length;
		$total_checkbox_is_checked = $('.select-product:checked').length; 
		

		if ($(this).is(':checked'))
		{
			if ($total_checkbox == $total_checkbox_is_checked)
			{
				$('#checkall').prop('checked', 'checked');
			}
		}
		else 
		{
			if ($total_checkbox > $total_checkbox_is_checked)
			{
				$('#checkall').prop('checked', false);
			}
		}

		if ($total_checkbox_is_checked == 0)
		{
			$('#btn-generate').addClass('disabled');
		}
		else
		{
			$('#btn-generate').removeClass('disabled');
		}
	});

	$('#btn-save-detail').click(function(event){
		event.preventDefault();
		actionForm = $('#edit-product-detail-form').attr('action');
		
		$.post(
			actionForm, 
			{
				name: $('#name').val(),
				price: $('#price').val(),
				margin_price: $('#margin_price').val(),
				btn_save_detail: 1
			}, 
			function (data){
				if (data != undefined && data != NaN && data != null)
				{
					var jsonDecode = $.parseJSON(data);
					if (jsonDecode.code == 200)
					{
						alert(jsonDecode.message);
					}
					else
					{
						alert(jsonDecode.message);
					}
				}
			}
		);

	});

	$(document).on('click', '.btn-remove', function(event){
		event.preventDefault();
		$(this).parent().parent().remove();
		totalRows = $('.row-keyword').length;

        $('.row-keyword').each(function(index, value){
        	console.log('index = ' + index);
        	console.log($(this).children('label').html());
            $(this).children('label').html('Keyword # ' + (index + 1));
            $(this).children('div:eq(1)').children('input').attr('id', 'keyword-' + index);
        });
	});

	$('#btn-add-more-keyword').click(function(event){
		event.preventDefault();

		totalRows = $('.row-keyword').length;
		var newRow = '<div class="form-group row-keyword">'
                    	+ '<label class="col-lg-4 control-label">Keyword # '+(totalRows + 1)+' </label>'
                        + '<div class="col-lg-6">'
                        	+ '<input type="text" class="form-control keyword" name="keyword[]" id="keyword-0">'
	                    + '</div>'
                        + '<div class="col-lg-2">'
                        	+ '<button class="btn-remove btn btn-danger">'
                            	+ '<i class="glyphicon glyphicon-remove" style=""></i>'
                           	+ '</button>'
                        + '</div>'
                    + '</div>'; 
        $('.row-keyword:last').after(newRow);


	});

	$('#btn_save_keyword').click(function(event){
		event.preventDefault();
		actionForm = $('#edit-product-detail-form').attr('action');

		
		
		$.post(
			actionForm,
			{
				specific_uses_keywords: $('#specific_uses_keywords').val(),
				thesaurus_subject_keywords: $('#thesaurus_subject_keywords').val(),
				generic_keyword: $('#generic_keyword').val(),
				btn_save_keyword: 1
			}, 
			function (data){
				if (data != undefined && data != NaN && data != null)
				{
					
					var jsonDecode = $.parseJSON(data);
					var table = '<table>';
					var tr = ''; 
					
					$.each(jsonDecode.data, function(index, v){
						if (v.code == 200)
						{
							tr += '<div class="alert alert-success">' + v.message + '</div>';
							myLi = '<li class="col-sm-3" data-keyword-id="'+v.id+'">'
									+ '<div class="checkbox">'
										+ '<label>'
											+ '<input type="checkbox" name="chk_product_keyword[]" class="inverted" value="'+v.id+'">'
											+ '<span class="text">'+v.name+'</span>'
										+ '</label>'
									+ '</div>'
								+ '</li>';
							$('ul.list-unstyled li:last-child').after(myLi);
						}
						else 
						{
							tr += '<div class="alert alert-danger">' + v.message + '</div>';
						}

					});

					$('#specific_uses_keywords').parent().parent().before(tr);
				
				}
			}
		);

	});

	$('#btn-delete-keyword').click(function(event){
		event.preventDefault();

		actionForm = $(this).attr('data-action-delete');
		
        myCheckboxes = [];
		$('input[name^=chk_product_keyword]').each(function(){
			fields = {}
			if ($(this).is(":checked"))
			{
				fields["checked"] = true; 
				fields["id"] = $(this).val();
			}
			else 
			{
				fields["checked"] = false; 
				fields["id"] = $(this).val();
			}
			console.log(fields);
			//console.log($(this).val());
			myCheckboxes.push(fields);
		});


		console.log(myCheckboxes);

		$.post(
			actionForm, 
		 	{
		 		checkbox_delete: myCheckboxes
		 	},
		 	function(data){
		 		//console.log(data);
		 		if (data != undefined && data != NaN && data != null)
		 		{
		 							
					var jsonDecode = $.parseJSON(data);
					var table = '<table>';
					var tr = ''; 
		 							
		 			$.each(jsonDecode.data, function(index, v){
		 				if (v.code == 200)
		 				{
		 					$('li[data-keyword-id="'+v.id+'"]').remove();
							tr += '<div class="alert alert-success">' + v.message + '</div>';
		 									
		 				}
		 				else 
		 				{
		 					tr += '<div class="alert alert-danger">' + v.message + '</div>';
		 				}

		 			});

		 			$('#product_keyword').parent().parent().before(tr);

		 			setTimeout("$('div.alert').hide();", 2000);


		 							
		 		}
		 	}
		);
	});

	
});

